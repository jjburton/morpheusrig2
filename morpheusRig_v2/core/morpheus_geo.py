"""
morpheus_geo
Josh Burton 
www.cgmonks.com

geo query and other functions for Morpheus 2.0

"""

import logging
logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

# From Maya =============================================================
import maya.cmds as mc
import maya.mel as mel

# From Red9 =============================================================
from Red9.core import Red9_Meta as r9Meta
from Red9.core import Red9_General as r9General

# From Zoo ==============================================================
from cgm.lib.zoo.zooPyMaya import skinWeights

# From cgm ==============================================================
from cgm.core import cgm_General as cgmGeneral
from cgm.core import cgm_Meta as cgmMeta
import morpheus_meta as MorphMeta
import morpheus_sharedData as MORPHYDATA
reload(MORPHYDATA)
from cgm.core.cgmPy import validateArgs as cgmValid

from cgm.lib import (curves,
                     deformers,
                     skinning,
                     names,
                     attributes,
                     #distance,
                     search,
                     lists,
                     geo,
                     #modules,
                     #constraints,
                     #rigging,
                     #attributes,
                     #joints,
                     guiFactory)


reload(deformers)
def flag_asset():pass

_d_assetGeoGroupsToCheck = {'tongue':'tongueGeoGroup',
                            'uprTeeth':'uprTeethGeoGroup',
                            'lwrTeeth':'lwrTeethGeoGroup',
                            'eyebrow':'eyebrowGeoGroup',
                            'hair':'hairGeoGroup',
                            'facialHair':'facialHairGeoGroup',
                            'clothes':'clothesGeoGroup',
                            'earLeft':'left_earGeoGroup',
                            'earRight':'right_earGeoGroup',
                            'eyeLeft':'left_eyeGeoGroup',
                            'eyeRight':'right_eyeGeoGroup'}

_d_asset_main_geoInfo = {'unified':{'msg':'geo_unified',
                              'msg_reset':'geo_resetUnified',},
                   #'msg_bridge':'geo_bridgeUnified',
                   #'msg_bsBridge':'bsNode_bridgeMain'},
                   'body':{'msg':'geo_baseBody',
                           'msg_reset':'geo_resetBody',
                           'msg_bsBridge':'bsNode_bodyShapers'},
                   #'msg_bridge':'geo_bridgeBody',
                   #'msg_bsBridge':'bsNode_bridgeBody'},
                   'head':{'msg':'geo_baseHead',
                           'msg_reset':'geo_resetHead',
                           'msg_bridge':'geo_bridgeFaceShapers',
                           'msg_bsBridge':'bsNode_faceShapers'}}
                #'msg_bridge':'geo_bridgeHead',
                #'msg_bsBridge':'bsNode_bridgeFace'},}
                
d_asset_bodyBsGeoGroupsToCheck = {'body':'bodyShapersGroup'}
d_asset_faceBsGeoGroupsToCheck = {'mouth':'mouthShapersGroup',
                                  'nose':'noseShapersGroup',
                                  'brow':'browShapersGroup',
                                  'jaw':'jawShapersGroup',
                                  'cheek':'cheekShapersGroup',
                                  'bodyMatch':'bodyMatchShapersGroup'}
                
def asset_get_dict(*args, **kws):
    """
    Returns a dict of geo inormation for Morpheus asset
    
    :param asset: asset instance
    :param influenceMode: string arg of which mode to use for the influence data
       :core: Get core geo dict
       :customGeo: Check the watch folders for customization geo for the tags in the dict
       :blendshapeTargets: Check the blendshape watch folders for blendshape targets
       :all: get all data sets
    :param onlyKeys
    :param ignoreKeys
    :param catchInstance: object instance to catch queried data on expected attributes
    """       
    _modes_ = ['core','customGeo','customBlendshapeTargets','all']
    class fncWrap(cgmGeneral.cgmFuncCls):		
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'asset_get_dict'	
            self._b_reportTimes = 1 #..we always want this on so we're gonna set it on
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'asset',"default":'M1_customizationNetwork',"argType":'morpheusBipedCustomizationAsset','help':"This should be a customization asset"},
                                         {'kw':'mode',"default":'all',},
                                         {'kw':'ignoreKeys',"default":None},
                                         {'kw':'onlyKeys',"default":None},                                         
                                         {'kw':'catchInstance',"default":None}]	    
            self.__dataBind__(*args, **kws)

            self.l_funcSteps = [{'step':'Validate args and kws','call':self._validate_},
                                {'step':'Gather geo info','call':self._gather_}
                                ]

        def _validate_(self):            
            try:
                self.mi_network = cgmMeta.validateObjArg(self.d_kws['asset'],mType = MorphMeta.cgmMorpheusMakerNetwork,noneValid = False)
            except Exception,error:
                raise Exception,"asset is invalid | {0}".format(error)
            
            try:#...Mode check
                _mode = self.d_kws['mode'] or None
                if cgmValid.isListArg(_mode):#...if it's a list, validate each entry
                    _l_modes = []
                    for m in _mode:
                        if m not in _modes:
                            self.log_error("'{0}' mode not found in valid modes: [{1}]".format(m,",".join(["{0}".format(o) for o in __modes__])))
                        else:
                            _l_modes.append(m)
                    self._l_modeCalls = _l_modes
                elif _mode not in _modes_:#if it's not a list, check it, then push it
                    raise ValueError,"'{0}' mode not found in valid modes: [{1}]".format(_mode,",".join(["{0}".format(o) for o in __modes__]))
                else:
                    self._l_modeCalls = [_mode]
            except Exception,err:
                raise Exception,"Mode check fail | error: {0}".format(err)
            
            try:
                self._l_ignoreKeys = []                
                _keys = self.d_kws['ignoreKeys'] or []
                if cgmValid.isListArg(_keys):#...if it's a list
                    self._l_ignoreKeys = _keys
                elif cgmValid.stringArg(_keys):
                    self._l_ignoreKeys = [_keys]  
            except Exception,err:
                raise Exception,"ignoreKeys check fail | error: {0}".format(err)
            
            try:
                self._l_onlyKeys = []                
                _keys = self.d_kws['onlyKeys'] or []
                if cgmValid.isListArg(_keys):#...if it's a list
                    self._l_onlyKeys = _keys
                elif cgmValid.stringArg(_keys):
                    self._l_onlyKeys = [_keys]  
            except Exception,err:
                raise Exception,"ignoreKeys check fail | error: {0}".format(err)
            
            _catchInstance = self.d_kws.get('catchInstance') or None
            self._catchInstance = False            
            if _catchInstance is not None:
                try:
                    _catchInstance.__dict__
                    self._catchInstance = _catchInstance
                except:
                    self.log_error("Bad _catchInstance: {0}".format(_catchInstance))
                    self._catchInstance = False
            
        def gather_core(self):
            self.md_coreGeo = {}
            
            if self._catchInstance:#...if we have one, push our processing data to it
                self._catchInstance.md_coreGeo = self.md_coreGeo            
            
            #This is for customization asset only
            for k in _d_asset_main_geoInfo.keys():
                try:
                    if k in self._l_ignoreKeys:
                        continue
                    if self._l_onlyKeys and k not in self._l_onlyKeys:
                        continue
                    _bfr = self.mi_network.getMessage(_d_asset_main_geoInfo[k]['msg'],False)
                    if not _bfr:
                        raise ValueError,"Repair {0} connection | {1}".format(k,_d_asset_main_geoInfo[k]['msg'])
                    else:self.log_debug("{0} found".format(k))   
                    self.md_coreGeo[k] = {'mi_base':cgmMeta.validateObjArg(_bfr,mayaType = ['mesh'])}
                except Exception,error:
                    raise Exception,"Core geo check failed on {0} | {1}".format(k,error)      
                
            return self.md_coreGeo
        
        def gather_customGeo(self):
            self.md_geoGroups = {}
            self.d_skinTargets = {}
            
            if self._catchInstance:#...if we have one, push our processing data to it
                self._catchInstance.md_geoGroups = self.md_geoGroups
                self._catchInstance.d_skinTargets = self.d_skinTargets
                
            for key in _d_assetGeoGroupsToCheck.keys():
                try:
                    self.log_info("Checking : '{0}' | msgAttr: '{1}'".format(key,_d_assetGeoGroupsToCheck[key]))
                    buffer = self.mi_network.masterNull.getMessage(_d_assetGeoGroupsToCheck[key])
                    if not buffer:raise RuntimeError,"Group not found"			    
                    mi_group = cgmMeta.validateObjArg(buffer,mayaType = ['group','transform'])
                    l_geoGroupObjs = mi_group.getAllChildren()
                    if not l_geoGroupObjs:
                        if key == 'body':
                            raise ValueError,"No body geo in base_geo_grp!"                               
                        self.log_warning("Empty group: '{0}'".format(mi_group.p_nameShort))
                    else:
                        l_toSkin = []
                        for o in l_geoGroupObjs:
                            if search.returnObjectType(o) in ['mesh','nurbsSurface']:
                                l_toSkin.append(o) 
                            else:
                                self.log_info("Not skinnable: '{0}'".format(o))				    
                        if not l_toSkin:
                            self.log_warning("No skinnable objects found")
                        else:
                            self.d_skinTargets[key] = l_toSkin
                            self.log_info("---To skin or attach:")			    
                            for o in l_toSkin:
                                self.log_info("     '{0}' -- {1}".format(names.getBaseName(o), o))				
                    self.md_geoGroups[key] = mi_group
                except Exception,error:raise Exception,"{0} | {1}".format(key,error)			
            return self.d_skinTargets
        
        def gather_customBlendshapeTargets(self):
            self.md_bsGeoGroups = {}
            self.d_bsGeoTargets = {}
            
            if self._catchInstance:#...if we have one, push our processing data to it
                self._catchInstance.md_bsGeoGroups = self.md_bsGeoGroups
                self._catchInstance.d_bsGeoTargets = self.d_bsGeoTargets        
            d_sectionKeys = {0:'body',1:'face'}
        
            for i,d in enumerate([d_asset_bodyBsGeoGroupsToCheck,d_asset_faceBsGeoGroupsToCheck]):
                str_sectionKey = d_sectionKeys[i]
                self.d_bsGeoTargets[str_sectionKey] = []
                try:
                    for key in d.keys():
                        try:
                            self.log_info("Checking : '{0}' | '{1}' | msgAttr: '{2}'".format(str_sectionKey,key,d[key]))
                            buffer = self.mi_network.masterNull.getMessage(d[key])
                            if not buffer:raise RuntimeError,"Group not found"			    
                            mi_group = cgmMeta.validateObjArg(buffer,mayaType = ['group','transform'])
                            l_geoGroupObjs = mi_group.getAllChildren()
                            if not l_geoGroupObjs:
                                self.log_warning("Empty group: '{0}'".format(mi_group.p_nameShort))
                            else:
                                l_toConnect = []
                                for o in l_geoGroupObjs:
                                    if search.returnObjectType(o) in ['mesh']:
                                        l_toConnect.append(o) 
                                    else:
                                        self.log_info("Not connectable: '{0}'".format(o))				    
                                if not l_toConnect:
                                    self.log_warning("No connectable objects found")
                                else:
                                    self.d_bsGeoTargets[str_sectionKey].extend(l_toConnect)
                                    self.log_info("---To set as target:")			    
                                    for o in l_toConnect:
                                        self.log_info("     '{0}' -- {1}".format(names.getBaseName(o), o))				
                        except Exception,error:raise Exception,"{0} | {1}".format(key,error)			
        
                    self.md_bsGeoGroups[key] = mi_group
                except Exception,error:raise Exception,"{0} | {1}".format(str_sectionKey,error)
            return self.d_bsGeoTargets

        def _gather_(self):
            _calls = self._l_modeCalls
            _result = False
            for _c in _calls:
                if _c is 'core':
                    _result = self.gather_core()
                elif _c is 'customGeo':
                    _result = self.gather_customGeo()
                elif _c is 'customBlendshapeTargets':
                    _result = self.gather_customBlendshapeTargets()
                elif _c is 'all':
                    _result = {'core':self.gather_core(),
                               'customGeo':self.gather_customGeo(),
                               'customBlendshapeTargets':self.gather_customBlendshapeTargets()}
                
            cgmGeneral.log_info_dict(_result,_calls)
            return _result
    return fncWrap(*args,**kws).go()

def get_core(network = 'M1_customizationNetwork', onlyKeys = [], ignoreKeys = [],catchInstance = None):
    """
    Returns a dict of core geo info for a morpheus customization asset

    :param asset: asset instance
    :param onlyKeys
    :param ignoreKeys
    :param catchInstance: object instance to catch queried data on expected attributes
    """  
    _md_coreGeo = {}
    
    mi_network = cgmMeta.validateObjArg(network,mType = MorphMeta.cgmMorpheusMakerNetwork,noneValid = False)
    onlyKeys = cgmValid.listArg(onlyKeys)
    ignoreKeys = cgmValid.listArg(ignoreKeys)
    if catchInstance:#...if we have one, push our processing data to it
        catchInstance.md_coreGeo = _md_coreGeo            

    #This is for customization asset only
    for k in _d_asset_main_geoInfo.keys():
        try:
            if onlyKeys and k not in onlyKeys:
                continue
            if ignoreKeys and k in ignoreKeys:
                continue
            _bfr = mi_network.getMessage(_d_asset_main_geoInfo[k]['msg'],False)
            if not _bfr:
                raise ValueError,"Repair {0} connection | {1}".format(k,_d_asset_main_geoInfo[k]['msg'])
            else:log.debug("{0} found".format(k))   
            _md_coreGeo[k] = {'mi_base':cgmMeta.validateObjArg(_bfr,mayaType = ['mesh'])}
        except Exception,error:
            raise Exception,"Core geo check failed on {0} | {1}".format(k,error)      
    return _md_coreGeo

def update_skinWeights(targets = None, useUnifiedAsBackup = True):
    """
    Update the skin weights on an object if it has the skinMaster message plug
    
    :parameters:
        targets(list)
        
    """  
    _str_funcName = 'update_skinWeights: '
    
    ml_targets = cgmMeta.validateObjListArg(targets,cgmMeta.cgmObject)
    for mObj in ml_targets:
        _str_obj = mObj.p_nameShort
        if not deformers.isSkinned(mObj.mNode):
            raise ValueError, _str_funcName + "{0} not skinned. Can't update.".format(_str_obj)
        
        _skin = skinning.querySkinCluster(mObj.mNode)
        _skinMasterGeo = mObj.getMessage('skinMaster')
        if _skinMasterGeo:#...if we have a skinMaster tagged to our object, update off that
            log.info(_str_funcName + "skinMaster found...updating weights")
            _skinMaster = skinning.querySkinCluster(_skinMasterGeo[0])
            mc.copySkinWeights(ss = _skinMaster, ds = _skin, noMirror = True,
                               surfaceAssociation = 'closestPoint', influenceAssociation = 'name')   
        
    
    #else:#...otherwise, use the the unified skinCluster
        #self.log_info("No sknMaster...updating from unified")
        #mc.copySkinWeights(ss = self._mi_sourceSkinCluster.mNode, ds = _skin, noMirror = True,
                           #surfaceAssociation = 'closestPoint', influenceAssociation = 'name')
 

def verify_attachment(*args, **kws):
    """
    Attach and/or verify attachment of a geo item to morpheus asset or rig. Ensures that it is skinned and blendshaped to match blendshapes.
    
    1)
    2)
    3)
    
    :param object: object to attach
    :param network: network instance
    :param bsMode: string arg of which blendshape mode to use attach process
       :all: bake all
    :param ignoreKeys
    :param catchInstance: object instance to catch queried data on expected attributes
    """       
    #TO DO: add more options brow/mouth, etc...
    _bsModes_ = ['all','face','body']
    _d_bsMode_lists = {'face':MORPHYDATA._l_bsNodes_asset_head + MORPHYDATA._l_facialRigBSNodes,
                       'body':MORPHYDATA._l_bsNodes_asset_body}


    class fncWrap(cgmGeneral.cgmFuncCls):		
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'MorphyGEO.verify_attachment'	
            self._b_autoProgressBar = True
            self._b_reportTimes = 1 #..we always want this on so we're gonna set it on
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'target',"default":None,"argType":'Skinnable geo type'},
                                         {'kw':'network',"default":'M1_customizationNetwork',"argType":'morpheusBipedCustomizationAsset','help':"This should be a customization asset"},
                                         {'kw':'bsMode',"default":'face',},
                                         {'kw':'rebuild',"default":False}
                                         ]	    
            self.__dataBind__(*args, **kws)

            self.l_funcSteps = [{'step':'Validate args and kws','call':self._fnc_validate_},
                                {'step':'Get source info','call':self._fnc_getSourceInfo_},                                
                                {'step':'Blendshape','call':self._fnc_blendshapes_},
                                {'step':'Skincluster','call':self._fnc_skinCluster_},                                
                                ]

        def _fnc_validate_(self):
            _objs= cgmValid.objStringList(self.d_kws['target'], mayaType=['mesh','nurbsSurface'], isTransform=True, 
                                          noneValid=True, 
                                          calledFrom=self._str_funcName)
            if not _objs:
                raise ValueError,"Not a valid target: {0}".format(self.d_kws['target'])
            
            self._targets = _objs
            self.ml_targets = cgmMeta.validateObjListArg(_objs,mType = cgmMeta.cgmObject)

            try:
                self.mi_network = cgmMeta.validateObjArg(self.d_kws['network'],mType = MorphMeta.cgmMorpheusMakerNetwork,noneValid = False)
            except Exception,error:
                raise Exception,"asset is invalid | {0}".format(error)
            
            try:#...Mode check
                _bsMode = self.d_kws['bsMode'] or None
                if cgmValid.isListArg(_bsMode):#...if it's a list, validate each entry
                    _l_modes = []
                    for m in _bsMode:
                        if m not in _bsModes_:
                            self.log_error("'{0}' mode not found in valid modes: [{1}]".format(m,",".join(["{0}".format(o) for o in _bsModes_])))
                        else:
                            _l_modes.append(m)
                    self._l_bsMode = _l_modes
                elif _bsMode not in _bsModes_:#if it's not a list, check it, then push it
                    raise ValueError,"'{0}' mode not found in valid modes: [{1}]".format(_bsMode,",".join(["{0}".format(o) for o in _bsModes_]))
                else:
                    self._l_bsMode = [_bsMode]
            except Exception,err:
                raise Exception,"Mode check fail | error: {0}".format(err) 
            
            self._b_rebuild = cgmValid.boolArg(self.d_kws['rebuild'],self._str_funcName)
            
        def _fnc_getSourceInfo_(self):
            if self.mi_network.mClass == 'cgmMorpheusMakerNetwork':
                try:#...get our core geo
                    asset_get_dict(self.mi_network, 'core', catchInstance = self)
                    self._mi_sourceSkinMesh = self.md_coreGeo['unified']['mi_base']
                    
                    _skin = skinning.querySkinCluster(self._mi_sourceSkinMesh.mNode) or False
                    if not _skin:
                        raise ValueError,"Unified geo is not skinned. Use a suitable network." 
                    self._mi_sourceSkinCluster = cgmMeta.cgmNode(_skin)
                    
                except Exception,error:raise Exception,"{0} | {1}".format('Core geo',error)
                
                try:#...get our blendshapes
                    self._l_bsNodesToBake = []
                    _l_bsNodesToCheck = []
                    self._d_bsNodesBakeData = {}
                    
                    if 'all' in self._l_bsMode:
                        for l in _d_bsMode_lists.values():
                            _l_bsNodesToCheck.extend(l)
                    else:
                        for m in self._l_bsMode:
                            _l_bsNodesToCheck.extend(_d_bsMode_lists.get(m))
                        
                    _l_bsNodesToCheck = lists.returnListNoDuplicates(_l_bsNodesToCheck)
                    
                    for bsn in _l_bsNodesToCheck:
                        _d_bsn = self._d_bsNodesBakeData[bsn] = {}
                        if mc.objExists(bsn) and search.returnObjectType(bsn) == 'blendShape':
                            self._l_bsNodesToBake.append(bsn)
                            
                            log.debug("Good blendshapeNode: '{0}'".format(bsn))
                            #Get our channels
                            _d_bsn['attrs'] = [str(a) for a in search.returnBlendShapeAttributes(bsn)]
                            #Get any drivers
                            #_d_bsn['drivers'] = {}#...make a dict
                            _d_bsn['baked'] = {}#...make a dict
                            _d_bsn['plug'] = {}
                            for a in _d_bsn['attrs']:
                                _buffer = ("{0}.{1}".format(bsn,a))
                                #_d_bsn['drivers'][a] = attributes.returnDriverAttribute(_buffer)#...get the driver
                                _d_bsn['plug'][a] = _buffer
                                _d_bsn['baked'][a] = False
                            if bsn in _d_bsMode_lists['face']:
                                _d_bsn['mi_defMesh'] =  self.md_coreGeo['head']['mi_base']
                            else:
                                _d_bsn['mi_defMesh'] =  self.md_coreGeo['body']['mi_base']
                        else:
                            log.info("Bad blendshapeNode: '{0}'".format(bsn))   
                        cgmGeneral.log_info_dict(_d_bsn,bsn)                            
                except Exception,error:raise Exception,"{0} | {1}".format('Blendshapes',error)               
            else:
                raise ValueError,"{0} not supported".format(self.mi_network.mClass)
    
        def _fnc_blendshapes_(self):
            self._l_noChangeBakes = []
            for mObj in self.ml_targets:
                _d_wiring = {}
                try:#blendshapeGroup...
                    grp = attributes.returnMessageObject(mObj.mNode,'bsGeoGroup')# Find the group
                    if mc.objExists( grp ):
                        mGroup = cgmMeta.cgmObject(grp)
                    else:#Make it
                        mGroup = cgmMeta.cgmObject(name='bsGeoGroup')#Create and initialize
                        mGroup.doStore('cgmName',mObj.mNode)
                        mGroup.doStore('cgmTypeModifier','bsGeo')
                        mGroup.doName()
                        mGroup.connectParentNode(mObj.mNode,'owner', 'bsGeoGroup')
                        
                        mGroup.parent = self.mi_network.masterNull.bsGeoGroup     
                except Exception,err:
                    raise Exception,"Verify bsGroup".format(err)
                            
                _blendshape = mObj.getDeformers('blendshape')
                
                try:#Baking....
                    _ml_shapes = []
                    for bsn in self._d_bsNodesBakeData.keys():
                        _d_bsn = self._d_bsNodesBakeData[bsn]
                        
                        #...for each one, let's bake she shapes and catch them
                        _catch = deformers.bakeBlendShapeNodesToTargetObject(mObj.mNode, 
                                                                             _d_bsn['mi_defMesh'].mNode, 
                                                                             bsn, 
                                                                             ignoreTargets=False,
                                                                             transferConnections=False,
                                                                             wrapMethod = 2)
                        
                        
                        if _catch:
                            self.log_info("Catch: {0}".format(_catch))
                            for i,o in enumerate( search.returnAllChildrenObjects(_catch[0],True) ):
                                a = _d_bsn['attrs'][i]
                                mBaked = cgmMeta.cgmObject(o)
                                _d_bsn['baked'][a] = mBaked
                                _d_wiring[mBaked] = self._d_bsNodesBakeData[bsn]['plug'][a]
                                mBaked.parent = mGroup
                                _ml_shapes.append(mBaked)
                            mc.delete(_catch[0])
                        else:
                            self.log_warning("Everythign culled from {0}".format(bsn))
                except Exception,err:raise Exception,"Baking failure | {0}".format(err)
                
                if _blendshape:#...just need to add to our blendshape
                    _mi_objBlendshape = cgmMeta.cgmBlendShape(_blendshape[0])
                    #_weight_attrs = [str(a) for a in _mi_objBlendshape.get_weight_attrs()]   
                    
                    for mObj in _ml_shapes:
                        if mObj.p_nameBase in [str(a) for a in _mi_objBlendshape.get_weight_attrs()]:#...can't store because it's changing
                            self.log_info("{0} exists. Replacing...".format(mObj.p_nameBase))
                        else:
                            self.log_info("{0} doesn't exist. Adding...".format(mObj.p_nameBase))
                            _res = _mi_objBlendshape.bsShape_add(mObj.mNode)
                            attributes.doConnectAttr(_d_wiring[mObj],
                                                     _mi_objBlendshape.mNode + ".w[{0}]".format(_res[0]))                            
                    
                else:
                    #Now we're gonna build our blenshape with our new targets
                    newBlendShapeNode = deformers.buildBlendShapeNode(mObj.mNode, [mObj.mNode for mObj in _ml_shapes])
                    _mi_objBlendshape = cgmMeta.cgmBlendShape(newBlendShapeNode)
                    _mi_objBlendshape.rename( "{0}_bsNode".format(mObj.p_nameBase) )
                    _weight_attrs = [str(a) for a in _mi_objBlendshape.get_weight_attrs()]   
                for mObj in _ml_shapes:
                    if mObj.p_nameBase in [str(a) for a in _mi_objBlendshape.get_weight_attrs()]:#...can't store because it's changing:
                        self.log_info("{0} exists. Wiring...".format(mObj.p_nameBase))
                        attributes.doConnectAttr(_d_wiring[mObj],
                                                 _mi_objBlendshape.mNode + ".{0}".format(mObj.p_nameBase))                         
                    else:
                        self.log_info("{0} doesn't exist. Adding...".format(mObj.p_nameBase))
                        _res = _mi_objBlendshape.bsShape_add(mObj.mNode)
                        attributes.doConnectAttr(_d_wiring[mObj],
                                                 _mi_objBlendshape.mNode + ".w[{0}]".format(_res[0]))   
                        
                    mc.delete(mObj.mNode)#delete our shape as we don't need it anymore
                    """for bsn in self._d_bsNodesBakeData.keys():
                        for a in self._d_bsNodesBakeData[bsn]['attrs']:
                            if self._d_bsNodesBakeData[bsn]['baked'][a]:
                                _index = _ml_shapes.index( self._d_bsNodesBakeData[bsn]['baked'][a])
                                attributes.doConnectAttr(self._d_bsNodesBakeData[bsn]['plug'][a],
                                                         newBlendShapeNode + ".w[{0}]".format(_index))
                            else:
                                self.log_debug("{0} missing a baked geo".format(a))"""

                        #mc.parent(o,mGroup.mNode)
                    #mc.delete(_catch[0])
                        
                        
                            

                            

                
        def _fnc_skinCluster_(self):
            for mObj in self.ml_targets:
                _str_obj = mObj.p_nameShort
                if deformers.isSkinned(mObj.mNode):
                    self.log_info("Skinned: {0}".format(_str_obj))
                    _skin = skinning.querySkinCluster(mObj.mNode)
                    _skinMasterGeo = mObj.getMessage('skinMaster')
                    if _skinMasterGeo:#...if we have a skinMaster tagged to our object, update off that
                        self.log_info("skinMaster found...updating weights")
                        _skinMaster = skinning.querySkinCluster(_skinMasterGeo[0])
                        mc.copySkinWeights(ss = _skinMaster, ds = _skin, noMirror = True,
                                           surfaceAssociation = 'closestPoint', influenceAssociation = 'name')                        
                    else:#...otherwise, use the the unified skinCluster
                        self.log_info("No sknMaster...updating from unified")
                        mc.copySkinWeights(ss = self._mi_sourceSkinCluster.mNode, ds = _skin, noMirror = True,
                                           surfaceAssociation = 'closestPoint', influenceAssociation = 'name')
                        
                else:
                    self.log_info("Skinning: {0}".format(_str_obj))                    
                    _skin = skinWeights.transferSkinning( self._mi_sourceSkinMesh.mNode, mObj.mNode )
                    mc.rename(_skin,"{0}_skinNode".format(mObj.p_nameBase))
                    mObj.doStore('skinMaster',self._mi_sourceSkinMesh.mNode)

    return fncWrap(*args,**kws).go()
    