import copy
import re

import logging
logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

# From Maya =============================================================
import maya.cmds as mc

# From Red9 =============================================================
from Red9.core import Red9_Meta as r9Meta
from Red9.core import Red9_General as r9General

# From cgm ==============================================================
from cgm.core import cgm_General as cgmGeneral
from cgm.core import cgm_Meta as cgmMeta
from cgm.core.lib import skinDat as SKINDAT
from cgm.core.classes import NameFactory as nFactory
from cgm.core.classes import SnapFactory as Snap
from cgm.core.classes import GuiFactory as gui
from cgm.core.rigger import TemplateFactory as tFactory
from cgm.core.cgmPy import validateArgs as cgmValid
from cgm.core.classes import NodeFactory as cgmNodeFactory
from cgm.core.cgmPy import os_Utils as cgmOS_UTILS
from morpheusRig_v2.core import morpheus_sharedData as MORPHYDATA

import morpheusRig_v2.assets.gui as mGUIFolder
import morpheusRig_v2.assets.skinData as mSKINDATFolder

from cgm.lib import (curves,
                     deformers,
                     distance,
                     search,
                     lists,
                     names,
                     modules,
                     constraints,
                     rigging,
                     attributes,
                     joints)

#======================================================================
# Processing factory
#======================================================================
def flag_initializationStuff():pass
#l_modulesToDoOrder = ['torso','neckHead']
#This is the main key for data tracking. It is also the processing order
#l_modulesToDoOrder = ['torso','neckHead','leg_left']
#l_modulesToDoOrder = ['torso','clavicle_left','clavicle_right','arm_left','arm_right']
#l_modulesToDoOrder = ['torso','clavicle_left']
#l_modulesToDoOrder = ['torso','neckHead','leg_left','leg_right']
l_modulesToDoOrder = ['torso','neckHead','leg_left','leg_right',
                      'clavicle_left','arm_left',
                      'thumb_left','index_left','middle_left','ring_left','pinky_left',                   
                      'clavicle_right','arm_right',
                      'thumb_right','index_right','middle_right','ring_right','pinky_right',
                      'eye_left','eye_right','eyelids_left','eyelids_right','mouthNose',
                      ]
l_faceModules = ['eye_left','eye_right','eyelids_left','eyelids_right','mouthNose']
#l_modulesToDoOrder = ['torso']


#This is the parent info for each module
d_moduleParents = {'torso':False,
                   'neckHead':'torso',
                   'leg_left':'torso',
                   'leg_right':'torso',
                   'foot_left':'leg_left',
                   'foot_right':'leg_right',                   
                   'clavicle_left':'torso',
                   'arm_left':'clavicle_left',
                   'thumb_left':'arm_left',
                   'index_left':'arm_left',
                   'middle_left':'arm_left',
                   'ring_left':'arm_left',
                   'pinky_left':'arm_left',
                   'clavicle_right':'torso',
                   'arm_right':'clavicle_right',
                   'thumb_right':'arm_right',
                   'index_right':'arm_right',
                   'middle_right':'arm_right',
                   'ring_right':'arm_right',
                   'pinky_right':'arm_right',
                   'eye_left':'neckHead',
                   'eyelids_left':'eye_left',
                   'eyelids_right':'eye_right',
                   'eye_right':'neckHead',
                   'mouthNose':'neckHead'}

d_moduleCheck = {'torso':{'moduleType':'torso'},#This is the intialization info
                 'neckHead':{'moduleType':'neckHead','cgmName':'neck'},
                 'leg_left':{'moduleType':'leg','cgmDirection':'left'},
                 'leg_right':{'moduleType':'leg','cgmDirection':'right'},
                 'foot_left':{'moduleType':'foot','cgmDirection':'left'}, 
                 'foot_right':{'moduleType':'foot','cgmDirection':'right'},                                  
                 'arm_left':{'moduleType':'arm','cgmDirection':'left'},
                 'clavicle_left':{'moduleType':'clavicle','cgmDirection':'left'},
                 'hand_left':{'moduleType':'hand','cgmDirection':'left'},
                 'thumb_left':{'moduleType':'thumb','cgmDirection':'left'},
                 'index_left':{'moduleType':'finger','cgmDirection':'left','cgmName':'index'}, 
                 'middle_left':{'moduleType':'finger','cgmDirection':'left','cgmName':'middle'}, 
                 'ring_left':{'moduleType':'finger','cgmDirection':'left','cgmName':'ring'}, 
                 'pinky_left':{'moduleType':'finger','cgmDirection':'left','cgmName':'pinky'},
                 'arm_right':{'moduleType':'arm','cgmDirection':'right'},
                 'clavicle_right':{'moduleType':'clavicle','cgmDirection':'right'},
                 'thumb_right':{'moduleType':'thumb','cgmDirection':'right'},
                 'index_right':{'moduleType':'finger','cgmDirection':'right','cgmName':'index'}, 
                 'middle_right':{'moduleType':'finger','cgmDirection':'right','cgmName':'middle'}, 
                 'ring_right':{'moduleType':'finger','cgmDirection':'right','cgmName':'ring'}, 
                 'pinky_right':{'moduleType':'finger','cgmDirection':'right','cgmName':'pinky'},  
                 'eye_left':{'moduleType':'eyeball','cgmDirection':'left'},
                 'eye_right':{'moduleType':'eyeball','cgmDirection':'right'},
                 'eyelids_left':{'moduleType':'eyelids','cgmDirection':'left'},
                 'eyelids_right':{'moduleType':'eyelids','cgmDirection':'right'},
                 'mouthNose':{'moduleType':'mouthNose'}}                 


#This is the template settings info
d_moduleTemplateSettings = {'torso':{'handles':5,'rollOverride':'{"-1":0,"0":0}','curveDegree':2,'rollJoints':1},
                            'neckHead':{'handles':2,'rollOverride':'{}','curveDegree':2,'rollJoints':3},
                            'leg':{'handles':4,'rollOverride':'{"-1":0}','curveDegree':1,'rollJoints':3},
                            'foot':{'handles':3,'rollOverride':'{}','curveDegree':1,'rollJoints':0},
                            'arm':{'handles':3,'rollOverride':'{}','curveDegree':1,'rollJoints':3},
                            'hand':{'handles':1,'rollOverride':'{}','curveDegree':1,'rollJoints':0},
                            'thumb':{'handles':4,'rollOverride':'{}','curveDegree':1,'rollJoints':0},   
                            'finger':{'handles':5,'rollOverride':'{}','curveDegree':1,'rollJoints':0},
                            'clavicle':{'handles':2,'rollOverride':'{}','curveDegree':1,'rollJoints':0},
                            'eye_left':{},
                            'eye_right':{},
                            'eyelids_left':{},
                            'eyelids_right':{},
                            'mouthNose':{}
                            }                            

#This dict is for which controls map to which keys
d_moduleControls = {'torso':['pelvis_bodyShaper','shoulders_bodyShaper'],
                    'neckHead':['neck_loc','head_loc'],
                    'head':['head_bodyShaper','headTop_bodyShaper'],
                    'leg_left':['l_upr_leg_loc','l_kneeMeat_sub_bodyShaper','l_ankleMeat_bodyShaper','l_ball_loc'],                    
                    'leg_right':['r_upr_leg_loc','r_kneeMeat_sub_bodyShaper','r_ankleMeat_bodyShaper','r_ball_loc'],                    
                    'foot_left':['l_ankle_bodyShaper','l_ball_loc','l_toes_bodyShaper'],                    
                    'foot_right':['r_ankle_bodyShaper','r_ball_loc','r_toes_bodyShaper'],                                        
                    'arm_left':['l_arm_loc','l_lwr_arm_loc','l_hand_bodyShaper'],
                    'hand_left':['l_hand_bodyShaper'],
                    'thumb_left':['l_thumb_1_bodyShaper','l_thumb_mid_bodyShaper',
                                  'l_thumb_2_bodyShaper','l_thumb_3_shaper1_shape2.cv[4]'],
                    'index_left':['l_root_index_loc','l_index_1_bodyShaper',
                                  'l_index_mid_bodyShaper','l_index_2_bodyShaper','l_index_2_shaperCurve1_shape2.cv[4]'], 
                    'middle_left':['l_root_middle_loc','l_middle_1_bodyShaper',
                                   'l_middle_mid_bodyShaper','l_middle_2_bodyShaper','l_middle_2_shaperCurve1_shape2.cv[4]'], 
                    'ring_left':['l_root_ring_loc','l_ring_1_bodyShaper',
                                 'l_ring_mid_bodyShaper','l_ring_2_bodyShaper','l_ring_2_shaperCurve1_shape2.cv[4]'], 
                    'pinky_left':['l_root_pinky_loc','l_pinky_1_bodyShaper',
                                  'l_pinky_mid_bodyShaper','l_pinky_2_bodyShaper',
                                  'l_pinky_1_shaperCurve3_shape2.cv[4]'],                     
                    'clavicle_left':['l_clav_loc','l_arm_loc'],
                    'clavicle_right':['r_clav_loc','r_arm_loc'],
                    'arm_right':['r_arm_loc','r_lwr_arm_loc','r_hand_bodyShaper'],
                    'thumb_right':['r_thumb_1_bodyShaper','r_thumb_mid_bodyShaper',
                                   'r_thumb_2_bodyShaper','r_thumb_3_shaper1_shape2.cv[4]'],
                    'index_right':['r_index_1_loc','r_index_1_bodyShaper',
                                   'r_index_mid_bodyShaper','r_index_2_bodyShaper','r_index_2_shaperCurve1_shape2.cv[4]'], 
                    'middle_right':['r_middle_1_loc','r_middle_1_bodyShaper',
                                    'r_middle_mid_bodyShaper','r_middle_2_bodyShaper','r_middle_2_shaperCurve1_shape2.cv[4]'], 
                    'ring_right':['r_ring_1_loc','r_ring_1_bodyShaper',
                                  'r_ring_mid_bodyShaper','r_ring_2_bodyShaper','r_ring_2_shaperCurve1_shape2.cv[4]'], 
                    'pinky_right':['r_pinky_1_loc','r_pinky_1_bodyShaper',
                                   'r_pinky_mid_bodyShaper','r_pinky_2_bodyShaper',
                                   'r_pinky_1_shaperCurve3_shape2.cv[4]'],
                    'eye_left':False,
                    'eye_right':False,
                    'eyelids_left':False,
                    'eyelids_right':False,
                    'mouthNose':False,
                    }


def verify_sizingData(mAsset = None, skinDepth = .75, locPos = False):
    """
    Gather info from customization asset
    """
    _str_funcName = 'verify_sizingData'	
    d_initialData = {}

    try:#>>> Verify our arg
        mAsset = cgmMeta.validateObjArg(mAsset,mClass = 'cgmMorpheusMakerNetwork',noneValid = False)
        _str_funcName = "{0}('{1}')".format(_str_funcName,mAsset.cgmName)	
    except Exception,error:
        raise Exception,"{0}Verify  fail | {1}".format(_str_funcName,error)

    #>> Collect our positional info
    #====================================================================
    #try:
        #mObjSet = mAsset.objSetAll#Should I use this or the message info
        #l_controlsBuffer = mAsset.objSetAll.value
    #except Exception,error:
        #raise Exception,"{0}Initial buffer fail | {1}".format(_str_funcName,error)
    try:
        mayaMainProgressBar = gui.doStartMayaProgressBar(len(l_modulesToDoOrder))
        if locPos:
            mi_null = cgmMeta.cgmObject(name = 'posGroup')
        for str_moduleKey in l_modulesToDoOrder:
            try:
                if mc.progressBar(mayaMainProgressBar, query=True, isCancelled=True ) :
                    break
                mc.progressBar(mayaMainProgressBar, edit=True, status = "On {0} | '{1}'...".format(_str_funcName,str_moduleKey), step=1)

                if str_moduleKey not in d_moduleControls.keys():
                    log.warning("{0} Missing controls info for: '{1}'".format(_str_funcName,str_moduleKey))
                    return False
                
                log.debug("{0} >> On str_moduleKey: '{1}'...".format(_str_funcName,str_moduleKey))
                controls = d_moduleControls.get(str_moduleKey)
                posBuffer = []
                if controls is False:
                    log.info("{0} No controls data.: '{1}'".format(_str_funcName,str_moduleKey))
                    continue
                for c in controls:
                    if not mc.objExists(c):
                        log.warning("Necessary positioning control not found: '%s'"%c)
                        return False
                    else:
                        log.debug("{0} >> found control: '{1}'".format(_str_funcName,c))
                        i_c = cgmMeta.cgmNode(c)
                        if i_c.isComponent():#If it's a component
                            log.debug("{0} >> Component Mode!".format(_str_funcName))
                            i_loc = cgmMeta.cgmObject(mc.spaceLocator()[0])#make a loc
                            Snap.go(i_loc.mNode,targets = c,move = True, orient = False)#Snap to the surface
                            if '.f' in i_c.getComponent():
                                mc.move(0,0,-skinDepth,i_loc.mNode,r=True,os=True)
                            #i_loc.tz -= skinDepth#Offset on z by skin depth
                            pos = i_loc.getPosition()#get position
                            i_loc.delete()
                        else:
                            pos = i_c.getPosition()
                        if not pos:return False
                        posBuffer.append(pos)
                        if locPos:
                            mi_loc = cgmMeta.asMeta(mc.spaceLocator(p = pos,n = "{0}_loc".format(c))[0])
                            if mi_loc:
                                mc.parent(mi_loc.mNode,mi_null.mNode)
                            
                d_initialData[str_moduleKey] = posBuffer
            except Exception,error:
                raise Exception,"'{0}' key fail | {1}".format(str_moduleKey,error)
    except Exception,error:
        try:gui.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar    
        except:pass	
        raise Exception,"{0} fail | {1}".format(_str_funcName,error)
    try:gui.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar    
    except:pass
    return d_initialData

def verifyMorpheusNodeStructure(mMorpheus):
    """"
    Returns a defined Morpheus asset

    What is a morpheus asset
    """
    _str_funcName = 'verifyMorpheusNodeStructure'
    assert mMorpheus.mClass == 'cgmMorpheusPuppet',"Not a cgmMorpheusPuppet"
    d_moduleInstances = {}

    def returnModuleMatch(moduleKey):
        for i_m in mMorpheus.moduleChildren:
            matchBuffer = 0
            for key in d_moduleCheck[moduleKey].keys():
                log.debug("attr: %s"%key)
                log.debug("value: '%s"%i_m.__dict__[key])
                log.debug("checkTo: '%s'"%d_moduleCheck[moduleKey].get(key))
                if i_m.hasAttr(key) and i_m.__dict__[key] == d_moduleCheck[moduleKey].get(key):
                    matchBuffer +=1
            if matchBuffer == len(d_moduleCheck[moduleKey].keys()):
                log.debug("Found Morpheus Module: '%s'"%moduleKey)
                return i_m
        return False    

    try:
        try:# Create the modules
            #=====================================================================
            mayaMainProgressBar = gui.doStartMayaProgressBar(len(l_modulesToDoOrder))
            for str_moduleKey in l_modulesToDoOrder:
                try:
                    if str_moduleKey in l_faceModules:
                        continue
                    if mc.progressBar(mayaMainProgressBar, query=True, isCancelled=True ) :
                        break
                    mc.progressBar(mayaMainProgressBar, edit=True, status = "On '%s'..."%(str_moduleKey), step=1)

                    if str_moduleKey not in d_moduleParents.keys():#Make sure we have a parent
                        raise StandardError, "Missing parent info for: '%s'"%str_moduleKey
                    if str_moduleKey not in d_moduleCheck.keys():#Make sure we have a parent
                        raise StandardError, "Missing check info for: '%s'"%str_moduleKey
                        return False  

                    if str_moduleKey in d_moduleTemplateSettings.keys():#Make sure we have settings
                        d_settingsDict = d_moduleTemplateSettings[str_moduleKey]
                    elif d_moduleCheck[str_moduleKey]['moduleType'] in d_moduleTemplateSettings.keys():
                        d_settingsDict = d_moduleTemplateSettings[ d_moduleCheck[str_moduleKey]['moduleType'] ]            
                    else:
                        log.info("Missing limb info for: '%s'"%str_moduleKey)
                        d_settingsDict = {}
                        return False

                    log.debug("'{0}' structure check...".format(str_moduleKey))                
                    if str_moduleKey in d_moduleInstances.keys():#if it's already stored, use it
                        i_module = d_moduleInstances[str_moduleKey]
                    else:
                        i_module = mMorpheus.getModuleFromDict(checkDict = d_moduleCheck[str_moduleKey])#Look for it
                        d_moduleInstances[str_moduleKey] = i_module#Store it if found
                    if not i_module:
                        log.debug("'{0}' creating...".format(str_moduleKey))                
                        kw_direction = False
                        kw_name = False
                        if 'cgmDirection' in d_moduleCheck[str_moduleKey].keys():
                            kw_direction = d_moduleCheck[str_moduleKey].get('cgmDirection')
                        if 'cgmName' in d_moduleCheck[str_moduleKey].keys():
                            kw_name = d_moduleCheck[str_moduleKey].get('cgmName')            
                        i_module = mMorpheus.addModule(mClass = 'cgmLimb',mType = d_moduleCheck[str_moduleKey]['moduleType'],name = kw_name, direction = kw_direction)
                        d_moduleInstances[str_moduleKey] = i_module#Store it

                    if i_module:
                        log.debug("'{0}' verifying...".format(str_moduleKey))                
                        i_module.__verify__()

                    #>>> Settings
                    log.debug("'{0}' settings check...".format(str_moduleKey))                		
                    for key in d_settingsDict.keys():
                        i_templateNull = i_module.templateNull
                        if i_templateNull.hasAttr(key):
                            log.debug("attr: '%s'"%key)  
                            log.debug("setting: '%s'"%d_settingsDict.get(key))                  
                            try:i_templateNull.__setattr__(key,d_settingsDict.get(key)) 
                            except:log.warning("attr failed: %s"%key)

                    #>>>Parent stuff       
                    log.debug("'{0}' parent check...".format(str_moduleKey))                
                    if d_moduleParents.get(str_moduleKey):#If we should be looking for a module parent
                        if d_moduleParents.get(str_moduleKey) in d_moduleInstances.keys():
                            i_moduleParent = False
                            if i_module.getMessage('moduleParent') and i_module.getMessage('moduleParent') == [d_moduleInstances[d_moduleParents[str_moduleKey]].mNode]:
                                i_moduleParent = None
                            else:
                                i_moduleParent = d_moduleInstances[d_moduleParents.get(str_moduleKey)]
                        else:
                            i_moduleParent = mMorpheus.getModuleFromDict(checkDict = d_moduleCheck.get(d_moduleParents.get(str_moduleKey)))

                        if i_moduleParent is None:
                            log.debug("'{0}' | moduleParent already connected: '{1}'".format(str_moduleKey,d_moduleParents.get(str_moduleKey)))                
                        elif i_moduleParent:
                            i_module.doSetParentModule(i_moduleParent.mNode)
                        else:
                            log.debug("'{0}' | moduleParent not found from key: '{1}'".format(str_moduleKey,d_moduleParents.get(str_moduleKey)))
                except Exception,error:
                    gui.doEndMayaProgressBar(mayaMainProgressBar)
                    raise Exception,"'{0}' | {1}".format(str_moduleKey,error)
                
            try:#Face Modules
                mi_customizationNetwork = mMorpheus.get_customizationNetwork()
                if not mi_customizationNetwork:
                    log.error("No customization network found to get facial helpers")
                else:
                    d_rigBlocks = {'eye_left':'rigBlock_eye_left',
                                   'eye_right':'rigBlock_eye_right',
                                   'mouthNose':'rigBlock_face_lwr'}
                    mi_templateNull = mi_customizationNetwork.mSimpleFaceModule.templateNull
                    for k in d_rigBlocks.keys():
                        mi_block = mi_templateNull.getMessageAsMeta(d_rigBlocks[k])
                        if not mi_block:
                            log.error("No rig block for {0}".format(k))
                        else:
                            log.info("'{0}' rig block: {1}".format(k,mi_block))
                            mi_block.__verifyModule__()
                            mi_module = mi_block.moduleTarget
                            try:#Parent
                                mi_module.doSetParentModule(d_moduleInstances[d_moduleParents[k]])
                            except Exception,error:
                                raise Exception,"Parent fail | {0}".format(error)                

            except Exception,error:
                raise Exception,"Face modules fail | {0}".format(error)
            #Gather modules
            mMorpheus.gatherModules()            
        except Exception,error:raise Exception,"{0} create modules fail | {1}".format(_str_funcName,error)

        # For each module
        #=====================================================================
        gui.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar    
        #i_limb.getGeneratedCoreNames()   
        return mMorpheus

    except Exception,error:
        gui.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar    	
        raise Exception,"{0} fail | error: {1}".format(_str_funcName,error)


def setState(mAsset,state = 0,
             **kws):
    """
    """ 
    _str_funcName = 'verifyMorpheusNodeStructure'	        
    assert mAsset.mClass == 'cgmMorpheusMakerNetwork', "Not a customization Network"
    assert mAsset.mPuppet.mClass == 'cgmMorpheusPuppet',"Puppet isn't there"

    try:#>>>Kw defaults
        rebuildFrom = kws.get('rebuildFrom') or None
        forceNew =  kws.get('forceNew') or False
        tryTemplateUpdate = kws.get('tryTemplateUpdate') or True
        loadTemplatePose = kws.get('loadTemplatePose') or True

        mi_Morpheus = mAsset.mPuppet
        d_customizationData = verify_sizingData(mAsset)
    except Exception,error:
        raise Exception,"{0} defaults fail | {1}".format(_str_funcName,error)

    try:#connect our geo to our unified mesh geo
        '''
	if mAsset.getMessage('geo_unified'):
	    mi_Morpheus.connectChildNode(mAsset.getMessage('geo_unified')[0],'unifiedGeo')
	else:
	    log.error("{0} no baseBody geo linked to mAsset".format(_str_funcName))
	    return False	
	'''
        if not d_customizationData:
            log.error("{0} no customization data found".format(_str_funcName))
            return False
    except Exception,error:
        raise Exception,"{0} validation fail | {1}".format(_str_funcName,error)

    try:
        mayaMainProgressBar = gui.doStartMayaProgressBar(len(l_modulesToDoOrder))

        for str_moduleKey in l_modulesToDoOrder:
            try:
                if mc.progressBar(mayaMainProgressBar, query=True, isCancelled=True ) :
                    break
                mc.progressBar(mayaMainProgressBar, edit=True, status = "Setting:'%s'..."%(str_moduleKey), step=1)
                mi_module = mi_Morpheus.getModuleFromDict(checkDict = d_moduleCheck[str_moduleKey])
                if not mi_module:
                    log.warning("Cannot find Module: '%s'"%str_moduleKey)
                    return False
                log.debug("Building: '%s'"%str_moduleKey)
                try:
                    kws['sizeMode'] = 'manual'
                    kws['posList'] = d_customizationData.get(str_moduleKey)
                    mi_module.setState(state,**kws)
                except StandardError,error:
                    log.error("Set state fail: '%s'"%mi_module.getShortName())
                    raise StandardError,error
            except Exception,error:
                raise Exception,"'{0}' | {1}".format(str_moduleKey,error)		
        gui.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar    
    except Exception,error:
        if kws:log.info("{0} kws : {1}".format(_str_funcName,kws))
        gui.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar  
        log.error(error)
        return False

def updateTemplate(mAsset,**kws):  
    assert mAsset.mClass == 'cgmMorpheusMakerNetwork', "Not a customization Network"
    assert mAsset.mPuppet.mClass == 'cgmMorpheusPuppet',"Puppet isn't there"
    _str_funcName = 'updateTemplate'	        

    try:
        d_customizationData = verify_sizingData(mAsset)
        i_Morpheus = mAsset.mPuppet
        if not d_customizationData:
            raise ValueError,"No customization data found"
        puppet_updateGeoFromAsset(mAsset)
        md_modules = {}
    except Exception,error:
        raise Exception,"{0} validate fail | {1}".format(_str_funcName,error)

    try:
        mayaMainProgressBar = gui.doStartMayaProgressBar(len(l_modulesToDoOrder))
        
        i_Morpheus.templateSettings_call('store')

        for str_moduleKey in l_modulesToDoOrder:
            try:
                if mc.progressBar(mayaMainProgressBar, query=True, isCancelled=True ) :
                    break
                mc.progressBar(mayaMainProgressBar, edit=True, status = "Setting:'%s'..."%(str_moduleKey), step=1)

                i_module = i_Morpheus.getModuleFromDict(checkDict = d_moduleCheck[str_moduleKey])
                if not i_module:
                    log.warning("Cannot find Module: '%s'"%str_moduleKey)
                    return False
                md_modules[str_moduleKey] = i_module
                try:
                    if d_customizationData.get(str_moduleKey):
                        i_module.doSize(sizeMode = 'manual',
                                        posList = d_customizationData.get(str_moduleKey))
                    else:
                        log.error("No sizing data: {0}".format(str_moduleKey))
                except Exception,err:raise Exception,"size | {0}".format(err)
                
                
            except Exception,error:
                raise Exception,"'{0}' | {1}".format(str_moduleKey,error)
            
        gui.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar 
        
        i_Morpheus.templateSettings_call('update')
        
        try:
            _md_snapTos = {'clavicle_left':'arm_left',
                           'clavicle_right':'arm_right'}
            
            for str_key in ['clavicle_left','clavicle_right']:
                mi_module = md_modules[str_key]
                mi_templateNull = mi_module.templateNull                                   
                ml_controlObjects = mi_templateNull.msgList_get('controlObjects')
                
                ml_childControlObjects = md_modules[_md_snapTos[str_key]].templateNull.msgList_get('controlObjects')
                Snap.go(ml_controlObjects[-1].mNode, ml_childControlObjects[0].mNode)
                
                
        except Exception, err:
            raise Exception,("Shoulder fixes fail [{0}]".format(err))
        
    except Exception,error:
        if kws:log.info("{0} kws : {1}".format(_str_funcName,kws))
        gui.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar  
        log.error(error)
        return False


#Shared Settings
#========================= 
def flag_geoStuffs():pass
_d_KWARG_mMorpheusAsset = {'kw':'mMorpheusAsset',"default":None,'help':"cgmMorpheusMakerNetwork mNode or str name","argType":"cgmPuppet"}
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# Asset Wrapper
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
l_geoGroups = ['bodyGeo','bsGeo','unifiedGeo','earGeo','eyeGeo','customGeo','eyebrowGeo','teethGeo','tongueGeo']
l_earGeoGroups = ['left_earGeo','right_earGeo']
l_eyeGeoGroups = ['left_eyeGeo','right_eyeGeo']
l_teethGeoGroups = ['upper_teethGeo','lower_teethGeo']
l_bsTargetGroups = ['faceTargets','bodyTargets']	
d_geoStoreKeyToGeoGroups = {'tongue':'tongueGeoGroup',
                            'unified':'unifiedGeoGroup',
                            'teethUpr':'upper_teethGeoGroup',
                            'teethLwr':'lower_teethGeoGroup',
                            'eyebrow':'eyebrowGeoGroup',
                            'earLeft':'left_earGeoGroup',
                            'earRight':'right_earGeoGroup',
                            'eyeLeft':'left_eyeGeoGroup',
                            'eyeRight':'right_eyeGeoGroup',
                            'body':'bodyGeoGroup'}

#List of tags for skinning
d_geoGroupTagToSkinClusterTag = {'tongue':['tongue'],
                                 'unified':['bodyWithLids','eyeOrbLeft','eyeOrbRight'],
                                 'teethUpr':['teethUpr'],
                                 'teethLwr':['teethLwr'],
                                 'earLeft':['head'],
                                 'earRight':['head'],
                                 'eyeLeft':['eyeballLeft'],
                                 'eyeRight':['eyeballRight'],
                                 'body':['bodyWithLids','eyeOrbLeft','eyeOrbRight'],
                                 'head':['bodyWithLids','eyeOrbLeft','eyeOrbRight']}

d_geoGroupToWrapTag = {'eyebrow':'head'}

d_geoGroupTagToSkinData = {'tongue':'M1_tongue',
                           'body':'M1_body',
                           'head':'M1_head'}

d_geoRenderFlags = {'castsShadows':1,
                    'receivesShadows':1,
                    'motionBlur':1,
                    'primaryVisibility':1,
                    'smoothShading':1,
                    'visibleInReflections':1,
                    'visibleInRefractions':1,
                    'doubleSided':1}

def geo_checkRenderFlags(objList):
    ml_geo = cgmMeta.validateObjListArg(objList,mayaType = 'mesh', mType = 'cgmObject', noneValid = True)
    if not ml_geo:
        raise ValueError,"Nothing validated to check."
    
    for i,mObj in enumerate(ml_geo):
        if not mObj:log.info("{0} not geo. Skipping...".format(objList[i]))
        
        mShape = mObj.getShapes(asMeta =True)[0]
        for a,v in d_geoRenderFlags.iteritems():
            log.info("Trying '{0}.{1} = {2}'".format(mShape.p_nameBase,a,v))
            try:attributes.doSetAttr(mShape.mNode,a,v)
            except e,err:
                log.error(error)
                
class MorpheusNetworkFunc(cgmGeneral.cgmFuncCls):
    def __init__(self,*args,**kws):
        """
        """	
        try:
            try:asset = kws['mMorpheusAsset']
            except:
                try:asset = args[0]
                except:raise Exception,"No kw or arg asset found'"
            if asset.mClass not in ['cgmMorpheusMakerNetwork']:
                raise Exception,"[mMorpheusAsset: '{0}']{Not a asset!}".format(asset.mNode)
        except Exception,error:raise Exception,"MorpheusNetworkFunc failed to initialize | {0}".format(error)
        self._str_funcName= "testMorpheusNetworkFunc"		
        super(MorpheusNetworkFunc, self).__init__(*args, **kws)
        self._mi_asset = asset
        self._b_ExceptionInterupt = False
        self._l_ARGS_KWS_DEFAULTS = [_d_KWARG_mMorpheusAsset]	
        #=================================================================
        
_d_main_geoInfo = {#'unified':{'msg':'geo_unified',
                              #'msg_reset':'geo_resetUnified',
                              #'msg_bridge':'geo_bridgeUnified',
                              #'msg_bsBridge':'bsNode_bridgeMain'},
                   'body':{'msg':'geo_baseBody',
                           'msg_reset':'geo_resetBody',
                           'msg_bridge':'geo_bridgeBody',
                           #'msg_bsBridge':'bsNode_bridgeBody'
                           },
                   'head':{'msg':'geo_baseHead',
                           'msg_reset':'geo_resetHead',
                           'msg_bridge':'geo_bridgeHead',
                           #'msg_bsBridge':'bsNode_bridgeFace'},
                           }}

def puppet_updateGeoFromAsset(*args,**kws):
    '''
    Function to gather, duplicate and place geo for a Morpheus asset

    Geo we should have:
    1) Unified body geo for curve casing
    2) Additional geo in appropriate groups
    3) Reset body for blendshapes
    4) Blendshape building here?
    '''
    class fncWrap(MorpheusNetworkFunc):
        def __init__(self,*args,**kws):
            """
            """	
            super(fncWrap, self).__init__(*args,**kws)
            self._b_reportTimes = True
            self._l_ARGS_KWS_DEFAULTS = [_d_KWARG_mMorpheusAsset,
                                         #{'kw':'clean',"default":'Morphy_customizationNetwork',"argType":'morpheusBipedCustomizationAsset','help':"This should be a customization asset"},
                                         ]
            self.__dataBind__(*args,**kws)
            self._str_funcName = "morphyAsset.puppet_updateGeoFromAsset('{0}')".format(self._mi_asset.cgmName)		    	    
            self.__updateFuncStrings__()
            self.l_funcSteps = [{'step':'Verify','call':self._fncStep_validate_},
                                {'step':'Mesh creation','call':self._fncStep_process_},
                                ]	

        def _fncStep_validate_(self):
            if not self._mi_asset.getMessage('mPuppet'):
                raise ValueError,"Missing Puppet"
            self._mi_puppet = self._mi_asset.mPuppet
            self._mi_puppetMasterNull = self._mi_puppet.masterNull
            self._mi_puppetGeoGroup = self._mi_puppetMasterNull.geoGroup

            self.l_baseBodyGeo = self._mi_asset.getMessage('geo_unified')
            if not self.l_baseBodyGeo:
                raise ValueError,"Missing geo_unified"		

            d_AssetGeo = get_assetActiveGeo(self._mi_asset) or {}
            if not d_AssetGeo.get('d_geoTargets'):
                raise ValueError,"geo_getActive failed to find any geo"
            self.d_AssetGeo = d_AssetGeo

            puppet_purgeGeo(self._mi_asset)


        def _fncStep_process_(self):
            try:
                mi_puppet = self._mi_puppet
                mi_asset = self._mi_asset
                mi_geoGroup = self._mi_puppet.masterNull.geoGroup
            except Exception,error:
                raise Exception,"Bring local fail | {0}".format(error)

            self.log_warning("This is a wip function. This needs to resolve what final geo is being used better")

            try:            
                #>>> Check Resetter
                #=================================================
                self.md_coreGeo = {}
                
                for k in _d_main_geoInfo.keys():
                    try:
                        _d = _d_main_geoInfo[k]
                        for k1 in _d.keys():
                            try:#...loop
                                mi_base = mi_asset.getMessageAsMeta(_d[k1])
                                if not mi_base:
                                    self.log_info("Creating from base")
                                    mi_base = mi_geoGroup.getMessageAsMeta(_d['msg'])
                                    self.log_info("mi_base: {0} | {1}".format(mi_base.p_nameShort,mi_base))
                                if not mi_base:
                                    raise ValueError,"No value on asset"
                                
                                mi_newMesh = mi_base.doDuplicate(parentOnly = False,inputConnections = False)
                                try:
                                    mi_newMesh.parent = mi_geoGroup.bodyGeoGroup
                                except:
                                    self.log_error("mi_newMesh: {0} | {1}".format(mi_newMesh.p_nameShort,mi_newMesh))
                                    self.log_error("parent: {0}".format(mi_geoGroup.bodyGeoGroup))
                                mi_newMesh.addAttr('cgmName',k,attrType='string',lock=True)
                                if '_' in k1:
                                    mi_newMesh.addAttr('cgmTypeModifier',k1.split('_')[-1],attrType = 'string', lock = True)
                                mi_newMesh.doName()
                                mi_geoGroup.doStore(_d[k1],mi_newMesh.mNode)
                                if k1 == 'msg':
                                    mi_newMesh.v = True
                                    self.md_coreGeo[k] = mi_newMesh
                                else:
                                    mi_newMesh.v = False
                            except Exception,error:
                                raise Exception,"!{0} Check!| {1}".format(k1, error)  
                    except Exception,error:
                        raise Exception,"!{0} Check!| {1}".format(k, error)                          
            except Exception,error:raise Exception,"Base Geo fail | {0}".format(error)		
                #i_target.visibility = False	

            try:
                for str_key in self.d_AssetGeo.get('d_geoTargets'):
                    if str_key in ['body']:continue
                    buffer = self.d_AssetGeo.get('d_geoTargets')[str_key]
                    if buffer:
                        for obj in buffer:
                            self.log_debug("'{0}' |  need to duplicate: '{1}'".format(str_key,obj))
                            newMesh = mc.duplicate(obj)
                            mMesh = cgmMeta.cgmObject(newMesh[0])
                            mMesh.doCopyNameTagsFromObject(obj)
                            attributes.doSetLockHideKeyableAttr(mMesh.mNode,False,True,True)			    
                            mMesh.parent = False
                            self.log_info("'{0}' |  parenting to: '{1}'".format(str_key,d_geoStoreKeyToGeoGroups.get(str_key)))
                            mMesh.parent = self._mi_puppetGeoGroup.getMessageAsMeta(d_geoStoreKeyToGeoGroups.get(str_key))#...parent it		    
                            mMesh.doName()	

                            mMesh.setDrawingOverrideSettings(pushToShapes=False)

            except Exception,error:raise Exception,"Geo duplication | {0}".format(error)	
            
            try:#...Unified
                if not self._mi_puppet.getMessage('unifiedGeo'):
                    newMesh = deformers.polyUniteGeo([self.md_coreGeo['body'].mNode,
                                                      self.md_coreGeo['head'].mNode])
                    mi_united_geo = cgmMeta.cgmObject(newMesh[0])
                    mi_united_geo.doStore('cgmName',"{0}.cgmName".format(mi_puppet.mNode))
                    
                    attributes.doSetLockHideKeyableAttr(mi_united_geo.mNode,False,True,True)
                    mi_united_geo.addAttr('cgmTypeModifier','unitedGeo',attrType = 'string', lock = True)
                    mi_united_geo.doName()		
                    
                    mi_united_geo.parent = self._mi_puppetGeoGroup.unifiedGeoGroup#...parent it		    
                    self._mi_puppet.doStore('unifiedGeo',mi_united_geo.mNode)
                    mi_united_geo.setDrawingOverrideSettings(pushToShapes=False)

                else:
                    self.log_info("Unified geo found")                
            except Exception,error:raise Exception,"Unified Geo | {0}".format(error)
                
            return True

    return fncWrap(*args,**kws).go()

def puppet_purgeGeo(*args,**kws):
    '''
    Function to gather, duplicate and place geo for a Morpheus asset
    '''
    class fncWrap(MorpheusNetworkFunc):
        def __init__(self,*args,**kws):
            """
            """	
            super(fncWrap, self).__init__(*args,**kws)
            self._b_reportTimes = True
            self._l_ARGS_KWS_DEFAULTS = [_d_KWARG_mMorpheusAsset,
                                         #{'kw':'clean',"default":'Morphy_customizationNetwork',"argType":'morpheusBipedCustomizationAsset','help':"This should be a customization asset"},
                                         ]
            self.__dataBind__(*args,**kws)
            self._str_funcName = "morphyAsset.puppet_purgeGeo('{0}')".format(self._mi_asset.cgmName)		    	    
            self.__updateFuncStrings__()
            self.l_funcSteps = [{'step':'Clean','call':self._fncStep_clean_},
                                ]	

        def _fncStep_clean_(self):
            if not self._mi_asset.getMessage('mPuppet'):
                raise ValueError,"Missing Puppet"
            self._mi_puppet = self._mi_asset.mPuppet
            self._mi_puppetMasterNull = self._mi_puppet.masterNull
            self._mi_puppetGeoGroup = self._mi_puppetMasterNull.geoGroup

            self.l_baseBodyGeo = self._mi_asset.getMessage('geo_unified')
            if not self.l_baseBodyGeo:
                raise ValueError,"Missing geo_unified"	

            try:
                mi_puppet = self._mi_puppet
            except Exception,error:
                raise Exception,"Bring local fail | {0}".format(error)

            d_currentPuppetGeo = get_puppetGeo(self._mi_asset)
            if d_currentPuppetGeo.get('d_geoTargets'):
                for str_key in d_currentPuppetGeo.get('d_geoTargets'):
                    buffer = d_currentPuppetGeo.get('d_geoTargets')[str_key]
                    if buffer:
                        for obj in buffer:
                            #self.log_info("'{0}' |  deleting: '{1}'".format(str_key,obj.p_nameShort))			    
                            try:mc.delete(obj.mNode)
                            except Exception,error:self.log_error("{0} failed to delete | {1}".format(obj,error))

    return fncWrap(*args,**kws).go()

def puppet_verifyGeoDeformation(*args,**kws):
    '''
    '''
    class fncWrap(MorpheusNetworkFunc):
        def __init__(self,*args,**kws):
            """
            """	
            super(fncWrap, self).__init__(*args,**kws)
            self._b_reportTimes = True
            self._l_ARGS_KWS_DEFAULTS = [_d_KWARG_mMorpheusAsset,
                                         #{'kw':'clean',"default":'Morphy_customizationNetwork',"argType":'morpheusBipedCustomizationAsset','help':"This should be a customization asset"},
                                         ]
            self.__dataBind__(*args,**kws)
            self._str_funcName = "morphyAsset.puppet_verifyGeoDeformation('{0}')".format(self._mi_asset.cgmName)		    	    
            self.__updateFuncStrings__()
            self.l_funcSteps = [{'step':'Verify','call':self._fncStep_validate_},
                                {'step':'Blendshapes','call':self._fncStep_blendshapes_},
                                {'step':'Facial Rig','call':self._fncStep_facialRig_},                                
                                {'step':'Skinning','call':self._fncStep_skinning_},
                                ]	

        def _fncStep_validate_(self):
            if not self._mi_asset.getMessage('mPuppet'):
                raise ValueError,"Missing Puppet"

            self._mi_puppet = self._mi_asset.mPuppet
            if not self._mi_puppet.isSkeletonized():
                return self._FailBreak_("Puppet not skeletonized")

            self._mi_puppetMasterNull = self._mi_puppet.masterNull
            self._mi_puppetGeoGroup = self._mi_puppetMasterNull.geoGroup

            self._d_skinBindDict = self._mi_puppet._UTILS.get_jointsBindDict(self._mi_puppet)

            d_puppetGeo = get_puppetGeo(self._mi_asset) or {}
            if not d_puppetGeo.get('d_geoTargets'):
                raise ValueError,"get_puppetGeo failed to find any geo"
            self.d_puppetGeo = d_puppetGeo
            
        def _fncStep_blendshapes_(self):
            try:
                mi_asset = self._mi_asset
                mi_puppet = self._mi_puppet
            except Exception,error:
                raise Exception,"Bring local fail | {0}".format(error)
            
            try:#...get asset stuff
                mi_asset_faceModule = mi_asset.mSimpleFaceModule
                mi_asset_geo_bridgeHead = mi_asset.getMessageAsMeta('geo_baseHead')#mi_asset.getMessageAsMeta('geo_bridgeHead')
                mi_asset_faceModule = mi_asset.getMessageAsMeta('mSimpleFaceModule')
                ml_asset_bsNodes = mi_asset_faceModule.rigNull.msgList_get('bsNodes')
                for bsn in ml_asset_bsNodes:
                    self.log_info(bsn.mNode)
                                    
            except Exception,error:
                raise Exception,"Get asset stuff fail | {0}".format(error)
            
            try:#...get puppet geo
                mi_p_geo_head = cgmMeta.validateObjArg( mi_puppet.masterNull.geoGroup.getMessage('geo_baseHead'))
                mi_p_geo_headBridge = cgmMeta.validateObjArg( mi_puppet.masterNull.geoGroup.getMessageAsMeta('geo_bridgeHead'))
            except Exception,error:
                raise Exception,"Get puppet geo fail | {0}".format(error) 
                
            
            #...setup blendshape bridge
            try:#...bake shapes
                l_return = deformers.bakeBlendShapeNodesToTargetObject(mi_p_geo_head.mNode,
                                                                       mi_asset_geo_bridgeHead.mNode, 
                                                                       [bsn.mNode for bsn in ml_asset_bsNodes],
                                                                       transferConnections = True
                                                                       )
                try:
                    for shape in l_return[1]:
                        rigging.doParentReturnName(shape,mi_puppet.masterNull.geoGroup.getMessage('faceTargetsGroup')[0])
                except Exception,error:
                    raise Exception,"Parent shape fail! | {0}".format(error)
                mc.delete(l_return[0])
                mi_puppet.masterNull.geoGroup.bsGeoGroup.v = False#...hide the geo group
                
            except Exception,error:
                raise Exception,"Bake targets fail! | {0}".format(error) 
        
        def _fncStep_facialRig_(self):
            try:
                mi_asset = self._mi_asset
                mi_puppet = self._mi_puppet
                #FaceAttr holder
            except Exception,error:
                raise Exception,"Bring local fail | {0}".format(error)
            
            puppet_importFaceSetup(mi_asset)
            
            #...wire facial
            try:#...connect jaw
                #jawBase jawNDV
                mi_jaw = self._d_skinBindDict['jaw'][0]
                mi_rigJaw = mi_jaw.getMessageAsMeta('rigJoint')
                mi_attrHolder = mi_puppet.face_attrHolder
                
                for attr in ['tx','ty','tz','rx','ry','rz']:
                    attributes.doSetAttr(mi_attrHolder.mNode, 'jawBase_{0}'.format(attr), mi_rigJaw.getMayaAttr(attr))
                    #mi_attrHolder.__dict__['jawBase_{0}'.format(attr)] = mi_rigJaw.__dict__[attr]
                    attributes.doConnectAttr("{0}.jawNDV_{1}".format(mi_attrHolder.mNode,attr),
                                             "{0}.{1}".format(mi_rigJaw.mNode,attr),)
                                
            except Exception,error:
                raise Exception,"Jaw setup | {0}".format(error)                
        
            
        def _fncStep_skinning_(self):
            try:
                mi_puppet = self._mi_puppet
            except Exception,error:
                raise Exception,"Bring local fail | {0}".format(error)

            try:
                for str_key in self.d_puppetGeo.get('d_geoTargets'):
                    try:
                        if str_key in ['unified']:
                            self.log_warning("Skipping: {0}".format(str_key))
                            continue
                        __ml_skinJoints = False
                        l_geo = self.d_puppetGeo.get('d_geoTargets')[str_key]
                        if l_geo:
                            if str_key in d_geoGroupTagToSkinClusterTag.keys():
                                l_skinKeyBuffer = d_geoGroupTagToSkinClusterTag[str_key]
                                self.log_debug("...looking to skin to one of {0}".format(l_skinKeyBuffer))
                                __ml_skinJoints = []                            
                                for k in l_skinKeyBuffer:
                                    buffer = self._d_skinBindDict.get(k)
                                    if buffer:
                                        self.log_debug("found skin joints on key: {0} | cnt : {1}".format(k,len(buffer)))
                                        __ml_skinJoints.extend(buffer)   
                                    else:
                                        self.log_error("No skin joints on key: {0} ".format(k))                                    		    
                                for mObj in l_geo:
                                    try:
                                        str_shortName = mObj.p_nameShort			    
                                        self.log_debug("'{0}' | Checking: '{1}'".format(str_key,str_shortName))			    
                                        if deformers.isSkinned(mObj.mNode):
                                            self.log_info("... is skinned")
                                        else:
                                            if not __ml_skinJoints:
                                                raise ValueError,"No skin joints found"
                                            self.log_info("Skinning: '{0}'".format(mObj))
                                            toBind = [mJnt.mNode for mJnt in __ml_skinJoints] + [mObj.mNode]
                                            skin = mc.skinCluster(toBind, tsb = True, normalizeWeights = True, mi = 4, dr = 5)
                                            mi_skin = cgmMeta.cgmNode(skin[0])
                                            mi_skin.doStore('cgmName',mObj.mNode)					
                                            #mi_skin.doCopyNameTagsFromObject(mObj.mNode,ignore=['cgmTypeModifier','cgmType'])
                                            mi_skin.addAttr('mClass','cgmNode',attrType='string',lock=True)
                                            mi_skin.doName()
                                            
                                        try:#Bring in skin data
                                            _bfr = d_geoGroupTagToSkinData.get(str_key,False)
                                            if _bfr:
                                                mFile = cgmOS_UTILS.DIR_SEPARATOR.join([mSKINDATFolder.__pathHere__  ,'{0}.cfg'.format(_bfr)])                                                  
                                                self.log_info(mFile)
                                                SKINDAT.data(targetMesh = mObj.mNode, filepath=mFile).applySkin(influenceMode = 'target', nameMatch = True)
                                        except Exception,error:
                                            raise Exception,"SkinDat fail | {0}".format(error)
                                        
                                    except Exception,error:
                                        raise Exception,"mObj fail {0} | {1}".format(mObj.mNode,error)
                            elif str_key in d_geoGroupToWrapTag.keys():
                                _tagKey = d_geoGroupToWrapTag[str_key]
                                _wrapTo = False
                                if _tagKey == 'head':
                                    _wrapTo = cgmMeta.validateObjArg( mi_puppet.masterNull.geoGroup.getMessage('geo_baseHead'))

                                if _wrapTo:
                                    for mObj in l_geo:
                                        if deformers.returnObjectDeformers(mObj.mNode,'wrap'):
                                            self.log_info("... is wrapped")
                                        else:
                                            _wrap = deformers.wrapDeformObject(mObj.mNode,
                                                                               _wrapTo.mNode)
                            else:
                                raise Exception,"Skin key list fail | {0}".format(str_key)                            
                    except Exception,error:
                        raise Exception,"key {0} | {1}".format(str_key,error)
            except Exception,error:raise Exception,"Geo Skinning | {0}".format(error)	    
            return True

    return fncWrap(*args,**kws).go()

def get_assetActiveGeo(*args,**kws):
    '''
    This function should return the active geo from the asset to then ensure that the puppet has as well.
    '''
    class fncWrap(MorpheusNetworkFunc):
        def __init__(self,*args,**kws):
            """
            """	
            super(fncWrap, self).__init__(*args,**kws)
            self._b_reportTimes = True
            self._l_ARGS_KWS_DEFAULTS = [_d_KWARG_mMorpheusAsset] 
            self._str_funcName = "morphyAsset.get_assetActiveGeo('{0}')".format(self._mi_asset.cgmName)
            self.l_funcSteps = [{'step':'Validate','call':self._fncStep_validate_},
                                {'step':'Base Body','call':self._fncStep_baseBodyCheck_},
                                {'step':'Geo Groups','call':self._fncStep_searchGeoGroups_},
                                ]	

            self.__dataBind__(*args,**kws)
            #self.__updateFuncStrings__()
            self._returnDict = {}

        def _fncStep_validate_(self):
            self._mi_assetMasterNull = self._mi_asset.masterNull
            self._mi_assetGeoGroup = self._mi_assetMasterNull.geoGroup	    

        def _fncStep_baseBodyCheck_(self):
            self.log_warning("This is a wip function. This needs to resolve what final geo is being used better")
            try:
                self.log_info("Base body geo: {0}".format(self._mi_asset.geo_unified))
            except Exception,error: raise Exception,"gather fail | error: {0}".format(error)
            #self._returnDict['baseBody']
            self._returnDict['baseBody'] = self._mi_asset.geo_unified

        def _fncStep_searchGeoGroups_(self):
            try:#> Gather geo ------------------------------------------------------------------------------------------	    
                self._returnDict['md_geoGroups'] = {}
                self._returnDict['d_geoTargets']= {}
                d_geoGroupsToCheck = MORPHYDATA._d_customizationGeoGroupsToCheck

                for key in d_geoGroupsToCheck.keys():
                    try:
                        self.log_debug("Checking : '{0}' | msgAttr: '{1}'".format(key,d_geoGroupsToCheck[key]))
                        buffer = self._mi_assetMasterNull.getMessage(d_geoGroupsToCheck[key])
                        if not buffer:raise RuntimeError,"Group not found"			    
                        mi_group = cgmMeta.validateObjArg(buffer,mayaType = ['group'])
                        l_geoGroupObjs = mi_group.getAllChildren(fullPath = True)
                        if not l_geoGroupObjs:
                            self.log_warning("Empty group: '{0}'".format(mi_group.p_nameShort))
                        else:
                            l_toSkin = []
                            for o in l_geoGroupObjs:
                                if search.returnObjectType(o) in ['mesh','nurbsSurface']:
                                    if attributes.doGetAttr(o,'v'):
                                        l_toSkin.append(o) 
                                    else:
                                        self.log_info("'{0}' | Not visible. Ignoring: '{1}'".format(key,o))				    
                                else:self.log_debug("Not skinnable: '{0}'".format(o))				    
                            if not l_toSkin:
                                self.log_warning("No skinnable objects found")
                            else:
                                self._returnDict['d_geoTargets'][key] = l_toSkin 
                                self.log_debug("--- Good Geo for {0}:".format(key))			    
                                for o in l_toSkin:
                                    self.log_debug("     '{0}'".format(o))				
                        self._returnDict['md_geoGroups'][key]  = mi_group
                    except Exception,error:raise Exception,"{0} | {1}".format(key,error)			
            except Exception,error:
                raise Exception,"Geo gather fail | {0}".format(error)	    

            #self.log_infoDict(self._returnDict)
            return self._returnDict
    return fncWrap(*args,**kws).go()

def get_puppetGeo(*args,**kws):
    '''
    This function should return the active geo from the asset to then ensure that the puppet has as well.
    '''
    class fncWrap(MorpheusNetworkFunc):
        def __init__(self,*args,**kws):
            """
            """	
            super(fncWrap, self).__init__(*args,**kws)
            self._b_reportTimes = True
            self._l_ARGS_KWS_DEFAULTS = [_d_KWARG_mMorpheusAsset] 
            self._str_funcName = "morphyAsset.get_puppetGeo('{0}')".format(self._mi_asset.cgmName)
            self.l_funcSteps = [{'step':'Validate','call':self._fncStep_validate_},
                                {'step':'Geo Groups','call':self._fncStep_searchGeoGroups_},
                                ]	

            self.__dataBind__(*args,**kws)

        def _fncStep_validate_(self):
            if not self._mi_asset.getMessage('mPuppet'):
                raise ValueError,"Missing Puppet"
            self._mi_puppet = self._mi_asset.mPuppet
            if not self._mi_puppet.getMessage('masterNull'):
                raise ValueError,"Puppet missing masterNull. Reverify and rerun"	
            self._mi_puppetMasterNull = self._mi_puppet.masterNull
            self._mi_puppetGeoGroup = self._mi_puppetMasterNull.geoGroup

            self._returnDict = {}

        def _fncStep_searchGeoGroups_(self):
            try:#> Gather geo ------------------------------------------------------------------------------------------	    
                self._returnDict['md_geoGroups'] = {}
                self._returnDict['d_geoTargets']= {}
                l_keys = d_geoStoreKeyToGeoGroups.keys() + ['head']
                
                for key in l_keys:
                    try:
                        if key in ['head','body']:
                            self.log_info("Checking : '{0}' ".format(key))                            
                            l_geoGroupObjs =  self._mi_puppetGeoGroup.getMessage( _d_main_geoInfo[key]['msg'] )
                        else:
                            self.log_info("Checking : '{0}' | msgAttr: '{1}'".format(key,d_geoStoreKeyToGeoGroups[key]))                            
                            buffer = self._mi_puppetGeoGroup.getMessage(d_geoStoreKeyToGeoGroups[key])
                            if not buffer:raise RuntimeError,"Group not found"			    
                            mi_group = cgmMeta.validateObjArg(buffer,mayaType = ['group','transform'])
                            l_geoGroupObjs = mi_group.getAllChildren(fullPath = True)
                            if not l_geoGroupObjs:
                                self.log_info("Empty group: '{0}'".format(mi_group.p_nameShort))
                            self._returnDict['md_geoGroups'][key]  = mi_group

                        l_toSkin = []
                        for o in l_geoGroupObjs:
                            if search.returnObjectType(o) in ['mesh','nurbsSurface']:
                                l_toSkin.append(cgmMeta.cgmObject(o)) 
                            else:
                                self.log_info("Not skinnable: '{0}'".format(o))				    
                        if not l_toSkin:
                            self.log_warning("No skinnable objects found")
                        else:
                            self._returnDict['d_geoTargets'][key] = l_toSkin 
                            self.log_info("--- Good Geo for {0}:".format(key))			    
                            for o in l_toSkin:
                                self.log_info("     '{0}'".format(o))				
                    except Exception,error:raise Exception,"{0} | {1}".format(key,error)			
            except Exception,error:
                raise Exception,"Geo gather fail | {0}".format(error)	    

            #self.log_infoDict(self._returnDict)
            return self._returnDict
    return fncWrap(*args,**kws).go()

def puppet_verifyAll(*args,**kws):
    '''
    '''
    class fncWrap(MorpheusNetworkFunc):
        def __init__(self,*args,**kws):
            """
            """	
            super(fncWrap, self).__init__(*args,**kws)
            self._b_reportTimes = True
            self._l_ARGS_KWS_DEFAULTS = [_d_KWARG_mMorpheusAsset] 
            self._str_funcName = "morphyAsset.puppet_verify('{0}')".format(self._mi_asset.cgmName)		    	    
            self.__updateFuncStrings__()
            self._b_autoProgressBar = 1
            self.l_funcSteps = [{'step':'Puppet Check','call':self._fncStep_puppet_},
                                {'step':'Nodes','call':self._fncStep_nodes_},
                                {'step':'Geo Groups','call':self._fncStep_geoGroups_},
                                {'step':'Geo','call':self._fncStep_geo_},	                        
                                {'step':'Master Control','call':self._fncStep_masterControl_},	                        	                        
                                ]	
            self.__dataBind__(*args,**kws)

        def _fncStep_puppet_(self):
            if not self._mi_asset.getMessage('mPuppet'):
                raise ValueError,"Missing Puppet"
            self._mi_puppet = self._mi_asset.mPuppet
            if not self._mi_puppet.getMessage('masterNull'):
                raise ValueError,"Puppet missing masterNull. Reverify and rerun"	

            self._mi_puppetMasterNull = self._mi_puppet.masterNull

        def _fncStep_geo_(self):
            puppet_updateGeoFromAsset(self._mi_asset)

        def _fncStep_masterControl_(self):
            #Verify we have a puppet and that puppet has a masterControl which we need for or master scale plug
            if not self._mi_puppet.getMessage('masterControl'):
                if not self._mi_puppet._verifyMasterControl():
                    raise StandardError,"MasterControl failed to verify"

            mi_assetMasterControl = self._mi_asset.masterControl#...masterControl of the asset
            mi_settings = mi_assetMasterControl.controlSettings
            mi_masterNull = self._mi_puppet.masterNull
            mi_puppetMasterControl = self._mi_puppet.masterControl
            self._mi_mi_puppetMasterControl = mi_puppetMasterControl

            mi_partsGroup = self._mi_puppet.masterNull.partsGroup
            #mi_masterNull.overrideEnabled = 1	
            #cgmMeta.cgmAttr(mi_settings.mNode,'puppetVis',lock=False).doConnectOut("%s.%s"%(mi_puppetMasterControl.mNode,'v'))	    
            cgmMeta.cgmAttr(mi_settings.mNode,'puppetVis',lock=False).doConnectOut("%s.%s"%(mi_partsGroup.mNode,'overrideVisibility'))
            cgmMeta.cgmAttr(mi_settings.mNode,'puppetLock',lock=False).doConnectOut("%s.%s"%(mi_partsGroup.mNode,'overrideDisplayType'))    
            for a in ['translate','rotate','scale']:
                cgmMeta.cgmAttr(mi_puppetMasterControl,a,lock=True)
            #mi_puppetMasterControl.v = 0

        def _fncStep_nodes_(self):
            verifyMorpheusNodeStructure(self._mi_puppet)

        def _fncStep_geoGroups_(self):
            mi_masterNull = self._mi_puppetMasterNull
            mi_geoGroup = self._mi_puppetMasterNull.geoGroup

            self.log_info("Checking geo groups...")	    
            for attr in l_geoGroups + l_earGeoGroups + l_eyeGeoGroups + l_bsTargetGroups + l_teethGeoGroups:
                try:
                    self.log_info("On: {0}".format(attr))
                    str_plug = attr+'Group'
                    str_newAttrName = '_mi_' + attr+  'Group'#Get a better attribute store string    
                    mi_geoGroup.addAttr(attr+'Group',attrType = 'messageSimple', lock = True)
                    bfr_grp = mi_geoGroup.getMessage(str_plug)
                    try:
                        if not bfr_grp:
                            #if log.getEffectiveLevel() == 10:log.debug('Creating %s'%attr)                                    
                            mGrp = cgmMeta.cgmObject(name=attr)#Create and initialize
                            mGrp.addAttr('cgmName',attr)
                            mGrp.doName()
                            mGrp.connectParentNode(mi_geoGroup.mNode,'puppet', str_plug)
                        else:
                            mGrp = mi_geoGroup.getMessageAsMeta(str_plug)
                        self.__dict__[str_newAttrName] = mGrp
                    except Exception,error:raise Exception,"Group verify fail | {0}".format(error)

                    try:
                        #>>> Special data parsing to get things named how we want
                        if not mGrp.hasAttr('cgmDirection'):
                            if 'left' in attr:
                                buffer = mGrp.cgmName
                                buffer = buffer.split('left_')
                                mGrp.doStore('cgmName',''.join(buffer[1:]),overideMessageCheck = True)		
                                mGrp.doStore('cgmDirection','left')
                            if 'right' in attr:
                                buffer = mGrp.cgmName
                                buffer = buffer.split('right_')
                                mGrp.doStore('cgmName',''.join(buffer[1:]),overideMessageCheck = True)		
                                mGrp.doStore('cgmDirection','right')
                        if not mGrp.hasAttr('cgmPosition'):
                            if 'upper' in attr:
                                buffer = mGrp.cgmName
                                buffer = buffer.split('upper_')
                                mGrp.doStore('cgmName',''.join(buffer[1:]),overideMessageCheck = True)		
                                mGrp.doStore('cgmPosition','upper')
                            if 'lower' in attr:
                                buffer = mGrp.cgmName
                                buffer = buffer.split('lower_')
                                mGrp.doStore('cgmName',''.join(buffer[1:]),overideMessageCheck = True)		
                                mGrp.doStore('cgmPosition','lower')
                        if 'Geo' in attr:
                            buffer = mGrp.cgmName
                            buffer = buffer.split('Geo')
                            mGrp.doStore('cgmName',''.join(buffer[0]),overideMessageCheck = True)		
                            mGrp.doStore('cgmTypeModifier','geo',overideMessageCheck = True)
                            mGrp.doName()
                    except Exception,error:raise Exception,"Special parse fail | {0}".format(error)

                    try:# Few Case things
                        #==============            
                        if attr in l_geoGroups:
                            mGrp.parent = mi_geoGroup
                        elif attr in l_earGeoGroups:
                            mGrp.parent = self._mi_earGeoGroup
                        elif attr in l_eyeGeoGroups:
                            mGrp.parent = self._mi_eyeGeoGroup	    
                        elif attr in l_bsTargetGroups:
                            mGrp.parent = self._mi_bsGeoGroup	
                        elif attr in l_teethGeoGroups:
                            mGrp.parent = self._mi_teethGeoGroup	
                        else:    
                            mGrp.parent = self._mi_geoGroup
                    except Exception,error:raise Exception,"Parent fail | {0}".format(error)
                    attributes.doSetLockHideKeyableAttr( mGrp.mNode )
                except Exception,error:
                    self.log_error("Group check fail. | attr: '{0}' | error: {1}".format(attr,error))	    

    return fncWrap(*args,**kws).go()

#============================================================================================================
#>>> Face Controls
#============================================================================================================
def flag_faceWiring():pass
__attrHolder = 'face_attrHolder'
"""'lipCenter_lwr_fwd':{'driverAttr':'fwdBack'},
    'lipCenter_lwr_back':{'driverAttr':'-fwdBack'},
    'lipCenter_lwr_up_left':{'driverAttr':'tx','driverAttr2':'ty','mode':'cornerBlend'},
    'lipCenter_lwr_up_right':{'driverAttr':'-tx','driverAttr2':'ty','mode':'cornerBlend'},
    'lipCenter_lwr_dn_left':{'driverAttr':'tx','driverAttr2':'-ty','mode':'cornerBlend'},
    'lipCenter_lwr_dn_right':"""
_d_faceBufferAttributes = {"nose":{'attrs':['out','in','sneer_up','sneer_dn',
                                            'seal_up_cntr','seal_up_outr'],
                                   'sideAttrs':'*'},
                           "cheek":{'attrs':['up','dn','blow','suck'],
                                    'sideAttrs':'*'},
                           "eyeSqueeze":{'attrs':['up','dn'],
                                       'sideAttrs':'*'},                           
                           "mouth":{'attrs':['up','dn','left','right','fwd','back','twist'],
                                    'sideAttrs':['twist']},
                           "brow":{'attrs':['center_up','center_dn',
                                            'inr_up','inr_dn','squeeze',
                                            'mid_up','mid_dn',
                                            'outr_up','outr_dn'],
                                   'sideAttrs':[ 'inr_up','inr_dn','squeeze',
                                                 'mid_up','mid_dn',
                                                 'outr_up','outr_dn']},                       
                           "jaw":{'attrs':['dn','clench','left','right','fwd','back']},
                           "jawDriven":{'attrs':['dn_tz','dn_rx','left_tx','left_ry','left_rz','right_tx','right_ry','right_rz','fwd_tz','back_tz']},
                           "jawBase":{'attrs':['tx','ty','tz','rx','ry','rz']},
                           "jawNDV":{'attrs':['tx','ty','tz','rx','ry','rz']},                                                        
                           "driver":{'attrs':['jaw_dn_tz','jaw_dn_rx','mouth_twist',
                                              'jaw_left_tx','jaw_left_ry','jaw_left_rz',
                                              'jaw_right_tx','jaw_right_ry','jaw_right_rz',
                                              'jaw_fwd_tz','jaw_back_tz',
                                              'smile_dn_pull','frown_dn_pull']},                           
                           "lipUpr":{'attrs':['rollOut','rollIn','moreIn','moreOut','up','upSeal_outr','upSeal_cntr',
                                              'seal_out_cntr','seal_out_outr',#'seal_out_cntr_diff','seal_out_outr_diff'
                                              ],
                                   'sideAttrs':'*'},
                           "lipLwr":{'attrs':['rollOut','rollIn','moreIn','moreOut','dn','dnSeal_outr','dnSeal_cntr',
                                              'seal_out_cntr','seal_out_outr',#'seal_out_cntr_diff','seal_out_outr_diff'
                                              ],
                                     'sideAttrs':'*'},                           
                           "seal":{'attrs':['center','left','right'],
                                   'sideAttrs':[]},
                           "jDiff":{'attrs':['dn_frown','dn_smile','dn_seal_outr','dn_seal_cntr','fwd_seal_outr','fwd_seal_cntr',
                                             'back_seal_outr','back_seal_cntr','left_seal_outr','left_seal_cntr','right_seal_outr','right_seal_cntr'],
                                   'sideAttrs':'*'},
                           "lips":{'attrs':['smile','wide','narrow','purse','twistUp','twistDn','out','frown'],
                                   'sideAttrs':'*'},
                           "lipCntr_upr":{'attrs':['fwd','back','up','dn'],
                                   'sideAttrs':['up','dn']},
                           "lipCntr_lwr":{'attrs':['fwd','back','up','dn'],
                                            'sideAttrs':['up','dn']}}

_d_baseFaceAttrValues = {'driver_jaw_back_tz':-1.177,
                         'driver_jaw_fwd_tz':1.693,
                         'driver_jaw_left_ry':1,
                         'driver_jaw_left_rz':2,
                         'driver_jaw_left_tx':0.75,
                         'driver_jaw_dn_rx':27.352,
                         'driver_jaw_dn_tz':0,#6.366
                         'driver_jaw_right_ry':-1,#-5
                         'driver_jaw_right_rz':-2,
                         'driver_jaw_right_tx':-.75,
                         'driver_mouth_twist':5,
                         'jawBase_tx':0,
                         'jawBase_ty':152.204,
                         'jawBase_tz':6.806,
                         'jawBase_rx':0,
                         'jawBase_ry':0,
                         'jawBase_rz':0,
                         'driver_smile_dn_pull':.5,
                         'driver_frown_dn_pull':.8,
                         'jaw_dn':1.0,
                         }
"""
"roll":{'attrs':['upr_out','upr_in','lwr_out','lwr_in','upr_moreIn','upr_moreOut','lwr_moreIn','lwr_moreOut','lwr_moreIn','upr_up','lwr_dn',
                                            'seal_upr_out_cntr','seal_upr_out_outr','seal_lwr_out_cntr','seal_lwr_out_outr',
                                            'seal_upr_out_cntr_diff','seal_upr_out_outr_diff','seal_lwr_out_cntr_diff','seal_lwr_out_outr_diff'],
                                   'sideAttrs':'*'},
"""
_d_controlToDrivenSetup = {'mouth':{'control':'mouth_anim',
                                    'controlType':'joystickReg',
                                    'dir':{'up':{'driven':'mouth_up',
                                                 'driver':'ty'},
                                           'down':{'driven':'mouth_dn',
                                                   'driver':'ty'},
                                           'left':{'driven':'mouth_left',
                                                   'driver':'tx'},
                                           'right':{'driven':'mouth_right',
                                                    'driver':'tx'}}}}

#...this defines how we wire our controls and attributes for the face attr holder noTrack
_d_faceControlsToConnect = {'browCenter':{'control':'center_brow_anim',
                                          'wiringDict':{'brow_center_up':{'driverAttr':'ty'},
                                                        'brow_center_dn':{'driverAttr':'-ty'}}},
                            
                            'inner_brow_left':{'control':'l_inner_brow_anim',
                                               'wiringDict':{'brow_inr_up_left':{'driverAttr':'ty'},
                                                             'brow_inr_dn_left':{'driverAttr':'-ty'},
                                                             'brow_squeeze_left':{'driverAttr':'-tx'}}},                             
                            'mid_brow_left':{'control':'l_mid_brow_anim',
                                             'wiringDict':{'brow_mid_up_left':{'driverAttr':'ty'},
                                                           'brow_mid_dn_left':{'driverAttr':'-ty'}}}, 
                            'outer_brow_left':{'control':'l_ outer_brow_anim',
                                               'wiringDict':{'brow_outr_up_left':{'driverAttr':'ty'},
                                                             'brow_outr_dn_left':{'driverAttr':'-ty'}}},
                            'inner_brow_right':{'control':'r_inner_brow_anim',
                                               'wiringDict':{'brow_inr_up_right':{'driverAttr':'ty'},
                                                             'brow_inr_dn_right':{'driverAttr':'-ty'},
                                                             'brow_squeeze_right':{'driverAttr':'-tx'}}},                             
                            'mid_brow_right':{'control':'r_mid_brow_anim',
                                             'wiringDict':{'brow_mid_up_right':{'driverAttr':'ty'},
                                                           'brow_mid_dn_right':{'driverAttr':'-ty'}}}, 
                            'outer_brow_right':{'control':'r_outer_brow_anim',
                                               'wiringDict':{'brow_outr_up_right':{'driverAttr':'ty'},
                                                             'brow_outr_dn_right':{'driverAttr':'-ty'}}}, 
                            
                            'eyeSqueeze_left':{'control':'l_eyeSqueeze_anim',
                                               'wiringDict':{'eyeSqueeze_up_left':{'driverAttr':'ty'},
                                                             'eyeSqueeze_dn_left':{'driverAttr':'-ty'}}}, 
                            'eyeSqueeze_right':{'control':'r_eyeSqueeze_anim',
                                               'wiringDict':{'eyeSqueeze_up_right':{'driverAttr':'ty'},
                                                             'eyeSqueeze_dn_right':{'driverAttr':'-ty'}}}, 
                            
                            'cheek_left':{'control':'l_cheek_anim',
                                          'wiringDict':{'cheek_up_left':{'driverAttr':'ty'},
                                                        'cheek_dn_left':{'driverAttr':'-ty'},
                                                        'cheek_blow_left':{'driverAttr':'tx'},
                                                        'cheek_suck_left':{'driverAttr':'-tx'}}},
                            'cheek_right':{'control':'r_cheek_anim',
                                          'wiringDict':{'cheek_up_right':{'driverAttr':'ty'},
                                                        'cheek_dn_right':{'driverAttr':'-ty'},
                                                        'cheek_blow_right':{'driverAttr':'tx'},
                                                        'cheek_suck_right':{'driverAttr':'-tx'}}},   
                            
                            'nose_left':{'control':'l_nose_anim',
                                         'wiringDict':{'nose_in_left':{'driverAttr':'-tx'},
                                                       'nose_out_left':{'driverAttr':'tx'},
                                                       'nose_sneer_up_left':{'driverAttr':'ty'},
                                                       'nose_sneer_dn_left':{'driverAttr':'-ty'}},
                                         'simpleArgs':['{0}.nose_seal_up_cntr_left = {0}.nose_sneer_up_left * {0}.seal_center'.format(__attrHolder),
                                                       '{0}.nose_seal_up_outr_left = {0}.nose_sneer_up_left * {0}.seal_left'.format(__attrHolder)
                                                       ]},
                            'nose_right':{'control':'r_nose_anim',
                                         'wiringDict':{'nose_in_right':{'driverAttr':'-tx'},
                                                       'nose_out_right':{'driverAttr':'tx'},
                                                       'nose_sneer_up_right':{'driverAttr':'ty'},
                                                       'nose_sneer_dn_right':{'driverAttr':'-ty'}},
                                         'simpleArgs':['{0}.nose_seal_up_cntr_right = {0}.nose_sneer_up_right * {0}.seal_center'.format(__attrHolder),
                                                       '{0}.nose_seal_up_outr_right = {0}.nose_sneer_up_right * {0}.seal_right'.format(__attrHolder)
                                                       ]},                            
                                                     
                            
                            'lipCorner_left':{'control':'l_lipCorner_anim',
                                              'wiringDict':{'lips_purse_left':{'driverAttr':'purse'},
                                                            'lips_out_left':{'driverAttr':'out'},
                                                            'lips_twistUp_left':{'driverAttr':'twist'},
                                                            'lips_twistDn_left':{'driverAttr':'-twist'},
                                                            'lips_smile_left':{'driverAttr':'ty'},
                                                            'lips_frown_left':{'driverAttr':'-ty'},                                                    
                                                            'lips_narrow_left':{'driverAttr':'-tx'},
                                                            'lips_wide_left':{'driverAttr':'tx'}}},
                            'lipCorner_right':{'control':'r_lipCorner_anim',
                                              'wiringDict':{'lips_purse_right':{'driverAttr':'purse'},
                                                            'lips_out_right':{'driverAttr':'out'},
                                                            'lips_twistUp_right':{'driverAttr':'twist'},
                                                            'lips_twistDn_right':{'driverAttr':'-twist'},
                                                            'lips_smile_right':{'driverAttr':'ty'},
                                                            'lips_frown_right':{'driverAttr':'-ty'},                                                    
                                                            'lips_narrow_right':{'driverAttr':'-tx'},
                                                            'lips_wide_right':{'driverAttr':'tx'}}},                            
                            
                            'lipCenter_upr':{'control':'upper_lipCenter_anim',
                                             'wiringDict':{'lipCntr_upr_fwd':{'driverAttr':'fwdBack'},
                                                           'lipCntr_upr_back':{'driverAttr':'-fwdBack'},
                                                           'lipCntr_upr_up_left':{'driverAttr':'tx','driverAttr2':'ty','mode':'cornerBlend'},
                                                           'lipCntr_upr_up_right':{'driverAttr':'-tx','driverAttr2':'ty','mode':'cornerBlend'},
                                                           'lipCntr_upr_dn_left':{'driverAttr':'tx','driverAttr2':'-ty','mode':'cornerBlend'},
                                                           'lipCntr_upr_dn_right':{'driverAttr':'-tx','driverAttr2':'-ty','mode':'cornerBlend'},                                                           
                                                           }},     
                            'lipCenter_lwr':{'control':'lower_lipCenter_anim',
                                             'wiringDict':{'lipCntr_lwr_fwd':{'driverAttr':'fwdBack'},
                                                           'lipCntr_lwr_back':{'driverAttr':'-fwdBack'},
                                                           'lipCntr_lwr_up_left':{'driverAttr':'tx','driverAttr2':'ty','mode':'cornerBlend'},
                                                           'lipCntr_lwr_up_right':{'driverAttr':'-tx','driverAttr2':'ty','mode':'cornerBlend'},
                                                           'lipCntr_lwr_dn_left':{'driverAttr':'tx','driverAttr2':'-ty','mode':'cornerBlend'},
                                                           'lipCntr_lwr_dn_right':{'driverAttr':'-tx','driverAttr2':'-ty','mode':'cornerBlend'},                                                           
                                                           }},                              
                            
                            
                            'mouth':{'control':'mouth_anim',
                                     'wiringDict':{'seal_center':{'driverAttr':'seal_center','noTrack':True},
                                                   'seal_left':{'driverAttr':'seal_left','noTrack':True},
                                                   'seal_right':{'driverAttr':'seal_right','noTrack':True},
                                                   'mouth_twist_left':{'driverAttr':'twist'},
                                                   'mouth_twist_right':{'driverAttr':'-twist'},
                                                   'mouth_fwd':{'driverAttr':'fwdBack'},
                                                   'mouth_back':{'driverAttr':'-fwdBack'},
                                                   'mouth_up':{'driverAttr':'ty'},
                                                   'mouth_dn':{'driverAttr':'-ty'},
                                                   'mouth_left':{'driverAttr':'tx'},
                                                   'mouth_right':{'driverAttr':'-tx'}}},
                                     #'simpleArgs':['{0}.seal_center = {0}.seal_center * {0}.lipUpr_up_left'.format(__attrHolder)]},
                            
                            'uprLip_left':{'control':'l_uprLip_anim',
                                          'wiringDict':{'lipUpr_rollIn_left':{'driverAttr':'tx'},
                                                        'lipUpr_rollOut_left':{'driverAttr':'-tx','driverAttr2':'{0}.lips_wide_left'.format(__attrHolder),'mode':'negVNeg'},
                                                        'lipUpr_up_left':{'driverAttr':'ty'},
                                                        'lipUpr_moreOut_left':{'driverAttr':'roll'},
                                                        'lipUpr_moreIn_left':{'driverAttr':'-roll'},
                                                        },
                                          'simpleArgs':['{0}.lipUpr_upSeal_outr_left = {0}.seal_left * {0}.lipUpr_up_left'.format(__attrHolder),
                                                        '{0}.lipUpr_upSeal_cntr_left = {0}.seal_center * {0}.lipUpr_up_left'.format(__attrHolder),
                                                        '{0}.lipUpr_seal_out_cntr_left = {0}.seal_left * {0}.lipUpr_rollOut_left'.format(__attrHolder),
                                                        '{0}.lipUpr_seal_out_outr_left = {0}.seal_center * {0}.lipUpr_rollOut_left'.format(__attrHolder)                                                        
                                                        ]}, 
                            'uprLip_right':{'control':'r_uprLip_anim',
                                           'wiringDict':{'lipUpr_rollIn_right':{'driverAttr':'tx'},
                                                         'lipUpr_rollOut_right':{'driverAttr':'-tx','driverAttr2':'{0}.lips_wide_right'.format(__attrHolder),'mode':'negVNeg'},
                                                         'lipUpr_up_right':{'driverAttr':'ty'},
                                                         'lipUpr_moreOut_right':{'driverAttr':'roll'},
                                                         'lipUpr_moreIn_right':{'driverAttr':'-roll'},
                                                         },
                                           'simpleArgs':['{0}.lipUpr_upSeal_outr_right = {0}.seal_right * {0}.lipUpr_up_right'.format(__attrHolder),
                                                         '{0}.lipUpr_upSeal_cntr_right = {0}.seal_center * {0}.lipUpr_up_right'.format(__attrHolder),
                                                         '{0}.lipUpr_seal_out_cntr_right = {0}.seal_right * {0}.lipUpr_rollOut_right'.format(__attrHolder),
                                                         '{0}.lipUpr_seal_out_outr_right = {0}.seal_center * {0}.lipUpr_rollOut_right'.format(__attrHolder)                                                        
                                                         ]},
                            'lwrLip_left':{'control':'l_lwrLip_anim',
                                           'wiringDict':{'lipLwr_rollIn_left':{'driverAttr':'tx'},
                                                         'lipLwr_rollOut_left':{'driverAttr':'-tx','driverAttr2':'{0}.lips_wide_left'.format(__attrHolder),'mode':'negVNeg'},
                                                         'lipLwr_dn_left':{'driverAttr':'ty'},
                                                         'lipLwr_moreOut_left':{'driverAttr':'roll'},
                                                         'lipLwr_moreIn_left':{'driverAttr':'-roll'},
                                                         },
                                           'simpleArgs':['{0}.lipLwr_dnSeal_outr_left = {0}.seal_left * {0}.lipLwr_dn_left'.format(__attrHolder),
                                                         '{0}.lipLwr_dnSeal_cntr_left = {0}.seal_center * {0}.lipLwr_dn_left'.format(__attrHolder),
                                                         '{0}.lipLwr_seal_out_cntr_left = {0}.seal_left * {0}.lipLwr_rollOut_left'.format(__attrHolder),
                                                         '{0}.lipLwr_seal_out_outr_left = {0}.seal_center * {0}.lipLwr_rollOut_left'.format(__attrHolder)                                                        
                                                         ]}, 
                            'lwrLip_right':{'control':'r_lwrLip_anim',
                                            'wiringDict':{'lipLwr_rollIn_right':{'driverAttr':'tx'},
                                                          'lipLwr_rollOut_right':{'driverAttr':'-tx','driverAttr2':'{0}.lips_wide_right'.format(__attrHolder),'mode':'negVNeg'},
                                                          'lipLwr_dn_right':{'driverAttr':'ty'},
                                                          'lipLwr_moreOut_right':{'driverAttr':'roll'},
                                                          'lipLwr_moreIn_right':{'driverAttr':'-roll'},
                                                          },
                                            'simpleArgs':['{0}.lipLwr_dnSeal_outr_right = {0}.seal_right * {0}.lipLwr_dn_right'.format(__attrHolder),
                                                          '{0}.lipLwr_dnSeal_cntr_right = {0}.seal_center * {0}.lipLwr_dn_right'.format(__attrHolder),
                                                          '{0}.lipLwr_seal_out_cntr_right = {0}.seal_right * {0}.lipLwr_rollOut_right'.format(__attrHolder),
                                                          '{0}.lipLwr_seal_out_outr_right = {0}.seal_center * {0}.lipLwr_rollOut_right'.format(__attrHolder)                                                        
                                                          ]},                             
                            
                            
                            
                            'jaw':{'control':'jaw_anim',
                                   'wiringDict':{'jaw_fwd':{'driverAttr':'fwdBack'},
                                                 'jaw_back':{'driverAttr':'-fwdBack'},
                                                 'jaw_clench':{'driverAttr':'ty'},
                                                 'jaw_dn':{'driverAttr':'-ty'},
                                                 'jaw_left':{'driverAttr':'tx'},
                                                 'jaw_right':{'driverAttr':'-tx'},
                                                 'jDiff_dn_smile_left':{'driverAttr':'{0}.lips_smile_left'.format(__attrHolder),
                                                                        'driverAttr2':'{0}.jaw_dn'.format(__attrHolder),
                                                                        'driverAttr3':'{0}.driver_smile_dn_pull'.format(__attrHolder),
                                                                        'driverAttr4':'{0}.seal_left'.format(__attrHolder),
                                                                        'mode':'multMinusFactoredValue'},
                                                 'jDiff_dn_smile_right':{'driverAttr':'{0}.lips_smile_right'.format(__attrHolder),
                                                                         'driverAttr2':'{0}.jaw_dn'.format(__attrHolder),
                                                                         'driverAttr3':'{0}.driver_smile_dn_pull'.format(__attrHolder),
                                                                         'driverAttr4':'{0}.seal_right'.format(__attrHolder),
                                                                         'mode':'multMinusFactoredValue'},
                                                 'jDiff_dn_frown_left':{'driverAttr':'{0}.lips_frown_left'.format(__attrHolder),
                                                                        'driverAttr2':'{0}.jaw_dn'.format(__attrHolder),
                                                                        'driverAttr3':'{0}.driver_frown_dn_pull'.format(__attrHolder),
                                                                        'driverAttr4':'{0}.seal_left'.format(__attrHolder),
                                                                        'mode':'multMinusFactoredValue'},
                                                 'jDiff_dn_frown_right':{'driverAttr':'{0}.lips_frown_right'.format(__attrHolder),
                                                                         'driverAttr2':'{0}.jaw_dn'.format(__attrHolder),
                                                                         'driverAttr3':'{0}.driver_frown_dn_pull'.format(__attrHolder),
                                                                         'driverAttr4':'{0}.seal_right'.format(__attrHolder),
                                                                         'mode':'multMinusFactoredValue'},                                                  
                                                 },
                                   'simpleArgs':['{0}.jDiff_fwd_seal_cntr_left = {0}.seal_center * {0}.jaw_fwd'.format(__attrHolder),
                                                 '{0}.jDiff_fwd_seal_cntr_right = {0}.seal_center * {0}.jaw_fwd'.format(__attrHolder),
                                                 '{0}.jDiff_fwd_seal_outr_left = {0}.seal_left * {0}.jaw_fwd'.format(__attrHolder),
                                                 '{0}.jDiff_fwd_seal_outr_right = {0}.seal_right * {0}.jaw_fwd'.format(__attrHolder),
                                                 '{0}.jDiff_back_seal_cntr_left = {0}.seal_center * {0}.jaw_back'.format(__attrHolder),
                                                 '{0}.jDiff_back_seal_cntr_right = {0}.seal_center * {0}.jaw_back'.format(__attrHolder),
                                                 '{0}.jDiff_back_seal_outr_left = {0}.seal_left * {0}.jaw_back'.format(__attrHolder),
                                                 '{0}.jDiff_back_seal_outr_right = {0}.seal_right * {0}.jaw_back'.format(__attrHolder),
                                                 '{0}.jDiff_dn_seal_cntr_left = {0}.seal_center * {0}.jaw_dn'.format(__attrHolder),
                                                 '{0}.jDiff_dn_seal_cntr_right = {0}.seal_center * {0}.jaw_dn'.format(__attrHolder),
                                                 '{0}.jDiff_dn_seal_outr_left = {0}.seal_left * {0}.jaw_dn'.format(__attrHolder),
                                                 '{0}.jDiff_dn_seal_outr_right = {0}.seal_right * {0}.jaw_dn'.format(__attrHolder),
                                                 '{0}.jDiff_left_seal_cntr_left = {0}.seal_center * {0}.jaw_left'.format(__attrHolder),
                                                 '{0}.jDiff_left_seal_cntr_right = {0}.seal_center * {0}.jaw_left'.format(__attrHolder),
                                                 '{0}.jDiff_left_seal_outr_left = {0}.seal_left * {0}.jaw_left'.format(__attrHolder),
                                                 '{0}.jDiff_left_seal_outr_right = {0}.seal_right * {0}.jaw_left'.format(__attrHolder),
                                                 '{0}.jDiff_right_seal_cntr_left = {0}.seal_center * {0}.jaw_right'.format(__attrHolder),
                                                 '{0}.jDiff_right_seal_cntr_right = {0}.seal_center * {0}.jaw_right'.format(__attrHolder),
                                                 '{0}.jDiff_right_seal_outr_left = {0}.seal_left * {0}.jaw_right'.format(__attrHolder),
                                                 '{0}.jDiff_right_seal_outr_right = {0}.seal_right * {0}.jaw_right'.format(__attrHolder),                                                 
                                                 ]},
                            'jawNDVSetup':{'control':__attrHolder,
                                           'wiringDict':{},
                                           'simpleArgs':['{0}.jawDriven_back_tz = {0}.jaw_back * {0}.driver_jaw_back_tz'.format(__attrHolder),
                                                         '{0}.jawDriven_fwd_tz = {0}.jaw_fwd * {0}.driver_jaw_fwd_tz'.format(__attrHolder),                                                                                                  
                                                         '{0}.jawDriven_dn_tz = {0}.jaw_dn * {0}.driver_jaw_dn_tz'.format(__attrHolder),
                                                         '{0}.jawDriven_dn_rx = {0}.jaw_dn * {0}.driver_jaw_dn_rx'.format(__attrHolder),
                                                         '{0}.jawDriven_left_tx = {0}.jaw_left * {0}.driver_jaw_left_tx'.format(__attrHolder),
                                                         '{0}.jawDriven_left_ry = {0}.jaw_left * {0}.driver_jaw_left_ry'.format(__attrHolder),
                                                         '{0}.jawDriven_left_rz = {0}.jaw_left * {0}.driver_jaw_left_rz'.format(__attrHolder),                                                          
                                                         '{0}.jawDriven_right_tx = {0}.jaw_right * {0}.driver_jaw_right_tx'.format(__attrHolder),
                                                         '{0}.jawDriven_right_ry = {0}.jaw_right * {0}.driver_jaw_right_ry'.format(__attrHolder),
                                                         '{0}.jawDriven_right_rz = {0}.jaw_right * {0}.driver_jaw_right_rz'.format(__attrHolder),#...
                                                         '{0}.jawNDV_tx = {0}.jawBase_tx + {0}.jawDriven_left_tx + {0}.jawDriven_right_tx'.format(__attrHolder),
                                                         '{0}.jawNDV_ty = {0}.jawBase_ty'.format(__attrHolder),                                                          
                                                         '{0}.jawNDV_tz = {0}.jawBase_tz + {0}.jawDriven_back_tz + {0}.jawDriven_fwd_tz + {0}.jawDriven_dn_tz'.format(__attrHolder), 
                                                         '{0}.jawNDV_rx = {0}.jawBase_rx + {0}.jawDriven_dn_rx'.format(__attrHolder), 
                                                         '{0}.jawNDV_ry = {0}.jawBase_ry + {0}.jawDriven_left_ry + {0}.jawDriven_right_ry'.format(__attrHolder), 
                                                         '{0}.jawNDV_rz = {0}.jawBase_rz + {0}.jawDriven_left_rz + {0}.jawDriven_right_rz'.format(__attrHolder), 
                                                         ]}}

"""_d_faceControlsToConnect ={'jawNDVSetup':{'control':__attrHolder,
                                          'wiringDict':{},
                                          'simpleArgs':['{0}.jawDriven_back_tz = {0}.jaw_back * {0}.driver_jaw_back_tz'.format(__attrHolder),
                                                        '{0}.jawDriven_fwd_tz = {0}.jaw_fwd * {0}.driver_jaw_fwd_tz'.format(__attrHolder),                                                                                                  
                                                        '{0}.jawDriven_dn_tz = {0}.jaw_dn * {0}.driver_jaw_dn_tz'.format(__attrHolder),
                                                        '{0}.jawDriven_dn_rx = {0}.jaw_dn * {0}.driver_jaw_dn_rx'.format(__attrHolder),
                                                        '{0}.jawDriven_left_tx = {0}.jaw_left * {0}.driver_jaw_left_tx'.format(__attrHolder),
                                                        '{0}.jawDriven_left_ry = {0}.jaw_left * {0}.driver_jaw_left_ry'.format(__attrHolder),
                                                        '{0}.jawDriven_left_rz = {0}.jaw_left * {0}.driver_jaw_left_rz'.format(__attrHolder),                                                          
                                                        '{0}.jawDriven_right_tx = {0}.jaw_right * {0}.driver_jaw_right_tx'.format(__attrHolder),
                                                        '{0}.jawDriven_right_ry = {0}.jaw_right * {0}.driver_jaw_right_ry'.format(__attrHolder),
                                                        '{0}.jawDriven_right_rz = {0}.jaw_right * {0}.driver_jaw_right_rz'.format(__attrHolder),#...
                                                        '{0}.jawNDV_tx = {0}.jawBase_tx + {0}.jawDriven_left_tx + {0}.jawDriven_right_tx'.format(__attrHolder), 
                                                        '{0}.jawNDV_tz = {0}.jawBase_tz + {0}.jawDriven_back_tz + {0}.jawDriven_fwd_tz'.format(__attrHolder), 
                                                        '{0}.jawNDV_rx = {0}.jawBase_rx + {0}.jawDriven_dn_rx'.format(__attrHolder), 
                                                        '{0}.jawNDV_ry = {0}.jawBase_ry + {0}.jawDriven_left_ry + {0}.jawDriven_right_ry'.format(__attrHolder), 
                                                        '{0}.jawNDV_rz = {0}.jawBase_rz + {0}.jawDriven_left_rz + {0}.jawDriven_right_rz'.format(__attrHolder), 

                                         ]}}"""

def faceControls_verify(*args, **kws):
    """
    Function to verify the wiring on the facial joystick controls for Morpheus
    @kws
    Arg 0 | kw 'attributeHolder'(None)  -- AttributeHolder to wire
    """
    class fncWrap(cgmGeneral.cgmFuncCls):
        def __init__(self,*args, **kws):
            """
            """	
            super(fncWrap, self).__init__(*args, **kws)
            self._b_reportTimes = True
            self._str_funcName = 'faceControls_verify'	
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'attributeHolder',"default":None,
                                          'help':"Name of the attribute Holder"},]	    
            self.l_funcSteps = [{'step':'Verify Attribute Holder','call':self._fncStep_attributeHolder_},
                                {'step':'Verify Attributes','call':self._fncStep_attributes_},
                                {'step':'Verify Control Wiring','call':self._fncStep_controlWiring_}
                                ]	    
            self.__dataBind__(*args, **kws)

        def _fncStep_attributeHolder_(self):
            """
            """
            _obj = False
            self._mi_obj = False
            self._d_controls = {}
            
            if self.d_kws['attributeHolder'] is not None:
                _obj = cgmValid.objString(arg=self.d_kws['attributeHolder'], noneValid=True, 
                                           calledFrom=self._str_funcName)
                if not _obj:
                   return self._FailBreak_("Bad obj")
               
                self._mi_obj = cgmMeta.cgmNode(_obj)
                
            if not _obj:
                self.log_info("Must create attributeHolder")
                self._mi_obj = cgmMeta.cgmObject()
                
            if not self._mi_obj:
                return self._FailBreak_("Should have had an object by now")
            else:#...check naming stuff
                self._mi_obj.addAttr('cgmName','face')
                self._mi_obj.addAttr('cgmType','attrHolder')
                self._mi_obj.doName()
                
            self.log_info(self._mi_obj)
            self._str_attrHolder = self._mi_obj.mNode
            
        def _fncStep_attributes_(self):
            l_sections = _d_faceBufferAttributes.keys()
            l_sections.sort()
            
            for section in l_sections:
                self._mi_obj.addAttr("XXX_{0}_attrs_XXX".format(section),
                                     attrType = 'int',
                                     keyable = False,
                                     hidden = False,lock=True) 
                
                _d_section = _d_faceBufferAttributes[section]
                l_attrs = _d_section.get('attrs')
                l_attrs.sort()
                
                #Build our names
                l_sidesAttrs = _d_section.get('sideAttrs') or []
                if l_sidesAttrs == '*':
                    l_sidesAttrs = copy.copy(l_attrs)
                    
                for a in l_attrs:
                    l_name = [section,a]                    
                    l_names = []
                    _d = {'left':[],'right':[]}
                    if a in l_sidesAttrs:
                        for side in ['left','right']:
                            l_buffer = copy.copy(l_name)
                            l_buffer.append(side)
                            l_names.append(l_buffer)
                            #_d[side].append(l_buffer)
                    if not l_names:l_names = [l_name]
                    for n in l_names:
                        #self.log_info(n)
                        self._mi_obj.addAttr("_".join(n),attrType = 'float',hidden = False)
            
            #Set some base values...
            for k in _d_baseFaceAttrValues.keys():
                try:
                    attributes.doSetAttr(self._mi_obj.mNode,k,_d_baseFaceAttrValues[k])
                except:
                    pass                    
                        
        def _fncStep_controlWiring_(self):
            for key in _d_faceControlsToConnect.keys():
                try:
                    _d_control = _d_faceControlsToConnect[key]
                    control = _d_control['control']
                    _d_wiring = _d_control['wiringDict']
                    _l_simpleArgs = _d_control.get('simpleArgs') or []
                    self.log_info(">>>>>>>>>> On {0}...".format(control))
                    if not mc.objExists(control):
                        raise ValueError,"Control not found: {0}".format(control)
                    
                    if _d_wiring:
                        try:
                            cgmNodeFactory.connect_controlWiring(control,self._mi_obj,
                                                                 _d_wiring,
                                                                 baseName = key,
                                                                 trackAttr = True,
                                                                 simpleArgs = _l_simpleArgs )
                        except Exception,error:
                            self.log_warning(" Wire call fail | error: {0}".format(error)   ) 
                    elif _l_simpleArgs:
                            for arg in _l_simpleArgs:
                                self.log_info("On arg: {0}".format(arg))
                                try:
                                    cgmNodeFactory.argsToNodes(arg).doBuild()			
                                except Exception,error:
                                    self.log_error("{0} arg failure | error: {1}".format(arg,error))
                            
                    else:
                        self.log_warning("No wiring data or simpleArgs for key: {0}".format(key))
                        
                    #self._d_controls[key] = cgmMeta.cgmObject(control)

                except Exception,error:
                    raise Exception,"Control '{0}' fail | error: {1}".format(key,error)                
                
    return fncWrap(*args,**kws).go()

def get_blendshapeListToMake():
    l_sections = _d_faceBufferAttributes.keys()
    l_sections.sort()    
    l_allNames = []
    for section in l_sections:        
        _d_section = _d_faceBufferAttributes[section]
        l_attrs = _d_section.get('attrs')
        l_attrs.sort()
        
        #Build our names
        l_sidesAttrs = _d_section.get('sideAttrs') or []
        if l_sidesAttrs == '*':
            l_sidesAttrs = copy.copy(l_attrs)
            
        for a in l_attrs:
            l_name = [section,a]                    
            l_names = []
            if a in l_sidesAttrs:
                for side in ['left','right']:
                    l_buffer = copy.copy(l_name)
                    l_buffer.append(side)
                    l_names.append(l_buffer)
            if not l_names:l_names = [l_name]
            for n in l_names:
                #self.log_info(n)
                l_allNames.append("_".join(n))
    
    log.info("{0} Blendshapes to create:".format(len(l_allNames)))
    for n in l_allNames:
        print(n)
        
def face_connectAttrHolderToBSNodes(*args, **kws):
    """

    @kws
    Arg 0 | kw 'attributeHolder'(None)  -- AttributeHolder to wire
    Arg 1 | kw 'wireIt'(True)  -- Whether to attempt to wire or not
    """
    class fncWrap(cgmGeneral.cgmFuncCls):
        def __init__(self,*args, **kws):
            """MORPHYDATA._d_faceBlendshapeWiring
            """	
            super(fncWrap, self).__init__(*args, **kws)
            self._b_reportTimes = True
            self._str_funcName = 'face_connectAttrHolderToBSNodes'	
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'attributeHolder',"default":None,
                                          'help':"Name of the attribute Holder"},
                                         {'kw':'wireIt',"default":True,
                                          'help':"Whether to attempt to wire or not"},
                                         {'kw':'l_blendshapes',"default":MORPHYDATA._l_facialRigBSNodes,
                                          'help':"Blendshape Node list to search"},
                                         {'kw':'d_wiring',"default":MORPHYDATA._d_faceBlendshapeWiring,
                                          'help':"Blendshape Node list to search"}                                         ]	    
            self.l_funcSteps = [{'step':'Check Data','call':self._fncStep_checkData_},
                                {'step':'Wirecheck','call':self._fncStep_wireDataCheck_},
                                {'step':'Wire','call':self._fncStep_wire_},
                                ]	    
            self.__dataBind__(*args, **kws)

        def _fncStep_checkData_(self):
            """
            """
            try:
                _obj = False
                self._mi_obj = False
                if self.d_kws['attributeHolder'] is not None:
                    _obj = cgmValid.objString(arg=self.d_kws['attributeHolder'], noneValid=True, 
                                               calledFrom=self._str_funcName)
                    if not _obj:
                       return self._FailBreak_("Bad obj")
                    self._mi_obj = cgmMeta.cgmNode(_obj)
                if not self._mi_obj:
                    return self._FailBreak_("Should have had an object by now")
                self._str_attrHolder = self._mi_obj.mNode
            except Exception,error:
                raise Exception,"attr holder fail | {0}".format(error)
            self._b_wireIt = cgmValid.boolArg(self.d_kws['wireIt'],self._str_funcName)
            try:
                self._l_bsNodes = []
                self._d_bsNodeChannels = {}
                self._d_bsChannelToNode = {}
                self._l_channels = []
                self._l_channelsPop = []
                for bsNode in self.d_kws['l_blendshapes']:
                    if mc.objExists(bsNode):
                        self._l_bsNodes.append(bsNode)
                        _l_attrs = search.returnBlendShapeAttributes(bsNode)
                        _l_attrs = [str(a) for a in _l_attrs]
                        self._d_bsNodeChannels[bsNode] = _l_attrs
                        self._l_channels.extend(_l_attrs)
                        for a in _l_attrs:
                            self._d_bsChannelToNode[a] = bsNode
                    else:
                        self.log_error("bsNode doesn't exist: {0}".format(bsNode))
            except Exception,error:
                raise Exception,"blendshape check fail | {0}".format(error)  
            #self.report_selfStored()
                        
        def _fncStep_wireDataCheck_(self):
            self._d_channelWireMatch = {}
            self._d_noMatch = {}
            self._l_channelsPop = copy.copy(self._l_channels)
            
            for a,v in self.d_kws['d_wiring'].items():
                #self.log_info("Checking {0}|{1}...".format(a,m))
                if not cgmValid.isListArg(v):
                    v = [v]
                    
                for m in v:
                    if m in self._l_channels:
                        #self._d_channelWireMatch[a] = "{0}.{1}".format(self._d_bsChannelToNode[m],m)
                        self._d_channelWireMatch["{0}.{1}".format(self._d_bsChannelToNode[m],m)] = a
                        self._l_channelsPop.remove(m)
                    elif a in self._l_channels:
                        self._d_channelWireMatch["{0}.{1}".format(self._d_bsChannelToNode[a],a) ] = a   
                        self._l_channelsPop.remove(a)                    
                    else:
                        self._d_noMatch[a] = m                    
                        #self.log_error("No match for: {0}|{1}".format(a,m))
                
            self.log_infoDict(self._d_channelWireMatch,"Matches")
            self.log_infoDict(self._d_noMatch,"Attrs with no match")
            self.log_info("No match found for...")
            for a in self._l_channelsPop:
                self.log_info(a)
        
        def _fncStep_wire_(self):
            if self._b_wireIt:
                for bs,a in self._d_channelWireMatch.items():
                    if self._mi_obj.hasAttr(a):
                        try:
                            attributes.doConnectAttr("{0}.{1}".format(self._mi_obj.mNode,a),bs)
                        except Exception,error:
                            self.log_error("Failed to connect {0} >> {1}".format(a,bs))
                            self.log_error("----------------- {0}".format(error))                            
                    else:
                        self.log_info("Missing attr: '{0}'".format(a))
            else:
                self.log_warning("Not set to wire")

    return fncWrap(*args,**kws).go()

def joystickFaceControls_morphyConnect(*args, **kws):
    """
    Function to verify the finalization setup  on the facial joystick controls for Morpheus asset
    @kws
    Arg 0 | kw 'attributeHolder'(None)  -- AttributeHolder to wire
    """
    class fncWrap(cgmGeneral.cgmFuncCls):
        def __init__(self,*args, **kws):
            """
            """	
            super(fncWrap, self).__init__(*args, **kws)
            self._b_reportTimes = True
            self._str_funcName = 'faceControls_verify'	
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'facePart',"default":'m1_face_part',
                                          'help':"Name of the face module"},
                                         {'kw':'parentPart',"default":'neck_part',
                                          'help':"Name of the parent module"}]	    
            self.l_funcSteps = [{'step':'Get Info','call':self._fncStep_gatherInfo_},
                                {'step':'Setup','call':self._fncStep_setup_},
                                ]	    
            self.__dataBind__(*args, **kws)

        def _fncStep_gatherInfo_(self):
            """
            """
            try:
                self._mi_obj = cgmMeta.validateObjArg(self.d_kws.get('facePart'), mType = 'cgmSimpleBSFace')
            except Exception,error:
                raise Exception,"Object validate fail! | {0}".format(error)  
            
            
        def _fncStep_setup_(self):
            try:#Make sure face part module has a module parent
                try:
                    self._mi_parentModule = cgmMeta.validateObjArg(self.d_kws.get('parentPart'))
                except Exception,error:
                    raise Exception,"Parent part validate fail! | {0}".format(error)    
                
                self._mi_obj.doSetParentModule(self._mi_parentModule.mNode)
            except Exception,error:
                self.log_error("Module parent set fail | {0}".format(error) )
            try:#Make sure we have our object set
                self._mi_obj.__verifyObjectSet__()
            except Exception,error:
                raise Exception,"ObjectSet fail | {0}".format(error)               
            try:#Verify msglist
                ml_buffer = self._mi_obj.rigNull.msgList_get('controlsAll')
                self._mi_obj.rigNull.msgList_connect(ml_buffer,'controlsAll','rigNull')#...push back
            except Exception,error:
                raise Exception,"msgList verify fail | {0}".format(error)                
            try:#Verify mirrorable
                for mObj in ml_buffer:
                    mObj._verifyMirrorable()
                    if mObj.mirrorSide is 0:
                        mObj.mirrorAxis = 'translateX,rotateZ'
                    self._mi_obj.rigNull.moduleSet.addObj(mObj.mNode)#...objects need to be added to object set
            except Exception,error:
                raise Exception,"Verify mirrorable | {0}".format(error)                                            
            try:#Index
                pass
            except Exception,error:
                raise Exception,"Mirror indices | {0}".format(error)                
    return fncWrap(*args,**kws).go()

def puppet_importFaceSetup(*args,**kws):
    '''
    Function to :
    1) Import face gui/camera etc
    2) Read imported data to identify correct components
    3) Parent and wire said nodes to the puppet
    '''
    class fncWrap(MorpheusNetworkFunc):
        def __init__(self,*args,**kws):
            """
            """	
            super(fncWrap, self).__init__(*args,**kws)
            self._b_reportTimes = True
            self._l_ARGS_KWS_DEFAULTS = [_d_KWARG_mMorpheusAsset,
                                         #{'kw':'clean',"default":'Morphy_customizationNetwork',"argType":'morpheusBipedCustomizationAsset','help':"This should be a customization asset"},
                                         ]
            self.__dataBind__(*args,**kws)
            self._str_funcName = "morphyAsset.puppet_importFaceSetup('{0}')".format(self._mi_asset.cgmName)		    	    
            self.__updateFuncStrings__()
            self.l_funcSteps = [{'step':'Verify','call':self._fncStep_validate_},
                                {'step':'Import','call':self._fncStep_import_},
                                {'step':'Wire','call':self._fncStep_wire_}
                                ]	

        def _fncStep_validate_(self):
            if not self._mi_asset.getMessage('mPuppet'):
                raise ValueError,"Missing Puppet"

            self._mi_puppet = self._mi_asset.mPuppet
            self._mi_puppetMasterNull = self._mi_puppet.masterNull
            self._path_folderGUI = mGUIFolder.__pathHere__
            self.log_info("GUI folder: {0}".format(self._path_folderGUI))
            
            self._mi_neckHead = self._mi_puppet.getModuleFromDict(moduleType = 'neckHead')
            if not self._mi_neckHead:
                raise ValueError,"No neck head found"
            
            _bfrImportList = self._mi_puppet.getMessage('faceRigImportList')
            if _bfrImportList:
                self.log_info("Purging existing face import")
                mc.delete(_bfrImportList)
            
        def _fncStep_import_(self):
            mFile = cgmOS_UTILS.DIR_SEPARATOR.join([self._path_folderGUI,'faceGUI.mb'])
            self.log_info("mFile: {0}".format(mFile))
            #
            self.d_importedStuff = {'m1_face_part':{'connectTo':'moduleAdd'},
                                    'faceCam':{'connectTo':self._mi_puppet},
                                    'facialRig_gui_grp':{'connectTo':self._mi_puppet},
                                    'face_attrHolder':{'connectTo':self._mi_puppet}}
            try:
                l_nodes = mc.file(mFile, i = True, pr = True, force = True,prompt = False, returnNewNodes = True, gr = True, gn = 'IMPORTSTUFFS')   
            except Exception,error:
                raise Exception,"Import fail! | {0}".format(error)
            
            #...wire all our nodes back to something for easy deletion
            #self.d_importedStuff['facialRig_gui_grp']['mObj'].__setMessageAttr__('importList',l_nodes)
            self._mi_puppet.__setMessageAttr__('faceRigImportList',l_nodes)
            
            #...find our nodes on first loop...
            for k in self.d_importedStuff.keys():
                _match = False
                for o in self._mi_puppet.getMessage('faceRigImportList'):
                    if names.getBaseName(o) == k:
                        log.info("'{0}' is a match...".format(o))
                        mObj = cgmMeta.validateObjArg(o,'cgmObject')
                        self.d_importedStuff[k]['mObj'] = mObj
                        _match = True
                        continue
                if not _match:
                    self.d_importedStuff[k]['mObj'] = False
                    self.log_error("Didn't find match for {0}".format(k))
            
            self.log_infoDict(self.d_importedStuff)
            
            for k in self.d_importedStuff.keys():
                #print self.d_importedStuff[k]['mObj']
                if self.d_importedStuff[k].get('connectTo') == 'moduleAdd':
                    self.d_importedStuff[k]['mObj'].doSetParentModule(self._mi_neckHead.mNode)
                    self._mi_puppet.gatherModules()#...do this before verifying Object Set                    
                    self.d_importedStuff[k]['mObj'].__verifyObjectSet__()
                elif self.d_importedStuff[k].get('connectTo'):
                    self.d_importedStuff[k]['mObj'].connectParentNode(self._mi_puppet,'puppet',k)
                    
                self.d_importedStuff[k]['mObj'].parent = self._mi_puppet.masterNull.noTransformGroup.mNode
            
            #Let's lose that face import group...
            for o in self._mi_puppet.getMessage('faceRigImportList'):
                if 'IMPORTSTUFFS' in names.getBaseName(o):
                    self.log_info("Found group. Deleting: {0}".format(o))
                    mc.delete(o)

        def _fncStep_wire_(self):
            #...get Controls
            mi_p_geo_head = cgmMeta.validateObjArg( self._mi_puppet.masterNull.geoGroup.getMessage('geo_baseHead'))            
            l_blendshapesNodes = deformers.returnObjectDeformers(mi_p_geo_head.mNode, deformerTypes='blendShape')
            if not l_blendshapesNodes:
                raise ValueError,"No blendshapes found on head!"
            face_connectAttrHolderToBSNodes(self.d_importedStuff['face_attrHolder']['mObj'].mNode,
                                            l_blendshapes = l_blendshapesNodes )
            pass
            
    return fncWrap(*args,**kws).go()



            