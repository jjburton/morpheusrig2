"""
------------------------------------------
GuiFactory: cgm.core
Author: Josh Burton
email: jjburton@cgmonks.com

Website : http://www.cgmonks.com
------------------------------------------

Class based ui builder for cgmTools
================================================================
"""
import morpheusRig_v2
from lib import MorpheusMaker_utils as MMUTILS
reload(MMUTILS)

DIR_SEPARATOR = MMUTILS.DIR_SEPARATOR 
REPORT_JOIN = MMUTILS.REPORT_JOIN

#>>> Root settings =============================================================
__version__ = 'beta.03282016'
__toolName__ = 'MorpheusMaker'
__description__ = "This is the Morpheus Rig 2.0 asset generator"
__toolURL__ = 'www.cgmonks.com'
__author__ = 'Josh Burton'
__owner__ = 'CG Monks'
__website__ = 'www.cgmonks.com'
__defaultSize__ = 290, 500

_l_moduleStatesShort = MMUTILS._l_moduleStatesShort
_l_moduleStates = MMUTILS._l_moduleStates
_l_boolStrings = ['no','yes']
_l_blendshapeBuildOptions = ['none','only necessary','full']
_l_facialSetupType = ['blendshape','joint']

#>>> From Standard =============================================================
import os

#>>> From Maya =============================================================
import maya.cmds as mc
import maya.mel as mel
import copy
import time
#>>> From Red9 =============================================================
from Red9.core import Red9_Meta as r9Meta
from Red9.core import Red9_General as r9General

#>>> From cgm ==============================================================
from cgm.core import cgm_Meta as cgmMeta
from cgm.core import cgm_General as cgmGeneral

import morpheusRig_v2.assets.baseMesh as mBaseMeshFolder
import morpheusRig_v2.presets as mPresetsFolder
from cgm.core.cgmPy import os_Utils as cgmOS_UTILS
from cgm.core.classes import GuiFactory as gui

from morpheusRig_v2.core import MorpheusPresetFactory as mPreset
from morpheusRig_v2.core import asset_preset_factory as MorphyPreset
reload(MorphyPreset)
from morpheusRig_v2.core import CustomizationFactory as CSTMF
from morpheusRig_v2.core import morpheus_meta as morphMeta

from cgm.lib.classes import NameFactory as nFactory
from cgm.core.classes import ControlFactory as ctrlFactory
from morpheusRig_v2.core import MorpheusFactory as morphyF

from cgm.lib import (search,
                     lists,
                     attributes,
                     guiFactory,
                     deformers,
                     dictionary)

from cgm.lib.zoo.zooPyMaya import baseMelUI as mUI

#>>>======================================================================
import logging
logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
#=========================================================================
def run():
    win = go()
#'cgmMorpheusPuppet'

def returnEligbleNetworks():
    try:
        return cgmMeta.getMetaNodesInitializeOnly(mTypes = ['cgmMorpheusMakerNetwork'],asMeta=True)
    except Exception,error:
        raise Exception,"returnEligbleNetworks failed | {0}".format(error)

class go(gui.cgmGUI):
    """
    Base CG Monks core gui
    """
    #These feed to Hamish's basemel ui stuff
    USE_Template = 'cgmUITemplate'
    WINDOW_NAME = __toolName__
    WINDOW_TITLE = '{0} - {1}'.format(__toolName__,__version__)
    DEFAULT_SIZE = __defaultSize__
    DEFAULT_MENU = None
    RETAIN = True
    MIN_BUTTON = True
    MAX_BUTTON = False
    FORCE_DEFAULT_SIZE = True  #always resets the size of the window when its re-created	

    def insert_init( self,*args,**kws):	
        #>>> Options
        log.debug(">>> %s.__init__"%__toolName__)
        if kws:log.debug("kws: %s"%str(kws))
        if args:log.debug("args: %s"%str(args))
        log.debug("WINDOW_NAME: '%s'"%go.WINDOW_NAME)
        log.debug("WINDOW_TITLE: '%s'"%go.WINDOW_TITLE)
        log.debug("DEFAULT_SIZE: %s"%str(go.DEFAULT_SIZE))

        self.dockCnt = '%sDock'%__toolName__	
        self.__toolName__ = __toolName__	
        self.WINDOW_NAME = go.WINDOW_NAME
        self.WINDOW_TITLE = go.WINDOW_TITLE
        self.DEFAULT_SIZE = go.DEFAULT_SIZE
        self.__version__ = __version__	
        self.l_allowedDockAreas = ['left','right']
        self._str_reportStart = " MorpheusMakerGui >> "
        self._int_stateBuffer = None

        self._path_baseMeshDir = mBaseMeshFolder.__pathHere__
        self._path_mPresetsDir = mPresetsFolder.__pathHere__
        self.l_BaseMeshes = cgmOS_UTILS.get_lsFromPath(self._path_baseMeshDir,'maya files')
        self._path_imageFail = DIR_SEPARATOR.join([mPresetsFolder.__pathHere__,'imageFail.png'])

        self.mi_ActiveAsset = False
        self.mi_ActivePuppet = False	
        self.mi_ActivePreset = False
        self.ml_customizationNetworks = returnEligbleNetworks()
        self.l_AssetUIGroups = []
        self.l_bodyBlendshapeTargets = []
        self.md_presets = {}

        self._UTILS = MMUTILS#...link to our utils
        self._GUIUTILS = gui#...store our gui instance for pass through

    #=========================================================================
    # Menu Building
    #=========================================================================
    def build_menus(self):
        try:
            self.uiMenu_FirstMenu = mUI.MelMenu( l='Asset', pmc=self.buildMenu_first)
            self.uiMenu_PuppetMenu = mUI.MelMenu( l='Puppet', pmc=self.buildMenu_puppet)			    
            self.uiMenu_OptionsMenu = mUI.MelMenu( l='Options', pmc=self.buildMenu_options)		
            self.uiMenu_HelpMenu = mUI.MelMenu( l='Help', pmc=self.buildMenu_help)
        except Exception,error:
            raise Exception,"{0} build_menus failed | {1}".format(self._str_reportStart,error)

    def buildMenu_first( self, *args):
        try:
            self.uiMenu_FirstMenu.clear()
            self.l_BaseMeshes = cgmOS_UTILS.get_lsFromPath(self._path_baseMeshDir,'maya files')

            #>>> New Assets
            self.uiMenu_NewAsset = mUI.MelMenuItem(self.uiMenu_FirstMenu,l='New Asset',en=True,subMenu=True)
            for i,b in enumerate(self.l_BaseMeshes):
                mUI.MelMenuItem( self.uiMenu_NewAsset, l=b,
                                 c=mUI.Callback(self.asset_loadBase,i))  

            #>>> Set active
            self.ml_customizationNetworks = returnEligbleNetworks()
            self.uiMenu_ActiveAsset = mUI.MelMenuItem(self.uiMenu_FirstMenu,l='Active',en=True,subMenu=True)
            for i,i_m in enumerate(self.ml_customizationNetworks):
                mUI.MelMenuItem( self.uiMenu_ActiveAsset, l=i_m.mNode,
                                 c= mUI.Callback(MMUTILS.asset_setActive,self,i)) 


            if MMUTILS.check_assetExists(self) and self.mi_ActiveAsset:
                try:#>>> New Assets
                    mi_masterNull = self.mi_ActiveAsset.masterNull
                    self.d_sudMenuOptions_assetVis = {'geo':{'mi_transform':mi_masterNull.geoGroup},
                                                      }
                    self.uiMenu_PuppetVis = mUI.MelMenuItem(self.uiMenu_FirstMenu,l='Vis',en=True,subMenu=True)
                    for i,opt in enumerate( self.d_sudMenuOptions_assetVis.keys()):
                        d_sub = self.d_sudMenuOptions_assetVis[opt]
                        v_buffer = d_sub['mi_transform'].v			
                        mUI.MelMenuItem( self.uiMenu_PuppetVis, l="{0} | {1}".format(opt,v_buffer),
                                         annotation = "Set the visibility for asset's '{0}'".format(opt),
                                         c = mUI.Callback(attributes.doSetAttr,d_sub['mi_transform'].mNode,'v',
                                                          not d_sub['mi_transform'].v ))

                    #>>> Vis options
                    i_vis = self.mi_ActiveAsset.masterControl.controlVis or False
                    if i_vis:
                        attrs = i_vis.getAttrs(channelBox=True)
                        if attrs:
                            attrSets = lists.returnListChunks(attrs,3)
                            for aSet in attrSets:
                                for a in aSet:
                                    v_buffer = i_vis.getMayaAttr(a)			                                    
                                    mUI.MelMenuItem( self.uiMenu_PuppetVis, l="{0} | {1}".format(a,v_buffer),
                                                     annotation = "Set the visibility for asset's '{0}'".format(a),
                                                     c = mUI.Callback(attributes.doSetAttr,i_vis.mNode,a,
                                                                      not i_vis.getMayaAttr(a) ))                                

                except Exception,error:log.error("first menu vis section fail | {0}".format(error))	    

            #self.uiMenu_PresetLoad = mUI.MelMenuItem(self.uiMenu_FirstMenu,l='Presets',en=False,subMenu=True)	
            mUI.MelMenuItem(self.uiMenu_FirstMenu,l='Verify Active',
                            c = lambda *a:self.asset_verifyActive())	

            mUI.MelMenuItem(self.uiMenu_FirstMenu,l='Delete Active',
                            c = lambda *a:self.asset_deleteActive())

            mUI.MelMenuItemDiv( self.uiMenu_FirstMenu )
            mUI.MelMenuItem( self.uiMenu_FirstMenu, l="Reload",
                             c=lambda *a: self.reload())		
            mUI.MelMenuItem( self.uiMenu_FirstMenu, l="Reset",
                             c=lambda *a: self.reset())  
        except Exception,error:
            raise Exception,"{0} buildMenu_first failed | {1}".format(self._str_reportStart,error)

    def buildMenu_Generate( self, *args):
        try:
            self.uiMenu_Generate.clear()
            #>>> Reset Options
            MelMenuItem(self.uiMenu_Generate,l='test',en=False)
        except Exception,error:
            raise Exception,"{0} buildMenu_Generate failed | {1}".format(self._str_reportStart,error)

    def buildMenu_puppet( self, *args):
        try:
            self.uiMenu_PuppetMenu.clear()
            try:
                mUI.MelMenuItem( self.uiMenu_PuppetMenu, l='Verify Puppet',c=mUI.Callback(self.asset_verifyPuppetNodes))
                if MMUTILS.check_puppetExists(self):
                    #mUI.MelMenuItem( self.uiMenu_PuppetMenu, l='UpdateTemplate',c=mUI.Callback(self.mi_ActiveAsset.updateTemplate))	   

                    try:#>>> New Assets
                        mi_masterNull = self.mi_ActivePuppet.masterNull
                        self.d_sudMenuOptions_puppetVis = {'skeleton':{'mi_transform':mi_masterNull.skeletonGroup},
                                                           'geo':{'mi_transform':mi_masterNull.geoGroup},
                                                           'parts':{'mi_transform':mi_masterNull.partsGroup}}
                        self.uiMenu_PuppetVis = mUI.MelMenuItem(self.uiMenu_PuppetMenu,l='Vis',en=True,subMenu=True)
                        for i,opt in enumerate( self.d_sudMenuOptions_puppetVis.keys()):
                            d_sub = self.d_sudMenuOptions_puppetVis[opt]
                            v_buffer = d_sub['mi_transform'].v			
                            mUI.MelMenuItem( self.uiMenu_PuppetVis, l="{0} | {1}".format(opt,v_buffer),
                                             annotation = "Set the visibility for puppet's '{0}'".format(opt),
                                             c = mUI.Callback(attributes.doSetAttr,d_sub['mi_transform'].mNode,'v',not d_sub['mi_transform'].v ))
                    except Exception,error:log.error("puppetMenu vis section fail | {0}".format(error))

                    try:#>>> Geo Menu
                        mi_masterNull = self.mi_ActivePuppet.masterNull
                        self.uiMenu_PuppetGeo = mUI.MelMenuItem(self.uiMenu_PuppetMenu,l='Geo',en=True,subMenu=True)	
                        mUI.MelMenuItem( self.uiMenu_PuppetGeo, l="rebuild",
                                         c = mUI.Callback(morphyF.puppet_updateGeoFromAsset,self.mi_ActiveAsset),
                                         annotation = 'Rebuild puppet geo. Warning: Will purge existing.')
                        mUI.MelMenuItem( self.uiMenu_PuppetGeo, l="Verify Deformation",
                                         c = mUI.Callback(morphyF.puppet_verifyGeoDeformation,self.mi_ActiveAsset),
                                         annotation = 'Verify skinning, blendshapes, etc...')			                 
                        mUI.MelMenuItem( self.uiMenu_PuppetGeo, l="Purge Geo",
                                         c = mUI.Callback(morphyF.puppet_purgeGeo,self.mi_ActiveAsset),
                                         annotation = 'Delete puppet geo')				                 

                    except Exception,error:log.error("puppetMenu geo section fail | {0}".format(error))

                    try:#>>> Template Menu
                        if self._int_stateBuffer >= 2:
                            self.uiMenu_PuppetTemplate = mUI.MelMenuItem(self.uiMenu_PuppetMenu,l='Template',en=True,subMenu=True)	
                            #mUI.MelMenuItem( self.uiMenu_PuppetTemplate, l="Verify Mirror Setup",
                                #                c = mUI.Callback(self.mi_ActivePuppet.mirrorSetup_verify,'template'),
                                #               annotation = 'Load a puppet config file to the templated puppet')			
                            mUI.MelMenuItem( self.uiMenu_PuppetTemplate, l="Save config",
                                             c = mUI.Callback(MMUTILS.puppet_saveTemplateConfig,self),
                                             annotation = 'Save a puppet config file to the templated puppet')			
                            mUI.MelMenuItem( self.uiMenu_PuppetTemplate, l="Load config",
                                             c = mUI.Callback(MMUTILS.puppet_loadTemplateConfig,self),
                                             annotation = 'Load a puppet config file to the templated puppet')

                    except Exception,error:log.error("Template section fail | {0}".format(error))		    

                    mUI.MelMenuItem( self.uiMenu_PuppetMenu, l='Delete Puppet',c=mUI.Callback(self.puppet_delete))	   
            except Exception,error:
                log.error("Failed to build puppet menu | {0}".format(error))
                mUI.MelMenuItem(self.uiMenu_PuppetMenu,l='No Puppet Loaded',en=False)

        except Exception,error:
            raise Exception,"{0} buildMenu_Puppet failed | {1}".format(self._str_reportStart,error)

    def buildMenu_options(self, *args):
        try:
            self.uiMenu_OptionsMenu.clear()   
            mUI.MelMenuItem(self.uiMenu_OptionsMenu,l='Report Update Puppet',
                            c = mUI.Callback(self.report_updatePuppet))	    
            mUI.MelMenuItem(self.uiMenu_OptionsMenu,l='Focus Asset',
                            c = mUI.Callback(self._UTILS.focus_modeChange,self,'asset'))
            mUI.MelMenuItem(self.uiMenu_OptionsMenu,l='Focus Puppet',
                            c = mUI.Callback(self._UTILS.focus_modeChange,self,'puppet'))
            mUI.MelMenuItem(self.uiMenu_OptionsMenu,l='Focus clear',
                            c = mUI.Callback(self._UTILS.focus_modeChange,self,'clear'))
            self.uiMenu_LightingPresets = mUI.MelMenuItem(self.uiMenu_OptionsMenu,l='Lighting Presets',en=False,subMenu=True)
            self.uiMenu_LoadSets = mUI.MelMenuItem(self.uiMenu_OptionsMenu,l='Load Sets',en=False,subMenu=True)
            self.uiMenu_LoadProps = mUI.MelMenuItem(self.uiMenu_OptionsMenu,l='Load Props',en=False,subMenu=True)

            mUI.MelMenuItem( self.uiMenu_OptionsMenu, l='Report Self',c=mUI.Callback(MMUTILS.log_self,self))	   
            mUI.MelMenuItem(self.uiMenu_OptionsMenu,l='Reload MMUtils',
                            c = mUI.Callback(reload,MMUTILS))    
            #>>> Dock Options				
            mUI.MelMenuItem( self.uiMenu_OptionsMenu, l="Dock",
                             cb=self.var_Dock.value,
                             c= lambda *a: self.do_dockToggle())

        except Exception,error:
            raise Exception,"{0} buildMenu_Generate failed | {1}".format(self._str_reportStart,error)

    def reload(self):	
        run()
    def reset(self):	
        mUI.Callback(gui.do_resetGuiInstanceOptionVars(self.l_optionVars,run))

    def setup_Variables(self):
        try:
            gui.cgmGUI.setup_Variables(self)#Initialize parent ones, then add our own
            self.create_guiOptionVar('ActiveAsset',defaultValue = '')
            self.create_guiOptionVar('PresetFrameCollapse',defaultValue = 1)
            self.create_guiOptionVar('FaceBuildOption',defaultValue = False, varType = 'bool')	    
            self.create_guiOptionVar('BlendShapesOption',defaultValue = 0, varType = 'int')	    
            self.create_guiOptionVar('FaceSetupTypeOption',defaultValue = 0, varType = 'int')	    

        except Exception,error:
            raise Exception,"{0} setup_Variables failed | {1}".format(self._str_reportStart,error)

    #=========================================================================
    # Gui Building
    #=========================================================================
    def build_layoutWrapper(self,parent):
        try:
            uiColumn_main = mUI.MelColumn(self)

            #>>> Tab replacement
            self._l_tabOptions = ['Asset','Puppet','Dev']	
            self.create_guiOptionVar('Mode',defaultValue = self._l_tabOptions[0])#Register the option var
            mUI.MelSeparator(uiColumn_main,ut = 'cgmUIHeaderTemplate',h=5)
            #Mode Change row 
            self.uiRow_ModeSet = mUI.MelHSingleStretchLayout(uiColumn_main,ut='cgmUISubTemplate',padding = 3)
            mUI.MelSpacer(self.uiRow_ModeSet,w=5)
            mUI.MelLabel(self.uiRow_ModeSet, label = 'Choose Mode: ',align='right')
            self.uiRow_ModeSet.setStretchWidget( mUI.MelSeparator(self.uiRow_ModeSet) )		
            self.uiRow_ModeSet.layout()

            self.uiRadioCollection_main = mUI.MelRadioCollection()
            self.uiOptions_main = []		

            #build our sub section options
            self.uiContainers_main = []
            self.uiContainers_main.append( self.buildTab_Asset( uiColumn_main) )
            self.uiContainers_main.append( self.buildTab_Puppet( uiColumn_main) )
            self.uiContainers_main.append( self.buildTab_Dev( uiColumn_main) )

            for item in self._l_tabOptions:
                self.uiOptions_main.append(self.uiRadioCollection_main.createButton(self.uiRow_ModeSet,label=item,
                                                                                    onCommand = mUI.Callback(MMUTILS.tab_setActive,self,item)))
            self.uiRow_ModeSet.layout()


            mc.radioCollection(self.uiRadioCollection_main,edit=True, sl=self.uiOptions_main[self._l_tabOptions.index(self.var_Mode.value)])

            self.asset_autoLoad()#Autoload asset	
            self.reports_updateAll()
            self.presets_updateLists()
        except Exception,error:
            raise Exception,"{0} build_layoutWrapper failed | {1}".format(self._str_reportStart,error)
        
    #@cgmGeneral.Timer
    def buildTab_Asset(self,parent):
        try:
            ui_customizeMainForm = mUI.MelFormLayout(self.Get(),numberOfDivisions=100)
            uiScroll_customize = mUI.MelScrollLayout(ui_customizeMainForm)				
            uiWrapper_Customize = uiScroll_customize
            #>>> Main Help	
            self.uiReport_CustomizeHelp = mUI.MelLabel(ui_customizeMainForm,
                                                       bgc = dictionary.returnStateColor('help'),
                                                       align = 'center',
                                                       label = 'Customize options for a Morpheus Asset',
                                                       h=20,
                                                       vis = self.var_ShowHelp.value)	

            self.l_helpElements.append(self.uiReport_CustomizeHelp)

            #>>> Main Report			
            self.uiReport_Customize = mUI.MelLabel(ui_customizeMainForm,
                                                   bgc = dictionary.returnStateColor('help'),
                                                   align = 'center',
                                                   label = '...',
                                                   h=20)

            """#>>> Mirror Buttons
            uiRow_MirrorButtons = mUI.MelHLayout(ui_customizeMainForm,en=False)
            self.l_AssetUIGroups.append(uiRow_MirrorButtons)
            gui.add_Button(uiRow_MirrorButtons,"<<< Push",
                           lambda *a:self.do_pushToOtherSide('left'),
                           "Restore symmetry by pushing right to left")
            gui.add_Button(uiRow_MirrorButtons,"Reset",
                           lambda *a:self.do_resetControls(),
                           "Reset Character")		
            gui.add_Button(uiRow_MirrorButtons,"Push >>>",
                           lambda *a:self.do_pushToOtherSide('right'),
                           "Restore symmetry by pushing left to right")	               
            uiRow_MirrorButtons.layout()"""


            #=============================
            #>>> Presets layout
            #=============================	
            # Frame
            self.uiFrame_PresetOptions = mUI.MelFrameLayout(uiWrapper_Customize,label = 'Presets',vis=True,
                                                            collapse=self.var_PresetFrameCollapse.value,
                                                            collapsable=True,
                                                            enable=True,
                                                            expandCommand = lambda:self.var_PresetFrameCollapse.setValue(0),
                                                            collapseCommand = lambda:self.var_PresetFrameCollapse.setValue(1)
                                                            )		
            uiContainer = mUI.MelColumnLayout(self.uiFrame_PresetOptions,
                                              ut = 'cgmUILockedTemplate',
                                              )
            # Help
            uiHelp_Presets = mUI.MelLabel(uiContainer,
                                          bgc = dictionary.returnStateColor('help'),
                                          align = 'center',
                                          label = "Load, Save, other preset functions",
                                          h=20,
                                          vis = self.var_ShowHelp.value)	
            self.l_helpElements.append(uiHelp_Presets)

            # Image
            imageRow = mUI.MelHRowLayout(uiContainer,bgc=[0,0,0])
            mUI.MelSpacer(imageRow,w=10)
            self.uiImage_Preset = mUI.MelImage(imageRow,w=230, h=230)
            self.uiImage_Preset.setImage(self._path_imageFail)
            mUI.MelSpacer(imageRow,w=10)	
            imageRow.layout()

            # Report
            self.uiReport_Preset = mUI.MelLabel(uiContainer,
                                                bgc = dictionary.returnStateColor('help'),
                                                align = 'center',
                                                label = '...',
                                                h=20)		
            # Object List
            self.uiObjectList_Presets = mUI.MelObjectScrollList(uiContainer, ut='cgmUISubTemplate', allowMultiSelection=False, h=125 )

            # Check Boxes
            uiRow_PresetOptionsButtons = mUI.MelHSingleStretchLayout(uiContainer,ut='cgmUISubTemplate',padding = 2)	
            mUI.MelSpacer(uiRow_PresetOptionsButtons,w=5)
            mUI.MelLabel(uiRow_PresetOptionsButtons,label='Load: ')
            uiRow_PresetOptionsButtons.setStretchWidget( mUI.MelSeparator(uiRow_PresetOptionsButtons) )	
            self.uiCB_FacePreset = gui.add_CheckBox(self,uiRow_PresetOptionsButtons,'PresetFaceOptionState',label = 'Face')
            self.uiCB_ArmsPreset = gui.add_CheckBox(self,uiRow_PresetOptionsButtons,'PresetArmsOptionState',label = 'Arms')
            self.uiCB_LegsPreset = gui.add_CheckBox(self,uiRow_PresetOptionsButtons,'PresetLegsOptionState',label = 'Legs')
            self.uiCB_TorsoPreset = gui.add_CheckBox(self,uiRow_PresetOptionsButtons,'PresetTorsoOptionState',label = 'Torso')

            uiRow_PresetOptionsButtons.layout()

            #>>> Randomize Buttons
            """
	    mc.setParent(uiWrapper_Customize)
	    SetHeader = gui.add_Header('Randomize')	
	    uiRow_RandomButtons = mUI.MelHLayout(uiWrapper_Customize,en=False)
	    self.l_AssetUIGroups.append(uiRow_RandomButtons)	
	    gui.add_Button(uiRow_RandomButtons,"Face",
			   )
	    gui.add_Button(uiRow_RandomButtons,"Body")
	    uiRow_RandomButtons.layout()
	    """
            #=============================
            #>>> Frame layouts
            #=============================
            mc.setParent(uiWrapper_Customize)
            #>>> General Options

            #mUI.MelSeparator(uiWrapper_Customize,h=5,ut = 'cgmUISubTemplate')

            # Button Row
            uiRow_PresetButtons = mUI.MelHLayout(uiContainer)
            gui.add_Button(uiRow_PresetButtons,"Load",
                           lambda *a:self.presets_PoseLoad(),
                           "Load a preset")
            gui.add_Button(uiRow_PresetButtons,"Save",
                           lambda *a:self.presets_PoseSave(),
                           "Save a new preset")
            uiRow_PresetButtons.layout()	
            mUI.MelSeparator(uiWrapper_Customize,h=5,ut = 'cgmUISubTemplate')

            """#>>> Blendshapes
            #=============================
            self.uiFrame_Blendshapes = mUI.MelFrameLayout(uiWrapper_Customize,label = 'Blendshapes',vis=True,
                                                          collapse=True,
                                                          collapsable=True,
                                                          enable=False,
                                                          )		
            uiContainer = mUI.MelColumnLayout(self.uiFrame_Blendshapes,
                                              ut = 'cgmUILockedTemplate',
                                              )
            #>>> Help...
            uiHelp_blendshape = mUI.MelLabel(uiContainer,
                                             bgc = dictionary.returnStateColor('help'),
                                             align = 'center',
                                             label = "Controls for accessing asset's blendshapes",
                                             h=20,
                                             vis = self.var_ShowHelp.value)	
            self.l_helpElements.append(uiHelp_blendshape)

            #>>> Blendshape buttons	
            uiRow_BlendshapeButtons = mUI.MelHLayout(uiContainer,en=False)
            self.l_AssetUIGroups.append(uiRow_BlendshapeButtons)	
            gui.add_Button(uiRow_BlendshapeButtons,"New body shape",en=False)
            gui.add_Button(uiRow_BlendshapeButtons,"New face shape",en=False)

            uiRow_BlendshapeButtons.layout()
            self.l_AssetUIGroups.append(uiRow_BlendshapeButtons)

            #>>> Body Blends
            self.uiFrame_BodyBlends = mUI.MelFrameLayout(uiContainer,label = 'Body',vis=True,
                                                         collapse=True,
                                                         collapsable=True,
                                                         enable=False,
                                                         ut = 'cgmUITemplate'
                                                         )	
            #>>> Facial Blends
            self.uiFrame_FaceBlends = mUI.MelFrameLayout(uiContainer,label = 'Face',vis=True,
                                                         collapse=True,
                                                         collapsable=True,
                                                         enable=False,
                                                         ut = 'cgmUITemplate'
                                                         )
            mUI.MelSeparator(uiWrapper_Customize,h=5,ut = 'cgmUISubTemplate')"""

            #>>>Color
            self.uiFrame_Color = mUI.MelFrameLayout(uiWrapper_Customize,label = 'Color',vis=True,
                                                    collapse=True,
                                                    collapsable=True,
                                                    enable=False,
                                                    )	
            mUI.MelSeparator(uiWrapper_Customize,h=5,ut = 'cgmUISubTemplate')

            #>>>Clothes
            self.uiFrame_Clothes = mUI.MelFrameLayout(uiWrapper_Customize,label = 'Clothes',vis=True,
                                                      collapse=True,
                                                      collapsable=True,
                                                      enable=False,
                                                      )			
            mUI.MelSeparator(uiWrapper_Customize,h=5,ut = 'cgmUISubTemplate')

            self.l_AssetUIGroups.extend([self.uiFrame_Color,self.uiFrame_Clothes ])#...self.uiFrame_Blendshapes,self.uiFrame_BodyBlends,self.uiFrame_FaceBlends

            #>>> Form Layout close out
            uiBottomAnchor = mUI.MelRow(ui_customizeMainForm,ut = 'cgmUISubTemplate')

            ui_customizeMainForm(edit = True,
                                 af = [(self.uiReport_CustomizeHelp,"top",0),	
                                       (uiScroll_customize,"left",0),
                                       (uiScroll_customize,"right",0),	
                                       (self.uiReport_CustomizeHelp,"left",0),
                                       (self.uiReport_CustomizeHelp,"right",0),	                           
                                       (self.uiReport_Customize,"left",0),
                                       (self.uiReport_Customize,"right",0),
                                       #(uiRow_MirrorButtons,"left",0),
                                       #(uiRow_MirrorButtons,"right",0),
                                       (uiBottomAnchor,"left",0),
                                       (uiBottomAnchor,"right",0),	                           
                                       (uiBottomAnchor,"bottom",0),],
                                 ac = [(self.uiReport_Customize,"top",0,self.uiReport_CustomizeHelp),
                                       #(uiRow_MirrorButtons,"top",0,self.uiReport_Customize),
                                       (uiScroll_customize,"top",2,self.uiReport_Customize),
                                       (uiScroll_customize,"bottom",0,uiBottomAnchor)],
                                 attachNone = [(uiBottomAnchor,"top")])		

            """
	    MainForm(edit = True,
		     af = [(TopSection,"top",0),	
			   (TopSection,"left",0),
			   (TopSection,"right",0),		               
			   (AllModulesRow,"left",0),
			   (AllModulesRow,"right",0),
			   (ModuleListScroll,"left",0),
			   (ModuleListScroll,"right",0),
			   (ModuleListScroll,"left",0),
			   (ModuleListScroll,"right",0),				       
			   (self.helpInfo,"left",8),
			   (self.helpInfo,"right",8),
			   (VerifyRow,"left",4),
			   (VerifyRow,"right",4),		               
			   (VerifyRow,"bottom",4)],
		     ac = [(AllModulesRow,"top",2,TopSection),
			   (ModuleListScroll,"top",2,AllModulesRow),
			   (ModuleListScroll,"bottom",0,self.helpInfo),
			   (self.helpInfo,"bottom",0,VerifyRow)],
		     attachNone = [(VerifyRow,"top")])		
	    """
            return ui_customizeMainForm
        except Exception,error:
            raise Exception,"{0} buildTab_Asset failed | {1}".format(self._str_reportStart,error)

    #@cgmGeneral.Timer
    def buildTab_Puppet(self,parent):

        try:
            t_timeStart = time.clock()  	                
            uiContainer_Puppet = mUI.MelColumnLayout(parent)
            self.l_TemplateButtonUIGroups = []
            self.l_SkeletonButtonUIGroups = []
            self.l_PuppetUIGroups = []
            #>>> Puppet Check
            uiHeader_PuppetCheck= gui.add_Header('Check Puppet')	    
            self.uiReport_PuppetHelp = mUI.MelLabel(uiContainer_Puppet,
                                                    bgc = dictionary.returnStateColor('help'),
                                                    align = 'center',
                                                    label = "Push a Puppet through rig states, update an existing rig",
                                                    vis = self.var_ShowHelp.value,
                                                    h=20)	        
            self.l_helpElements.append(self.uiReport_PuppetHelp)	
            try:#>>> Puppet state change container ===========================================
                self.uiReport_PuppetReport = mUI.MelLabel(uiContainer_Puppet,
                                                          bgc = dictionary.returnStateColor('help'),
                                                          align = 'center',
                                                          label = '...',
                                                          h=20)	



                mUI.MelSeparator(uiContainer_Puppet,ut = 'cgmUISubTemplate',h=5)        

                #Mode Change row 
                self.uiRow_pStateSet = mUI.MelHSingleStretchLayout(uiContainer_Puppet,ut='cgmUISubTemplate',
                                                                   padding = 3,h = 25)
                mUI.MelSpacer(self.uiRow_pStateSet,w=5)
                mUI.MelLabel(self.uiRow_pStateSet, label = 'State: ',align='right')
                self.uiRow_pStateSet.setStretchWidget( mUI.MelSeparator(self.uiRow_pStateSet) )		
                self.uiRadioCollection_states = mUI.MelRadioCollection()
                self.uiOptions_pStates = []		

                for i,item in enumerate(_l_moduleStatesShort):
                    self.uiOptions_pStates.append(self.uiRadioCollection_states.createButton(self.uiRow_pStateSet,label=item))

                gui.add_Button(self.uiRow_pStateSet,'Do',
                               lambda *a: self._UTILS.puppet_changeState(self))
                mUI.MelSpacer(self.uiRow_pStateSet,w=5)
                self.uiRow_pStateSet.layout()
                log.info(">"*10  + ' uiReport_PuppetReport build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		
            except Exception,error:
                raise Exception,"build uiReport_PuppetReport | {0}".format(error)

            try:#>>> Settings Section ================================================================================
                t_timeStart = time.clock()  	                

                mc.setParent(uiContainer_Puppet)
                gui.add_Header('Settings')		            
                self.uiFrame_GeneralOptions = mUI.MelFrameLayout(uiContainer_Puppet,label = 'General Options',vis=True,
                                                                 collapse=False,
                                                                 collapsable=True,
                                                                 enable=True
                                                                 )	

                self.l_PuppetUIGroups.append(self.uiFrame_GeneralOptions)
                #>>> Change name Row
                self.uiRow_ChangeName = mUI.MelHSingleStretchLayout(self.uiFrame_GeneralOptions,expand = True,ut = 'cgmUISubTemplate')
                mUI.MelSpacer(self.uiRow_ChangeName,w=5)
                mUI.MelLabel(self.uiRow_ChangeName,l='Puppet name',align='right')
                self.uiTextField_PuppetName = mUI.MelTextField(self.uiRow_ChangeName,backgroundColor = [1,1,1],h=20,
                                                               ut = 'cgmUITemplate',
                                                               ec = lambda *a:self._UTILS.puppet_doChangeName(self),
                                                               annotation = "Change puppet's name here")
                mUI.MelSpacer(self.uiRow_ChangeName,w = 5)
                self.uiRow_ChangeName.setStretchWidget(self.uiTextField_PuppetName)
                self.uiRow_ChangeName.layout()	

                log.info(">"*10  + ' settings build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		

            except Exception,error:
                raise Exception,"build settings | {0}".format(error)


            try:#>>> Template Section ================================================================================
                t_timeStart = time.clock()  	                

                mc.setParent(uiContainer_Puppet)        
                gui.add_LineBreak()                
                uiHeader_Template = gui.add_Header('Template')
                try:#Mirror Row ---------------------------------------------------------------------
                    self.l_helpElements.extend(gui.add_InstructionBlock(uiContainer_Puppet,"Template specific tools. Must be templated to use.",vis = self.var_ShowHelp.value))        	        	

                    uiRow_templateMirrorButtons = mUI.MelHLayout(uiContainer_Puppet,en=True)
                    self.l_TemplateButtonUIGroups.append(uiRow_templateMirrorButtons)
                    gui.add_Button(uiRow_templateMirrorButtons,"<<< Push",
                                   lambda *a:self.mi_ActivePuppet.mirror_do(mode = 'template',mirrorMode = 'symLeft'),		                   
                                   annotationText="Restore symmetry by pushing right to left")
                    gui.add_Button(uiRow_templateMirrorButtons,"Verify Mirror Setup",
                                   lambda *a:self.mi_ActivePuppet.mirrorSetup_verify(mode = 'template'),		                   
                                   annotationText="Verify template mirror setup on active puppet")		
                    gui.add_Button(uiRow_templateMirrorButtons,"Push >>>",#lambda *a:self.do_pushToOtherSide('right')
                                   lambda *a:self.mi_ActivePuppet.mirror_do(mode = 'template',mirrorMode = 'symRight'),		                   		                   
                                   annotationText="Restore symmetry by pushing left to right")	               
                    uiRow_templateMirrorButtons.layout()	    
                    self.uiRow_templateMirrorButtons = uiRow_templateMirrorButtons
                    mc.setParent(uiContainer_Puppet)
                    gui.add_LineSubBreak()     

                    log.info(">"*10  + ' template build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		

                except Exception,error:raise Exception,"Mirror row fail | {0}".format(error)

                try:#Pose Row ---------------------------------------------------------------------
                    t_timeStart = time.clock()  	                

                    self.uiRow_templateUtilities = mUI.MelHSingleStretchLayout(uiContainer_Puppet, ut='cgmUISubTemplate',padding = 3,h = 25)
                    self.l_TemplateButtonUIGroups.append(self.uiRow_templateUtilities)

                    mUI.MelSpacer(self.uiRow_templateUtilities,w=5)
                    mUI.MelLabel(self.uiRow_templateUtilities, label = 'Template Pose: ',align='right')
                    self.uiRow_templateUtilities.setStretchWidget( mUI.MelSeparator(self.uiRow_templateUtilities) )		
                    gui.add_Button(self.uiRow_templateUtilities,"Save",
                                   lambda *a:self.mi_ActivePuppet.templateSettings_call('store'),		                   
                                   annotationText="Save template pose to the puppet")
                    gui.add_Button(self.uiRow_templateUtilities,"Load",
                                   lambda *a:self.mi_ActivePuppet.templateSettings_call('load'),		                   
                                   annotationText="Load saved template pose from the puppet")
                    gui.add_Button(self.uiRow_templateUtilities,"Reset",
                                   lambda *a:self.mi_ActivePuppet.templateSettings_call('reset'),		                   
                                   #mUI.Callback(self.mi_ActiveAsset.templateSettingsCall,'reset'),		                   
                                   annotationText="Reset the template controls")		    
                    gui.add_Button(self.uiRow_templateUtilities,"Update",
                                   lambda *a:self.mi_ActiveAsset.updateTemplate(),		                   
                                   #mUI.Callback(self.mi_ActiveAsset.updateTemplate),
                                   annotationText="Update the template pose -- for example if you've modified the customization asset")		
                    mUI.MelSpacer(self.uiRow_templateUtilities,w=5)
                    self.uiRow_templateUtilities.layout()		
                    mc.setParent(uiContainer_Puppet)

                    log.info(">"*10  + ' pose build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		

                except Exception,error:raise Exception,"utilities row fail | {0}".format(error)
                gui.add_LineBreak()
            except Exception,error:raise Exception,"Template section fail | {0}".format(error)


            try:#>>> Skeleton Section ================================================================================
                t_timeStart = time.clock()  	                

                uiHeader_Skeleton = gui.add_Header('Skeleton')
                try:#Mirror Row ---------------------------------------------------------------------
                    self.l_helpElements.extend(gui.add_InstructionBlock(uiContainer_Puppet,"Skeleton specific tools.",vis = self.var_ShowHelp.value))
                    uiRow_skeletonUtilsButtons = mUI.MelHLayout(uiContainer_Puppet,en=True)
                    #self.l_TemplateButtonUIGroups.append(uiRow_skeletonUtilsButtons)
                    self.l_SkeletonButtonUIGroups.append(uiRow_skeletonUtilsButtons)
                    #self.l_PuppetUIGroups.append(uiRow_skeletonUtilsButtons)
                    #self.uiRow_skeletonUtilsButtons =  uiRow_skeletonUtilsButtons#link so we can turn it off and on
                    gui.add_Button(uiRow_skeletonUtilsButtons,"Axis On",
                                   lambda *a:self._UTILS.puppet_joint_setAxis(self,True),		                   		                   
                                   annotationText="Turn on the Joint Axis")
                    gui.add_Button(uiRow_skeletonUtilsButtons,"Axis Off",
                                   lambda *a:self._UTILS.puppet_joint_setAxis(self,False),		                   		                   		                   
                                   annotationText="Turn Off the Joint Axis")		    
                    gui.add_Button(uiRow_skeletonUtilsButtons,"Select",
                                   lambda *a:self._UTILS.puppet_joint_select(self),		                   		                   		                   		                   
                                   annotationText="Select Module Joints")		
                    gui.add_Button(uiRow_skeletonUtilsButtons,"Report",#lambda *a:self.do_pushToOtherSide('right')
                                   lambda *a:self.mi_ActivePuppet._UTILS.get_report(self.mi_ActivePuppet,rigReport = 1),		                   		                   		                   		                   		                   
                                   annotationText="Get a joint report on the puppet")	               
                    uiRow_skeletonUtilsButtons.layout()	    
                    self.uiRow_skeletonUtilsButtons = uiRow_skeletonUtilsButtons
                    mc.setParent(uiContainer_Puppet)
                except Exception,error:raise Exception,"Mirror row fail | {0}".format(error)
                log.info(">"*10  + ' skeleton build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		

            except Exception,error:raise Exception,"Skeleton section fail | {0}".format(error)
            gui.add_LineBreak()

            try:#>>> Rig Section ================================================================================
                t_timeStart = time.clock()  	                

                uiHeader_Skeleton = gui.add_Header('Rig Options')
                self.l_helpElements.extend(gui.add_InstructionBlock(uiContainer_Puppet,"Options for how the rig is built",vis = self.var_ShowHelp.value))		
                try:#Face Options ---------------------------------------------------------------------
                    #Mode Change row 
                    self.uiRow_pRigFaceSet = mUI.MelHSingleStretchLayout(uiContainer_Puppet,ut='cgmUISubTemplate',
                                                                         padding = 3,h = 25,en = False)
                    mUI.MelSpacer(self.uiRow_pRigFaceSet,w=5)
                    mUI.MelLabel(self.uiRow_pRigFaceSet, label = 'Rig Face?: ',align='right')
                    self.uiRow_pRigFaceSet.setStretchWidget( mUI.MelSeparator(self.uiRow_pRigFaceSet) )		
                    self.uiRadioCollection_faceBuildOptions = mUI.MelRadioCollection()
                    self.uiOptions_pRigFaces = []		
                    for i,item in enumerate(_l_boolStrings):
                        self.uiOptions_pRigFaces.append(self.uiRadioCollection_faceBuildOptions.createButton(self.uiRow_pRigFaceSet,
                                                                                                             label=item,
                                                                                                             onCommand = mUI.Callback(self.var_FaceBuildOption.setValue,i)))
                    mUI.MelSpacer(self.uiRow_pRigFaceSet,w=5)
                    self.uiRow_pRigFaceSet.layout()

                    try:#Change to current state
                        mc.radioCollection(self.uiRadioCollection_faceBuildOptions,edit=True, sl=self.uiOptions_pRigFaces[self.var_FaceBuildOption.value])
                    except Exception,err:
                        log.error("Face build options toggle set fail| {0}".format(err))		
                    mc.setParent(uiContainer_Puppet)	

                    log.info(">"*10  + ' face build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		

                except Exception,error:raise Exception,"Face Build row fail | {0}".format(error)
                try:#Blendshape Options ---------------------------------------------------------------------
                    t_timeStart = time.clock()  	                

                    #Mode Change row 
                    self.uiRow_pFaceRigTypeSet = mUI.MelHSingleStretchLayout(uiContainer_Puppet,ut='cgmUISubTemplate',
                                                                             padding = 3,h = 25)
                    mUI.MelSpacer(self.uiRow_pFaceRigTypeSet,w=5)
                    mUI.MelLabel(self.uiRow_pFaceRigTypeSet, label = 'Face Setup: ',align='right')
                    self.uiRow_pFaceRigTypeSet.setStretchWidget( mUI.MelSeparator(self.uiRow_pFaceRigTypeSet) )		
                    self.uiRadioCollection_faceBuildOptions = mUI.MelRadioCollection()
                    self.uiOptions_pFaceRigTypes = []		
                    for i,item in enumerate(_l_facialSetupType):
                        self.uiOptions_pFaceRigTypes.append(self.uiRadioCollection_faceBuildOptions.createButton(self.uiRow_pFaceRigTypeSet,
                                                                                                                 label=item,
                                                                                                                 onCommand = mUI.Callback(self.var_FaceSetupTypeOption.setValue,i)))
                    mUI.MelSpacer(self.uiRow_pFaceRigTypeSet,w=5)
                    self.uiRow_pFaceRigTypeSet.layout()
                    self.uiRow_pFaceRigTypeSet(edit = True, enable = False)
                    try:#Change to current state
                        mc.radioCollection(self.uiRadioCollection_faceBuildOptions,edit=True, sl=self.uiOptions_pFaceRigTypes[self.var_FaceSetupTypeOption.value])
                    except Exception,err:
                        log.error("Blendshapes build options toggle set fail| {0}".format(err))		
                    mc.setParent(uiContainer_Puppet)	

                    log.info(">"*10  + ' blendshape build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		

                except Exception,error:raise Exception,"Blenshapes Build row fail | {0}".format(error)			

                try:#Blendshape Options ---------------------------------------------------------------------
                    t_timeStart = time.clock()  	                

                    #Mode Change row 
                    self.uiRow_pBSBuildOptionsSet = mUI.MelHSingleStretchLayout(uiContainer_Puppet,ut='cgmUISubTemplate',
                                                                                padding = 3,h = 25,en = False)
                    mUI.MelSpacer(self.uiRow_pBSBuildOptionsSet,w=5)
                    mUI.MelLabel(self.uiRow_pBSBuildOptionsSet, label = 'Blendshapes: ',align='right')
                    self.uiRow_pBSBuildOptionsSet.setStretchWidget( mUI.MelSeparator(self.uiRow_pBSBuildOptionsSet) )		
                    self.uiRadioCollection_faceBuildOptions = mUI.MelRadioCollection()
                    self.uiOptions_pBSBuildOptionss = []		
                    for i,item in enumerate(_l_blendshapeBuildOptions):
                        self.uiOptions_pBSBuildOptionss.append(self.uiRadioCollection_faceBuildOptions.createButton(self.uiRow_pBSBuildOptionsSet,
                                                                                                                    label=item,
                                                                                                                    onCommand = mUI.Callback(self.var_BlendShapesOption.setValue,i)))
                    mUI.MelSpacer(self.uiRow_pBSBuildOptionsSet,w=5)
                    self.uiRow_pBSBuildOptionsSet.layout()

                    try:#Change to current state
                        mc.radioCollection(self.uiRadioCollection_faceBuildOptions,edit=True, sl=self.uiOptions_pBSBuildOptionss[self.var_BlendShapesOption.value])
                    except Exception,err:
                        log.error("Blendshapes build options toggle set fail| {0}".format(err))		
                    mc.setParent(uiContainer_Puppet)		    				
                except Exception,error:raise Exception,"Blenshapes Build row fail | {0}".format(error)		
                gui.add_LineBreak()
                log.info(">"*10  + ' blendshape options build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		

            except Exception,error:raise Exception,"Rig section fail | {0}".format(error)	    

            try:#>>> Generate ================================================================================
                t_timeStart = time.clock()  	                

                uiHeader_Generate = gui.add_Header('Generate')
                self.l_helpElements.extend(gui.add_InstructionBlock(uiContainer_Puppet,
                                                                    "Export a puppet state to a clean file.",
                                                                    vis = self.var_ShowHelp.value))        	        	
                for o in 'template','cleanMesh','skeleton','skinnedMesh','rig':
                    gui.add_Button(uiContainer_Puppet,o,en=False)
                gui.add_LineBreak()        
                gui.add_Button(uiContainer_Puppet,"Export Current State",
                               lambda *a:self._UTILS.puppet_export(self),		                   		                   		                   
                               annotationText="Export the current puppet state to a clean file")
            except Exception,error:raise Exception,"Generate section fail | {0}".format(error)	    

            log.info(">"*10  + ' generate build =  %0.3f seconds  ' % (time.clock()-t_timeStart) + '<'*10) 		

            return uiContainer_Puppet
        except Exception,error:
            raise Exception,"{0} buildTab_Puppet failed | {1}".format(self._str_reportStart,error)

    def buildTab_Dev(self,parent):
        try:
            uiContainer_Dev = mUI.MelColumnLayout(parent)
            self.l_helpElements.extend(gui.add_InstructionBlock(uiContainer_Dev,"Please stay out of this if you want things to work:)",vis = self.var_ShowHelp.value))        	        	

            #>>> Puppet Check
            uiHeader_PuppetCheck= gui.add_Header('Tagging')

            gui.add_Button(uiContainer_Dev,'Tag curve from joint, select joint/curve', lambda *a: CSTMF.doTagCurveFromJoint())

            gui.add_LineBreak()
            uiHeader_PuppetCheck= gui.add_Header('Add Contrain to...')
            gui.add_Button(uiContainer_Dev,'Point,Orient,Scale', lambda *a: CSTMF.doTagContrainToTargets(constraintTypes = ['point','orient','scale']))
            gui.add_Button(uiContainer_Dev,'Parent', lambda *a: CSTMF.doTagContrainToTargets(constraintTypes = ['parent']))
            gui.add_Button(uiContainer_Dev,'Point', lambda *a: CSTMF.doTagContrainToTargets(constraintTypes = ['point']))
            gui.add_Button(uiContainer_Dev,'Orient', lambda *a: CSTMF.doTagContrainToTargets(constraintTypes = ['orient']))	
            gui.add_Button(uiContainer_Dev,'Aim', lambda *a: CSTMF.doTagContrainToTargets(constraintTypes = ['aim']))
            gui.add_Button(uiContainer_Dev,'Scale', lambda *a: CSTMF.doTagContrainToTargets(constraintTypes = ['scale']))
            gui.add_LineBreak()
            gui.add_Button(uiContainer_Dev,'Report Contrain To Tags', lambda *a: CSTMF.reportContrainToTags())
            gui.add_LineBreak()

            #>>> Generate            
            uiHeader_Generate = gui.add_Header('cgmGuiDefaults')        
            gui.add_Button(uiContainer_Dev)
            gui.add_Button(uiContainer_Dev,'Debug test', lambda *a: self.do_DebugEchoTest())

            return uiContainer_Dev
        except Exception,error:
            raise Exception,"{0} buildTab_Puppet failed | {1}".format(self._str_reportStart,error)

    #=============================
    #>>> Build Frames
    #=============================
    def buildFrame_BodyBlends(self):
        try:
            self.uiFrame_BodyBlends.clear()
            self.l_bodyBlendshapeTargets = []
            self.l_bodyBlendshapeSliders = []
            self.l_bodyBlendshapeFloatFields = []

            if self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('bodyBlendshapeNodes'):
                self.l_bodyBlendshapeTargets = deformers.returnBlendShapeAttributes( self.mi_ActiveAsset.getMessage('bodyBlendshapeNodes')[0])
                self.uiFrame_BodyBlends(edit = True, label = "Body | targets: %i"%len(self.l_bodyBlendshapeTargets))				    
                bsNode = self.mi_ActiveAsset.getMessage('bodyBlendshapeNodes')[0]	    
                for i,t in enumerate(self.l_bodyBlendshapeTargets):
                    uiRow_BlendshapeAttr = mUI.MelHSingleStretchLayout(self.uiFrame_BodyBlends ,ut='cgmUISubTemplate',expand = True, padding = 5)
                    mUI.MelSpacer(uiRow_BlendshapeAttr,w=5)
                    label = mUI.MelLabel(uiRow_BlendshapeAttr,label = ('%s:'%t.rjust(3,' ')))
                    #Float field
                    uiFloatField = mUI.MelFloatField(uiRow_BlendshapeAttr,w=30,v=(attributes.doGetAttr(bsNode,t)),minValue=-10,maxValue=10,precision = 2,#Float field
                                                     changeCommand = mUI.Callback(self.blendshape_updateValueFromField,bsNode,i,self.l_bodyBlendshapeTargets,self.l_bodyBlendshapeSliders,self.l_bodyBlendshapeFloatFields))	
                    #Slider
                    uiSlider_Test = mUI.MelFloatSlider(uiRow_BlendshapeAttr,-2,2,0,v=(attributes.doGetAttr(bsNode,t)),
                                                       dragCommand = mUI.Callback(self.blendshape_updateValue,bsNode,i,self.l_bodyBlendshapeTargets,self.l_bodyBlendshapeSliders,self.l_bodyBlendshapeFloatFields),		                            		                               
                                                       changeCommand = mUI.Callback(self.blendshape_updateValue,bsNode,i,self.l_bodyBlendshapeTargets,self.l_bodyBlendshapeSliders,self.l_bodyBlendshapeFloatFields))		                            
                    uiRow_BlendshapeAttr.setStretchWidget(uiSlider_Test)#Set stretch

                    gui.add_Button(uiRow_BlendshapeAttr,'R',
                                   mUI.Callback(self.blendshape_resetValue,bsNode,i,self.l_bodyBlendshapeTargets,self.l_bodyBlendshapeSliders,self.l_bodyBlendshapeFloatFields),
                                   'Reset this slider')
                    mUI.MelSpacer(uiRow_BlendshapeAttr,w=5)		
                    uiRow_BlendshapeAttr.layout()

                    #uiSlider_Test.setPostChangeCB( self.blendshape_updateBodyValue(i))		
                    self.l_bodyBlendshapeSliders.append(uiSlider_Test)#append our slider for later query		
                    self.l_bodyBlendshapeFloatFields.append(uiFloatField)#append our slider for later query		
                    self.uiFrame_BodyBlends(e=True,collapse=False)
            else:
                self.uiFrame_BodyBlends(edit = True, label = "Body | targets: 0")				    	    
        except Exception,error:
            raise Exception,"{0} buildFrame_BodyBlends failed | {1}".format(self._str_reportStart,error)

    def buildFrame_FaceBlends(self):
        try:
            self.uiFrame_FaceBlends.clear()
            self.l_faceBlendshapeTargets = []
            self.l_faceBlendshapeSliders = []
            self.l_faceBlendshapeFloatFields = []

            if self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('faceBlendshapeNodes'):
                self.l_faceBlendshapeTargets = deformers.returnBlendShapeAttributes( self.mi_ActiveAsset.getMessage('faceBlendshapeNodes')[0])
                self.uiFrame_FaceBlends(edit = True, label = "Face | targets: %i"%len(self.l_faceBlendshapeTargets))				    
                bsNode = self.mi_ActiveAsset.getMessage('faceBlendshapeNodes')[0]	    
                for i,t in enumerate(self.l_faceBlendshapeTargets):
                    uiRow_BlendshapeAttr = mUI.MelHSingleStretchLayout(self.uiFrame_FaceBlends ,ut='cgmUISubTemplate',expand = True, padding = 5)
                    mUI.MelSpacer(uiRow_BlendshapeAttr,w=5)
                    mUI.MelLabel(uiRow_BlendshapeAttr,label = ('%s:'%t.rjust(3,' ')))
                    #Float field
                    uiFloatField = mUI.MelFloatField(uiRow_BlendshapeAttr,w=30,v=(attributes.doGetAttr(bsNode,t)),minValue=-10,maxValue=10,precision = 2,#Float field
                                                     changeCommand = mUI.Callback(self.blendshape_updateValueFromField,bsNode,i,self.l_faceBlendshapeTargets,self.l_faceBlendshapeSliders,self.l_faceBlendshapeFloatFields))	
                    #Slider
                    uiSlider_Test = mUI.MelFloatSlider(uiRow_BlendshapeAttr,-2,2,0,v=(attributes.doGetAttr(bsNode,t)),
                                                       dragCommand = mUI.Callback(self.blendshape_updateValue,bsNode,i,self.l_faceBlendshapeTargets,self.l_faceBlendshapeSliders,self.l_faceBlendshapeFloatFields),		                            		                               
                                                       changeCommand = mUI.Callback(self.blendshape_updateValue,bsNode,i,self.l_faceBlendshapeTargets,self.l_faceBlendshapeSliders,self.l_faceBlendshapeFloatFields))		                            
                    uiRow_BlendshapeAttr.setStretchWidget(uiSlider_Test)#Set stretch	
                    gui.add_Button(uiRow_BlendshapeAttr,'R',
                                   mUI.Callback(self.blendshape_resetValue,bsNode,i,self.l_faceBlendshapeTargets,self.l_faceBlendshapeSliders,self.l_faceBlendshapeFloatFields),
                                   'Reset this slider')
                    mUI.MelSpacer(uiRow_BlendshapeAttr,w=5)		
                    uiRow_BlendshapeAttr.layout()

                    #uiSlider_Test.setPostChangeCB( self.blendshape_updateFaceValue(i))		
                    self.l_faceBlendshapeSliders.append(uiSlider_Test)#append our slider for later query		
                    self.l_faceBlendshapeFloatFields.append(uiFloatField)#append our slider for later query		
                    self.uiFrame_FaceBlends(e=True,collapse=False)

            else:
                self.uiFrame_FaceBlends(edit = True, label = "Face | targets: 0")	
        except Exception,error:
            raise Exception,"{0} buildFrame_FaceBlends failed | {1}".format(self._str_reportStart,error)

    def buildFrame_PresetOptions(self):
        try:
            #self.uiFrame_PresetOptions.clear()
            #self.uiForm_PresetOptions = mUI.MelFormLayout(self.uiFrame_PresetOptions,h=100, ut='cgmUITemplate')

            self.uiObjectList_Presets.clear()
            self.uiObjectList_Presets(edit = True, selectCommand = lambda *a:self.presets_SetActive())

            options = self.d_PresetFiles.keys()
            options.sort()#sort the options alphabetically
            for item in options:
                self.uiObjectList_Presets.append(item)
                #presets_SetActive
                popUpMenu = mUI.MelPopupMenu(self.uiObjectList_Presets,button = 3)	
                mUI.MelMenuItem(popUpMenu ,
                                label = 'Open Presets Dir',
                                c = lambda *a: self.presets_OpenDir())
                mUI.MelMenuItem(popUpMenu ,
                                label = "Delete selected",
                                c = lambda *a: self.presets_DeletePose())
        except Exception,error:
            raise Exception,"{0} buildFrame_PresetOptions failed | {1}".format(self._str_reportStart,error)

    def buildFrame_GeneralOptions(self):
        try:
            self.uiFrame_GeneralOptions.clear()

            #>>> Change name Row
            self.uiRow_ChangeName = mUI.MelHSingleStretchLayout(self.uiFrame_GeneralOptions,expand = True,ut = 'cgmUISubTemplate')
            mUI.MelSpacer(self.uiRow_ChangeName,w=5)
            mUI.MelLabel(self.uiRow_ChangeName,l='Asset name',align='right')
            self.uiTextField_PuppetName = mUI.MelTextField(self.uiRow_ChangeName,backgroundColor = [1,1,1],h=20,ut = 'cgmUITemplate',
                                                           ec = lambda *a:self.asset_doChangeName(),
                                                           annotation = "Change asset's name here")
            mUI.MelSpacer(self.uiRow_ChangeName,w = 5)
            self.uiRow_ChangeName.setStretchWidget(self.uiTextField_PuppetName)
            self.uiRow_ChangeName.layout()	

            if self.mi_ActiveAsset:# and self.mi_ActiveAsset.isCustomizable():
                container = mUI.MelColumn(self.uiFrame_GeneralOptions,
                                          ut = 'cgmUISubTemplate')
                self.uiTextField_PuppetName(e=True,text=self.mi_ActiveAsset.cgmName)
                self.uiTextField_PuppetName(edit = True,backgroundColor = [1,1,1])#Cause maya 2012 is stupid

                #>>> Vis options
                i_vis = self.mi_ActiveAsset.masterControl.controlVis or False
                if i_vis:
                    attrs = i_vis.getAttrs(channelBox=True)
                    if attrs:
                        attrSets = lists.returnListChunks(attrs,3)
                        for aSet in attrSets:
                            row = mUI.MelHRowLayout(container)#make a row
                            mUI.MelSpacer(row,w=1)			
                            for a in aSet:
                                mUI.MelCheckBox(row,
                                                v = attributes.doGetAttr(i_vis.mNode,a),
                                                onCommand = mUI.Callback(attributes.doSetAttr,i_vis.mNode,a,1),
                                                offCommand = mUI.Callback(attributes.doSetAttr,i_vis.mNode,a,0),
                                                label = a,
                                                )
                                mUI.MelSpacer(row,w=1)
                            mUI.MelSpacer(row,w=1)
                            row.layout()

            else:
                self.uiTextField_PuppetName(e=True,text='')
        except Exception,error:
            raise Exception,"{0} buildFrame_GeneralOptions failed | {1}".format(self._str_reportStart,error)

    #=========================================================================
    # Puppet tools 
    #=========================================================================
    '''
    def puppet_changeState(self):
	_str_funcName = 'puppet_changeState'	
	try:
	    if self.mi_ActivePuppet:
		mPuppet = self.mi_ActivePuppet
	    elif self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('mPuppet'):
		mPuppet = self.mi_ActiveAsset.mPuppet
	    else:
		log.error("{0} no puppet found".format(_str_funcName))
		return False

	    log.info("{0} Puppet found | '{1}'".format(_str_funcName,mPuppet.p_nameShort))
	    try:
		int_state = self.uiRadioCollection_states.getSelectedIndex()
		log.info("{0} State | '{1}'".format(_str_funcName,int_state))		
		if int_state < 2:#was 3
		    self.mi_ActiveAsset.setState( int_state,rebuildFrom = 'define', forceNew = True )
		else:
		    self.mi_ActiveAsset.setState(int_state,forceNew = True )
	    except StandardError,error:
		log.error("Failed to change state |  %s"%error)
	    log.info("{0} puppet_changeState complete".format(self._str_reportStart))
	    self.report_updatePuppet()
	except Exception,error:
	    self.reports_updateAll()	    
	    raise Exception,"{0} puppet_changeState failed | {1}".format(self._str_reportStart,error)
	'''
    def puppet_delete(self):
        _str_funcName = 'puppet_delete'	
        try:
            if self.mi_ActivePuppet:
                mPuppet = self.mi_ActivePuppet
            elif self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('mPuppet'):
                mPuppet = self.mi_ActiveAsset.mPuppet
            else:
                log.error("{0} no puppet found".format(_str_funcName))
                return False
            log.info("{0} | Puppet found : '{1}'".format(_str_funcName,mPuppet.p_nameShort))
            mPuppet.delete()
            self.report_updatePuppet()
        except Exception,error:
            raise Exception,"{0} {1} failed | {2}".format(self._str_reportStart,_str_funcName,error)

    #=========================================================================
    # Asset tools 
    #=========================================================================	    
    def asset_verifyPuppetNodes(self):
        try:
            if self.mi_ActiveAsset:
                try:
                    self.mi_ActiveAsset.verifyPuppet()
                    self.mi_ActivePuppet = self.mi_ActiveAsset.mPuppet
                    log.info("{0} '{1}' verified".format(self._str_reportStart,self.mi_ActivePuppet.cgmName))		    
                except Exception,error:
                    log.warning("Could not verify Puppet nodes: {0}".format(error))
                self.report_updatePuppet()
            else:
                log.warning("No active asset")
        except Exception,error:
            raise Exception,"{0} asset_verifyPuppetNodes failed | {1}".format(self._str_reportStart,error)

    def asset_loadBase(self,index):
        try:
            #>>> Join the path
            bufferList = copy.copy(self.ml_customizationNetworks)#make a compare list to see what's loaded
            log.debug(self.l_BaseMeshes)
            mFile = DIR_SEPARATOR.join([self._path_baseMeshDir,self.l_BaseMeshes[index]])
            mc.file(mFile, i = True, pr = True, force = True,prompt = False) 

            #Update the lists and compare so we can make it active
            self.ml_customizationNetworks = returnEligbleNetworks()
            log.debug("self.ml_customizationNetworks: %s"%self.ml_customizationNetworks)
            log.debug("bufferList: %s"%bufferList)
            new = lists.returnDifference(bufferList,self.ml_customizationNetworks)
            log.debug("new: %s"%new)
            if len(new) == 1 and new[0] in self.ml_customizationNetworks:
                index = self.ml_customizationNetworks.index(new[0])
                MMUTILS.asset_setActive(self,index)

            self.reports_updateAll()
        except Exception,error:
            raise Exception,"{0} asset_loadBase failed | {1}".format(self._str_reportStart,error)

    def asset_autoLoad(self):
        try:
            if self.ml_customizationNetworks:
                for i,i_n in enumerate(self.ml_customizationNetworks):
                    log.debug("var_ActiveAsset: %s"%self.var_ActiveAsset.value)
                    if i_n.mNode and self.var_ActiveAsset.value == i_n.getShortName():
                        self.mi_ActiveAsset = self.ml_customizationNetworks[i]#link it
                        gui.doSetInstancedUIItemsEnableState(True,self.l_AssetUIGroups)

                        if self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('mPuppet'):
                            self.mi_ActivePuppet = self.mi_ActiveAsset.mPuppet	

                        return True
            self.mi_ActiveAsset = False
            return False
        except Exception,error:
            raise Exception,"{0} asset_autoLoad failed | {1}".format(self._str_reportStart,error)
    '''    
    #@cgmGeneral.Timer
    def asset_setActive(self,index):
        try:
            self.mi_ActiveAsset = self.ml_customizationNetworks[index]#link it
            if not self.mi_ActiveAsset.isCustomizable():
                log.warning("{0} not customizable | {1}".format(self._str_reportStart,self.mi_ActiveAsset.p_nameShort))

            gui.doSetInstancedUIItemsEnableState(True,self.l_AssetUIGroups)	
            self.var_ActiveAsset.value = self.mi_ActiveAsset.getShortName()
            if self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('mPuppet'):
                self.mi_ActivePuppet = self.mi_ActiveAsset.mPuppet
                self.report_updatePuppet()
            self.reports_updateAsset()
        except Exception,error:
            log.warning("Set active fail. index : {0} | error: {1}".format(index,error))'''

    def asset_verifyActive(self):
        """ 
        Verify active Asset
        """  	
        try:
            if self.mi_ActiveAsset:
                try:
                    self.mi_ActiveAsset.__verify__()
                except Exception,error:
                    log.warning("Could not verify active asset: {0}".format(error))
                self.reports_updateAsset()
            else:
                log.warning("Could not verify active asset")
        except Exception,error:
            raise Exception,"{0} asset_verifyActive failed | {1}".format(self._str_reportStart,error)

    def asset_deleteActive(self):
        """ 
        Delete active Asset
        """  
        try:
            if MMUTILS.check_assetExists(self):
                try:
                    buffer = self.mi_ActiveAsset.cgmName
                    mc.delete( self.mi_ActiveAsset.masterNull.mNode )
                    self.mi_ActiveAsset = False
                    log.warning("Deleted: %s"%buffer)
                    gui.doSetInstancedUIItemsEnableState(False,self.l_AssetUIGroups)	
                except:
                    log.warning("Could not delete active asset")

                self.reports_updateAll()
            else:
                log.warning("Could not delete active asset")
        except Exception,error:
            raise Exception,"{0} asset_deleteActive failed | {1}".format(self._str_reportStart,error)

    #=========================================================================
    # UI Reports
    #=========================================================================	    
    def report_updateCustomize(self):
        try:
            if self.mi_ActiveAsset and self.mi_ActiveAsset.isCustomizable():
                report = ["asset: '{0}'".format(self.mi_ActiveAsset.cgmName),
                          "mesh: '{0}'".format(self.mi_ActiveAsset.meshClass),
                          "v{0}".format(str(self.mi_ActiveAsset.version))]

                self.uiReport_Customize(edit = True, label = REPORT_JOIN.join(report) )

                """#Blendshape node stuff
                buffer = self.mi_ActiveAsset.getMessage('bodyBlendshapeNodes')
                if buffer:body_bsNode = buffer[0]
                else:body_bsNode = False
                if body_bsNode:
                    self.l_bodyBlendshapeTargets = deformers.returnBlendShapeAttributes(body_bsNode)
                    self.uiFrame_Blendshapes(e=True, collapse=False)#Expand The blendshapes if we have them
                else:
                    self.uiFrame_BodyBlends(edit = True, collapse=True)"""

                gui.doSetInstancedUIItemsEnableState(True,self.l_AssetUIGroups)#Turn on Asset stuff
                #self.uiTextField_PuppetName(edit = True,backgroundColor = [1,1,1])#Cause maya 2012 is stupid

            else:
                self.uiReport_Customize(edit = True, label = "..." )
                #self.uiTextField_PuppetName(e=True,text='')		
                gui.doSetInstancedUIItemsEnableState(False,self.l_AssetUIGroups)#Turn off Asset stuff    
        except Exception,error:
            raise Exception,"{0} report_updateCustomize failed | {1}".format(self._str_reportStart,error)


    def report_updatePresets(self):
        try:
            if self.mi_ActivePreset:
                report = ["mesh: %s"%self.mi_ActivePreset.d_assetInfo.get('meshClass'),
                          "v%s"%str(self.mi_ActivePreset.d_assetInfo.get('version')),
                           "author: {0}".format(self.mi_ActivePreset.infoDict.get('author'))]

                self.uiReport_Preset(edit = True, label = REPORT_JOIN.join(report) )

            else:
                self.uiReport_Preset(edit = True, label = "..." )
        except Exception,error:
            raise Exception,"{0} report_UpdatePrest failed | {1}".format(self._str_reportStart,error)


    def report_updateCustomizeHelp(self):
        try:
            log.debug("Networks: %s"%self.ml_customizationNetworks)	
            if not self.l_BaseMeshes:
                self.uiReport_CustomizeHelp(edit = True, label = "Not seeing base mesh files, check your install")
            elif not self.ml_customizationNetworks:
                self.uiReport_CustomizeHelp(edit = True, label = "Add a Morpheus asset")
            elif not self.mi_ActiveAsset:
                self.uiReport_CustomizeHelp(edit = True, label = "Pick an active asset")
            elif self.mi_ActiveAsset:
                if self.mi_ActiveAsset.isCustomizable():
                    self.uiReport_CustomizeHelp(edit = True, label = "Have fun customizing!")
                else:
                    self.uiReport_CustomizeHelp(edit = True, label = "This asset doesn't check out")		
            else:
                self.uiReport_CustomizeHelp(edit = True, label = "What's going on here...")
        except Exception,error:
            raise Exception,"{0} report_updateCustomizeHelp failed | {1}".format(self._str_reportStart,error)

    #>>> Puppet Reports
    #=======================================================
    def report_updatePuppetHelp(self):
        if not self.mi_ActiveAsset:
            self.uiReport_PuppetHelp(edit = True, label = "No network loaded")
        elif not self.mi_ActiveAsset.getMessage('mPuppet'):
            self.uiReport_PuppetHelp(edit = True, label = "No Puppet connected to Customization Network")			    
        elif not self.mi_ActiveAsset.mPuppet.getMessage('moduleChildren'):
            self.uiReport_PuppetHelp(edit = True, label = "Define Morpheus Puppet")	  	    
        else:
            self.uiReport_PuppetHelp(edit = True, label = "...")

    #@cgmGeneral.Timer
    def report_updatePuppet(self):
        if self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('mPuppet'):
            try:
                mPuppet = self.mi_ActiveAsset.mPuppet
                if self._int_stateBuffer is None:
                    state = mPuppet.getState()
                    self._int_stateBuffer = state
                else:
                    state = self._int_stateBuffer

                l_moduleChildren = mPuppet.getMessage('moduleChildren')
                if l_moduleChildren: childCount = len(l_moduleChildren)
                else:childCount = 0
                report = ["Name: %s"%mPuppet.cgmName,
                          "Modules: %s"%str(childCount),
                          "State: %s"% _l_moduleStates[state],
                          ]
                self.uiReport_PuppetReport(edit = True, label = REPORT_JOIN.join(report) )
                #self.button_UpdateTemplate(edit = True, enable = True)
                
                for uiGroups in [self.l_TemplateButtonUIGroups,self.l_SkeletonButtonUIGroups]:
                    gui.doSetInstancedUIItemsEnableState(False,uiGroups)#Turn off Asset stuff    		    

                if state >= 2:
                    gui.doSetInstancedUIItemsEnableState(True,self.l_TemplateButtonUIGroups)#Turn On template stuff
                if state >= 3:
                    gui.doSetInstancedUIItemsEnableState(True,self.l_SkeletonButtonUIGroups)#Turn On template stuff

                try:#Change to current state
                    mc.radioCollection(self.uiRadioCollection_states,edit=True, sl=self.uiOptions_pStates[state])
                except Exception,err:
                    log.error("puppet state toggle set fail| {0}".format(err))

                self.uiRow_pStateSet(edit = True, enable = True)#...enable the puppet state row
                gui.doSetInstancedUIItemsEnableState(True,self.l_PuppetUIGroups)#Turn On template stuff

                self.uiTextField_PuppetName(e=True,text= mPuppet.cgmName)
                self.uiTextField_PuppetName(edit = True,backgroundColor = [1,1,1])#Cause maya 2012 is stupid                

                return True
            except Exception,err:
                log.error("{0} puppet failed to update | {1}".format(self._str_reportStart,err))
        '''
                    if self.mi_ActiveAsset:# and self.mi_ActiveAsset.isCustomizable():
                container = mUI.MelColumn(self.uiFrame_GeneralOptions,
                                          ut = 'cgmUISubTemplate')
                self.uiTextField_PuppetName(e=True,text=self.mi_ActiveAsset.cgmName)
                self.uiTextField_PuppetName(edit = True,backgroundColor = [1,1,1])#Cause maya 2012 is stupid

                #>>> Vis options
                i_vis = self.mi_ActiveAsset.masterControl.controlVis or False
                if i_vis:
                    attrs = i_vis.getAttrs(channelBox=True)
                    if attrs:
                        attrSets = lists.returnListChunks(attrs,3)
                        for aSet in attrSets:
                            row = mUI.MelHRowLayout(container)#make a row
                            mUI.MelSpacer(row,w=1)			
                            for a in aSet:
                                mUI.MelCheckBox(row,
                                                v = attributes.doGetAttr(i_vis.mNode,a),
                                                onCommand = mUI.Callback(attributes.doSetAttr,i_vis.mNode,a,1),
                                                offCommand = mUI.Callback(attributes.doSetAttr,i_vis.mNode,a,0),
                                                label = a,
                                                )
                                mUI.MelSpacer(row,w=1)
                            mUI.MelSpacer(row,w=1)
                            row.layout()

            else:
                self.uiTextField_PuppetName(e=True,text='')
        '''
        gui.doSetInstancedUIItemsEnableState(False,self.l_TemplateButtonUIGroups)#Turn off Asset stuff    
        self.uiReport_PuppetReport(edit = True, label = "No puppet found" )
        mc.radioCollection(self.uiRadioCollection_states,edit=True, sl=self.uiOptions_pStates[0])	    
        self.uiRow_pStateSet(edit = True, enable = False)	
        self._int_stateBuffer = None
        gui.doSetInstancedUIItemsEnableState(False,self.l_PuppetUIGroups)#Turn On template stuff
        self.uiTextField_PuppetName(e=True,text='')#...empty the puppet name field

    #@cgmGeneral.Timer
    def reports_updateAll(self):
        try:
            self.reports_updateAsset()
            self.report_updatePuppetHelp()
            self.report_updatePuppet()
        except Exception,error:
            raise Exception,"{0} reports_updateAll failed | {1}".format(self._str_reportStart,error)
        
    #@cgmGeneral.Timer
    def reports_updateAsset(self):
        try:
            self.ml_customizationNetworks = returnEligbleNetworks()	
            #self.report_updateCustomizeHelp()
            self.report_updateCustomize()
            #self.buildFrame_BodyBlends()#Rebuild the blendshape frames
            #self.buildFrame_FaceBlends()#Rebuild the blendshape frames
        except Exception,error:
            raise Exception,"{0} reports_updateAll failed | {1}".format(self._str_reportStart,error)

    #=========================================================================
    # Customization Functions
    #=========================================================================	 
    def do_pushToOtherSide(self,direction = 'right',mode = 'asset'):
        try:
            selection = mc.ls(sl=True) or []
            if selection:
                l_controls = selection
            #Find controls
            elif direction == 'left':
                l_controls = self.mi_ActiveAsset.getMessage('controlsLeft')
            elif direction == 'right':
                l_controls = self.mi_ActiveAsset.getMessage('controlsRight')
            else:	
                log.warning("Not a recognized direction: %s"%direction)
                return False

            log.debug("Left controls: %s"%self.mi_ActiveAsset.getMessage('controlsLeft'))
            log.debug("right controls: %s"%self.mi_ActiveAsset.getMessage('controlsRight'))

            if l_controls:
                mayaMainProgressBar = gui.doStartMayaProgressBar(len(l_controls))	   
                for o in l_controls:
                    log.debug("Pushing: '%s'"%o)
                    a = ctrlFactory.go(o)
                    if mc.progressBar(mayaMainProgressBar, query=True, isCancelled=True ) :
                        break
                    mc.progressBar(mayaMainProgressBar, edit=True, status = "On: '%s'"%a.i_object.getShortName(), step=1)		
                    a.doPushToMirrorObject('')
                gui.doEndMayaProgressBar(mayaMainProgressBar)
            else:
                log.warning("No controls found")
                return False
        except Exception,error:
            raise Exception,"{0} do_pushToOtherSide failed | {1}".format(self._str_reportStart,error)

    def do_resetControls(self):
        try:
            MMUTILS.check_assetExists(self)

            if not self.mi_ActiveAsset.hasAttr('objSet_all') or not self.mi_ActiveAsset.getMessage('objSet_all'):
                log.warning("No all object set found")
                return False
            self.mi_ActiveAsset.objSet_all.reset()
            self.reports_updateAll()

        except Exception,error:
            raise Exception,"{0} do_resetControls failed | {1}".format(self._str_reportStart,error)


    #=========================================================================
    # Blendshape Functions
    #=========================================================================	
    def blendshape_updateValueFromField(self,bsNode,index,l_targets,l_sliders,l_floatFields):
        try:
            if index < len(l_targets):
                if not bsNode:
                    log.warning("No blendshape node found")
                    return False
                value = l_floatFields[index](q=True,value=True)
                #Need to get the value
                try:
                    mc.setAttr("%s.%s"%(bsNode,l_targets[index]),value)
                    l_sliders[index](e=True,value=value)
                except:log.warning("Failed to set value, check lock or connected status")	    
                #attributes.doSetAttr(bsNode,l_targets[index],value)
        except Exception,error:
            raise Exception,"{0} blendshape_updateValueFromField failed | {1}".format(self._str_reportStart,error)

    def blendshape_updateValue(self,bsNode,index,l_targets,l_sliders,l_floatFields):
        try:
            if index < len(l_targets):
                if not bsNode:
                    log.warning("No blendshape node found")
                    return False
                value = l_sliders[index](q=True,value=True)
                #Need to get the value
                try:
                    mc.setAttr("%s.%s"%(bsNode,l_targets[index]),value)
                    l_floatFields[index](e=True,value=value)		
                except:log.warning("Failed to set value, check lock or connected status")
                #attributes.doSetAttr(bsNode,l_targets[index],value)
                l_floatFields[index](e=True,value=value)
        except Exception,error:
            raise Exception,"{0} blendshape_updateValue failed | {1}".format(self._str_reportStart,error)

    def blendshape_resetValue(self,bsNode,index,l_targets,l_sliders,l_floatFields):
        try:
            if index < len(l_targets):
                if not bsNode:
                    log.warning("No blendshape node found")
                    return False
                l_sliders[index].reset()
                #Need to get the value
                try:
                    mc.setAttr("%s.%s"%(bsNode,l_targets[index]),0)
                    l_floatFields[index](e=True,value=0)
                except:log.warning("Failed to set value, check lock or connected status")	    
                #attributes.doSetAttr(bsNode,l_targets[index],0)
        except Exception,error:
            raise Exception,"{0} blendshape_resetValue failed | {1}".format(self._str_reportStart,error)

    #=========================================================================
    # Presets Functions
    #=========================================================================
    #@r9General.Timer
    def presets_updateLists(self):
        """
        Build our presets list and instances
        """
        try:
            self.d_PresetFiles = {}
            self.d_PresetThumbs = {}

            buffer = mPresetsFolder.returnFilesFromFolder(['*.pose']) or []
            thumbBuffer = mPresetsFolder.returnFilesFromFolder(['*.png','*.bmp'])	
            log.debug("Thumbs: %s"%thumbBuffer)
            for f in buffer:
                mFile = DIR_SEPARATOR.join([mPresetsFolder.__pathHere__,f])	    
                pName = f.split('.')[0]
                self.d_PresetFiles[pName] = mFile
                #Let's get our images
                for thumbFile in thumbBuffer:
                    if pName == thumbFile.split('.')[0]:
                        self.d_PresetThumbs[pName] = DIR_SEPARATOR.join([mPresetsFolder.__pathHere__,thumbFile])
                        break
                if pName not in self.d_PresetThumbs.keys():#use failsafe
                    self.d_PresetThumbs[pName] = DIR_SEPARATOR.join([mPresetsFolder.__pathHere__,'imageFail.png'])

            #Update the label
            if self.d_PresetFiles:
                self.uiFrame_PresetOptions(edit = True, label = "Presets | files: %i"%len(self.d_PresetFiles))			    
            else:
                self.uiFrame_PresetOptions(edit = True, label = "Presets | files: 0")		

            self.buildFrame_PresetOptions()#Update the options
        except Exception,error:
            raise Exception,"{0} presets_updateLists failed | {1}".format(self._str_reportStart,error)
        
    def presets_PoseSave(self):
        """
        Save a new preset and reload the presets stuff
        """
        try:
            if not self.mi_ActiveAsset:
                log.error("Must have active asset to save preset")
                return False
            
            result = mc.promptDialog(
                title='Preset FileName',
                message='Enter Name:',
                button=['OK', 'Cancel'],
                defaultButton='OK',
                cancelButton='Cancel',
                dismissString='Cancel')
            if result == 'OK':
                newName = mc.promptDialog(query=True, text=True)
                filePath = DIR_SEPARATOR.join([mPresetsFolder.__pathHere__,newName+'.pose'])	    
                log.info("New name: '%s'"%newName)
                log.info("Path: '%s'"%filePath) 
            else:
                log.error("poseSave cancelled")
                return False
                
            try:poseBuffer = MorphyPreset.data(asset = self.mi_ActiveAsset.mNode)
            except Exception,error:log.error("Data initialization | {0}".format(error))
            
            try:poseBuffer.poseSave(filepath = filePath, useFilter=False)#this useFilter flag 
            except Exception,error:log.error("poseSave | {0}".format(error))

            self.presets_updateLists()
            
            mThumb = self.d_PresetThumbs.get(newName)
            self.uiImage_Preset.setImage(mThumb)
            
        except Exception,error:
            raise Exception,"{0} presets_poseSave failed | {1}".format(self._str_reportStart,error)
        
    def presets_PoseSaveOLD(self,*args):
        """
        Save a new preset and reload the presets stuff
        """
        try:
            result = mc.promptDialog(
                title='Preset FileName', 
                message='Enter Name:',
                button=['OK', 'Cancel'],
                defaultButton='OK',
                cancelButton='Cancel',
                dismissString='Cancel')
            if result == 'OK':
                newName = mc.promptDialog(query=True, text=True)
                filePath = DIR_SEPARATOR.join([mPresetsFolder.__pathHere__,newName+'.cfg'])	    
                log.info("New name: '%s'"%newName)
                log.info("Path: '%s'"%filePath)
                nodes = self.mi_ActiveAsset.objSet_all.value
                #for n in nodes:
                    #log.info(n)
                try:poseBuffer = mPreset.go()
                except Exception,error:"pose initialization | {0}".format(error)
                try:poseBuffer.poseSave(nodes,filePath,useFilter=False)#this useFilter flag 
                except Exception,error:"poseSave | {0}".format(error)

                self.presets_updateLists()
                mThumb = self.d_PresetThumbs.get(newName)
                self.uiImage_Preset.setImage(mThumb)
        except Exception,error:
            raise Exception,"{0} presets_poseSave failed | {1}".format(self._str_reportStart,error)


    def presets_OpenDir(self):
        try:
            import os
            import subprocess
            log.debug("path: '%s'"%mPresetsFolder.__pathHere__)
            newPath = os.path.normpath(str(mPresetsFolder.__pathHere__))	
            #oldpath = str(copy.copy(mPresetsFolder.__pathHere__))
            #newPath = oldpath.replace("/", "\\")	
            subprocess.Popen('explorer "%s"' % newPath)
        except Exception,error:
            raise Exception,"{0} presets_OpenDir failed | {1}".format(self._str_reportStart,error)

    def presets_DeletePose(self, *args):
        """
        Delete the selected preset file from drive
        """
        try:
            preset = self.uiObjectList_Presets.getSelectedItems()[0]

            newPath = os.path.normpath(self.d_PresetFiles.get(preset))	
            os.remove(newPath)

            self.presets_updateLists()
        except Exception,error:
            raise Exception,"{0} presets_DeletePose failed | {1}".format(self._str_reportStart,error)

    #@r9General.Timer
    def presets_SetActive(self):
        """
        """
        try:
            self.mi_ActivePreset = MorphyPreset.data()
            selected = self.uiObjectList_Presets.getSelectedItems() 
            log.debug( selected)
            mFile = self.d_PresetFiles.get(selected[0])#Get the file
            mThumb = self.d_PresetThumbs.get(selected[0])
            log.debug("file: '%s'"%mFile)		
            try:#Try to read a file
                self.mi_ActivePreset._readPose(mFile)
                self.uiImage_Preset.setImage(mThumb)
                log.debug("thumb: %s"%mThumb)
            except:
                log.warning("'%s' isn't a good preset file"%mFile)
                self.mi_ActivePreset = False
                
            self.report_updatePresets()
        except Exception,error:
            raise Exception,"{0} presets_SetActive failed | {1}".format(self._str_reportStart,error)

    #@r9General.Timer
    def presets_PoseLoad(self,*args):
        """
        Save a new preset and reload the presets stuff
        """
        try:
            try:log.debug(self.mi_ActiveAsset.getMessage('objSet_all'))
            except:
                if not self.asset_autoLoad():
                    raise StandardError,"Active asset no longer exists"

            selected = self.uiObjectList_Presets.getSelectedItems()
            if not selected:
                log.warning("No preset selected")
                return False
            mFile = self.d_PresetFiles.get(selected[0])#Get the file

            if selected and self.mi_ActivePreset and self.mi_ActiveAsset:
                log.debug(self.mi_ActiveAsset.objSet_all.value)
                controls = self.mi_ActiveAsset.objSet_all.value
                if self.uiCB_ArmsPreset.getValue() or self.uiCB_FacePreset.getValue() or self.uiCB_LegsPreset.getValue() or self.uiCB_TorsoPreset:
                    #If any flags are set, we're gonna filter
                    controlsBuffer = []
                    for c in controls:
                        if '.' not in c:#easy attribute check
                            i_c = cgmMeta.cgmNode(c)
                            if i_c.hasAttr('controlPart'):	    
                                if self.uiCB_ArmsPreset.getValue() and i_c.controlPart == 'arm':
                                    controlsBuffer.append(c)
                                elif self.uiCB_LegsPreset.getValue() and i_c.controlPart == 'leg':
                                    controlsBuffer.append(c)
                                elif self.uiCB_TorsoPreset.getValue() and i_c.controlPart == 'torso':
                                    controlsBuffer.append(c)
                                elif self.uiCB_FacePreset.getValue() and i_c.controlPart == 'face':
                                    controlsBuffer.append(c)
                    if controlsBuffer:controls = controlsBuffer#push our buffer if we have values

                self.mi_ActivePreset.poseLoad(controls,mFile,False)

                #self.buildFrame_BodyBlends()#Rebuild the blendshape frames
                #self.buildFrame_FaceBlends()#Rebuild the blendshape frames 
        except Exception,error:
            raise Exception,"{0} presets_PoseLoad failed | {1}".format(self._str_reportStart,error)