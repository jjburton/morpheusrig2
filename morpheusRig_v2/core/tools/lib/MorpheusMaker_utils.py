"""
------------------------------------------
GuiFactory: cgm.core
Author: Josh Burton
email: jjburton@cgmonks.com

Website : http://www.cgmonks.com
------------------------------------------

Class based ui builder for cgmTools
================================================================
"""
DIR_SEPARATOR = '/' 
REPORT_JOIN = ' | '

#>>> Root settings =============================================================
_l_moduleStatesShort = ['def','size','tpl','skl','rig']
_l_moduleStates = ['Define','Size','Template','Skeleton','Rig']
_l_moduleStatesExport = ['defined','sized','templated','skeletonized','rigged']

#>>> From Standard =============================================================
import os

#>>> From Maya =============================================================
import maya.cmds as mc
import maya.mel as mel
import copy

#>>> From Red9 =============================================================
from Red9.core import Red9_Meta as r9Meta
from Red9.core import Red9_General as r9General

#>>> From cgm ==============================================================
from cgm.core import cgm_Meta as cgmMeta
from cgm.core import cgm_General as cgmGeneral
from cgm.core.cgmPy import validateArgs as cgmValid
import morpheusRig_v2.assets.baseMesh as mBaseMeshFolder
import morpheusRig_v2.presets as mPresetsFolder
import morpheusRig_v2.assets.user as mUserExportFolder

from cgm.core.lib.zoo.path import Path as zooPath

from cgm.core.classes import GuiFactory as cgmGUI

from morpheusRig_v2.core import MorpheusPresetFactory as mPreset
from morpheusRig_v2.core import CustomizationFactory as CSTMF

from cgm.lib.classes import NameFactory as nFactory
from cgm.core.classes import ControlFactory as ctrlFactory
#from cgm.core.rigger import MorpheusFactory as morphyF
from morpheusRig_v2.core import MorpheusFactory as morphyF

from cgm.lib import (search,
                     lists,
                     attributes,
                     guiFactory,
                     deformers,
                     dictionary)

#>>>======================================================================
import logging
logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
#=========================================================================

class MorpheusMakerFunction(cgmGeneral.cgmFuncCls):
    def __init__(self,*args,**kws):
        """
        """	
        super(MorpheusMakerFunction, self).__init__(self,*args,**kws)
        guiInstance = args[0]

        self._str_funcName = 'MorpheusMakerFunction'	
        self._l_ARGS_KWS_DEFAULTS = [{'kw':'guiInstance', "default":None, 'help':"This is your RigFactory go instance", "argType":"RigFactory.go"},
                                     {'kw':'reportTimes', "default":True, 'help':"Change to report defaults", "argType":"bool"}]
        self.__dataBind__(*args, **kws)
        self._gui = guiInstance
        self._b_ExceptionInterupt = 1
        #self.l_funcSteps = [{'step':'Get Data','call':self._getData}]
        #=================================================================

def exampleFunc(*args, **kws):
    class fncWrap(MorpheusMakerFunction):
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'morphuesMaker.puppet_changeState'
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'guiInstance', "default":None, 'help':"This is your MorpheusMaker instance", "argType":"RigFactory.go"},
                                         {'kw':'axisValue', "default":True, 'help':"Change to report defaults", "argType":"bool"}]	    

            self.__dataBind__(*args, **kws)
            self._b_autoProgressBar = True
            self._b_reportTimes = True
            #self.l_funcSteps = [{'step':'Validate','call':self._step_validate},
                                # ]
            #=================================================================
            #self.l_strSteps = ['Start','template objects','curve','root control','helpers']

        def __func__(self):
            self.log_info(self._gui)
            pass
    return fncWrap(*args, **kws).go()

#=========================================================================================================
#>>> Puppet
#=========================================================================================================
def puppet_joint_setAxis(*args, **kws):
    class fncWrap(MorpheusMakerFunction):
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'morphuesMaker.puppet_joint_setAxis'
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'guiInstance', "default":None, 'help':"This is your MorpheusMaker instance", "argType":"RigFactory.go"},
                                         {'kw':'axisValue', "default":True, 'help':"Change to report defaults", "argType":"bool"}]	    

            self.__dataBind__(*args, **kws)
            self._b_autoProgressBar = True
            self._b_reportTimes = True
            #self.l_funcSteps = [{'step':'Validate','call':self._step_validate},
                                # ]
            #=================================================================
            #self.l_strSteps = ['Start','template objects','curve','root control','helpers']

        def __func__(self):
            try:
                mi_gui = self._gui
            except Exception,error:raise Exception,"Bring local fail | error: {0}".format(error)

            try:
                self._b_axisValue = cgmValid.boolArg(self.d_kws['axisValue'],calledFrom = self._str_reportStart,noneValid = False)
                if not mi_gui.mi_ActivePuppet:
                    raise ValueError,"No puppet found"
                mi_puppet = mi_gui.mi_ActivePuppet
            except Exception,error:raise Exception,"Validation fail | error: {0}".format(error)

            try:
                self._l_joints = mi_puppet._UTILS.get_joints(mi_puppet,skinJoints = 1)
                for jnt in self._l_joints:
                    #self.log_info(jnt)
                    try:
                        attributes.doSetAttr(jnt,'displayLocalAxis',self._b_axisValue)
                    except Exception,error:
                        self.log_error("joint '{0}' fail | error: {1}".format(jnt,error))
            except Exception,error:raise Exception,"joint fail | error: {0}".format(error)
    return fncWrap(*args, **kws).go()

def puppet_joint_select(*args, **kws):
    class fncWrap(MorpheusMakerFunction):
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'morphuesMaker.puppet_joint_select'
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'guiInstance', "default":None, 'help':"This is your MorpheusMaker instance", "argType":"RigFactory.go"},
                                         ]	    

            self.__dataBind__(*args, **kws)
            self._b_autoProgressBar = True
            self._b_reportTimes = True
            #self.l_funcSteps = [{'step':'Validate','call':self._step_validate},
                                # ]
            #=================================================================
            #self.l_strSteps = ['Start','template objects','curve','root control','helpers']

        def __func__(self):
            try:
                mi_gui = self._gui
            except Exception,error:raise Exception,"Bring local fail | error: {0}".format(error)

            try:
                if not mi_gui.mi_ActivePuppet:
                    raise ValueError,"No puppet found"
                mi_puppet = mi_gui.mi_ActivePuppet
            except Exception,error:raise Exception,"Validation fail | error: {0}".format(error)

            try:
                self._l_joints = mi_puppet._UTILS.get_joints(mi_puppet,skinJoints = 1,select = 1)
            except Exception,error:raise Exception,"query fail | error: {0}".format(error)
    return fncWrap(*args, **kws).go()

def puppet_loadTemplateConfig(self):
    try:
        if self.mi_ActivePuppet:
            mi_Puppet = self.mi_ActivePuppet
        elif self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('mi_Puppet'):
            mi_Puppet = self.mi_ActiveAsset.mi_Puppet
        else:
            raise ValueError,"No puppet found"

        _path_userExportBase = mUserExportFolder.__pathHere__         
        _path_configFile = cgmValid.filepath(fileFilter='Config Files (*.pose)' , fileMode = 1, startDir=_path_userExportBase)

        log.info(_path_configFile)
        
        if _path_configFile:
            mi_Puppet.templateSettings_call('import',_path_configFile)
            return True
        return False


    except Exception,error:
        raise Exception,"{0} puppet_loadTemplateConfig failed | {1}".format(self._str_reportStart,error)

def puppet_saveTemplateConfig(self):
    try:
        if self.mi_ActivePuppet:
            mi_Puppet = self.mi_ActivePuppet
        elif self.mi_ActiveAsset and self.mi_ActiveAsset.getMessage('mi_Puppet'):
            mi_Puppet = self.mi_ActiveAsset.mi_Puppet
        else:
            raise ValueError,"No puppet found"

        _path_userExportBase = mUserExportFolder.__pathHere__         
        _path_configFile = cgmValid.filepath(fileFilter='Config Files (*.pose)' , startDir=_path_userExportBase)

        log.info(_path_configFile)
        
        if _path_configFile:
            mi_Puppet.templateSettings_call('export',_path_configFile)
            return True
        return False
    except Exception,error:
        raise Exception,"{0} puppet_saveTemplateConfig failed | {1}".format(self._str_reportStart,error)

def puppet_changeState(*args, **kws):
    class fncWrap(MorpheusMakerFunction):
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'morphuesMaker.puppet_changeState'
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'guiInstance', "default":None, 'help':"This is your MorpheusMaker instance", "argType":"RigFactory.go"},
                                         ]	    

            self.__dataBind__(*args, **kws)
            self._b_autoProgressBar = True
            self._b_reportTimes = True
            #self.l_funcSteps = [{'step':'Validate','call':self._step_validate},
                                # ]
            #=================================================================
            #self.l_strSteps = ['Start','template objects','curve','root control','helpers']

        def __func__(self):
            try:
                mi_gui = self._gui
            except Exception,error:raise Exception,"Bring local fail | error: {0}".format(error)

            if mi_gui.mi_ActivePuppet:
                mPuppet = mi_gui.mi_ActivePuppet
            elif mi_gui.mi_ActiveAsset and mi_gui.mi_ActiveAsset.getMessage('mPuppet'):
                mPuppet = mi_gui.mi_ActiveAsset.mPuppet
            else:
                log.error("No puppet found")
                return False

            self.log_info("Puppet found | '{0}'".format(mPuppet.p_nameShort))
            try:
                int_state = mi_gui.uiRadioCollection_states.getSelectedIndex()
                self.log_info("State : '{0}'".format(int_state))
                #if int_state < 2:#was 3
                if int_state <= 1:#was 3                
                    mi_gui.mi_ActiveAsset.setState( int_state,rebuildFrom = 'define', forceNew = True )
                else:
                    mi_gui.mi_ActiveAsset.setState(int_state,forceNew = True )

                mi_gui._int_stateBuffer = int_state
            except Exception,error:
                log.error(Exception)
                mi_gui.report_updatePuppet()                
                raise Exception,"Failed to change state |  %s"%error
            self.log_info("puppet_changeState complete")
            mi_gui.report_updatePuppet()
    return fncWrap(*args, **kws).go()

def puppet_export(*args, **kws):
    class fncWrap(MorpheusMakerFunction):
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'morphuesMaker.puppet_export'    
            self.__dataBind__(*args, **kws)
            self._b_autoProgressBar = True
            self._b_reportTimes = True
            self.l_funcSteps = [{'step':'Validate','call':self._step_validate},
                                {'step':'Export Dirs','call':self._step_exportDirs},
                                {'step':'Export Asset Config','call':self._step_assetConfig},
                                {'step':'Export Template Config','call':self._step_templateConfig},                             
                                {'step':'Export Puppet','call':self._step_puppetExport},                                
                                ]
        def _step_validate(self):
            if self._gui.mi_ActivePuppet:
                self.mPuppet = self._gui.mi_ActivePuppet
            elif self._gui.mi_ActiveAsset and self._gui.mi_ActiveAsset.getMessage('self.mPuppet'):
                self.mPuppet = self._gui.mi_ActiveAsset.self.mPuppet
            else:
                log.error("No puppet found")
                return self._FailBreak_(False)

            try:
                self.mPuppet.mNode
                self._str_characterName = self.mPuppet.cgmName                
            except Exception,error:self.log_error("Puppet error | error: {0}".format(error))

            try:
                _l_nameBuild = [self._str_characterName]
                self._str_dateTime = cgmGeneral.returnTimeStr("%m%d%Y_%H%M%S")
                if self._gui._int_stateBuffer:
                    _l_nameBuild.append(_l_moduleStatesExport[self._gui._int_stateBuffer])
                else:
                    _l_nameBuild.append("noStateFound")
                _l_nameBuild.append(self._str_dateTime)
                self._str_fileBaseName = "_".join(_l_nameBuild)
            except Exception,error:raise Exception,"Create Export Name | error: {0}".format(error)

        def _step_exportDirs(self):
            try:
                _str_userPath = mUserExportFolder.__pathHere__ 
                self._str_charDirPath = DIR_SEPARATOR.join([_str_userPath,self._str_characterName])
                zooPath(self._str_charDirPath).create()#...verify our folder
                self.log_info("Character dir: '{0}'".format(self._str_charDirPath))

                self._d_exportDirPaths = {}
                for s in ['configAsset','textures','configTemplate']:
                    self._d_exportDirPaths[s] = DIR_SEPARATOR.join([self._str_charDirPath,s])
                    zooPath(self._d_exportDirPaths[s]).create()#...verify our folder
                    self.log_info("{1} dir: '{0}'".format(self._d_exportDirPaths[s],s))                      
            except Exception,error:raise Exception,"Verify character export dir fail | error: {0}".format(error)            
        def _step_assetConfig(self):
            try:#>> Export Asset Config
                _str_cfgFileName = "_".join([self._str_characterName,'asset',self._str_dateTime])                    
                filePath = DIR_SEPARATOR.join([ self._d_exportDirPaths['configAsset'],
                                                _str_cfgFileName +'.cfg'])
                self.log_info("Asset CFG File : '%s'"%_str_cfgFileName)                    
                self.log_info("Asset CFG Path: '%s'"%filePath)
                nodes = self._gui.mi_ActiveAsset.objSetAll.value
                try: mPreset.go().poseSave(nodes,filePath,useFilter=False)#this useFilter flag 
                except Exception,error:"poseSave | {0}".format(error)     

            except Exception,error:
                self.log_error("Export Asset cfg fail | error: {0}".format(error)) 

        def _step_templateConfig(self):
            try:#>> Export Template Config
                if self._gui._int_stateBuffer >=2:
                    _str_pCfgFileName = "_".join([self._str_characterName,'template',self._str_dateTime])                    
                    filePath = DIR_SEPARATOR.join([ self._d_exportDirPaths['configTemplate'],
                                                    _str_pCfgFileName +'.cfg'])
                    self.log_info("Template CFG File : '%s'"%_str_pCfgFileName)                    
                    self.log_info("Template CFG Path: '%s'"%filePath)

                    try: self.mPuppet._UTILS.template_saveConfig(self.mPuppet,filePath)
                    except Exception,error:"poseSave | {0}".format(error)     
                else:
                    self.log_warning("Puppet not templated, cannot export template config")
            except Exception,error:
                self.log_error("Export Template cfg fail | error: {0}".format(error))

        def _step_puppetExport(self):        
            try:                
                try:
                    _filePath = DIR_SEPARATOR.join([self._str_charDirPath,self._str_fileBaseName+'.ma'])	    
                    self.log_info("New name: '{0}'".format(self._str_fileBaseName))
                    self.log_info("Path: '{0}'".format(_filePath))	    
                    mc.select([self.mPuppet.mNode,self.mPuppet.masterNull.mNode])
                except Exception,error:raise Exception,"Build file path fail | error: {0}".format(error)

                #file -force -options "v=0" -typ "mayaAscii" -pr -es "J:/Dropbox/MRv2Dev/Assets/Morphy/maya/scenes/jTesting/gui/test.ma";
                mc.undoInfo(openChunk=True)

                try:#master control unlock and what not for export
                    _mi_masterControl = self.mPuppet.masterControl
                    for attr in mc.listAttr(_mi_masterControl.mNode, locked = 1):
                        mAttr = cgmMeta.cgmAttr(_mi_masterControl,attr)
                        if not mAttr.p_hidden and mAttr.p_locked:
                            mAttr.p_locked = False                  

                except Exception,error:raise Exception,"Puppet prep fail | error: {0}".format(error)


                mc.delete( self._gui.mi_ActiveAsset.masterNull.mNode )		
                mc.file(_filePath, force = True, options = True, type = "mayaAscii", es = True)
                mc.undoInfo(closeChunk=True)
                mc.undo()
                mc.select(cl=True)
            except Exception,error:
                mc.undo()                
                mc.undoInfo(closeChunk=True)
                raise Exception,"export fail | error: {0}".format(error)
    return fncWrap(*args, **kws).go()

def puppet_doChangeName(self):
    """ 
    Change the name of a puppet
    """  
    try:
        #>>> Variables
        varCheck = self.uiTextField_PuppetName(q=True,text=True)
        log.debug("varCheck: '%s'"%varCheck)
        if self.mi_ActivePuppet:
            log.debug("ActivePuppet: '%s'"%self.mi_ActivePuppet.getShortName())	    
            if varCheck:
                log.debug("self.mi_ActivePuppet.cgmName: '%s'"%self.mi_ActivePuppet.cgmName)		    
                oldName = self.mi_ActivePuppet.cgmName
                if oldName != varCheck:
                    log.debug('Changing name...')
                    #>> puppet
                    str_name = str(varCheck)
                    log.warning("Changing name from '%s' to '%s'"%(self.mi_ActivePuppet.cgmName,str_name))
                    self.mi_ActivePuppet.cgmName = str_name

                    #self.doStore('cgmName',name,overideMessageCheck = True)
                    self.mi_ActivePuppet.__verify__()
                    self.mi_ActivePuppet.masterControl.rebuildControlCurve()  

                    self.report_updatePuppet()			
                    return True
                return False
    except Exception,error:
        raise Exception,"{0} puppet_doChangeName failed | {1}".format(self._str_reportStart,error)

@cgmGeneral.Timer    
def focus_modeChange(*args, **kws):
    class fncWrap(MorpheusMakerFunction):
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'morphuesMaker.focus_modeChange'
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'guiInstance', "default":None,
                                          'help':"This is your MorpheusMaker instance",
                                          "argType":"RigFactory.go"},
                                         {'kw':'activeMode', "default":'puppet',
                                          'help':"This is the focus change",
                                          "argType":"string"},                                         
                                         ]	    

            self.__dataBind__(*args, **kws)
            self._b_autoProgressBar = True
            self._b_reportTimes = True

        def __func__(self):
            try:
                mi_gui = self._gui
            except Exception,error:
                raise Exception,"Bring local fail | error: {0}".format(error)

            try:
                if not mi_gui.mi_ActiveAsset:
                    self.log_debug("No asset found")
                    return False

                mi_asset = mi_gui.mi_ActiveAsset
                check_puppetExists(mi_gui)

                if not mi_gui.mi_ActivePuppet:
                    self.log_debug("No puppet found")
                    return False                    
                mi_puppet = mi_gui.mi_ActivePuppet
                self._str_activeMode = cgmValid.stringArg(self.d_kws['activeMode'],
                                                          calledFrom = self._str_reportStart,
                                                          noneValid = False)

            except Exception,error:
                self.log_error("Validation fail | error: {0}".format(error))                
                return False

            try:
                __str_modeLower = self._str_activeMode.lower()
                if __str_modeLower == 'puppet':
                    #
                    mi_asset.masterNull.template = 1#...make sure this is on
                    mi_puppet.masterNull.v = 1

                elif __str_modeLower == 'asset':
                    #Hide the puppet
                    mi_puppet.masterNull.v = 0
                    mi_asset.masterNull.template = 0#...make sure this is off
                    mi_asset.masterNull.v = 1#...make sure this is on

                else:
                    #clear
                    mi_puppet.masterNull.v = 1
                    mi_asset.masterNull.template = 0#...make sure this is off
                    mi_asset.masterNull.v = 1#...make sure this is off                    

            except Exception,error:
                raise Exception,"Mode change error: | Mode: {0} | {1}".format(self._str_activeMode,error)
    return fncWrap(*args, **kws).go()

#=========================================================================================================
#>>> Asset
#=========================================================================================================
def asset_setActive(*args, **kws):
    class fncWrap(MorpheusMakerFunction):
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'morphuesMaker.asset_setActive'
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'guiInstance', "default":None, 'help':"This is your MorpheusMaker instance", "argType":"RigFactory.go"},
                                         {'kw':'index', "default":0, 'help':"Index of asset to set to", "argType":"int"}]	    

            self.__dataBind__(*args, **kws)
            self._b_reportTimes = True
            self._b_autoProgressBar = True	    
            self.l_funcSteps = [{'step':'validate','call':self._fncStep_valid_},
                                {'step':'Initial Set','call':self._fncStep_initialSet_},	                        
                                {'step':'Reports Update','call':self._fncStep_updateReports_},
                                ]		
        def _fncStep_valid_(self):
            try:
                self._int_indexArg = cgmValid.valueArg(self.d_kws['index'],calledFrom = self._str_reportStart,noneValid = False)
            except Exception,error:raise Exception,"Bring local fail | error: {0}".format(error)

        def _fncStep_initialSet_(self):
            try:
                mi_gui = self._gui

                mi_gui.mi_ActiveAsset = mi_gui.ml_customizationNetworks[self._int_indexArg]#link it
                if not mi_gui.mi_ActiveAsset:
                    return self._FailBreak_("Set to {0}. Nothing there".format(self._int_indexArg))
                
                #if not mi_gui.mi_ActiveAsset.isCustomizable():
                    #self.log_warning("not customizable | {0}".format(mi_gui.mi_ActiveAsset.p_nameShort))

                cgmGUI.doSetInstancedUIItemsEnableState(True,mi_gui.l_AssetUIGroups)	
                mi_gui.var_ActiveAsset.value = mi_gui.mi_ActiveAsset.getShortName()

                if mi_gui.mi_ActiveAsset and mi_gui.mi_ActiveAsset.getMessage('mPuppet'):
                    mi_gui.mi_ActivePuppet = mi_gui.mi_ActiveAsset.mPuppet
                    mi_gui.report_updatePuppet()
            except Exception,error:raise Exception,"active set fail | error: {0}".format(error)

        def _fncStep_updateReports_(self):
            try:
                self._gui.reports_updateAsset()
            except Exception,error:raise Exception,"Bring local fail | error: {0}".format(error)
    return fncWrap(*args, **kws).go()

#=========================================================================================================
#>>> Tabs
#=========================================================================================================
def tab_setActive(self,arg):
    """ 
    Change the name of a puppet
    """  
    try:
        #>>> Variables
        try:
            cgmGUI.doToggleModeState(arg,
                                     self._l_tabOptions,
                                     self.var_Mode.name,
                                     self.uiContainers_main)
        except Exception,error:
            log.error("Set tab fail | error: {0}".format(error))                
            return False   

        try:
            if arg.lower() in ['asset','puppet']:
                focus_modeChange(self,arg)
            else:
                focus_modeChange(self,'clear')

        except Exception,error:
            log.error("Set focus fail | error: {0}".format(error))                
            return False          

    except Exception,error:
        raise Exception,"{0} tab_setActive failed | {1}".format(self._str_reportStart,error)



#=========================================================================================================
#>>> Reports
#=========================================================================================================
@cgmGeneral.Timer
def log_self(self):
    cgmGUI.log_selfReport(self)
    try:
        check_assetExists(self)
        check_puppetExists(self)
        log.info("{0} Stored...".format(self._str_reportStart))
        d_report = {'ActiveAsset':self.mi_ActiveAsset,
                    'ActivePreset':self.mi_ActivePreset,
                    'ActivePuppet':self.mi_ActivePuppet,
                    'State Buffer':self._int_stateBuffer,
                    'Base Mesh dir':self._path_baseMeshDir,
                    'Presets dir':self._path_mPresetsDir}

        for str_key in d_report.keys():
            log.info(cgmGeneral._str_baseStart * 2  + " '{0}' : {1} ".format(str_key,d_report[str_key]))		    

    except Exception,error:
        raise Exception,"{0} log_self failed | {1}".format(self._str_reportStart,error)



#=========================================================================================================
#>>> CHECKS
#=========================================================================================================
@cgmGeneral.Timer
def check_assetExists(self):
    _str_funcName = 'check_assetExists'	
    try:
        if self.mi_ActiveAsset:
            if mc.objExists(self.mi_ActiveAsset.mNode):
                return True		
    except Exception,error:
        log.error("{0} fail! | error: {1}".format(_str_funcName,error))
        log.error("Asset no longer exists. reloading")
    self.mi_ActiveAsset = False
    self.reports_updateAll()
    return False

@cgmGeneral.Timer
def check_puppetExists(self):
    _str_funcName = 'check_puppetExists'	
    try:
        try:
            if self.mi_ActivePuppet:
                if mc.objExists(self.mi_ActivePuppet.mNode):
                    return True			
        except:pass

        if check_assetExists(self) and self.mi_ActiveAsset.getMessage('mPuppet'):
            log.info('here')
            self.mi_ActivePuppet = self.mi_ActiveAsset.mPuppet
            self._int_stateBuffer = None		
            return True
    except Exception,error:
        log.error("{0} fail! | error: {1}".format(_str_funcName,error))	    
        log.debug("Puppet no longer exists. reloading")
    #log.info("{0 | False".format(_str_funcName))
    self.mi_ActivePuppet = False
    self._int_stateBuffer = None	
    self.report_updatePuppet()	
    self.mi_ActivePuppet = False
    self._int_stateBuffer = None	
    return False  