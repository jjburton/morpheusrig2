"""
------------------------------------------
morpheus_meta: cgm.core
Author: Josh Burton
email: jjburton@cgmonks.com

Website : http://www.cgmonks.com
------------------------------------------

================================================================
"""
import maya.cmds as mc

import random
import re
import copy
import time

# From Red9 =============================================================
from Red9.core import Red9_Meta as r9Meta
from Red9.core import Red9_AnimationUtils as r9Anim

#========================================================================
import logging
logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
#========================================================================

# From cgm ==============================================================
from cgm.core import cgm_General as cgmGeneral
from cgm.core.lib import curve_Utils as crvUtils
from cgm.core import cgm_Meta as cgmMeta
from cgm.core import cgm_PuppetMeta as cgmPM
from cgm.core.lib import nameTools
from cgm.core.rigger import ModuleFactory as mFactory
from cgm.core.rigger import PuppetFactory as pFactory
from morpheusRig_v2.core import MorpheusFactory as morphyF
from cgm.core.classes import NodeFactory as nodeF
from cgm.lib.ml import ml_resetChannels
from cgm.lib import (modules,
                     distance,
                     deformers,
                     controlBuilder,
                     attributes,
                     search,
                     curves)

cgmModuleTypes = mFactory.__l_modulesClasses__
__l_faceModuleTypes__ = mFactory.__l_faceModules__
_l_moduleStates = mFactory._l_moduleStates

########################################################################
    
class cgmMorpheusMakerNetwork(cgmMeta.cgmNode):
    """"""
    def __init__(self,*args,**kws):
        """Constructor"""
        #if log.getEffectiveLevel() == 10:log.debug(">>> cgmMorpheusMakerNetwork.__init__")
        #if kws:log.debug("kws: %s"%str(kws))
        #if args:log.debug("args: %s"%str(args))
        doVerify = kws.get('doVerify') or False

        #>>>Keyword args
        super(cgmMorpheusMakerNetwork, self).__init__(*args,**kws)
	#>>> TO USE Cached instance ---------------------------------------------------------
	if self.cached:
	    #log.debug('CACHE : Aborting __init__ on pre-cached %s Object' % self.mNode)
	    return
	#====================================================================================	

	#====================================================================================	
        if self.__justCreatedState__ or doVerify:
            if not self.__verify__():
                raise StandardError,"Failed!"
        self._UTILS = morphyF

    def __bindData__(self):
        pass
    ##@r9General.Timer
    def __verify__(self):
        """ 
        """ 
        self.addAttr('mClass','cgmMorpheusMakerNetwork',attrType='string',lock=True)
        self.addAttr('cgmType','customizationNetwork',attrType='string',lock=True)

        self.addAttr('meshClass',initialValue = 'm1',attrType='string',lock=True)
        self.addAttr('version',initialValue = '',attrType='string',lock=True)

        self.addAttr('masterNull',attrType='messageSimple',lock=True)
        self.addAttr('masterControl',attrType='messageSimple',lock=True)

        self.addAttr('mPuppet',attrType='messageSimple',lock=True)
	self.addAttr('mSimpleFaceModule',attrType='messageSimple',lock=True)#...testing this

        #>>> Necessary attributes
        #===============================================================
	#> Helpers
	#self.addAttr('helper_eyeLeft',attrType = 'messageSimple',lock=True)#...connection to base body
	#self.addAttr('helper_eyeRight',attrType = 'messageSimple',lock=True)#...connection to base body
	#self.addAttr('helper_tongue',attrType = 'messageSimple',lock=True)#...connection to base body
	
	
        #> Curves and joints
        self.addAttr('controlCurves',attrType = 'message')
        self.addAttr('joints_left',attrType = 'message',lock=True)
        self.addAttr('joints_right',attrType = 'message',lock=True)	
        self.addAttr('roots_left',attrType = 'message',lock=True)
        self.addAttr('roots_right',attrType = 'message',lock=True)
        self.addAttr('jointList',attrType = 'message',lock=True)

        #>>> Geo =======================================================
        self.addAttr('geo_baseBody',attrType = 'messageSimple',lock=True)#...connection to base body
	self.addAttr('geo_baseHead',attrType = 'messageSimple',lock=True)#...connection to base head
	self.addAttr('geo_unified',attrType = 'messageSimple',lock=True)#...connection to unified geo for easy skinning tranfer, etc
	self.addAttr('geo_unifiedHeadWrapBridge',attrType = 'messageSimple',lock=True)#...	
	self.addAttr('geo_resetHead',attrType = 'messageSimple',lock=True)#...
	self.addAttr('geo_resetBody',attrType = 'messageSimple',lock=True)#...
	self.addAttr('geo_resetUnified',attrType = 'messageSimple',lock=True)#...
	
        #>>> Bridges
	self.addAttr('geo_bridgeFaceShapers',attrType = 'messageSimple',lock=True)	
        #self.addAttr('geo_bridgeMain',attrType = 'messageSimple',lock=True)
        #self.addAttr('geo_bridgeFace',attrType = 'messageSimple',lock=True)	
        #self.addAttr('geo_bridgeBody',attrType = 'messageSimple',lock=True)	

        #>>> Nodes =====================================================
        #self.addAttr('bodyBlendshapeNodes',attrType = 'messageSimple',lock=True)
        #self.addAttr('faceBlendshapeNodes',attrType = 'messageSimple',lock=True)
        #self.addAttr('bsNode_bridgeMain',attrType = 'messageSimple',lock=True)
        self.addAttr('bsNode_faceShapers',attrType = 'messageSimple',lock=True)
        self.addAttr('bsNode_bodyShapers',attrType = 'messageSimple',lock=True)

        #self.addAttr('skinCluster',attrType = 'messageSimple',lock=True)

        #>>> Controls ==================================================
        self.addAttr('controlsLeft',attrType = 'message',lock=True)
        self.addAttr('controlsRight',attrType = 'message',lock=True)
        self.addAttr('controlsCenter',attrType = 'message',lock=True)

        #>>> Object Sets ===============================================
        self.addAttr('objSet_all',attrType = 'messageSimple',lock=True)
        self.addAttr('objSet_left',attrType = 'messageSimple',lock=True)
        self.addAttr('objSet_right',attrType = 'messageSimple',lock=True)

        #>>> Watch groups
        self.addAttr('autoPickerWatchGroups',attrType = 'message',lock=True)#These groups will setup pickers for all sub groups of them	
        self.doName()

        #MasterNull
        #==============   
        if not mc.objExists(attributes.returnMessageObject(self.mNode,'masterNull')):#If we don't have a masterNull, make one
            self.i_masterNull = cgmMeta.cgmObject()
            self.addAttr('masterNull',self.i_masterNull.mNode,attrType = 'messageSimple',lock=True)
        else:
            self.i_masterNull = self.masterNull#Linking to instance for faster processing. Good idea?

        self.i_masterNull.doStore('cgmName', self.mNode + '.cgmName')	
        self.i_masterNull.doStore('cgmTypeModifier', 'customizationAsset')	

        self.i_masterNull.doName()
        attributes.doSetLockHideKeyableAttr(self.i_masterNull.mNode,channels=['tx','ty','tz','rx','ry','rz','sx','sy','sz'])

        # Groups
        #======================================================================
        l_baseGroups = ['noTransform','geo','customGeo','hairGeo',
                        'clothesGeo','baseGeo','bsGeo']
        l_geoGroups = ['customGeo','bsGeo','baseGeo']
        l_customGeoGroups = ['clothesGeo','hairGeo','earGeo','eyeballGeo','eyebrowGeo','uprTeethGeo','lwrTeethGeo','tongueGeo','facialHairGeo']
        l_earGeoGroups = ['left_earGeo','right_earGeo']
        l_eyeGeoGroups = ['left_eyeGeo','right_eyeGeo','left_corneaGeo','right_corneaGeo']	
        l_bsTargetGroups = ['faceTargets','bodyTargets']
        l_bsFaceTargets = ['fullFaceTargets','browShapers','noseShapers',
	                   'jawShapers','cheekShapers','mouthShapers','bodyMatchShapers']
        l_bsBodyTargets = ['fullBodyTargets','bodyShapers']
        
        l_autoPickerWatchAttrs = ['left_earGeo','right_earGeo','left_eyeGeo','right_eyeGeo']
        l_autoPickerWatchGroups = []
        for attr in l_baseGroups + l_customGeoGroups+ l_bsTargetGroups + l_bsBodyTargets + l_bsFaceTargets + l_earGeoGroups + l_eyeGeoGroups:
            try:
                self.i_masterNull.addAttr(attr+'Group',attrType = 'messageSimple', lock = True)
                grp = attributes.returnMessageObject(self.masterNull.mNode,attr+'Group')# Find the group
                Attr = 'i_' + attr+'Group'#Get a better attribute store string           
                if mc.objExists( grp ):
                    #If exists, initialize it
                    self.__dict__[Attr]  = r9Meta.MetaClass(grp)#initialize

                else:#Make it
                    #if log.getEffectiveLevel() == 10:log.debug('Creating %s'%attr)                                    
                    self.__dict__[Attr]= cgmMeta.cgmObject(name=attr)#Create and initialize
                    self.__dict__[Attr].doName()
                    #self.i_masterNull.connectChildNode(self.__dict__[Attr].mNode, attr+'Group','puppet') #Connect the child to the holder
                    self.__dict__[Attr].connectParentNode(self.i_masterNull.mNode,'puppet', attr+'Group')

                    #if log.getEffectiveLevel() == 10:log.debug("Initialized as 'self.%s'"%(Attr))         
                if not  self.__dict__[Attr].getAttr('cgmName'):
                    self.__dict__[Attr].addAttr('cgmName',attr,lock = True)
                    self.__dict__[Attr].doName()		    
                #>>> Special data parsing to get things named how we want
                if 'left' in attr and not self.__dict__[Attr].hasAttr('cgmDirection'):
                    buffer = self.__dict__[Attr].cgmName
                    buffer = buffer.split('left_')
                    self.__dict__[Attr].doStore('cgmName',''.join(buffer[1:]),overideMessageCheck = True)		
                    self.__dict__[Attr].doStore('cgmDirection','left')
                if 'right' in attr and not self.__dict__[Attr].hasAttr('cgmDirection'):
                    buffer = self.__dict__[Attr].cgmName
                    buffer = buffer.split('right_')
                    self.__dict__[Attr].doStore('cgmName',''.join(buffer[1:]),overideMessageCheck = True)		
                    self.__dict__[Attr].doStore('cgmDirection','right')

                if 'Targets' in attr and not self.__dict__[Attr].hasAttr('cgmTypeModifier'):
                    buffer = self.__dict__[Attr].cgmName
                    buffer = buffer.split('Targets')
                    self.__dict__[Attr].doStore('cgmName',''.join(buffer[0]),overideMessageCheck = True)		
                    self.__dict__[Attr].doStore('cgmTypeModifier','targets',overideMessageCheck = True)
                    self.__dict__[Attr].doName()

                if 'Geo' in attr and not self.__dict__[Attr].hasAttr('cgmTypeModifier'):
                    buffer = self.__dict__[Attr].cgmName
                    buffer = buffer.split('Geo')
                    self.__dict__[Attr].doStore('cgmName',''.join(buffer[0]),overideMessageCheck = True)		
                    self.__dict__[Attr].doStore('cgmTypeModifier','geo',overideMessageCheck = True)
                    self.__dict__[Attr].doName()

                # Few Case things
                #==============            
                if attr == 'geo':
                    self.__dict__[Attr].doParent(self.i_noTransformGroup)
                elif attr in l_geoGroups:
                    self.__dict__[Attr].doParent(self.i_geoGroup)	
                elif attr in l_customGeoGroups:
                    self.__dict__[Attr].doParent(self.i_customGeoGroup)
                elif attr in l_earGeoGroups:
                    self.__dict__[Attr].doParent(self.i_earGeoGroup)
                elif attr in l_eyeGeoGroups:
                    self.__dict__[Attr].doParent(self.i_eyeballGeoGroup)	    
                elif attr in l_bsTargetGroups:
                    self.__dict__[Attr].doParent(self.i_bsGeoGroup)
                elif attr in l_bsFaceTargets:
                    self.__dict__[Attr].doParent(self.i_faceTargetsGroup)
                elif attr in l_bsBodyTargets:
                    self.__dict__[Attr].doParent(self.i_bodyTargetsGroup)	    
                else:    
                    self.__dict__[Attr].doParent(self.i_masterNull)

                if attr in l_autoPickerWatchAttrs:#append to our list to store
                    l_autoPickerWatchGroups.append(self.__dict__[Attr].mNode)

                attributes.doSetLockHideKeyableAttr( self.__dict__[Attr].mNode )
            except Exception,error:
                log.error("'{0}' Group check fail. | attr: '{1}' | error: {2}".format(self.p_nameShort,attr,error))
        
        self.autoPickerWatchGroups = l_autoPickerWatchGroups#store them

        # Master Curve
        #==================================================================
        masterControl = attributes.returnMessageObject(self.mNode,'masterControl')
        if mc.objExists( masterControl ):
            #If exists, initialize it
            self.mi_masterControl = self.masterControl
            self.mi_masterControl.mClass = 'cgmMasterControl'

        else:#Make it
            self.mi_masterControl = cgmMasterControl(puppet = self)#Create and initialize
            self.mi_masterControl.__verify__()
        self.mi_masterControl.parent = self.i_masterNull.mNode
        self.mi_masterControl.doName()


        # Vis setup
        # Setup the vis network
        #====================================================================
        #self.mi_masterControl.controlVis.addAttr('controls',attrType = 'bool',keyable = False, locked = True)
        if not self.mi_masterControl.hasAttr('controlVis') or not self.mi_masterControl.getMessage('controlVis'):
            log.error("This is an old master control or the vis control has been deleted. rebuild")
        else:
            iVis = self.mi_masterControl.controlVis
            visControls = 'left','right','sub','main'
            visArg = [{'result':[iVis,'leftSubControls_out'],'drivers':[[iVis,'left'],[iVis,'subControls'],[iVis,'controls']]},
                      {'result':[iVis,'rightSubControls_out'],'drivers':[[iVis,'right'],[iVis,'subControls'],[iVis,'controls']]},
                      {'result':[iVis,'subControls_out'],'drivers':[[iVis,'subControls'],[iVis,'controls']]},		      
                      {'result':[iVis,'leftControls_out'],'drivers':[[iVis,'left'],[iVis,'controls']]},
                      {'result':[iVis,'rightControls_out'],'drivers':[[iVis,'right'],[iVis,'controls']]}
                      ]
            nodeF.build_mdNetwork(visArg)

        # Settings setup

        # Setup the settings network
        #====================================================================	
        i_settings = self.mi_masterControl.controlSettings
        str_nodeShort = str(i_settings.getShortName())
        #Skeleton/geo settings
        for attr in ['geo','bsGeo','customGeo','puppet']:
            i_settings.addAttr(attr,enumName = 'off:lock:on', defaultValue = 1, attrType = 'enum',keyable = False,hidden = False)
            nodeF.argsToNodes("%s.%sVis = if %s.%s > 0"%(str_nodeShort,attr,str_nodeShort,attr)).doBuild()
            nodeF.argsToNodes("%s.%sLock = if %s.%s == 2:0 else 2"%(str_nodeShort,attr,str_nodeShort,attr)).doBuild()

        #Divider
        i_settings.addAttr('________________',attrType = 'int',keyable = False,hidden = False,lock=True)

        #>>>Connect some flags
        #=====================================================================
        i_geoGroup = self.masterNull.geoGroup
        i_geoGroup.overrideEnabled = 1		
        cgmMeta.cgmAttr(i_settings.mNode,'geoVis',lock=False).doConnectOut("%s.%s"%(i_geoGroup.mNode,'overrideVisibility'))
        cgmMeta.cgmAttr(i_settings.mNode,'geoLock',lock=False).doConnectOut("%s.%s"%(i_geoGroup.mNode,'overrideDisplayType'))    

        i_geoGroup = self.masterNull.bsGeoGroup
        i_geoGroup.overrideEnabled = 1		
        cgmMeta.cgmAttr(i_settings.mNode,'bsGeoVis',lock=False).doConnectOut("%s.%s"%(i_geoGroup.mNode,'overrideVisibility'))
        cgmMeta.cgmAttr(i_settings.mNode,'bsGeoLock',lock=False).doConnectOut("%s.%s"%(i_geoGroup.mNode,'overrideDisplayType'))    

        i_geoGroup = self.masterNull.customGeoGroup
        i_geoGroup.overrideEnabled = 1		
        cgmMeta.cgmAttr(i_settings.mNode,'customGeoVis',lock=False).doConnectOut("%s.%s"%(i_geoGroup.mNode,'overrideVisibility'))
        cgmMeta.cgmAttr(i_settings.mNode,'customGeoLock',lock=False).doConnectOut("%s.%s"%(i_geoGroup.mNode,'overrideDisplayType'))    

        return True

    def doChangeName(self,name = ''):
        #if log.getEffectiveLevel() == 10:log.debug(">>> cgmMorpheusMakerNetwork.doChangeName")
        #if log.getEffectiveLevel() == 10:log.debug("name: '%s'"%name)	
        if name == self.cgmName:
            log.error("Network already named '%s'"%self.cgmName)
            return
        if name != '':
            name = str(name)
            log.warning("Changing name from '%s' to '%s'"%(self.cgmName,name))
            self.cgmName = name
            #self.doStore('cgmName',name,overideMessageCheck = True)
            self.__verify__()
            self.masterControl.rebuildControlCurve()

    def isCustomizable(self):
        """
        TODO:
        Checks if an asset is good to go or not
        1) Check for controls
        2) Check for geo
        --skinned?
        --blendshaped?
        """
        #if log.getEffectiveLevel() == 10:log.debug(">>> cgmMorpheusMakerNetwork.doChangeName")	
        controlsLeft = self.getMessage('controlsLeft')
        if not controlsLeft:
            log.warning("{0} | No left controls. Aborting check.".format(self.p_nameShort))
            return False

        controlsRight = self.getMessage('controlsRight')	
        if not controlsRight:
            log.warning("{0} | No right controls. Aborting check.".format(self.p_nameShort))
            return False

        if len(controlsLeft) != len(controlsRight):
            log.warning("Control lengths don't match. Left: %i, Right: %i "%(len(controlsLeft),len(controlsRight)))
            return False	    

        #Geo
        #baseBodyGeo = self.getMessage('geo_base')
        #if not baseBodyGeo:
            #log.warning("{0} | No base geo. Aborting check.".format(self.p_nameShort))
            #return False

        #Skincluster
        #skinCluster = self.getMessage('skinCluster')
        #if not deformers.returnObjectDeformers(baseBodyGeo[0],'skinCluster'):
            #log.warning("0} | No skinCluster. Aborting check.".format(self.p_nameShort))
            #return False

        #>>> Blendshape nodes
        #if not deformers.returnObjectDeformers(baseBodyGeo[0],'blendShape'):
            #log.warning("{0} | No body blendshape node. Aborting check.".format(self.p_nameShort))
            #return False
        """
	if not self.getMessage('faceBlendshapeNodes'):
	    log.warning("No face blendshape node. Aborting check.")
	    return False"""		

        return True

    def verifyPuppet(self):
        """
        Verify a Morpheus Puppet, creates one if not exists, otherwise make sure it's intact
        """
        _str_funcName = 'verifyPuppet'
        try:
            try:#>> Initial puppet...
                if not self.hasAttr('mPuppet'):
                    self.__verify__()

                if not self.getMessage('mPuppet'):
                    mi_puppet = cgmMorpheusPuppet(name = str(self.cgmName),doVerify=True)
                else:
                    mi_puppet = self.mPuppet
                    mi_puppet.__verify__()	

                self.mPuppet = mi_puppet.mNode
            except Exception,error:raise Exception,"Initial puppet fail | error: {0}".format(error)

            morphyF.puppet_verifyAll(self)

            return True
        except Exception,error:raise Exception,"{0} fail | error: {1}".format(_str_funcName,error)	

    def verify_customizationData(self): 
        return morphyF.verify_customizationData(self)

    def setState(self,state,**kws):
        _str_funcName = 'setState({0})'.format(self.cgmName)
        try:
            return morphyF.setState(self,state,**kws)
        except Exception,error:raise Exception,"{0} fail | error: {1}".format(_str_funcName,error)	

    def updateTemplate(self,**kws):
        if not self.mPuppet.isTemplated():
            log.error("'{0}' Not templated!".format(self.cgmName))
            return False
        return morphyF.updateTemplate(self,saveTemplatePose = True,**kws)  

    ##@r9General.Timer
    def doUpdate_pickerGroups(self):
        """
        Update the groups that are stored to self.autoPickerWatchGroups. By that, we mean
        setup a condition node network for each child of that group
        """
        pickerGroups = self.getMessage('autoPickerWatchGroups')
        if not pickerGroups:
            log.warning("No autoPickerWatchGroups detected")
            return False
        #if log.getEffectiveLevel() == 10:log.debug( pickerGroups )
        #nodeF.build_conditionNetworkFromGroup('group1',controlObject = 'settings')
        i_settingsControl = self.masterControl.controlSettings
        settingsControl = i_settingsControl.getShortName()

        #if log.getEffectiveLevel() == 10:log.debug( i_settingsControl.mNode )
        for i_grp in self.autoPickerWatchGroups:
            shortName = i_grp.getShortName()
            #if log.getEffectiveLevel() == 10:log.debug(shortName)
            #Let's get basic info for a good attr name
            d = nameTools.returnObjectGeneratedNameDict(shortName,ignore=['cgmTypeModifier','cgmType'])
            n = nameTools.returnCombinedNameFromDict(d)
            nodeF.build_conditionNetworkFromGroup(shortName, chooseAttr = n, controlObject = settingsControl)

    ##@r9General.Timer
    def doUpdateBlendshapeNode(self,blendshapeAttr):
        """ 
        Update a blendshape node by it checking it's source folder
        """ 
        #if log.getEffectiveLevel() == 10:log.debug(">>> cgmMorpheusMakerNetwork.doUpdateBlendshapeNode")
        #if log.getEffectiveLevel() == 10:log.debug("blendshapeAttr: '%s'"%blendshapeAttr)	

        # Get our base info
        #==================	        
        targetGeoGroup = self.masterNull.getMessage(blendshapeAttr)[0]#Targets group
        if not targetGeoGroup:
            log.warning("No group found")
            return False

        bsTargetObjects = search.returnAllChildrenObjects(targetGeoGroup,True)
        if not bsTargetObjects:
            log.error("No geo group found")
            return False

        baseGeo = self.getMessage('baseBodyGeo')[0]
        if not bsTargetObjects:
            log.error("No geo found")
            return False	

        #See if we have a blendshape node

        bsNode = deformers.buildBlendShapeNode(baseGeo,bsTargetObjects,'tmp')

        i_bsNode = cgmMeta.cgmNode(bsNode)
        i_bsNode.addAttr('cgmName','body',attrType='string',lock=True)    
        i_bsNode.addAttr('mClass','cgmNode',attrType='string',lock=True)
        i_bsNode.doName()
        p.bodyBlendshapeNodes = i_bsNode.mNode  

    def delete(self):
        """
        Delete the Puppet
        """
        if self.getMessage('mPuppet'):self.mPuppet.delete()
        mc.delete(self.masterNull.mNode)
        #mc.delete(self.mNode)
        del(self)
	
    def anim_key(self,**kws):
	_str_funcName = "%s.animKey()"%self.p_nameShort  
	start = time.clock()
	b_return = None
	l_slBuffer = mc.ls(sl=True) or []	
	try:
	    try:buffer = self.puppetSet.getList()
	    except:buffer = []
	    if buffer:
		mc.select(buffer)
		mc.setKeyframe(**kws)
		b_return =  True
	    b_return = False
	    log.info("%s >> Complete Time >> %0.3f seconds " % (_str_funcName,(time.clock()-start)) + "-"*75)     
	    if l_slBuffer:mc.select(l_slBuffer)		    
	    return b_return
	except Exception,error:
	    log.error("%s.animKey>> animKey fail | %s"%(self.getBaseName(),error))
	    if l_slBuffer:mc.select(l_slBuffer)		    
	    return False

    def mirrorMe(self,*args,**kws):
	l_slBuffer = mc.ls(sl=True) or []
	self.objSet_all.select()
	l_controls = mc.ls(sl=True)
	log.info(l_controls)
	if l_controls:
	    r9Anim.MirrorHierarchy().mirrorData(l_controls,mode = '')		
	    mc.select(l_controls)
	    if l_slBuffer:mc.select(l_slBuffer)
	    return True	  
	if l_slBuffer:mc.select(l_slBuffer)	
	return False

    def mirror_do(self,mode = None, mirrorMode = 'symLeft'):
	"""
	Match function from PuppetFactory
	"""
	l_slBuffer = mc.ls(sl=True) or []
	try:
	    l_mirrorModes = ['symLeft','symRight']
	    if mirrorMode not in l_mirrorModes:
		raise ValueError,"Mode : {0} not in list: {1}".format(mirrorMode,l_mirrorModes)
	except Exception,error: raise Exception,"Mirror Mode validate | error: {0}".format(error)
	
	l_controls = self.objSet_all.value
	if l_controls:
	    if mirrorMode == 'symLeft':
		r9Anim.MirrorHierarchy().makeSymmetrical(l_controls,mode = '',primeAxis = "Left" )
	    elif mirrorMode == 'symRight':
		r9Anim.MirrorHierarchy().makeSymmetrical(l_controls,mode = '',primeAxis = "Right" )
	    else:
		raise StandardError,"Don't know what to do with this mode: {0}".format(mirrorMode)
	    if l_slBuffer:mc.select(l_slBuffer)
	    else:mc.select(l_controls)
	    return True
	if l_slBuffer:mc.select(l_slBuffer)
	return False		

    def anim_reset(self,*args,**kws):
	l_slBuffer = mc.ls(sl=True) or []	
	#self._mi_puppet.puppetSet.select()
	self.objSet_all.select()
	try:
	    if mc.ls(sl=True):
		ml_resetChannels.main(transformsOnly = False)	
		if l_slBuffer:mc.select(l_slBuffer)		
		return True
	    if l_slBuffer:mc.select(l_slBuffer)	    
	    return False  
	except Exception,error:
	    self.log_error("Failed to reset | errorInfo: {%s}"%error)
	if l_slBuffer:mc.select(l_slBuffer)	    	
	return False

    def anim_select(self):
	self.objSet_all.select()
	
class cgmMorpheusPuppet(cgmPM.cgmPuppet):
    pass
    """
    def __init__(self, node = None, name = None, initializeOnly = False, *args,**kws):
	cgmPuppet.__init__(self, node = node, name = name, initializeOnly = initializeOnly, *args,**kws)
        """
    
    def get_customizationNetwork(self):
	"""
	Call to check message connections for a customization network
	"""
	for plug in mc.listConnections("{0}.message".format(self.mNode)):
	    #log.info("Checking {0}".format(plug))
	    if attributes.doGetAttr(plug,'mClass') == 'cgmMorpheusMakerNetwork':
		return cgmMeta.asMeta(plug)
	return False
#=========================================================================      
# R9 Stuff - We force the update on the Red9 internal registry  
#=========================================================================      
r9Meta.registerMClassInheritanceMapping()
#=========================================================================