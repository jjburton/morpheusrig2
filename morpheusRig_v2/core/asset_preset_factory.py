"""
------------------------------------------
Author: Josh Burton
email: jjburton@cgmonks.com

Website : http://www.cgmonks.com
------------------------------------------

================================================================
"""
DIR_SEPARATOR = '/' 

#>>> From Standard =============================================================
import os
#import getpass
#import time

#>>> From Maya =============================================================
import maya.cmds as mc
import maya.mel as mel
import copy
    
#>>> From cgm ==============================================================
import morpheusRig_v2.assets.baseMesh as mBaseMeshFolder
import morpheus_meta as MorphyMeta

from cgm.core import cgm_Meta as cgmMeta
from cgm.core import cgm_General as cgmGeneral
from cgm.lib import (search,
                     names,
                     attributes,
                     guiFactory,
                     deformers,
                     dictionary)
    
#>>> From Red9 =============================================================
from Red9.core import Red9_Meta as r9Meta
from Red9.core import Red9_General as r9General
from Red9.core import Red9_AnimationUtils as r9Anim
from Red9.core import Red9_CoreUtils as r9Core
from Red9.core import Red9_PoseSaver as r9Pose
import Red9.startup.setup as r9Setup

import Red9.packages.configobj as configobj

#>>>======================================================================
import logging
logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
#=========================================================================

class dataSTANDALONE(object):
    '''
    Class to storing customization asset data. Utilizes red9.packages.ConfigObj for storage format. 
    
    :param asset: Morpheus customization asset
    :param controls: Controls to apply data to
    :param filepath: File to read on call or via self.read() call to browse to one
    
    '''    
    _configToStored = {'pose':'d_pose'} 
    
    def __init__(self, asset = None, controls = None, filepath = None, **kws):
        """

        """        
        self.mi_asset = False
        self.mi_poseData = r9Pose.PoseData()

        self.l_assetControls = []
        self.d_pose = {}
        
        self.str_filepath = None          
        self.d_general = {}
        if asset is not None and controls is None:
            self.validateAsset(asset)
            
        self.validateControls(controls)
        
        if filepath is not None:
            try:self.read(filepath)
            except:log.error("Filepath failed to read.")
            
    def validateFilepath(self, filepath = None, fileMode = 0):
            '''
            Validates a given filepath or generates one with dialog if necessary
            '''        
            if filepath is None:
                startDir = mc.workspace(q=True, rootDirectory=True)
                filepath = mc.fileDialog2(dialogStyle=2, fileMode=fileMode, startingDirectory=startDir,
                                          fileFilter='Pose file (*.pose)')
                if filepath:filepath = filepath[0]
                
            if filepath is None:
                return False
            
            self.filepath = filepath
            log.info("filepath validated...")        
            return filepath
        
    def validateAsset(self,asset = None):
        log.info("asset: {0}".format(asset))
        self.mi_asset = cgmMeta.validateObjArg(asset,mType = MorphyMeta.cgmMorpheusMakerNetwork, noneValid = True)
        if not self.mi_asset:
            log.error("No asset validated")
        return self.mi_asset
    
    def validateControls(self, controls = None):
        if controls is None:
            if self.mi_asset:
                self.l_assetControls = self.mi_asset.objSet_all.getList()
            else:
                log.warning("No asset and no controls. Can't validate...")
                
        log.info("Validated {0} controls.".format(len(self.l_assetControls)))    
        
    def poseSave(self, nodes = None, filepath=None, useFilter=True, storeThumbnail=True):
        
        if filepath is None and self.filepath is None:
            filepath = self.validateFilepath(filepath)
        elif self.filepath:
            filepath = self.filepath
            
        if nodes is None:
            nodes = self.l_assetControls
            
        if not filepath:
            raise ValueError,"No file path specified."
            
        self.mi_poseData.poseSave(nodes, filepath=filepath, useFilter=True, storeThumbnail=True)
        
    def poseLoad(self, nodes = None, filepath=None, useFilter=True, 
                relativePose=False, relativeRots='projected', 
                relativeTrans='projected', 
                maintainSpaces=False, percent=None):
        
        if filepath is None and self.filepath is None:
            filepath = self.validateFilepath(filepath, fileMode = 1)
        #elif self.filepath:
            #filepath = self.filepath
            
        if nodes is None:
            nodes = self.l_assetControls        
        log.info(filepath)    
        log.info(nodes)
        
        self.mi_poseData.poseLoad(self, nodes, filepath=None, useFilter=False, relativePose=False, relativeRots='projected',
                                  relativeTrans='projected', maintainSpaces=False, percent=None)     
    
            
class data(r9Pose.PoseData):
    '''
    
    :param asset: Morpheus customization asset
    :param controls: Controls to apply data to
    :param filepath: File to read on call or via self.read() call to browse to one
    
    '''       
    _configToStored = {'customization':'d_customization',
                       'surface':'d_surfaceInfo',
                       'asset':'d_assetInfo'} 
    
    def __init__(self, *args,**kws):
        super(data, self).__init__(*args,**kws)
        self.thumbnailRes=[290,290]
        self.useFilter=False        
        
        self.mi_asset = False
        self.l_assetControls = []
        self.d_customization = {}
        self.d_assetInfo = {}
        self.d_surfaceInfo = {}
        
        self.filepath = None    
        
        asset = kws.get('asset') or None
        controls = kws.get('controls') or None
        filepath = kws.get('filepath') or None
        
        if asset is not None and controls is None:
            self.validateAsset(asset)
            
        self.validateControls(controls)
        
        if filepath is not None:
            try:self.validateFilepath(kws.get('filepath'))
            except Exception,err:log.error("Filepath failed to validate. {0}".format(err))     
            
    def report(self):
        log.info("Read Data Report "+ cgmGeneral._str_hardBreak)
        log.info("Config File: {0}".format(self.filepath))
        for k in data._configToStored.keys():
            _d_bfr = self.__dict__[data._configToStored[k]]
            if _d_bfr:
                log.info("{0} ".format(k) + cgmGeneral._str_subLine)
                l_keys = _d_bfr.keys()
                l_keys.sort()
                for k1 in l_keys:
                    _bfr = _d_bfr[k1]
                    if isinstance(_bfr,dict):
                        log.info(">" + "Nested Dict: {0}".format(k1) + cgmGeneral._str_subLine)
                        l_bufferKeys = _bfr.keys()
                        l_bufferKeys.sort()
                        for k2 in l_bufferKeys:
                            log.info("-"*3 +'>' + " {0} : {1} ".format(k2,_bfr[k2]))			
                    else:
                        log.info(">" + " {0} : {1} ".format(k1,_d_bfr[k1])) 
                        
    def validateFilepath(self, filepath = None, fileMode = 0):
        '''
        Validates a given filepath or generates one with dialog if necessary
        '''        
        if filepath is None:
            startDir = mc.workspace(q=True, rootDirectory=True)
            filepath = mc.fileDialog2(dialogStyle=2, fileMode=fileMode, startingDirectory=startDir,
                                      fileFilter='Pose file (*.pose)')
            if filepath:filepath = filepath[0]
            
        if filepath is None:
            return False
        
        if not os.path.exists(filepath):
            raise ValueError,"Filepath does not exist: {0}".format(filepath)
        self.filepath = filepath
        log.info("filepath validated...")        
        return filepath
    
    def validateAsset(self,asset = None):
        log.info("asset: {0}".format(asset))
        self.mi_asset = cgmMeta.validateObjArg(asset,mType = MorphyMeta.cgmMorpheusMakerNetwork, noneValid = True)
        if not self.mi_asset:
            log.error("No asset validated")
            
        self.d_assetInfo = {'name':self.mi_asset.getShortName(),
                            'meshClass':self.mi_asset.getMayaAttr('meshClass'),
                            'version':self.mi_asset.getMayaAttr('version')}
        return self.mi_asset
    
    def validateControls(self, controls = None):
        if controls is None:
            if self.mi_asset:
                self.l_assetControls = self.mi_asset.objSet_all.getList()
            else:
                log.warning("No asset and no controls. Can't validate...")
                
        log.info("Validated {0} controls.".format(len(self.l_assetControls)))                                                 
    # --------------------------------------------------------------------------------
    # Data map overloads -  ---
    # --------------------------------------------------------------------------------
    def _writePose(self, filepath, force=False):
        if not force:
            if os.path.exists(filepath) and not os.access(filepath,os.W_OK):
                raise IOError('File is Read-Only - write aborted : %s' % filepath)
        
        ConfigObj = configobj.ConfigObj(indent_type='\t')
        ConfigObj['filterNode_settings']=self.settings.__dict__
        ConfigObj['poseData']=self.poseDict
        ConfigObj['info']=self.infoDict
        
        #...added
        ConfigObj['asset'] = self.d_assetInfo
        ConfigObj['surface'] = self.d_surfaceInfo
        ConfigObj['customization'] = self.d_customization
        ConfigObj['poseType'] = 'cgmMorpheusAssetConfig'
        #...end added
        
        if self.skeletonDict:
            ConfigObj['skeletonDict']=self.skeletonDict
            
        ConfigObj.filename = filepath
        ConfigObj.write()
        
    def _readPose(self, filename):
        if filename:
            if os.path.exists(filename):
                #for key, val in configobj.ConfigObj(filename)['filterNode_settings'].items():
                #    self.settings.__dict__[key]=decodeString(val)

                #...added
                _config = configobj.ConfigObj(filename)
                if _config.get('poseType') != 'cgmMorpheusAssetConfig':
                    raise ValueError,"This isn't a cgmMorpheusAssetConfig config | {0}".format(filename)
                        
                for k in data._configToStored.keys():
                    if _config.has_key(k):
                        self.__dict__[data._configToStored[k]] = _config[k]
                    else:
                        log.error("Config file missing section {0}".format(k))
                        return False       
                #...end added
                    
                self.poseDict=configobj.ConfigObj(filename)['poseData']
                if 'info' in configobj.ConfigObj(filename):
                    self.infoDict=configobj.ConfigObj(filename)['info']
                if 'skeletonDict' in configobj.ConfigObj(filename):
                    self.skeletonDict=configobj.ConfigObj(filename)['skeletonDict']                
            else:
                raise StandardError('Given filepath doesnt not exist : %s' % filename)
        else:
            raise StandardError('No FilePath given to read the pose from')
    # --------------------------------------------------------------------------------
    # PoseData overloads - 
    # -------------------------------------------------------------------------------- 
    def _buildBlock_poseDict(self, nodes = None):  
        '''
        Overload for storing blendshape attrs
        '''   
        if nodes is None:
            nodes = self.l_assetControls
            
        r9Pose.PoseData._buildBlock_poseDict(self,nodes)
        return self.poseDict
    
    def poseSave(self, nodes = None, filepath=None, useFilter=True, storeThumbnail=True):
        if filepath is None and self.filepath is None:
            filepath = self.validateFilepath(filepath)
        elif self.filepath:
            filepath = self.filepath
            
        if nodes is None:
            nodes = self.l_assetControls
            
        if not filepath:
            raise ValueError,"No file path specified."
            
        r9Pose.PoseData.poseSave(self,nodes, filepath=filepath, useFilter=True, storeThumbnail=True)
        
    def poseLoad(self, nodes = None, filepath=None, useFilter=True, 
                relativePose=False, relativeRots='projected', 
                relativeTrans='projected', 
                maintainSpaces=False, percent=None):
        
        if filepath is None and self.filepath is None:
            filepath = self.validateFilepath(filepath, fileMode = 1)
        #elif self.filepath:
            #filepath = self.filepath
            
        if nodes is None:
            nodes = self.l_assetControls        
        log.info(filepath)    
        log.info(nodes)
        
        r9Pose.PoseData.poseLoad(self, nodes, filepath=filepath, useFilter=False, relativePose=False, relativeRots='projected',
                                 relativeTrans='projected', maintainSpaces=False, percent=None)     



class data2(r9Pose.PoseData):
    '''
    Subclass to r9's posedata for 
    
    :param asset: Morpheus customization asset
    :param controls: Controls to apply data to
    :param filepath: File to read on call or via self.read() call to browse to one
    
    '''       
    def __init__(self, *args,**kws):
        super(data, self).__init__(*args,**kws)
        self.thumbnailRes=[230,230]
        self.useFilter=False        
        
        self.mi_asset = False
        self.l_assetControls = []
        self.d_pose = {}
        
        self.filepath = None    
        
        log.info(kws)
        log.info("here")
        asset = kws.get('asset') or None
        controls = kws.get('controls') or None
        filepath = kws.get('filepath') or None
        
        if asset is not None and controls is None:
            self.validateAsset(asset)
            
        self.validateControls(controls)
        
        if filepath is not None:
            try:self.read(kws.get('filepath'))
            except:log.error("Filepath failed to read.")        

    def _buildBlock_poseDict(self, nodes = None):  
        '''
        Overload for storing blendshape attrs
        '''   
        if nodes is None:
            nodes = self.l_assetControls
            
        r9Pose.PoseData._buildBlock_poseDict(self,nodes)
        return self.poseDict
    
        if self.poseDict:
            """for node in self.poseDict.keys():
                _dat = self.poseDict.get(node)
                log.info('%s: %s'%(str(node),str()))
                if type(_dat) is dict:
                    log.info(msg)
                    for d_node in _dat.keys():
                        log.info('%s: %s'%(str(d_node),str(_dat.get(d_node))))"""
            for k in self.poseDict.keys():
                _d_bfr = self.poseDict[k]
                if _d_bfr:
                    log.info("{0} ".format(k) + cgmGeneral._str_subLine)
                    l_keys = _d_bfr.keys()
                    l_keys.sort()
                    for k1 in l_keys:
                        _bfr = _d_bfr[k1]
                        if isinstance(_bfr,dict):
                            log.info(">" + "Nested Dict: {0}".format(k1) + cgmGeneral._str_subLine)
                            l_bufferKeys = _bfr.keys()
                            l_bufferKeys.sort()
                            for k2 in l_bufferKeys:
                                log.info("-"*3 +'>' + " {0} : {1} ".format(k2,_bfr[k2]))			
                        else:
                            log.info(">" + " {0} : {1} ".format(k1,_d_bfr[k1]))                            
                        
    def _buildBlock_info(self):
        """
        Basic info for the pose file, this could do with expanding
        """
        r9Pose.PoseData._buildBlock_info(self)

        self.infoDict['poseType'] = 'cgmMorpheusAssetConfig'
        if not self.mi_asset:
            raise ValueError,"Must have customization asset validated to build block"
        self.infoDict['asset'] = {'meshClass':self.mi_asset.getMayaAttr('meshClass'),
                                  'version':self.mi_asset.getMayaAttr('version')}
        
        
    def poseSave(self, nodes = None, filepath=None, useFilter=True, storeThumbnail=True):
        if filepath is None and self.filepath is None:
            filepath = self.validateFilepath(filepath)
        elif self.filepath:
            filepath = self.filepath
            
        if nodes is None:
            nodes = self.l_assetControls
            
        if not filepath:
            raise ValueError,"No file path specified."
            
        r9Pose.PoseData.poseSave(self,nodes, filepath=filepath, useFilter=True, storeThumbnail=True)
        
    def poseLoad(self, nodes = None, filepath=None, useFilter=True, 
                relativePose=False, relativeRots='projected', 
                relativeTrans='projected', 
                maintainSpaces=False, percent=None):
        
        if filepath is None and self.filepath is None:
            filepath = self.validateFilepath(filepath, fileMode = 1)
        #elif self.filepath:
            #filepath = self.filepath
            
        if nodes is None:
            nodes = self.l_assetControls        
        log.info(filepath)    
        log.info(nodes)
        
        r9Pose.PoseData.poseLoad(self, nodes, filepath=None, useFilter=False, relativePose=False, relativeRots='projected',
                                 relativeTrans='projected', maintainSpaces=False, percent=None)     

    def validateFilepath(self, filepath = None, fileMode = 0):
        '''
        Validates a given filepath or generates one with dialog if necessary
        '''        
        if filepath is None:
            startDir = mc.workspace(q=True, rootDirectory=True)
            filepath = mc.fileDialog2(dialogStyle=2, fileMode=fileMode, startingDirectory=startDir,
                                      fileFilter='Pose file (*.pose)')
            if filepath:filepath = filepath[0]
            
        if filepath is None:
            return False
        
        self.filepath = filepath
        log.info("filepath validated...")        
        return filepath
    
    def validateAsset(self,asset = None):
        log.info("asset: {0}".format(asset))
        self.mi_asset = cgmMeta.validateObjArg(asset,mType = MorphyMeta.cgmMorpheusMakerNetwork, noneValid = True)
        if not self.mi_asset:
            log.error("No asset validated")
        return self.mi_asset
    
    def validateControls(self, controls = None):
        if controls is None:
            if self.mi_asset:
                self.l_assetControls = self.mi_asset.objSet_all.getList()
            else:
                log.warning("No asset and no controls. Can't validate...")
                
        log.info("Validated {0} controls.".format(len(self.l_assetControls)))

class data2(object):
    '''
    Class to customization asset data. Utilizes red9.packages.ConfigObj for storage format. 
    
    :param asset: Morpheus customization asset
    :param controls: Controls to apply data to
    :param filepath: File to read on call or via self.read() call to browse to one
    
    '''    
    _configToStored = {'pose':'d_pose'} 
    
    
    def __init__(self, asset = None, controls = None, filepath = None, **kws):
        """

        """        
        self.mi_asset = False
        self.l_assetControls = []
        self.d_pose = {}
        
        self.str_filepath = None
        #self.d_general = cgmGeneral.get_mayaEnviornmentDict()
        #self.d_general['file'] = mc.file(q = True, sn = True)            
        self.d_general = {}
        if asset is not None and controls is None:
            self.validateAsset(asset)
            
        self.validateControls(controls)
        
        if filepath is not None:
            try:self.read(filepath)
            except:log.error("Filepath failed to read.")
            
    def validateAsset(self,asset = None):
        self.mi_asset = cgmMeta.validateObjArg(asset,mType = MorphyMeta.cgmMorpheusMakerNetwork,noneValid = True)
        if not self.mi_asset:
            log.error("No asset validated")
        return self.mi_asset
    
    def validateControls(self, controls = None):
        if controls is None:
            if self.mi_asset:
                self.l_assetControls = self.mi_asset.objSet_all.getList()
            else:
                log.warning("No asset and no controls. Can't validate...")
                
        log.info("Validated {0} controls.".format(len(self.l_assetControls)))
                
    def validateFilepath(self, filepath = None, fileMode = 0):
        '''
        Validates a given filepath or generates one with dialog if necessary
        '''        
        if filepath is None:
            startDir = mc.workspace(q=True, rootDirectory=True)
            filepath = mc.fileDialog2(dialogStyle=2, fileMode=fileMode, startingDirectory=startDir,
                                      fileFilter='Config file (*.cfg)')
            if filepath:filepath = filepath[0]
            
        if filepath is None:
            return False
        
        self.str_filepath = filepath
        log.info("filepath validated...")        
        return filepath
                
    def _buildPoseDict(self, nodes):  
        '''
        Overload for storing blendshape attrs
        '''   
        for i,node in enumerate(nodes):
            key= names.getBaseName(node)
            
            self.d_pose[key]={}
            self.d_pose[key]['ID']=i#selection order index
            self.d_pose[key]['longName']= names.getLongName(node) #longNode name
    
            if self.metaPose:
                self.d_pose[key]['metaData']=getMetaDict(node)   #metaSystem the node is wired too  
    
            if search.returnObjectType(node) == 'blendShape' and '.' not in node:
                channels=r9Anim.getSettableChannels(node,incStatics=True)
                channels.extend(deformers.returnBlendShapeAttributes(node))
            else:
                channels=r9Anim.getSettableChannels(node,incStatics=True)
    
            if channels:
                self.d_pose[key]['attrs']={}
                for attr in channels:
                    try:
                        self.d_pose[key]['attrs'][attr]=mc.getAttr('%s.%s' % (node,attr))
                    except:
                        log.debug('%s : attr is invalid in this instance' % attr)
    
        if self.d_pose:
            """for node in self.d_pose.keys():
                log.info('%s: %s'%(str(node),str(self.d_pose.get(node))))
                if type(node) is dict:
                    for d_node in node.keys():
                        log.info('%s: %s'%(str(d_node),str(node.get(d_node))))   """
            for k in self.d_pose.keys():
                _d_bfr = self.d_pose[k]
                if _d_bfr:
                    log.info("{0} ".format(k) + cgmGeneral._str_subLine)
                    l_keys = _d_bfr.keys()
                    l_keys.sort()
                    for k1 in l_keys:
                        _bfr = _d_bfr[k1]
                        if isinstance(_bfr,dict):
                            log.info(">" + "Nested Dict: {0}".format(k1) + cgmGeneral._str_subLine)
                            l_bufferKeys = _bfr.keys()
                            l_bufferKeys.sort()
                            for k2 in l_bufferKeys:
                                log.info("-"*3 +'>' + " {0} : {1} ".format(k2,_bfr[k2]))			
                        else:
                            log.info(">" + " {0} : {1} ".format(k1,_d_bfr[k1]))                
                        
    def _buildInfoBlock(self):
        """
        Basic info for the pose file, this could do with expanding
        """
        self.d_general['author']=getpass.getuser()
        self.d_general['date']=time.ctime()
        
        #self.infoDict['metaPose']=self.metaPose
        #if self.metaRig:
            #self.infoDict['metaRigNode']  =self.metaRig.mNode
            #self.infoDict['metaRigNodeID']=self.metaRig.mNodeID
        #if self.rootJnt:
            #self.infoDict['skeletonRootJnt']=self.rootJnt    
    
        #self.howdy =  "howdy all"    
        
    def write(self, filepath = None):
        '''
        Write the Data ConfigObj to file
        '''
        filepath = self.validateFilepath(filepath)
        
            
        ConfigObj = configobj.ConfigObj(indent_type='\t')
        ConfigObj['configType']= 'cgmMorpheusAssetConfig' 
        ConfigObj['user'] = self.d_general
        ConfigObj['pose'] = self.d_pose
        
        #ConfigObj['general']=self.d_general  
        ConfigObj.filename = filepath
        ConfigObj.write()
        return True    
    
    def read(self, filepath = None, report = True):
        '''
        Read the Data ConfigObj from file and report the data if so desired.
        '''        
        filepath = self.validateFilepath(filepath, fileMode = 1)
        
        if not os.path.exists(filepath):            
            raise ValueError('Given filepath doesnt not exist : {0}'.format(filepath))   
        
        _config = configobj.ConfigObj(filepath)
        if _config.get('configType') != 'cgmMorpheusAssetConfig':
            raise ValueError,"This isn't a cgmMorpheusAssetConfig config | {0}".format(filepath)
                
        for k in data._configToStored.keys():
            if _config.has_key(k):
                self.__dict__[data._configToStored[k]] = _config[k]
            else:
                log.error("Config file missing section {0}".format(k))
                return False
            
        if report:self.report()
        return True
            
    def report(self):
        log.info("Read Data Report "+ cgmGeneral._str_hardBreak)
        log.info("Config File: {0}".format(self.str_filepath))
        for k in data._configToStored.keys():
            _d_bfr = self.__dict__[data._configToStored[k]]
            if _d_bfr:
                log.info("{0} ".format(k) + cgmGeneral._str_subLine)
                l_keys = _d_bfr.keys()
                l_keys.sort()
                for k1 in l_keys:
                    _bfr = _d_bfr[k1]
                    if isinstance(_bfr,dict):
                        log.info(">" + "Nested Dict: {0}".format(k1) + cgmGeneral._str_subLine)
                        l_bufferKeys = _bfr.keys()
                        l_bufferKeys.sort()
                        for k2 in l_bufferKeys:
                            log.info("-"*3 +'>' + " {0} : {1} ".format(k2,_bfr[k2]))			
                    else:
                        log.info(">" + " {0} : {1} ".format(k1,_d_bfr[k1]))    
