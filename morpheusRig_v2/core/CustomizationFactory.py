__version__ = '1.04062016'

import copy
import re

import logging
logging.basicConfig()
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

import cgm.core

# From Maya =============================================================
import maya.cmds as mc
import maya.mel as mel

# From Red9 =============================================================
from Red9.core import Red9_Meta as r9Meta
from Red9.core import Red9_General as r9General

# From cgm ==============================================================
from cgm.core import cgm_General as cgmGeneral
from cgm.core import cgm_Meta as cgmMeta
from cgm.core import cgm_PuppetMeta as cgmPM
from cgm.lib.classes import NameFactory as nFactory
from cgm.core.rigger.lib import rig_Utils as rUtils
from cgm.core.cgmPy import validateArgs as cgmValid
from cgm.core.classes import NodeFactory as cgmNodeFactory
reload(cgmNodeFactory)
import morpheus_meta as MorphMeta
import morpheus_geo as MORPHYGEO
reload(MORPHYGEO)
import MorpheusFactory as morphyF
import morpheus_sharedData as MORPHYDATA
import cgm.core.lib.skinDat as SKIN

from cgm.lib import (curves,
                     deformers,
                     names,
                     distance,
                     search,
                     lists,
                     modules,
                     constraints,
                     rigging,
                     attributes,
                     joints,
                     guiFactory)
reload(constraints)
reload(rigging)
reload(nFactory)
reload(search)
reload(deformers)
from cgm.lib.zoo.zooPyMaya import skinWeights

#======================================================================
# Functions for a cgmMorpheusMakerNetwork
#======================================================================
def isCustomizable(self):
    """
    Checks if an asset is good to go or not
    """
    return True

#======================================================================
# Processing factory
#======================================================================
def go(*args, **kws):
    """
    Customization template builder from template file for setting up a cgmMorpheusMakerNetwork

    :parameters:
    0 - 'customizationNode'(morpheusCustomizationAsset - None) | Morpheus system biped customization asset

    :returns:
        Nothing
    ##Dict ------------------------------------------------------------------
    ##'mi_segmentCurve'(cgmObject) | segment curve
    ##'segmentCurve'(str) | segment curve string

    :raises:
    Exception | if reached

    """       
    class fncWrap(cgmGeneral.cgmFuncCls):		
        def __init__(self,*args, **kws):
            super(fncWrap, self).__init__(*args, **kws)
            self._str_funcName = 'customizationFactory.go'	
            self._b_reportTimes = 1 #..we always want this on so we're gonna set it on
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'customizationNode',"default":'Morphy_customizationNetwork',"argType":'morpheusBipedCustomizationAsset','help':"This should be a customization asset"}]	    
            self.__dataBind__(*args, **kws)
            #Now we're gonna register some steps for our function...
            '''
	    doBridge_bsNode(self.p)
	    if stopAt == 'bsBridge':return
	    #doBody_bsNode(self)
	    #if stopAt == 'bsBody':return		
	    #doFace_bsNode(self)
	    #if stopAt == 'bsFace':return		
	    doSkinBody(self)
	    if stopAt == 'skin':return
	    doConnectVis(self)
	    if stopAt == 'vis':return
	    doLockNHide(self,p)
	    '''
            self.l_funcSteps = [{'step':'Gather Info','call':self._validate_},
                                {'step':'Mirror Template','call':self._mirrorBridge_},
                                {'step':'Rig Body','call':self._rigBodyBridge_},
                                {'step':'Constraints','call':self._do_setupConstraints_},                                
                                {'step':'RigBlocks','call':self._do_rigBlocks_},
                                {'step':'Check Reset/Bridge Geo','call':self._do_check_resetAndBridgeGeo_},                                
                                ##{'step':'Blendshape Bridge','call':self._do_bsNodeBridge_},
                                {'step':'Blendshape Body','call':self._do_bsNodeBody_},
                                {'step':'Blendshape Face','call':self._do_bsNodeFace_},
                                {'step':'Skincluster','call':self._do_skinBody_},
                                {'step':'Attach','call':self._do_attach_},	
                                {'step':'Shaper Wiring','call':self._do_shaperWiring_},	                                
                                {'step':'Connect Vis','call':self._do_connectVis_},
                                {'step':'LockNHide','call':self._do_lockNHide_},
                                {'step':'Finalize','call':self._fnc_finalize_},
                                ]

        def _validate_(self):
            try:
                self.mi_network = cgmMeta.validateObjArg(self.d_kws['customizationNode'],mType = MorphMeta.cgmMorpheusMakerNetwork,noneValid = False)
            except Exception,error:
                raise Exception,"customizationNode is invalid | {0}".format(error)
            
            _bfr = self.mi_network.getMessageAsMeta('mSimpleFaceModule')
            if not _bfr:
                raise ValueError,"Repair mSimpleFace connection"
            self.mi_simpleFaceModule = _bfr
            
            MORPHYGEO.asset_get_dict(self.mi_network.mNode, 'all', catchInstance = self)

            self.log_infoNestedDict('d_bsGeoTargets')
            self.log_infoNestedDict('md_bsGeoGroups')

        def _mirrorBridge_(self):_mirrorTemplate_(self)
        def _rigBodyBridge_(self):_rigBody_(self)  
        def _do_setupConstraints_(self):_setupConstraints_(self) 
        def _do_bsNodeBridge_(self):_bs_bridge_(self)    	
        def _do_bsNodeBody_(self):_bs_body_(self)    
        def _do_rigBlocks_(self):_rigBlocks_(self)    	        
        def _do_bsNodeFace_(self):_bs_face_(self)    
        def _do_skinBody_(self):_skinBody_(self)    
        def _do_connectVis_(self):_connectVis_(self) 
        def _do_shaperWiring_(self):_shaperWiring_(self)
        def _do_attach_(self):_attach_(self)    	
        def _do_lockNHide_(self):doLockNHide(self,self.mi_network.mNode,False)    
        def _do_check_resetAndBridgeGeo_(self):_check_resetAndBridgeGeo_(self)            
        def _fnc_finalize_(self):
            self.mi_network.addAttr('version',__version__,lock=True)
            cgmMeta
    return fncWrap(*args, **kws).go()

def _shaperWiring_(self):
    mObj = cgmMeta.cgmObject(name = 'customization_attrHolder')
    shaperShapeControls_verify(mObj.mNode)
    connect_customizationBlendshapes()    

        
def _mirrorTemplate_(self):
    """ 
    Segement orienter. Must have a JointFactory Instance
    """ 
    try:
        # Get our base info
        #==================	        
        p = self.mi_network
        d_constraintParentTargets = {}
        d_constraintAimTargets = {}
        d_constraintPointTargets = {}
        d_constraintScaleTargets = {}
        d_constraintOrientTargets = {}    
        int_centre = 0
        int_left = 0
        int_right = 0
        
        #Get skin joints
        try:#>>> Split out the left joints
            self.l_leftJoints = []
            self.l_leftRoots = []
            for i_jnt in self.mi_network.jointList:
                self.log_info(i_jnt)
                if i_jnt.hasAttr('cgmDirection') and i_jnt.cgmDirection == 'left':
                    self.l_leftJoints.append(i_jnt.mNode)
                    if i_jnt.parent:#if it has a panent
                        i_parent = cgmMeta.cgmObject(i_jnt.parent)
                        self.log_info("'%s' child of '%s'"%(i_jnt.getShortName(),i_parent.getShortName()))
                        if not i_parent.hasAttr('cgmDirection'):
                            self.l_leftRoots.append(i_jnt.mNode)  
                            #If a joint is going to be mirrored need to grab snapshot of it's message connections to repair after mirroring
                            #Yay maya... obj.message that is '1_jnt', 'left_2_jnt' becomes '1_jnt', 'left_2_jnt,'right_2_jnt' after mirroring

                    if i_jnt.hasAttr('constraintParentTargets') and i_jnt.constraintParentTargets:
                        d_constraintParentTargets[i_jnt.getShortName()] = i_jnt.getMessage('constraintParentTargets',False)

                    if i_jnt.hasAttr('constraintAimTargets') and i_jnt.constraintAimTargets:
                        d_constraintAimTargets[i_jnt.getShortName()] = i_jnt.getMessage('constraintAimTargets',False)

                    if i_jnt.hasAttr('constraintPointTargets') and i_jnt.constraintPointTargets:
                        d_constraintPointTargets[i_jnt.getShortName()] = i_jnt.getMessage('constraintPointTargets',False)

                    if i_jnt.hasAttr('constraintScaleTargets') and i_jnt.constraintScaleTargets:
                        d_constraintScaleTargets[i_jnt.getShortName()] = i_jnt.getMessage('constraintScaleTargets',False)

                    if i_jnt.hasAttr('constraintOrientTargets') and i_jnt.constraintOrientTargets:
                        d_constraintOrientTargets[i_jnt.getShortName()] = i_jnt.getMessage('constraintOrientTargets',False)

                else:
                    #>>> tag our centre joints for mirroring later
                    i_jnt.addAttr('mirrorSide',attrType = 'enum', enumName = 'Centre:Left:Right', value = 0,keyable = False, hidden = True)
                    i_jnt.addAttr('mirrorIndex',attrType = 'int', value = int_centre,keyable = False, hidden = True)
                    #i_jnt.addAttr('mirrorAxis',value = 'translateX,translateY,translateZ')
                    int_centre+=1#enumerate won't work here

            self.l_leftJoints = lists.returnListNoDuplicates(self.l_leftJoints)
            self.l_leftRoots = lists.returnListNoDuplicates(self.l_leftRoots)
            p.joints_left = self.l_leftJoints
            p.roots_left = self.l_leftRoots
        except Exception,error:raise Exception,"left side gather fail | {0}".format(error)

        #>>> Customization network node
        self.log_info("ShaperJoints: %s"%self.mi_network.getMessage('jointList',False))
        self.log_info("leftJoints: %s"%self.mi_network.getMessage('joints_left',False))
        self.log_info("leftRoots: %s"%self.mi_network.getMessage('roots_left',False))
        
        
        try:#Mirror our joints, make mirror controls and store them appropriately
            #====================================================================
            try:
                self.l_leftJoints = self.mi_network.getMessage('joints_left',False)
                if not self.l_leftJoints:
                    raise ValueError,"No left joints found."
                if not self.mi_network.roots_left:
                    raise ValueError,"No left roots found."            
                self.l_rightJoints = []
                self.l_rightRoots = []
                segmentBuffers = []
            except Exception,error:raise Exception,"Initial checks fail. {0}".format(error)

            for r,i_root in enumerate(self.mi_network.roots_left):
                try:
                    l_mirrored = mc.mirrorJoint(i_root.mNode,mirrorBehavior = True, mirrorYZ = True)

                    mc.select(cl=True)
                    mc.select(i_root.mNode,hi=True)
                    l_base = mc.ls( sl=True )
                    segmentBuffer = []	
                    segName = i_root.getBaseName()

                    try:#Fist a quick naming loop before cullling out non joint stuff
                        ml_mirroredBuffer = cgmMeta.validateObjListArg(l_mirrored)
                        for i,mObj in enumerate(ml_mirroredBuffer):
                            if mObj.getMayaType() == 'shape':continue#...skip this one
                            mObj.cgmDirection = 'right'
                            mObj.doName()
                        for i,mObj in enumerate(ml_mirroredBuffer):#This is so inefficient...come back to this
                            l_mirrored[i] = mObj.mNode
                    except Exception,error:raise Exception,"Name loop fail | {0}".format(error)

                    try:
                        l_mirrored = mc.ls(l_mirrored,type = 'joint')#...Filter out, locs or anything
                        l_base =  mc.ls(l_base,type = 'joint')#...Filter out, locs or anything
                        if len(l_mirrored) != len(l_base):
                            raise ValueError,"Lengths don't match"
                    except Exception,error:
                        for i,obj in enumerate(l_mirrored):
                            self.log_error("{0} | should mirror | {1}".format(obj,l_base[i]))
                        raise Exception,"filter fail | {0}".format(error)

                    try:
                        int_max = len(l_mirrored)+1
                        mayaMainProgressBar = guiFactory.doStartMayaProgressBar(int_max)
                    except Exception,error:
                        self.log_error("len(l_mirrored): {0}".format(int_max))
                        raise Exception,"main progress bar set fail | {0}".format(error)

                    for i,jnt in enumerate(l_mirrored):
                        try:
                            if mc.progressBar(mayaMainProgressBar, query=True, isCancelled=True ) :
                                break
                            mc.progressBar(mayaMainProgressBar, edit=True, status = "Mirroring segment '%s':'%s'..."%(segName,jnt), step=1)
                        except Exception,error:raise Exception,"progress bar set fail | {0}".format(error)
                        try:
                            i_jnt = cgmMeta.cgmObject(jnt)
                            i_jnt.doStore('cgmMirrorMatch',l_base[i])
                            attributes.storeInfo(l_base[i],'cgmMirrorMatch',i_jnt.mNode)
                        except Exception,error:raise Exception,"store fail | {0}".format(error)

                        try:#>>> Make curve
                            index = self.l_leftJoints.index(l_base[i]) #Find our main index
                            i_mirror = self.mi_network.joints_left[index] #store that mirror instance so we're not calling it every line
                            #if not i_mirror:raise ValueError,"Should have an i_mirror here"
                            buffer = mc.duplicate(i_mirror.getMessage('controlCurve')) #Duplicate curve
                            i_crv = cgmMeta.cgmObject( buffer[0] )
                            i_crv.cgmDirection = 'right'#Change direction
                            i_jnt.doStore('controlCurve',i_crv.mNode)#Store new curve to new joint
                            i_crv.doStore('cgmSource',i_jnt.mNode)
                            i_crv.doName()#name it

                        except Exception,error:
                            self.log_error("index: {0} | i_mirror: {1}".format(index,i_mirror))
                            raise Exception,"curve create fail | {0}".format(error)

                        try:#>>> Mirror the curve
                            s_prntBuffer = i_crv.parent#Shouldn't be necessary later
                            grp = mc.group(em=True)#Group world center
                            i_crv.parent = grp
                            attributes.doSetAttr(grp,'sx',-1)#Set an attr
                            i_crv.parent = s_prntBuffer
                            mc.delete(grp)
                        except Exception,error:raise Exception,"curve mirror fail | {0}".format(error)

                        try:#color it
                            l_colorRight = modules.returnSettingsData('colorRight',True)
                            if i_crv.hasAttr('cgmTypeModifier') and i_crv.cgmTypeModifier == 'secondary':
                                colorIndex = 1
                            else:
                                colorIndex = 0
                            curves.setCurveColorByName(i_crv.mNode,l_colorRight[colorIndex])#Color it, need to get secodary indexes
                            self.l_rightJoints.append(i_jnt.mNode)
                            segmentBuffer.append(i_jnt.mNode)#store to our segment buffer
                        except Exception,error:raise Exception,"Coloring fail | {0}".format(error)

                        try:#>>> Tag for mirroring
                            i_jnt.addAttr('mirrorSide',attrType = 'enum', enumName = 'Centre:Left:Right', value = 2,keyable = False, hidden = True)
                            i_jnt.addAttr('mirrorIndex',attrType = 'int', value = int_right,keyable = False, hidden = True)
                            i_jnt.addAttr('mirrorAxis',value = 'translateX,translateY,translateZ')

                            i_mirror.addAttr('mirrorSide',attrType = 'enum', enumName = 'Centre:Left:Right', value = 1,keyable = False, hidden = True)
                            i_mirror.addAttr('mirrorIndex',attrType = 'int', value = int_left,keyable = False, hidden = True)
                            i_mirror.addAttr('mirrorAxis',value = 'translateX,translateY,translateZ')
                            int_left +=1
                            int_right +=1
                        except Exception,error:raise Exception,"mirror tag fail | {0}".format(error)

                        try:#>>> See if we need to grab contraintTargets attr
                            if i_mirror.hasAttr('constraintParentTargets') and i_mirror.constraintParentTargets:
                                targets = []
                                for t in i_mirror.constraintParentTargets:
                                    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
                                    if 'cgmDirection' in d_search.keys() and d_search.get('cgmDirection')=='left':d_search['cgmDirection'] = 'right'
                                    testName = nFactory.returnCombinedNameFromDict(d_search)
                                    targets.append(testName)
                                d_constraintParentTargets[i_jnt.getShortName()] = targets

                            if i_mirror.hasAttr('constraintAimTargets') and i_mirror.constraintAimTargets:
                                targets = []
                                for t in i_mirror.constraintAimTargets:
                                    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
                                    if 'cgmDirection' in d_search.keys() and d_search.get('cgmDirection')=='left':d_search['cgmDirection'] = 'right'
                                    testName = nFactory.returnCombinedNameFromDict(d_search)
                                    targets.append(testName)
                                d_constraintAimTargets[i_jnt.getShortName()] = targets

                            if i_mirror.hasAttr('constraintPointTargets') and i_mirror.constraintPointTargets:
                                targets = []
                                for t in i_mirror.constraintPointTargets:
                                    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
                                    if 'cgmDirection' in d_search.keys() and d_search.get('cgmDirection')=='left':d_search['cgmDirection'] = 'right'
                                    testName = nFactory.returnCombinedNameFromDict(d_search)
                                    targets.append(testName)
                                d_constraintPointTargets[i_jnt.getShortName()] = targets	

                            if i_mirror.hasAttr('constraintScaleTargets') and i_mirror.constraintScaleTargets:
                                targets = []
                                for t in i_mirror.constraintScaleTargets:
                                    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
                                    if 'cgmDirection' in d_search.keys() and d_search.get('cgmDirection')=='left':d_search['cgmDirection'] = 'right'
                                    testName = nFactory.returnCombinedNameFromDict(d_search)
                                    targets.append(testName)
                                d_constraintScaleTargets[i_jnt.getShortName()] = targets	

                            if i_mirror.hasAttr('constraintOrientTargets') and i_mirror.constraintOrientTargets:
                                targets = []
                                for t in i_mirror.constraintOrientTargets:
                                    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
                                    if 'cgmDirection' in d_search.keys() and d_search.get('cgmDirection')=='left':d_search['cgmDirection'] = 'right'
                                    testName = nFactory.returnCombinedNameFromDict(d_search)
                                    targets.append(testName)
                                d_constraintOrientTargets[i_jnt.getShortName()] = targets	


                            """
			    #>>> See if we need to grab contraintTargets attr
			    if i_mirror.hasAttr('constraintParentTargets') and i_mirror.constraintParentTargets:
				self.log_info("constraintParentTargets detected, searching to transfer!")
				targets = []
				for t in i_mirror.constraintParentTargets:
				    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
				    if 'cgmDirection' in d_search.keys():d_search['cgmDirection'] = 'right'
				    testName = nFactory.returnCombinedNameFromDict(d_search)
				    if mc.objExists(testName):targets.append(testName)
				d_constraintParentTargets[i] = targets

			    if i_mirror.hasAttr('constraintAimTargets') and i_mirror.constraintAimTargets:
				self.log_info("constraintAimTargets detected, searching to transfer!")
				targets = []
				for t in i_mirror.constraintAimTargets:
				    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
				    if 'cgmDirection' in d_search.keys():d_search['cgmDirection'] = 'right'
				    testName = nFactory.returnCombinedNameFromDict(d_search)
				    targets.append(testName)
				d_constraintAimTargets[i] = targets	

			    if i_mirror.hasAttr('constraintPointTargets') and i_mirror.constraintPointTargets:
				self.log_info("constraintPointTargets detected, searching to transfer!")
				targets = []
				for t in i_mirror.constraintPointTargets:
				    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
				    if 'cgmDirection' in d_search.keys():d_search['cgmDirection'] = 'right'
				    testName = nFactory.returnCombinedNameFromDict(d_search)
				    targets.append(testName)
				d_constraintPointTargets[i] = targets	

			    if i_mirror.hasAttr('constraintScaleTargets') and i_mirror.constraintScaleTargets:
				self.log_info("constraintScaleTargets detected, searching to transfer!")
				targets = []
				for t in i_mirror.constraintScaleTargets:
				    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
				    if 'cgmDirection' in d_search.keys():d_search['cgmDirection'] = 'right'
				    testName = nFactory.returnCombinedNameFromDict(d_search)
				    targets.append(testName)
				d_constraintScaleTargets[i] = targets	

			    if i_mirror.hasAttr('constraintOrientTargets') and i_mirror.constraintOrientTargets:
				self.log_info("constraintOrientTargets detected, searching to transfer!")
				targets = []
				for t in i_mirror.constraintOrientTargets:
				    d_search = nFactory.returnObjectGeneratedNameDict(t.mNode)
				    if 'cgmDirection' in d_search.keys():d_search['cgmDirection'] = 'right'
				    testName = nFactory.returnCombinedNameFromDict(d_search)
				    targets.append(testName)
				d_constraintOrientTargets[i] = targets	
				"""
                        except Exception,error:raise Exception,"Target mirror transfer fail | {0}".format(error)

                    self.l_rightRoots.append(segmentBuffer[0])#Store the root
                    guiFactory.doEndMayaProgressBar(mayaMainProgressBar)#Close out this progress bar
                    segmentBuffers.append(segmentBuffer)#store the segement buffer
                except Exception,error:raise Exception,"root {0} fail | {1}".format(i_root.p_nameShort, error)

        except Exception,error:raise Exception,"actual mirror fail | {0}".format(error)

        p.roots_right = self.l_rightRoots#store the roots to our network	

        #p.addAttr('rightJoints',attrType = 'message',value = self.l_rightJoints,lock=True)
        p.joints_right = self.l_rightJoints


        #>>> 
        #==================     
        try:#Connect constraintParent/Point/Scale/AimTargets when everything is done
            if d_constraintParentTargets:
                for k in d_constraintParentTargets.keys():
                    self.log_info("'%s' targets: %s"%(k,d_constraintParentTargets[k]))
                    i_k = r9Meta.MetaClass(k)
                    i_k.addAttr('constraintParentTargets',attrType='message',value = d_constraintParentTargets[k])
            if d_constraintAimTargets:
                for k in d_constraintAimTargets.keys():
                    self.log_info("'%s' targets: %s"%(k,d_constraintAimTargets[k]))	    
                    i_k = r9Meta.MetaClass(k)
                    i_k.addAttr('constraintAimTargets',attrType='message',value = d_constraintAimTargets[k])
            if d_constraintPointTargets:
                for k in d_constraintPointTargets.keys():
                    self.log_info("'%s' targets: %s"%(k,d_constraintPointTargets[k]))	    	    
                    i_k = r9Meta.MetaClass(k)
                    i_k.addAttr('constraintPointTargets',attrType='message',value = d_constraintPointTargets[k])
            if d_constraintScaleTargets:
                for k in d_constraintScaleTargets.keys():
                    self.log_info("'%s' targets: %s"%(k,d_constraintScaleTargets[k]))	    	    
                    i_k = r9Meta.MetaClass(k)
                    i_k.addAttr('constraintScaleTargets',attrType='message',value = d_constraintScaleTargets[k])
            if d_constraintOrientTargets:
                for k in d_constraintOrientTargets.keys():
                    self.log_info("'%s' targets: %s"%(k,d_constraintOrientTargets[k]))	    	    
                    i_k = r9Meta.MetaClass(k)
                    i_k.addAttr('constraintOrientTargets',attrType='message',value = d_constraintOrientTargets[k]) 
        except Exception,error:raise Exception,"Constraint target transfer fail | {0}".format(error)

    except Exception,error:
        raise Exception,"_mirrorTemplate_ fail | error: {0}".format(error)




def _rigBody_(self):
    # Get our base info
    #==================	        
    p = self.mi_network
    mayaMainProgressBar = guiFactory.doStartMayaProgressBar(len(self.mi_network.jointList))
    self.l_skinJoints = []

    mc.select(cl=True)
    i_controlSet = cgmMeta.cgmObjectSet(setName = 'customControls',setType = 'tdSet',qssState=True)#Build us a simple quick select set
    i_controlSetLeft = cgmMeta.cgmObjectSet(setName = 'customControlsLeft',setType = 'tdSet',qssState=True)#Build us a simple quick select set
    i_controlSetRight = cgmMeta.cgmObjectSet(setName = 'customControlsRight',setType = 'tdSet',qssState=True)#Build us a simple quick select set

    for i,i_jnt in enumerate(self.mi_network.jointList):#+ self.mi_network.rightJoints
        try:
            if mc.progressBar(mayaMainProgressBar, query=True, isCancelled=True ) :
                break
            mc.progressBar(mayaMainProgressBar, edit=True, status = "On: '%s'"%i_jnt.getShortName(), step=1)

            """ Only need this if we're gonna constrain rather than skin eyes/ears
	    if i_jnt.cgmName =='ear':#update the group
		if i_jnt.cgmDirection == 'left':
		    p.masterNull.left_earGeoGroup.doCopyTransform(i_jnt.mNode)
		else:
		    p.masterNull.right_earGeoGroup.doCopyTransform(i_jnt.mNode)

	    if i_jnt.cgmName =='eye':#update the group
		if i_jnt.cgmDirection == 'left':
		    p.masterNull.left_eyeGeoGroup.doCopyTransform(i_jnt.mNode)
		else:
		    p.masterNull.right_eyeGeoGroup.doCopyTransform(i_jnt.mNode)
	    """

            if i_jnt.cgmName == 'ankle':
                buffer = updateTransform(i_jnt.controlCurve,i_jnt)	  
                i_jnt.controlCurve = buffer
                i_crv = i_jnt.controlCurve
                i_crv.parent = False
                mc.makeIdentity(i_crv.mNode, apply = True, t=True, r = True, s = True)
                vBuffer = mc.xform(i_crv.mNode,q=True,sp=True,ws=True)	    
                i_crv.scalePivotY = 0
                i_crv.cgmType = 'bodyShaper'
                i_crv.doName()
                i_jnt.doGroup(True)
                i_jnt.parent = i_crv.mNode

                i_controlSet.addObj(i_crv.mNode)#Add to our selection set

                if i_crv.cgmDirection == 'left':
                    i_controlSetLeft.addObj(i_crv.mNode)
                else:
                    i_controlSetRight.addObj(i_crv.mNode)		
                i_crv.connectParentNode(self.mi_network.mNode, 'puppet')#...tag to puppet to get to the network
            else:
                i_crv = i_jnt.controlCurve
                if i_crv:
                    i_jnt.addAttr('cgmType','bodyShaper',attrType = 'string')
                    curves.parentShapeInPlace(i_jnt.mNode,i_crv.mNode)
                    i_jnt.doName()
                    i_jnt.doGroup(True)	

                    if i_jnt.cgmName in ['hip','neck','head','shoulders','arm','hand','shoulderMeat','upr_leg']:
                        pBuffer = i_jnt.parent
                        if not pBuffer:
                            log.warning("'%s' lacks a parent. It should have one by now"%i_jnt.getShortName())
                            return False
                        i_prnt = cgmMeta.cgmObject(pBuffer)
                        parentPBuffer = i_prnt.parent
                        i_prnt.parent = False
                        if i_jnt.cgmName == 'shoulders':
                            mc.pointConstraint(parentPBuffer,i_prnt.mNode, maintainOffset=True)					    	    
                        else:
                            mc.parentConstraint(parentPBuffer,i_prnt.mNode, maintainOffset=True)
                    '''
		    if i_jnt.cgmName == 'ball':#make a ball loc
			i_loc = i_jnt.doLoc()
			i_loc.parent = i_jnt.mNode
			mc.move(0,2.455,0, [i_loc.mNode],ws=True,relative=True)
			'''
                i_controlSet.addObj(i_jnt.mNode)
                if i_crv.hasAttr('cgmTypeModifier') and i_crv.cgmTypeModifier == 'secondary':
                    i_jnt.addAttr('cgmTypeModifier',attrType='string',value = 'sub')
                    
                if i_jnt.hasAttr('cgmDirection'):
                    if i_jnt.cgmDirection == 'left':
                        i_controlSetLeft.addObj(i_jnt.mNode)
                    elif i_jnt.cgmDirection == 'right':
                        i_controlSetRight.addObj(i_jnt.mNode)		    
                
            i_jnt.connectParentNode(self.mi_network.mNode, 'puppet')#...tag to puppet to get to the network
            self.l_skinJoints.append(i_jnt)
        except Exception,error:
            raise Exception,"initial loop fail | jnt: {0} | error: {1}".format(i_jnt.p_nameShort,error)
        
    #Fix Ankles
    iLeft = cgmMeta.cgmObject('l_ankle_bodyShaper')
    iRight = cgmMeta.cgmObject('r_ankle_bodyShaper')
    iLeft.doStore('cgmMirrorMatch',iRight.mNode)
    iRight.doStore('cgmMirrorMatch',iLeft.mNode)

    #Store sets
    self.mi_network.objSet_all = i_controlSet.mNode
    self.mi_network.objSet_left = i_controlSetLeft.mNode
    self.mi_network.objSet_right = i_controlSetRight.mNode

    guiFactory.doEndMayaProgressBar(mayaMainProgressBar)
    self.mi_network.controlsLeft = i_controlSetLeft.value
    self.mi_network.controlsRight = i_controlSetRight.value
    
    #self.mi_network.msgList_connect(i_controlSet.value,'controlsAll','puppet')

def _setupConstraints_(self):
    l_aimJoints = []
    for i,i_jnt in enumerate(self.mi_network.jointList):
        constraintTypes = []
        constraintTargets = {}
        aimTargets = []
        aim_ijnt = False
        str_cgmName = i_jnt.cgmName
        #Gather the info to set stuff up
        if i_jnt.hasAttr('constraintScaleTargets'):
            constraintTypes.append('scale')
            constraintTargets['scale'] = i_jnt.getMessage('constraintScaleTargets',False)	
        if i_jnt.hasAttr('constraintParentTargets'):
            constraintTypes.append('parent')
            #if 'scale' not in constraintTypes:
                #constraintTypes.append('scale')#parent type needs scale
                #constraintTargets['scale'] = i_jnt.getMessage('constraintParentTargets',False)			
            constraintTargets['parent'] = i_jnt.getMessage('constraintParentTargets',False)	    
        if i_jnt.hasAttr('constraintPointTargets'):
            constraintTypes.append('point')	    
            constraintTargets['point'] = i_jnt.getMessage('constraintPointTargets',False)
        if i_jnt.hasAttr('constraintOrientTargets'):
            constraintTypes.append('orient')	    
            constraintTargets['orient'] = i_jnt.getMessage('constraintOrientTargets',False)

        if i_jnt.hasAttr('constraintAimTargets'):
            l_aimJoints.append(i_jnt)
            #aimTargets = i_jnt.getMessage('constraintAimTargets',False)

        if constraintTypes:
            log.info("'{0}' constraint list: {1}".format(i_jnt.getShortName(), constraintTypes))

            #if 'aim' in constraintTypes and aimTargets:
                #aim_ijnt = True
                #constraintTypes.remove('aim')

            if constraintTypes and constraintTargets:
                #Need to pair through to see when constraints can be setup together
                constraintPairs = []
                cullList = copy.copy(constraintTypes)
                while cullList:
                    for C in cullList:
                        pairBuffer = []
                        cTargets = constraintTargets.get(C)	
                        for c in constraintTypes:
                            if cTargets == constraintTargets.get(c):
                                pairBuffer.append(c)
                                cullList.remove(c)
                        constraintPairs.append(pairBuffer)

                log.info("constraintPairs: %s"%constraintPairs)   
                for pair in constraintPairs:
                    targets = constraintTargets.get(pair[0])
                    log.info("%s targets: %s"%(pair, targets))
                    pBuffer = i_jnt.parent
                    i_prnt = cgmMeta.cgmObject(pBuffer)
                    i_prnt.addAttr('cgmTypeModifier','%sConstraint'%('_'.join(pair)),'string')	    
                    #parentPBuffer = i_prnt.parent
                    #i_prnt.parent = False
                    i_prnt.doName()	
                    """
		    if i_jnt.hasAttr('constraintAimTargets'):
			mode = 0
		    else:
			mode = 1
			"""
                    mode = 0
                    if 'scale' in pair and str_cgmName in ['eyeOrb','mouth','noseBase']:
                        pair.remove('scale')
                        driverAttr = 'scaleX'
                        l_driverAttrs = []
                        for obj in targets:
                            l_driverAttrs.append("{0}.{1}".format(obj,driverAttr))
                        rUtils.connect_singleDriverAttrToMulti("{0}.scale".format(i_prnt.mNode) ,l_driverAttrs)			

                    if i_jnt.controlPart == 'face':
                        constraints.doConstraintObjectGroup(targets,group = i_prnt.mNode,constraintTypes=pair,mode=0)		                        
                        #i_prnt.parent = parentPBuffer
                    else:
                        i_prnt.parent = False			
                        constraints.doConstraintObjectGroup(targets,group = i_prnt.mNode,constraintTypes=pair,mode=mode)		

    if l_aimJoints:
        for i_jnt in l_aimJoints:
            if i_jnt.cgmName == 'ankleMeat':
                if i_jnt.cgmDirection == 'left':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = False, weight = 1, aimVector = [0,1,0], upVector = [0,0,1], worldUpVector = [0,0,-1], worldUpType = 'vector' )    		
                elif i_jnt.cgmDirection == 'right':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = False, weight = 1, aimVector = [0,-1,0], upVector = [0,0,-1], worldUpVector = [0,0,-1], worldUpType = 'vector' )    			    


            elif i_jnt.controlPart == 'face':
                """
                a  = cgmMeta.cgmObject()
                a.doGroup(True)
                i_jnt.doGroup(True)
                """
                if i_jnt.hasAttr('cgmDirection') and i_jnt.cgmDirection == 'left':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = True, weight = 1, aimVector = [0,0,1], upVector = [0,1,0], worldUpVector = [0,1,0], worldUpType = 'vector' )    
                elif i_jnt.hasAttr('cgmDirection') and i_jnt.cgmDirection == 'right':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = True, weight = 1, aimVector = [0,0,-1], upVector = [0,-1,0], worldUpVector = [0,1,0], worldUpType = 'vector' )    		
            else:
                if i_jnt.cgmDirection == 'left':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = False, weight = 1, aimVector = [0,0,1], upVector = [0,1,0], worldUpVector = [0,0,1], worldUpType = 'vector' )    
                elif i_jnt.cgmDirection == 'right':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = False, weight = 1, aimVector = [0,0,-1], upVector = [0,-1,0], worldUpVector = [0,0,1], worldUpType = 'vector' )    

            attributes.doSetLockHideKeyableAttr(i_jnt.mNode,channels = ['rx','ry','rz'])		
    mc.delete('controlCurves')




def _skinBody_(self):
    p = self.mi_network
    #Get skin joints

    if not self.l_skinJoints:
        self.log_warning("No skin joints found!")
        return False	
        #if not returnSkinJoints(self):
            #log.error("No skinJoints found")

    l_skinJoints = []
    i_jntEyeLeft = False
    i_jntEyeRight = False
    i_jntEarLeft = False
    i_jntEarRight = False

    for i_jnt in self.l_skinJoints:
        if i_jnt.cgmName not in ['ear']:
            l_skinJoints.append(i_jnt.mNode)
        if i_jnt.cgmName == 'ear':
            if i_jnt.cgmDirection == 'left':
                i_jntEarLeft = i_jnt
            else:
                i_jntEarRight = i_jnt		
        elif i_jnt.cgmName == 'eye':
            if i_jnt.cgmDirection == 'left':
                i_jntEyeLeft = i_jnt
            else:
                i_jntEyeRight = i_jnt
        elif i_jnt.cgmName == 'mouthCavity':
            mJnt_mouthCavity = i_jnt


    d_skinJoints = {'body':l_skinJoints,
                    'tongue':[mJnt_mouthCavity.mNode],
                    'uprTeeth':[mJnt_mouthCavity.mNode],
                    'lwrTeeth':[mJnt_mouthCavity.mNode],
                    'earLeft':[i_jntEarLeft.mNode],
                    'earRight':[i_jntEarRight.mNode],
                    'eyeLeft':[i_jntEyeLeft.mNode],
                    'eyeRight':[i_jntEyeRight.mNode]}	

    #>>> Main skin 
    #> Gather geo and skin
    for key in d_skinJoints.keys():
        try:
            log.info("Working to skin: {0}".format(key))
            if key == 'body':
                l_toSkin = [self.md_coreGeo['unified']['mi_base'].mNode,
                            self.md_coreGeo['body']['mi_base'].mNode,
                            self.md_coreGeo['head']['mi_base'].mNode]
                mi_skinDat = SKIN.data(filepath = 'J:/Dropbox/MRv2Dev/Assets/Morphy/maya/scenes/customizationAssets/unifiedSkinning_simplify.cfg')
            else:
                l_toSkin = self.d_skinTargets[key]
            if not l_toSkin:
                raise ValueError,"No skin targets found!"
            if not d_skinJoints.get(key):
                raise ValueError,"No skin joints"	    
            else:
                for i,geo in enumerate(l_toSkin):
                    self.log_info("Skinning: '{0}'".format(geo))
                    toBind = d_skinJoints[key] + [geo]#...combine
                    cluster = mc.skinCluster(toBind, tsb = True, normalizeWeights = True, mi = 4, dr = 5)
                    mi_cluster = cgmMeta.cgmNode(cluster[0])
                    if key == 'body':
                        mi_skinDat.validateTargetMesh(geo)
                        if i == 0:
                            mi_skinDat.applySkin(influenceMode = 'config',nameMatch = True)                            
                        else:
                            mi_skinDat.applySkin(influenceMode = 'config',nameMatch = True)                            
                    else:
                        mi_cluster.doCopyNameTagsFromObject(self.md_geoGroups[key].mNode,ignore=['cgmTypeModifier','cgmType'])
                    mi_cluster.addAttr('mClass','cgmNode',attrType='string',lock=True)
                    mi_cluster.doName()
        except Exception,error:raise Exception,"Skinning {0} fail! | {1}".format(key,error)	
        
        try:
            unified_base = self.md_coreGeo['unified']['mi_base'].mNode
            l_geo =  self.d_skinTargets.get('hair') or []
            for g in l_geo:
                #Wrap
                skinWeights.transferSkinning( unified_base, g )
                cgmMeta.cgmNode(g).doStore('skinMaster',unified_base)
                self.log_info("Skinning: '{0}'".format(g))
                
        except Exception,error:raise Exception,"Skin hair stuff | {1}".format(error)	        

def _attach_(self):
    p = self.mi_network

    #>>> Main skin 
    #> Gather geo and skin
    l_wrapKeys = ['eyebrow','facialHair']
    
    for key in l_wrapKeys:
        try:
            l_geo = self.d_skinTargets.get(key) or []
            for g in l_geo:
                #Wrap
                self.log_info("Attaching: '{0}'".format(g))
                MORPHYGEO.verify_attachment(g,bsMode = 'face')
                
                #deformers.wrapDeformObject(g, self.md_coreGeo['head']['mi_base'].mNode)
        except Exception,error:raise Exception,"{0} | {1}".format(key,error)		

def _connectVis_(self):
    p = self.mi_network
    iVis = p.masterControl.controlVis
    for c in self.mi_network.objSet_all.value:
        if '.' not in c:
            i_c = cgmMeta.cgmNode(c)
            i_attr = cgmMeta.cgmAttr(i_c,'visibility',hidden = True,lock = True)

            if i_c.hasAttr('cgmTypeModifier') and i_c.cgmTypeModifier == 'sub':
                if i_c.hasAttr('cgmDirection'):
                    if i_c.cgmDirection == 'left':
                        i_attr.doConnectIn("%s.leftSubControls_out"%iVis.mNode)
                    if i_c.cgmDirection == 'right':
                        i_attr.doConnectIn("%s.rightSubControls_out"%iVis.mNode)
                else:
                    i_attr.doConnectIn("%s.subControls_out"%iVis.mNode)

            else:
                if i_c.hasAttr('cgmDirection'):
                    if i_c.cgmDirection == 'left':
                        i_attr.doConnectIn("%s.leftControls_out"%iVis.mNode)
                    if i_c.cgmDirection == 'right':
                        i_attr.doConnectIn("%s.rightControls_out"%iVis.mNode)
                else:
                    i_attr.doConnectIn("%s.controls"%iVis.mNode)


#>>>GEO Stuff
def _check_resetAndBridgeGeo_(self):
    """ 
    Checks the reset and Bridge geo exists. Creates it if not.
    """ 
    # Get our base info
    #==================	        
    log.info(">>> go._check_resetAndBridgeGeo_") 
    p = self.mi_network

    #>>> Check Resetter
    #=================================================
    for k in MORPHYGEO._d_asset_main_geoInfo.keys():
        _d = MORPHYGEO._d_asset_main_geoInfo[k]
        try:
            try:
                _bfr = p.getMessage(_d['msg_reset'])
                if _bfr:
                    log.info('{0} Reset Geo exists!'.format(k))
                else:
                    mi_newMesh = self.md_coreGeo[k]['mi_base'].doDuplicate(po = False, ic=False)
                    self.md_coreGeo[k]['mi_reset'] = mi_newMesh
                    mi_newMesh.addAttr('cgmName',k,attrType='string',lock=True)
                    mi_newMesh.addAttr('cgmTypeModifier','DONOTTOUCH_RESET',attrType='string',lock=True)                    
                    mi_newMesh.doName()
                    mi_newMesh.parent =  p.masterNull.bsGeoGroup
                    
                    p.doStore(_d['msg_reset'],mi_newMesh.mNode)
                    
                    mi_newMesh.v = False
                    log.info('{0} Reset Geo created!'.format(k))     
            except Exception,error:
                raise Exception,"!Reset Check!| {1}".format(error)
            
            try:
                if _d.get('msg_bridge'):
                    _bfr = p.getMessage(_d['msg_bridge'])
                    if _bfr:
                        log.info('{0} Bridge Geo exists!'.format(k))
                    else:
                        mi_newMesh = self.md_coreGeo[k]['mi_base'].doDuplicate(po = False,ic=False)
                        self.md_coreGeo[k]['mi_bridge'] = mi_newMesh
                        mi_newMesh.addAttr('cgmName', k,attrType='string',lock=True)
                        mi_newMesh.addAttr('cgmTypeModifier','bsShaperBridge',lock = True)
                        mi_newMesh.doName()
                        mi_newMesh.parent =  p.masterNull.bsGeoGroup
                        
                        p.doStore(_d['msg_bridge'],mi_newMesh.mNode)
                        mi_newMesh.v = False
                        
                        log.info('{0} Bridge Geo created!'.format(k))     
            except Exception,error:
                raise Exception,"!Bridge Check!| {1}".format(error)            
        except Exception,error:
            raise Exception,"Failed on {0} | {1}".format(k,error)
        
def _rigBlocks_(self):
    """ 
    Checks the reset and Bridge geo exists. Creates it if not.
    """ 
    # Get our base info
    #==================	        
    log.info(">>> go._rigBlocks_") 
    d_joints = {}
    for i_jnt in self.l_skinJoints:
        tag_name = i_jnt.cgmName
        if tag_name == 'eye':
            if i_jnt.cgmDirection == 'left':
                d_joints['eye_left'] = i_jnt
            else:
                d_joints['eye_right'] = i_jnt
        elif tag_name == 'head':
            d_joints['head'] = i_jnt
        elif tag_name == 'mouthCavity':
            d_joints['mouthCavity'] = i_jnt
        elif tag_name == 'brow':
            d_joints['brow'] = i_jnt
             
    mi_template = self.mi_simpleFaceModule.templateNull
    mi_template.rigBlock_eye_left.parent = d_joints['eye_left']
    mi_template.rigBlock_eye_right.parent = d_joints['eye_right']
    mi_template.rigBlock_face_upr.parent = d_joints['brow']
    mi_template.rigBlock_face_lwr.parent = d_joints['mouthCavity']
    
    mi_template.rigBlock_eye_left.v = False
    mi_template.rigBlock_eye_right.v = False
    mi_template.rigBlock_face_upr.v = False
    mi_template.rigBlock_face_lwr.v = False  
    
    
    return
    #>>> Check Resetter
    #=================================================
    for k in MORPHYGEO._d_asset_main_geoInfo.keys():
        _d = MORPHYGEO._d_asset_main_geoInfo[k]
        try:
            try:
                _bfr = p.getMessage(_d['msg_reset'])
                if _bfr:
                    log.info('{0} Reset Geo exists!'.format(k))
                else:
                    mi_newMesh = self.md_coreGeo[k]['mi_base'].doDuplicate(po = False)
                    self.md_coreGeo[k]['mi_reset'] = mi_newMesh
                    mi_newMesh.addAttr('cgmName',k,attrType='string',lock=True)
                    mi_newMesh.addAttr('cgmTypeModifier','DONOTTOUCH_RESET',attrType='string',lock=True)                    
                    mi_newMesh.doName()
                    mi_newMesh.parent =  p.masterNull.bsGeoGroup
                    
                    p.doStore(_d['msg_reset'],mi_newMesh.mNode)
                    
                    mi_newMesh.v = False
                    log.info('{0} Reset Geo created!'.format(k))     
            except Exception,error:
                raise Exception,"!Reset Check!| {1}".format(error)
            
            try:
                _bfr = p.getMessage(_d['msg_bridge'])
                if _bfr:
                    log.info('{0} Bridge Geo exists!'.format(k))
                else:
                    mi_newMesh = self.md_coreGeo[k]['mi_base'].doDuplicate(po = False)
                    self.md_coreGeo[k]['mi_bridge'] = mi_newMesh
                    mi_newMesh.addAttr('cgmName', k,attrType='string',lock=True)
                    mi_newMesh.addAttr('cgmTypeModifier','bsBridge',lock = True)
                    mi_newMesh.doName()
                    mi_newMesh.parent =  p.masterNull.bsGeoGroup
                    
                    p.doStore(_d['msg_bridge'],mi_newMesh.mNode)
                    mi_newMesh.v = False
                    
                    log.info('{0} Bridge Geo created!'.format(k))     
            except Exception,error:
                raise Exception,"!Bridge Check!| {1}".format(error)            
        except Exception,error:
            raise Exception,"Failed on {0} | {1}".format(k,error)    
        
def _bs_bridge_(self):
    """ 
    Sets up main,face and body blendshape bridges
    """ 
    # Get our base info
    #==================	        
    log.info(">>> go.doBridge_bsNode") 
    p = self.mi_network
    l_targets = []  
    
    try:#>>> Check on face bridge (shape pushed back into main bridge from wrapped unified geo to face)
        #=================================================
        self.log_info('Facewrap bridge setup')
        bridgeFaceBlendshapeNode = p.getMessage('bsNode_bridgeFace')
        bridgeFace= p.getMessage('geo_bridgeHead')
    
        if not bridgeFace:
            raise ValueError,"Should have 'geo_bridgeHead'" 
        if not bridgeFaceBlendshapeNode:
            raise ValueError,"Should have a 'bsNode_bridgeFace'"
                
    except Exception,error:
        raise Exception,"Facewrap check fail | {0}".format(error)  
    return
    try:#>>> Check Body bridge
        #================================================= 
        bridgeBodyBlendshapeNode = p.getMessage('bsNode_bridgeBody')
        
        if not p.getMessage('bsNode_bridgeBody'):
            log.info("Building body bridge...")
            #Blendshape	
            bsNode_body = deformers.buildBlendShapeNode( self.md_coreGeo['body']['mi_base'].mNode,
                                                            [mi_bridgeBody.mNode, mi_newMesh.mNode],'tmp')
            mi_bsNode = cgmMeta.validateObjArg(bsNode_body,'cgmNode',setClass = True)
            mi_bsNode.addAttr('cgmName','unifiedBridge',attrType='string',lock=True)    
            mi_bsNode.doName()
            p.bsNode_bridgeBody = mi_bsNode.mNode	
            log.info('Body Bridge good!')	
    
        attrs = deformers.returnBlendShapeAttributes(i_bsNode.mNode)
        for a in attrs:
            self.log_info(a)
            attributes.doSetAttr(i_bsNode.mNode,a,1)#Turn it on	the bridge targets
        log.info('Bridge good!')        
    except Exception,error:
        raise Exception,"Body bridge fail | {0}".format(error) 
    

def _bs_bridgeWRAPSETUPOLD_(self):
    """ 
    Sets up main,face and body blendshape bridges
    """ 
    # Get our base info
    #==================	        
    log.info(">>> go.doBridge_bsNode") 
    p = self.mi_network
    l_targets = []  
    
    try:#>>> Facewrap geo bridge (shape pushed back into main bridge from wrapped unified geo to face)
        #=================================================
        #1) dup unified
        #2) wrap to head
        #3) add to body blendshape
        self.log_info('Facewrap bridge setup')
        bridgeFaceBlendshapeNode = p.getMessage('bsNode_bridgeFace')
        bridgeFace= p.getMessage('geo_bridgeHead')
    
        if not bridgeFace:
            raise ValueError,"Should have 'geo_bridgeHead'" 
        if not bridgeFaceBlendshapeNode:
            raise ValueError,"Should have a 'bsNode_bridgeFace'"
        
        try:#create and connect
            _d = self.md_coreGeo['unified']
            mi_newMesh = self.md_coreGeo['unified']['mi_reset'].doDuplicate(po = False)#....dup
            _d['mi_faceWrapBridge'] = mi_newMesh
            mi_newMesh.addAttr('cgmTypeModifier','faceWrapBridge',attrType='string',lock=True)                    
            mi_newMesh.doName()
            mi_newMesh.parent =  p.masterNull.bsGeoGroup
            p.doStore('geo_unifiedHeadWrapBridge',mi_newMesh.mNode)  
            mi_newMesh.v = False
        except Exception,error:
            raise Exception,"Create fail | {0}".format(error)    
        
        try:#wrap
            deformers.wrapDeformObject(mi_newMesh.mNode, bridgeFace)
        except Exception,error:
            raise Exception,"wrap fail | {0}".format(error)          
    except Exception,error:
        raise Exception,"Facewrap bridge fail | {0}".format(error)  
    
    try:#>>> Check Main bridge
        #=================================================
        bridgeMainBlendshapeNode = p.getMessage('bsNode_bridgeMain')
        mi_bridgeBody = p.getMessageAsMeta('geo_bridgeUnified')
    
        if bridgeMainBlendshapeNode and mi_bridgeBody:#if we have one, we're good to go
            log.info('Main Bridge exists!')
        else:
            try:
                #Blendshape	
                bsNode_unified = deformers.buildBlendShapeNode( self.md_coreGeo['unified']['mi_base'].mNode,
                                                                [mi_bridgeBody.mNode, mi_newMesh.mNode],'tmp')
                mi_bsNode = cgmMeta.validateObjArg(bsNode_unified,'cgmNode',setClass = True)
                mi_bsNode.addAttr('cgmName','unifiedBridge',attrType='string',lock=True)    
                mi_bsNode.doName()
                p.bsNode_bridgeMain = mi_bsNode.mNode	
                log.info('Unified Bridge good!')
            except Exception,error:
                self.log_warning('miBase: {0}'.format( self.md_coreGeo['unified']['mi_base']))
                #self.log_warning('mi_bridgeBody: {0}'.format( mi_bridgeBody))            
                raise Exception,"build fail | {0}".format(error) 
            
        attrs = deformers.returnBlendShapeAttributes(mi_bsNode.mNode)
        for a in attrs:
            self.log_info("Setting: {0}".format(a))
            attributes.doSetAttr(mi_bsNode.mNode,a,1)#...turn it on the bridge targets            
    except Exception,error:
        raise Exception,"Unified bridge fail | {0}".format(error) 
    
    try:#>>> Set the correct weights on the unified bridge facewrap target to keep it from pushing over
        #=================================================
        self.log_info('Facewrap bridge blendshapeNode setup')        
        try:#clear 
            str_unified = self.md_coreGeo['unified']['mi_base'].mNode
            str_unifedBSNode = mi_bsNode.mNode
            str_head =  self.md_coreGeo['head']['mi_base'].mNode
            vtx_max = mc.polyEvaluate(str_unified,v=True)
            for i,vtx in enumerate(range(0,vtx_max)):
                self.progressBar_set( status = "Clearing {0}".format(i), progress = i, maxValue = vtx_max)		    				    		    		                    
                mc.setAttr('{0}.inputTarget[0].inputTargetGroup[1].targetWeights[{1}]'.format(str_unifedBSNode,vtx),0.0)  
        except Exception,error:
            raise Exception,"clear fail | {0}".format(error)  
        
        try:#clear 
            vtx_max = mc.polyEvaluate(str_head,v=True)
            
            for i,vtx in enumerate(range(0,vtx_max)):
                self.progressBar_set( status = "Setting {0}".format(i), progress = i, maxValue = vtx_max)		    				    		    		                                    
                pos = distance.returnWorldSpacePosition('{0}.vtx[{1}]'.format(str_head,vtx))
                val = distance.returnClosestPointOnMeshInfoFromPos(pos,str_unified)['closestVertexIndex']
                mc.setAttr('{0}.inputTarget[0].inputTargetGroup[1].targetWeights[{1}]'.format(str_unifedBSNode,val),1.0)  
        except Exception,error:
            raise Exception,"set fail | {0}".format(error)              
    except Exception,error:
        raise Exception,"facewrap target weights per vertex | {0}".format(error)       
    return True
    
    try:#>>> Setup the bridge blendshape node
        #=================================================
        self.log_info('Facewrap bridge blendshapeNode setup')        
        try:#create 
            #Blendshape	
            bsNode_unified = deformers.buildBlendShapeNode( mi_bridgeBody.mNode,[mi_newMesh.mNode],'tmp')
            mi_bsNode = cgmMeta.validateObjArg(bsNode_unified,'cgmNode',setClass = True)
            mi_bsNode.addAttr('cgmName','bodyBridge',attrType='string',lock=True)    
            mi_bsNode.doName()
            attributes.doSetAttr(mi_bsNode.mNode,mi_newMesh.getShortName(),1)
            p.bsNode_bridgeBody = mi_bsNode.mNode	
            log.info('Body Bridge good!')
        except Exception,error:
            raise Exception,"Create fail | {0}".format(error)    
                
    except Exception,error:
        raise Exception,"bridge bsNode fail | {0}".format(error)       

    """#Blendshape	
    bsNode = deformers.buildBlendShapeNode(p.bridgeMainGeo,l_targets,'tmp')

    i_bsNode = cgmMeta.cgmNode(bsNode)
    i_bsNode.addAttr('cgmName','bridgeTargets',attrType='string',lock=True)    
    i_bsNode.addAttr('mClass','cgmNode',attrType='string',lock=True)
    i_bsNode.doName()
    attributes.doSetAttr(i_bsNode.mNode,i_target.getShortName(),1)#Turn it on	

    attrs = deformers.returnBlendShapeAttributes(i_bsNode.mNode)
    for a in attrs:
        self.log_info(a)
        attributes.doSetAttr(i_bsNode.mNode,a,1)#Turn it on	the bridge targets
    log.info('Bridge good!')

    return True       

    #>>> Check Face Bridge
    #================================================= 
    '''
    Make sure we have a faceBridge, create and connect 
    '''
    bridgeFaceBlendshapeNode = p.getMessage('bsNode_bridgeFace')
    bridgeFace= p.getMessage('geo_bridgeHead')

    if not bridgeFace:
        raise ValueError,"Should have geo_bridgeHead"
    
    try:#Create a bridge for the facial stuff to be wrapped to
        _d = self.md_coreGeo['unified']
        mi_newMesh = self.md_coreGeo['unified']['mi_reset'].doDuplicate(po = False)
        _d['mi_faceWrapBridge'] = mi_newMesh
        mi_newMesh.addAttr('cgmTypeModifier','faceWrapBridge',attrType='string',lock=True)                    
        mi_newMesh.doName()
        mi_newMesh.parent =  p.masterNull.bsGeoGroup
        p.doStore(_d['msg_reset'],mi_newMesh.mNode)  
        mi_newMesh.v = False
    except Exception,error:
        self.log_warning('miBase: {0}'.format( self.md_coreGeo['unified']['mi_base']))
        self.log_warning('mi_bridgeBody: {0}'.format( mi_bridgeBody))            
        raise Exception,"Face wrap bridge fail | {0}".format(error)        
    #Connect add that to the list of the blendshapes for the unified bsNOde
    #Create that unified bsNode
    #Weighting so only the face is affected


    l_targets.extend(p.getMessage('bridgeFaceGeo'))
    
    return

    #>>> Check Body Bridge
    #================================================= 
    bridgeBodyBlendshapeNode = p.getMessage('bsNode_bridgeBody')

    if not p.getMessage('bridgeBodyGeo'):
        newMesh = mc.duplicate(geo_unified)
        i_target = cgmMeta.cgmObject(newMesh[0])
        i_target.addAttr('cgmName','bodyBridge',attrType='string',lock=True)
        i_target.doName()
        i_target.parent = p.masterNull.bsGeoGroup.mNode#parent it
        p.bridgeBodyGeo = i_target.mNode
        i_target.visibility = False

    l_targets.extend(p.getMessage('bridgeBodyGeo'))



    #Blendshape	
    bsNode = deformers.buildBlendShapeNode(p.bridgeMainGeo,l_targets,'tmp')

    i_bsNode = cgmMeta.cgmNode(bsNode)
    i_bsNode.addAttr('cgmName','bridgeTargets',attrType='string',lock=True)    
    i_bsNode.addAttr('mClass','cgmNode',attrType='string',lock=True)
    i_bsNode.doName()
    attributes.doSetAttr(i_bsNode.mNode,i_target.getShortName(),1)#Turn it on	

    attrs = deformers.returnBlendShapeAttributes(i_bsNode.mNode)
    for a in attrs:
        self.log_info(a)
        attributes.doSetAttr(i_bsNode.mNode,a,1)#Turn it on	the bridge targets
    log.info('Bridge good!')

    return True    """

def _bs_body_(self):
    """ 
    Sets up body blendshapes. It lacks one whereas the face has one setup from the facial rig
    """ 
    # Get our base info
    #==================	        
    log.info(">>> go._bs_body_") 
    p = self.mi_network
    l_targets = self.d_bsGeoTargets['body']

    if not l_targets:
        log.error("No body geo targets found")
        return False

    #str_bridge = p.getMessage('geo_bridgeBody')
    #if not str_bridge:
        #log.error("No body bridge geo found")
        #return False
    
    #str_bridge = str_bridge[0]

    bsNode = deformers.buildBlendShapeNode(self.md_coreGeo['body']['mi_base'].mNode,l_targets,'tmp')

    i_bsNode = cgmMeta.cgmNode(bsNode)
    i_bsNode.addAttr('cgmName','bodyShapers',attrType='string',lock=True)    
    i_bsNode.addAttr('mClass','cgmNode',attrType='string',lock=True)
    #i_bsNode.addAttr('targetsGroup',targetGeoGroup,attrType='messageSimple',lock=True)

    i_bsNode.doName()
    p.bsNode_bridgeBody = i_bsNode.mNode

    #Add these to our obj set as well as the bsNode
    #p.objSet_all.addObj(i_bsNode.mNode)
    #attrs = deformers.returnBlendShapeAttributes(i_bsNode.mNode)
    #for a in attrs:
        #p.objSet_all.addObj("{0}.{1}".format(i_bsNode.mNode,a))

def _bs_face_(self):
    """ 
    Sets up face blendshapes
    """ 
    # Get our base info
    #==================	        
    log.info(">>> go._bs_face_") 
    p = self.mi_network
    
    
    try:#Setup the new blendshape node for our shapers
        l_targets = self.d_bsGeoTargets['face']
        mi_bridge = p.getMessageAsMeta('geo_bridgeFaceShapers')
        if not mi_bridge:
            log.error("No geo found on geo_bridgeFaceShapers")
            return False
        
        if not l_targets:
            log.error("No geo found")
            return False
    
        bsNode = deformers.buildBlendShapeNode(mi_bridge.mNode,l_targets,'tmp')
    
        i_bsNode = cgmMeta.cgmNode(bsNode)
        i_bsNode.addAttr('cgmName','faceShapers',attrType='string',lock=True)    
        i_bsNode.addAttr('mClass','cgmNode',attrType='string',lock=True)
        #i_bsNode.addAttr('targetsGroup',targetGeoGroup,attrType='messageSimple',lock=True)
        i_bsNode.doName()
        
        p.bsNode_faceShapers = i_bsNode.mNode
    except Exception,error:
        raise Exception,"shaper bsNode fail | {0}".format(error)
    
    mi_bsNode_faceBridge = p.getMessageAsMeta('bsNode_bridgeFace')
    log.info(mi_bsNode_faceBridge.mNode)
    l_existingTargets = deformers.returnBlendShapeAttributes(mi_bsNode_faceBridge.mNode)
    mc.blendShape( mi_bsNode_faceBridge.mNode, edit=True, t=(self.md_coreGeo['head']['mi_base'].mNode,
                                                             len(l_existingTargets)+1,#index
                                                             mi_bridge.mNode, 1.0) )
    
    
    #Add these to our obj set as well as the bsNode
    #p.objSet_all.addObj(i_bsNode.mNode)
    attrs = deformers.returnBlendShapeAttributes(mi_bsNode_faceBridge.mNode)
    for a in attrs:
        attributes.doSetAttr(mi_bsNode_faceBridge.mNode,a,1.0)
        #p.objSet_all.addObj("{0}.{1}".format(i_bsNode.mNode,a))

    #Add our blendshape target to the blendshape node on our

@r9General.Timer
def returnSkinJoints(self):
    assert self.cls == 'CustomizationFactory.go',"Not a CustomizationFactory.go instance!"
    assert mc.objExists(self.p.mNode),"Customization node no longer exists"
    log.info(">>> go.returnSkinJoints")  
    l_skinJoints  = ['pelvis_body_shaper']
    l_skinJoints.extend( search.returnChildrenJoints(l_skinJoints[0]) )
    log.info (l_skinJoints)
    self.l_skinJoints = l_skinJoints
    return l_skinJoints  


def doAddControlConstraints(self):
    """ 
    Segement orienter. Must have a JointFactory Instance
    """ 
    log.info(">>> doRigBody")
    # Get our base info
    #==================	        
    assert self.cls == 'CustomizationFactory.go',"Not a CustomizationFactory.go instance!"
    assert mc.objExists(self.p.mNode),"Customization node no longer exists"
    l_aimJoints = []
    for i,i_jnt in enumerate(self.p.jointList):
        constraintTypes = []
        constraintTargets = {}
        aimTargets = []
        aim_ijnt = False
        #Gather the info to set stuff up
        if i_jnt.hasAttr('constraintScaleTargets'):
            constraintTypes.append('scale')
            constraintTargets['scale'] = i_jnt.getMessage('constraintScaleTargets',False)	
        if i_jnt.hasAttr('constraintParentTargets'):
            constraintTypes.append('parent')
            #if 'scale' not in constraintTypes:
                #constraintTypes.append('scale')#parent type needs scale
                #constraintTargets['scale'] = i_jnt.getMessage('constraintParentTargets',False)			
            constraintTargets['parent'] = i_jnt.getMessage('constraintParentTargets',False)	    
        if i_jnt.hasAttr('constraintPointTargets'):
            constraintTypes.append('point')	    
            constraintTargets['point'] = i_jnt.getMessage('constraintPointTargets',False)
        if i_jnt.hasAttr('constraintOrientTargets'):
            constraintTypes.append('orient')	    
            constraintTargets['orient'] = i_jnt.getMessage('constraintOrientTargets',False)

        if i_jnt.hasAttr('constraintAimTargets'):
            l_aimJoints.append(i_jnt)
            #aimTargets = i_jnt.getMessage('constraintAimTargets',False)

        if constraintTypes:
            log.info("'%s' constraint list: %s"%(i_jnt.getShortName(), constraintTypes))

            #if 'aim' in constraintTypes and aimTargets:
                #aim_ijnt = True
                #constraintTypes.remove('aim')

            if constraintTypes and constraintTargets:
                #Need to pair through to see when constraints can be setup together
                constraintPairs = []
                cullList = copy.copy(constraintTypes)
                while cullList:
                    for C in cullList:
                        pairBuffer = []
                        cTargets = constraintTargets.get(C)	
                        for c in constraintTypes:
                            if cTargets == constraintTargets.get(c):
                                pairBuffer.append(c)
                                cullList.remove(c)
                        constraintPairs.append(pairBuffer)

                log.info("constraintPairs: %s"%constraintPairs)   
                for pair in constraintPairs:
                    targets = constraintTargets.get(pair[0])
                    log.info("%s targets: %s"%(pair, targets))
                    pBuffer = i_jnt.parent
                    i_prnt = cgmMeta.cgmObject(pBuffer)
                    i_prnt.addAttr('cgmTypeModifier','%sConstraint'%('_'.join(pair)),'string')	    
                    parentPBuffer = i_prnt.parent
                    i_prnt.parent = False
                    i_prnt.doName()	
                    """
		    if i_jnt.hasAttr('constraintAimTargets'):
			mode = 0
		    else:
			mode = 1
			"""
                    mode = 0
                    if i_jnt.controlPart == 'face':
                        constraints.doConstraintObjectGroup(targets,group = i_prnt.mNode,constraintTypes=pair,mode=1)		                        
                        i_prnt.parent = parentPBuffer
                    else:
                        constraints.doConstraintObjectGroup(targets,group = i_prnt.mNode,constraintTypes=pair,mode=mode)		

    if l_aimJoints:
        for i_jnt in l_aimJoints:
            if i_jnt.cgmName == 'ankleMeat':
                if i_jnt.cgmDirection == 'left':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = False, weight = 1, aimVector = [0,1,0], upVector = [0,0,1], worldUpVector = [0,0,-1], worldUpType = 'vector' )    		
                elif i_jnt.cgmDirection == 'right':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = False, weight = 1, aimVector = [0,-1,0], upVector = [0,0,-1], worldUpVector = [0,0,-1], worldUpType = 'vector' )    			    


            elif i_jnt.controlPart == 'face':
                """
                a  = cgmMeta.cgmObject()
                a.doGroup(True)
                i_jnt.doGroup(True)
                """
                if i_jnt.hasAttr('cgmDirection') and i_jnt.cgmDirection == 'left':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = True, weight = 1, aimVector = [0,0,1], upVector = [0,1,0], worldUpVector = [0,1,0], worldUpType = 'vector' )    
                elif i_jnt.hasAttr('cgmDirection') and i_jnt.cgmDirection == 'right':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = True, weight = 1, aimVector = [0,0,-1], upVector = [0,-1,0], worldUpVector = [0,1,0], worldUpType = 'vector' )    		
            else:
                if i_jnt.cgmDirection == 'left':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = False, weight = 1, aimVector = [0,0,1], upVector = [0,1,0], worldUpVector = [0,0,1], worldUpType = 'vector' )    
                elif i_jnt.cgmDirection == 'right':
                    constBuffer = mc.aimConstraint( i_jnt.getMessage('constraintAimTargets',False),i_jnt.mNode,maintainOffset = False, weight = 1, aimVector = [0,0,-1], upVector = [0,-1,0], worldUpVector = [0,0,1], worldUpType = 'vector' )    

            attributes.doSetLockHideKeyableAttr(i_jnt.mNode,channels = ['rx','ry','rz'])		


    mc.delete('controlCurves')

def doConnectVis(self):
    log.info(">>> doConnectVis")
    # Get our base info
    #==================	        
    assert self.cls == 'CustomizationFactory.go',"Not a CustomizationFactory.go instance!"
    assert mc.objExists(self.p.mNode),"Customization node no longer exists"
    p = self.p

    iVis = p.masterControl.controlVis

    for c in self.p.objSet_all.value:
        if '.' not in c:
            i_c = cgmMeta.cgmNode(c)
            i_attr = cgmMeta.cgmAttr(i_c,'visibility',hidden = True,lock = True)

            if i_c.hasAttr('cgmTypeModifier') and i_c.cgmTypeModifier == 'sub':
                if i_c.hasAttr('cgmDirection'):
                    if i_c.cgmDirection == 'left':
                        i_attr.doConnectIn("%s.leftSubControls_out"%iVis.mNode)
                    if i_c.cgmDirection == 'right':
                        i_attr.doConnectIn("%s.rightSubControls_out"%iVis.mNode)
                else:
                    i_attr.doConnectIn("%s.subControls_out"%iVis.mNode)

            else:
                if i_c.hasAttr('cgmDirection'):
                    if i_c.cgmDirection == 'left':
                        i_attr.doConnectIn("%s.leftControls_out"%iVis.mNode)
                    if i_c.cgmDirection == 'right':
                        i_attr.doConnectIn("%s.rightControls_out"%iVis.mNode)
                else:
                    i_attr.doConnectIn("%s.controls"%iVis.mNode)

def doLockNHide(self, customizationNode = 'MorpheusCustomization', unlock = False):
    """
    Lock and hides for a Morpheus customization asset
    """
    log.info(">>> doLockNHide")
    # Get our base info
    #==================	        
    try:
        customizationNode.mNode
        p = customizationNode
    except:
        if mc.objExists(customizationNode):
            p = r9Meta.MetaClass(customizationNode)
        else:
            p = MorphMeta.cgmMorpheusMakerNetwork(name = customizationNode)

    mayaMainProgressBar = guiFactory.doStartMayaProgressBar(len(self.mi_network.objSet_all.value))    
    for c in self.mi_network.objSet_all.value:        
        if '.' not in c and mc.ls(c, type='transform'):
            i_c = cgmMeta.cgmNode(c)
            if mc.progressBar(mayaMainProgressBar, query=True, isCancelled=True ) :
                break
            mc.progressBar(mayaMainProgressBar, edit=True, status = "LockNHide: '%s'"%i_c.getShortName(), step=1)            

            if unlock:
                for c in ['tx','ty','tz','rx','ry','rz','sx','sy','sz']:
                    cgmMeta.cgmAttr(i_c,c,lock=False,keyable=True,hidden=False)

                if i_c.hasAttr('radius'):
                    cgmMeta.cgmAttr(i_c,'radius',value = 5, hidden = False, keyable = False)
                    i_c.drawStyle = 0
            elif i_c.hasAttr('cgmName'):
                #Special Stuff
                str_name = i_c.cgmName
                if str_name in ['arm','head','face','eye','eyeOrb']:
                    attributes.doConnectAttr(('%s.scaleZ'%i_c.mNode),('%s.scaleX'%i_c.mNode),True)
                    attributes.doConnectAttr(('%s.scaleZ'%i_c.mNode),('%s.scaleY'%i_c.mNode),True)
                    cgmMeta.cgmAttr(i_c,'scaleX',lock=True,hidden=True)
                    cgmMeta.cgmAttr(i_c,'scaleY',lock=True,hidden=True)		    
                    zAttr = cgmMeta.cgmAttr(i_c,'scaleZ')
                    zAttr.p_nameAlias = '%sScale'%str_name                    
                    i_c.rotateOrder = 5#This is to fix a bug happening on scale cycle error

                #>>>All translates/rotates
                if str_name in ['ankleMeat','hipMeat','face']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['tx','ty','tz','rx','ry','rz'])                 


                #>>> Translates ==================================================== 
                #>>> all
                if str_name in ['upr_arm']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['tx','ty','tz'])	 		
                #>>> tx
                if str_name in ['quad','hamstring','lwr_leg','torsoMid','pelvis','sternum','shoulders','trapezius',
                                'foreArm','lwr_arm','hand','neck','head','elbowMeat',
                                'thumb_mid', 'thumb_2',
                                'index_mid', 'index_2',
                                'middle_mid', 'middle_2',
                                'ring_mid', 'ring_2',
                                'pinky_mid', 'pinky_2','lwrFace','shoulderMeat',
                                'noseTop', 'noseMid', 'noseBase', 'noseTip','neckTop','kneeMeat',
                                'brow', 'forehead', 'headTop','mouth', 'underLip','underNotse','cranium',
                                'mouthCavity', 'lwrFace', 'chin', 'headBack']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['tx'])	                 
                #>>> ty
                if str_name in ['ankle','toes','ball','heel','lwr_leg','bicep','tricep',#'neckTop',
                                'foreArm','elbowMeat','lwr_arm','hand','pectoral','brow','shoulderMeat',
                                'thumb_1', 'thumb_mid', 'thumb_2',
                                'index_1', 'index_mid', 'index_2',
                                'middle_1', 'middle_mid', 'middle_2','kneeMeat',
                                'ring_1', 'ring_mid', 'ring_2',
                                'pinky_1', 'pinky_mid', 'pinky_2']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['ty'])	
                #>>> tz
                if str_name in ['torsoMid','pelvis','sternum','shoulders','trapezius',
                                'neckTop',]:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['tz'])	

                #>>> Rotates ========================================================
                #> All rotates
                if str_name in ['ankle','toes','ball','heel','calf','lwr_leg','upr_leg','wristMeat',
                                'hamstring','quad','sternum','shoulders','upr_arm','lwr_arm',
                                'shoulderBlade','trapezius','bicep','tricep','foreArm',
                                'elbowMeat','hand', 'neck','pectoral','kneeMeat',
                                'thumb_1', 'thumb_mid', 'thumb_2',
                                'index_1', 'index_mid', 'index_2',
                                'middle_1', 'middle_mid', 'middle_2',
                                'ring_1', 'ring_mid', 'ring_2',#jaw,
                                'pinky_1', 'pinky_mid', 'pinky_2','cranium','lwrFace',
                                'brow', 'eyeOrb', 'mouth', 'mouthCavity']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['rx','ry','rz'])	
                #> rx
                if str_name in ['asdf','eye']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['rx'])
                #> ry
                if str_name in ['forehead','noseBase','eye','headTop','neckTop',]:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['ry'])
                #> rz
                if str_name in ['forehead','noseBase','headTop','neckTop']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['rz'])	

                #>>> Scales ========================================================
                #>>>sx
                if str_name in ['arm']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['sx'])                           
                #>>>sy
                if str_name in ['ankleMeat','quad','hamstring','cranium','lwrFace',
                                'arm','calf','sternum','noseTop','neck']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['sy'])            
                #>>>sz
                if str_name in ['hipMeat','wristMeat','kneeMeat','noseTop','lwrFace','ball','upr_arm']:
                    attributes.doSetLockHideKeyableAttr(i_c.mNode,channels = ['sz']) 

                if i_c.hasAttr('radius'):
                    cgmMeta.cgmAttr(i_c,'radius',value = 0, hidden = True, lock=True, keyable = False)
                    i_c.drawStyle = 7#make it a square so we can hide it

                #>>> Limits ==================================================== 
                #>>> all
                if str_name in ['eye']:
                    mc.transformLimits(i_c.mNode, sz = [-1,1], esz = [0,1])
                if str_name in ['lwrFace']:
                    mc.transformLimits(i_c.mNode, ty = [-2,0], ety = [1,0])
    for loc in mc.ls(type='locator'):
        mShape = cgmMeta.cgmNode(loc)
        str_transform =  mShape.getTransform()
        mLoc = cgmMeta.cgmObject(str_transform)
        for a in ['tx','ty','tz','rx','ry','rz','sx','sy','sz']:
            cgmMeta.cgmAttr(str_transform,a,lock=not unlock,keyable=True,hidden=False)
        mLoc.v = unlock


    guiFactory.doEndMayaProgressBar(mayaMainProgressBar)
    #mc.cycleCheck(e=False)
    log.warning("Cycle check turned off")

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
# Utilities
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
def autoTagControls(customizationNode = 'MorpheusCustomization',l_controls=None): 
    """
    For each joint:
    1) tag it mClass as cgmObject
    2) Find the closest curve
    3) Tag that curve as a cgmObject mClass
    """
    # Get our base info
    #==============	        
    #>>> module null data 
    assert mc.objExists(customizationNode),"'%s' doesn't exist"%customizationNode
    log.info(">>> autoTagControls")
    log.info("l_joints: %s"%customizationNode)
    log.info("l_controls: %s"%l_controls)
    p = r9Meta.MetaClass(customizationNode)
    buffer = p.getMessage('jointList')
    for o in buffer:
        i_o = cgmMeta.cgmObject(o)
        i_o.addAttr('mClass','cgmObject',attrType = 'string', lock = True)    
    if l_controls is None:#If we don't have any controls passed
        l_iControls = []
        if mc.objExists('controlCurves'):#Check the standard group
            l_controls = search.returnAllChildrenObjects('controlCurves')
        for c in l_controls:
            i_c = cgmMeta.cgmObject(c)
            i_c.addAttr('mClass','cgmObject',attrType = 'string', lock = True)
            l_iControls.append(i_c.mNode)
        #p.addAttr('controlCurves',attrType = 'message', value = l_iControls)
        p.controlCurves = l_iControls
    for i_o in p.jointList:
        closestObject = distance.returnClosestObject(i_o.mNode,p.getMessage('controlCurves'))
        if closestObject:
            log.info("'%s' <<tagging>> '%s'"%(i_o.getShortName(),closestObject))            
            i_closestObject = cgmMeta.cgmObject(closestObject)
            i_closestObject.addAttr('mClass','cgmObject',attrType = 'string', lock = True)
            i_o.addAttr('controlCurve',value = i_closestObject.mNode,attrType = 'messageSimple', lock = True)
            i_closestObject.addAttr('cgmSource',value = i_o.mNode,attrType = 'messageSimple', lock = True)
            i_closestObject.doCopyNameTagsFromObject(i_o.mNode,['cgmType','cgmTypeModifier'])

            i_o.addAttr('cgmType','shaper',attrType='string',lock = True)            
            i_closestObject.addAttr('cgmType','bodyShaper',attrType='string',lock = True)

            i_o.doName()
            i_closestObject.doName()

def doTagCurveFromJoint(curve = None, joint = None): 
    """
    """
    # Get our base info
    #==============	        
    #>>> module null data 
    if curve is None:
        curve = mc.ls(sl=True)[1] or False
    if joint is None:
        joint = mc.ls(sl=True)[0] or False

    assert mc.objExists(curve),"'%s' doesn't exist"%curve
    assert mc.objExists(joint),"'%s' doesn't exist"%joint

    log.info(">>> doTagCurveFromJoint")
    log.info("curve: %s"%curve)
    log.info("joint: %s"%joint)
    i_o = cgmMeta.cgmObject(joint)
    i_o.addAttr('mClass','cgmObject',attrType = 'string', lock = True)

    i_crv = cgmMeta.cgmObject(curve)
    i_crv.addAttr('mClass','cgmObject',attrType = 'string', lock = True)
    i_o.addAttr('controlCurve',value = i_crv.mNode,attrType = 'messageSimple', lock = True)
    i_crv.addAttr('cgmSource',value = i_o.mNode,attrType = 'messageSimple', lock = True)
    i_crv.doCopyNameTagsFromObject(i_o.mNode,['cgmType','cgmTypeModifier'])

    #i_o.addAttr('cgmTypeModifier','shaper',attrType='string',lock = True)            
    i_crv.addAttr('cgmType','shaper',attrType='string',lock = True)

    i_o.doName()
    i_crv.doName()
    return True

def doTagParentContrainToTargets(obj = None, targets = None): 
    """
    """
    # Get our base info
    #==============	        
    #>>> module null data 
    if obj is None:
        obj = mc.ls(sl=True)[-1] or False
    if targets is None:
        targets = mc.ls(sl=True)[:-1] or False

    assert mc.objExists(obj),"'%s' doesn't exist"%obj
    assert targets,"No targets found"
    for t in targets:
        assert mc.objExists(t),"'%s' doesn't exist"%t

    log.info(">>> doTagContrainToTargets")
    log.info("obj: %s"%obj)
    log.info("targets: %s"%targets)
    i_o = cgmMeta.cgmObject(obj)
    i_o.addAttr('mClass','cgmObject',attrType = 'string', lock = True)

    i_o.addAttr('constraintParentTargets',attrType='message',value = targets)
    return True

def doTagAimContrainToTargets(obj = None, targets = None): 
    """
    """
    # Get our base info
    #==============	        
    #>>> module null data 
    if obj is None:
        obj = mc.ls(sl=True)[-1] or False
    if targets is None:
        targets = mc.ls(sl=True)[:-1] or False

    assert mc.objExists(obj),"'%s' doesn't exist"%obj
    assert targets,"No targets found"
    for t in targets:
        assert mc.objExists(t),"'%s' doesn't exist"%t

    log.info(">>> doTagContrainToTargets")
    log.info("obj: %s"%obj)
    log.info("targets: %s"%targets)
    i_o = cgmMeta.cgmObject(obj)
    i_o.addAttr('mClass','cgmObject',attrType = 'string', lock = True)

    i_o.addAttr('constraintAimTargets',attrType='message',value = targets)
    return True

def doTagPointContrainToTargets(obj = None, targets = None): 
    """
    """
    # Get our base info
    #==============	        
    #>>> module null data 
    if obj is None:
        obj = mc.ls(sl=True)[-1] or False
    if targets is None:
        targets = mc.ls(sl=True)[:-1] or False

    assert mc.objExists(obj),"'%s' doesn't exist"%obj
    assert targets,"No targets found"
    for t in targets:
        assert mc.objExists(t),"'%s' doesn't exist"%t

    log.info(">>> doTagContrainToTargets")
    log.info("obj: %s"%obj)
    log.info("targets: %s"%targets)
    i_o = cgmMeta.cgmObject(obj)
    i_o.addAttr('mClass','cgmObject',attrType = 'string', lock = True)

    i_o.addAttr('constraintPointTargets',attrType='message',value = targets)
    return True

def doTagContrainToTargets(obj = None, targets = None, constraintTypes = ['point']): 
    """
    """
    # Get our base info
    #==============	        
    #>>> module null data 
    if obj is None:
        obj = mc.ls(sl=True)[-1] or False
    if targets is None:
        targets = mc.ls(sl=True)[:-1] or False

    assert mc.objExists(obj),"'%s' doesn't exist"%obj
    assert targets,"No targets found"
    for t in targets:
        assert mc.objExists(t),"'%s' doesn't exist"%t

    log.info(">>> doTagContrainToTargets")
    log.info("obj: %s"%obj)
    log.info("targets: %s"%targets)
    i_o = cgmMeta.cgmObject(obj)
    i_o.addAttr('mClass','cgmObject',attrType = 'string', lock = True)

    for c in constraintTypes:
        if c in ['point','aim','scale','parent','orient']:
            i_o.addAttr('constraint%sTargets'%c.capitalize(),attrType='message',value = targets)
        else:
            log.warning("'%s' not a known constraint type"%c)
    return True

@r9General.Timer
def updateTransform(i_curve,i_sourceObject):
    log.info(">>> updateTransform: '%s'"%i_curve.getShortName())    
    childrenToWorld = []	
    children = mc.listRelatives(i_curve.mNode,children=True,type = 'transform')
    if children:
        for c in children:
            childrenToWorld.append(rigging.doParentToWorld(c))
    transform = rigging.groupMeObject(i_sourceObject.mNode,False,False)
    i_transform = cgmMeta.cgmObject(transform)
    for attr in i_curve.getUserAttrs():
        attributes.doCopyAttr(i_curve.mNode,attr,transform)
    for attr in i_sourceObject.getUserAttrs():
        attributes.doCopyAttr(i_sourceObject.mNode,attr,transform)
    buffer = curves.parentShapeInPlace(transform,i_curve.mNode)
    mc.delete(i_curve.mNode)
    if childrenToWorld:
        for c in childrenToWorld:
            rigging.doParentReturnName(c,transform)
    return i_transform.mNode

def reportContrainToTags():
    for o in mc.ls(sl=True):
        i_jnt = cgmMeta.cgmNode(o)

        #Gather the info to set stuff up
        if i_jnt.hasAttr('constraintScaleTargets'):
            log.info("'%s' constraintScaleTargets: %s"%(o,i_jnt.getMessage('constraintScaleTargets',False)))
        if i_jnt.hasAttr('constraintParentTargets'):
            log.info("'%s' constraintParentTargets: %s"%(o,i_jnt.getMessage('constraintParentTargets',False)))

        if i_jnt.hasAttr('constraintPointTargets'):
            log.info("'%s' constraintPointTargets: %s"%(o,i_jnt.getMessage('constraintPointTargets',False)))

        if i_jnt.hasAttr('constraintOrientTargets'):
            log.info("'%s' constraintOrientTargets: %s"%(o,i_jnt.getMessage('constraintOrientTargets',False)))

        if i_jnt.hasAttr('constraintAimTargets'):
            log.info("'%s' constraintAimTargets: %s"%(o,i_jnt.getMessage('constraintAimTargets',False)))
            

def flag_bsWiring():pass

__attrHolder = 'customization_attrHolder'
_d_shaperBufferAttributes = {"eyebrow":{'attrs':['inr_thin','inr_thick','mid_thin','mid_thick','outr_thin','outr_thick',
                                                 'long','shape']},
                             'ear':{'attrs':['lwr_out','lwr_in','big','lobe',
                                             'upr_out','upr_in','pointed',
                                             'upr_squared','lwr_squared'],
                                    'sideAttrs':'*'},
                             "teethUpr":{'attrs':['fang','buck','small','big','shaped','snaggle','space','gums']},
                             "teethLwr":{'attrs':['fang','buck','small','big','shaped','snaggle','space','gums']},                             
                             "brow":{'attrs':['out','in']},
                             "cheek":{'attrs':['bones_out','bones_in','hollow','round_outr','round_inr','jowels']},
                             "jaw":{'attrs':['weak','strong']},
                             "lids":{'attrs':['heavy','lwr_back']},
                             'chin':{'attrs':['weak','strong','out','wide','narrow','cleft','round']},
                             'lips':{'attrs':['big_upr_inr','big_upr_outr','big_lwr_inr','big_lwr_outr',
                                              'thick_upr_inr','thick_upr_outr','thick_lwr_inr','thick_lwr_outr']},
                             "noseBulb":{'attrs':['thick','thin','flat','out','long','inflate','facet','tall','short']},
                             "noseUnder":{'attrs':['thick','thin','up','out']},  
                             "noseBridge":{'attrs':['thick','thin','flat','out','facet']},
                             "nostrils":{'attrs':['in','wide','out','flat','tall','gone']},
                             
                             "neckTop":{'attrs':['define','facet','thick','in','out']},                             
                             "neck":{'attrs':['define','facet','in','out']},
                             "torsoUpr":{'attrs':['facet','smooth','back','chest','back_in','back_out','chest_in','chest_out']},
                             "torsoLwr":{'attrs':['facet','smooth','bottom','abs','bottom_in','bottom_out','abs_in','abs_out']},
                             "hip":{'attrs':['facet','smooth',
                                             'back_define','back_in','back_out',
                                             'front_define','front_in','front_out'],'sideAttrs':'*'},
                             "legUpr":{'attrs':['facet','smooth','quad','hamstring','quad_in','quad_out','hamstring_in','hamstring_out'],'sideAttrs':'*'},
                             "legLwr":{'attrs':['facet','smooth','calf','shin','calf_in','calf_out','shin_in','shin_out'],'sideAttrs':'*'},
                             "ankle":{'attrs':['facet','smooth','define','in','out'],'sideAttrs':'*'}, 
                             
                             "shoulder":{'attrs':['facet','smooth','define','in','out'],'sideAttrs':'*'}, 
                             "armUpr":{'attrs':['facet','smooth','bicep','tricep','bicep_in','bicep_out','tricep_in','tricep_out'],'sideAttrs':'*'},
                             "armLwr":{'attrs':['facet','smooth','define','in','out'],'sideAttrs':'*'},
                             "wrist":{'attrs':['facet','define','in','out'],'sideAttrs':'*'},
                             
                             "thumb":{'attrs':['out','in','wide','narrow','knuckles','facet'],'sideAttrs':'*'},
                             "fingers":{'attrs':['out','in','wide','narrow','knuckles','facet'],'sideAttrs':'*'},                             
                             }
'''
"eyebrow":{'attrs':['inr_thin','inr_thick','mid_thin','mid_thick','outr_thin','outr_thick',
                    'long','shape']},
'ear':{'attrs':['lwr_out','lwr_in','big','lobe',
                'upr_out','upr_in','pointed',
                'upr_squared','lwr_squared'],
       'sideAttrs':'*'},
"teethUpr":{'attrs':['fang','buck','small','big','shape','snaggle','space','gums']},
"teethLwr":{'attrs':['fang','buck','small','big','shape','snaggle','space','gums']},
'''
_d_shaperControlsToConnect = { 'ear_left':{'control':'l_ear_bodyShaper',
                                           'wiringDict':{'ear_upr_out_left':{'driverAttr':'upr_out'},
                                                         'ear_upr_in_left':{'driverAttr':'-upr_out'},
                                                         'ear_upr_squared_left':{'driverAttr':'upr_squared'},                                                                                                            
                                                         'ear_lwr_out_left':{'driverAttr':'lwr_out'},
                                                         'ear_lwr_in_left':{'driverAttr':'-lwr_out'},
                                                         'ear_lwr_squared_left':{'driverAttr':'lwr_squared'},
                                                         'ear_big_left':{'driverAttr':'big'},                                                      
                                                         'ear_lobe_left':{'driverAttr':'lobe'},                                                      
                                                         'ear_pointed_left':{'driverAttr':'pointed'},                                                      
                                                         }},
                               'ear_right':{'control':'r_ear_bodyShaper',
                                           'wiringDict':{'ear_upr_out_right':{'driverAttr':'upr_out'},
                                                         'ear_upr_in_right':{'driverAttr':'-upr_out'},
                                                         'ear_upr_squared_right':{'driverAttr':'upr_squared'},                                                                                                            
                                                         'ear_lwr_out_right':{'driverAttr':'lwr_out'},
                                                         'ear_lwr_in_right':{'driverAttr':'-lwr_out'},
                                                         'ear_lwr_squared_right':{'driverAttr':'lwr_squared'},
                                                         'ear_big_right':{'driverAttr':'big'},                                                      
                                                         'ear_lobe_right':{'driverAttr':'lobe'},                                                      
                                                         'ear_pointed_right':{'driverAttr':'pointed'},                                                      
                                                         }},
                               
                              'eyebrow':{'control':'brow_bodyShaper',
                                         'wiringDict':{'eyebrow_inr_thin':{'driverAttr':'-eyebrow_inr'},
                                                       'eyebrow_inr_thick':{'driverAttr':'eyebrow_inr'},
                                                       'eyebrow_mid_thin':{'driverAttr':'-eyebrow_mid'},
                                                       'eyebrow_mid_thick':{'driverAttr':'eyebrow_mid'},
                                                       'eyebrow_outr_thin':{'driverAttr':'-eyebrow_outr'},
                                                       'eyebrow_outr_thick':{'driverAttr':'eyebrow_outr'},
                                                       'eyebrow_long':{'driverAttr':'eyebrow_long','noTrack':True},
                                                       'eyebrow_shape':{'driverAttr':'eyebrow_raise'},
                                                       }},
                              'teeth':{'control':'mouthCavity_bodyShaper',
                                      'wiringDict':{'teethUpr_fang':{'driverAttr':'upr_fang'},
                                                    'teethUpr_buck':{'driverAttr':'upr_buck'},
                                                    'teethUpr_big':{'driverAttr':'upr_big'},
                                                    'teethUpr_small':{'driverAttr':'-upr_big'},
                                                    'teethUpr_shaped':{'driverAttr':'upr_shaped'},
                                                    'teethUpr_snaggle':{'driverAttr':'upr_snaggle'},
                                                    'teethUpr_space':{'driverAttr':'upr_space'},
                                                    'teethUpr_gums':{'driverAttr':'upr_gums'},
                                                    
                                                    'teethLwr_fang':{'driverAttr':'lwr_fang'},
                                                    'teethLwr_buck':{'driverAttr':'lwr_buck'},
                                                    'teethLwr_big':{'driverAttr':'lwr_big'},
                                                    'teethLwr_small':{'driverAttr':'-lwr_big'},
                                                    'teethLwr_shaped':{'driverAttr':'lwr_shaped'},
                                                    'teethLwr_snaggle':{'driverAttr':'lwr_snaggle'},
                                                    'teethLwr_space':{'driverAttr':'lwr_space'},
                                                    'teethLwr_gums':{'driverAttr':'lwr_gums'},                      
                                                    }},
                              
                              'brow':{'control':'cranium_bodyShaper',
                                      'wiringDict':{'brow_out':{'driverAttr':'out'},
                                                    'brow_in':{'driverAttr':'-out'},
                                                    'cheek_bones_out':{'driverAttr':'cheekBones'},
                                                    'cheek_bones_in':{'driverAttr':'-cheekBones'},                                                    
                                                    'lids_lwr_back':{'driverAttr':'lids_lwr_back', 'noTrack':True},                                                    
                                                    'lids_heavy':{'driverAttr':'lids_heavy','noTrack':True}}},
                              'lips':{'control':'mouth_bodyShaper',
                                      'wiringDict':{'lips_big_upr_inr':{'driverAttr':'big_upr_inr'},
                                                    'lips_big_upr_outr':{'driverAttr':'big_upr_outr'},
                                                    'lips_big_lwr_inr':{'driverAttr':'big_lwr_inr'},
                                                    'lips_big_lwr_outr':{'driverAttr':'big_lwr_outr'},
                                                    'lips_thick_upr_inr':{'driverAttr':'thick_upr_inr'},
                                                    'lips_thick_upr_outr':{'driverAttr':'thick_upr_outr'},
                                                    'lips_thick_lwr_inr':{'driverAttr':'thick_lwr_inr'},
                                                    'lips_thick_lwr_outr':{'driverAttr':'thick_lwr_outr'}}},                              
                              'chin':{'control':'chin_bodyShaper',
                                      'wiringDict':{'chin_weak':{'driverAttr':'-strong'},
                                                    'chin_strong':{'driverAttr':'strong'},
                                                    'chin_round':{'driverAttr':'round'},
                                                    'chin_out':{'driverAttr':'out'},
                                                    'chin_wide':{'driverAttr':'wide'},
                                                    'chin_narrow':{'driverAttr':'-wide'},                                                    
                                                    'chin_cleft':{'driverAttr':'cleft'}}},
                              'lwrFace':{'control':'lwrFace_bodyShaper',
                                         'wiringDict':{'jaw_weak':{'driverAttr':'-jaw_strong'},
                                                       'jaw_strong':{'driverAttr':'jaw_strong','noTrack':True},
                                                       'cheek_hollow':{'driverAttr':'cheekHollow'}, 
                                                       'cheek_round_outr':{'driverAttr':'cheekRound_outr'}, 
                                                       'cheek_round_inr':{'driverAttr':'cheekRound_innr'},                                                        
                                                       'cheek_jowels':{'driverAttr':'jowels'}}}, 
                                                                                   
                            
                              'noseBulb':{'control':'nose_bodyShaper',
                                          'wiringDict':{'noseBulb_thick':{'driverAttr':'thick'},
                                                        'noseBulb_thin':{'driverAttr':'-thick'},
                                                        'noseBulb_flat':{'driverAttr':'-out'},
                                                        'noseBulb_out':{'driverAttr':'out'},
                                                        'noseBulb_long':{'driverAttr':'long'},
                                                        'noseBulb_short':{'driverAttr':'-tall'},
                                                        'noseBulb_tall':{'driverAttr':'tall'},
                                                        'noseBulb_inflate':{'driverAttr':'inflate'},
                                                        'noseBulb_facet':{'driverAttr':'facet'}}},
                              'noseUnder':{'control':'underNose_sub_bodyShaper',
                                         'wiringDict':{'noseUnder_thick':{'driverAttr':'thick'},
                                                       'noseUnder_thin':{'driverAttr':'-thick'},
                                                       'noseUnder_up':{'driverAttr':'up'},
                                                       'noseUnder_out':{'driverAttr':'out'}}},                               
                              'noseBridge':{'control':'noseTop_bodyShaper',
                                      'wiringDict':{'noseBridge_thick':{'driverAttr':'thick'},
                                                    'noseBridge_thin':{'driverAttr':'-thick'},
                                                    'noseBridge_flat':{'driverAttr':'-out'},
                                                    'noseBridge_out':{'driverAttr':'out'},
                                                    'noseBridge_facet':{'driverAttr':'facet'}}},                               
                              'noseBase':{'control':'noseBase_bodyShaper',
                                          'wiringDict':{'nostrils_in':{'driverAttr':'-wide'},
                                                        'nostrils_wide':{'driverAttr':'wide'},
                                                        'nostrils_flat':{'driverAttr':'-out'},
                                                        'nostrils_out':{'driverAttr':'out'},
                                                        'nostrils_gone':{'driverAttr':'gone'},
                                                        'nostrils_tall':{'driverAttr':'tall'}}},
                                                        
                              
                              'neckTop':{'control':'neckTop_bodyShaper',
                                         'wiringDict':{'neckTop_in':{'driverAttr':'-shape'},
                                                       'neckTop_out':{'driverAttr':'shape'},
                                                       'neckTop_thick':{'driverAttr':'thick'},
                                                       'neckTop_facet':{'driverAttr':'facet'},
                                                       'neckTop_define':{'driverAttr':'define'}}},                                  
                              'neck':{'control':'neck_bodyShaper',
                                      'wiringDict':{'neck_in':{'driverAttr':'-shape'},
                                                    'neck_out':{'driverAttr':'shape'},
                                                    'neck_facet':{'driverAttr':'facet'},
                                                    'neck_define':{'driverAttr':'define'}}},                                 
                              'torsoUpr':{'control':'shoulders_bodyShaper',
                                          'wiringDict':{'torsoUpr_back_in':{'driverAttr':'-back_shape'},
                                                        'torsoUpr_back_out':{'driverAttr':'back_shape'},
                                                        'torsoUpr_chest_in':{'driverAttr':'-chest_shape'},
                                                        'torsoUpr_chest_out':{'driverAttr':'chest_shape'},
                                                        'torsoUpr_back':{'driverAttr':'back'},
                                                        'torsoUpr_chest':{'driverAttr':'chest'},
                                                        'torsoUpr_facet':{'driverAttr':'facet'},
                                                        'torsoUpr_smooth':{'driverAttr':'smooth'}}},
                              'torsoLwr':{'control':'torsoMid_bodyShaper',
                                          'wiringDict':{'torsoLwr_abs_in':{'driverAttr':'-abs_shape'},
                                                        'torsoLwr_abs_out':{'driverAttr':'abs_shape'},
                                                        'torsoLwr_bottom_in':{'driverAttr':'-bottom_shape'},
                                                        'torsoLwr_bottom_out':{'driverAttr':'bottom_shape'},
                                                        'torsoLwr_bottom':{'driverAttr':'bottom'},
                                                        'torsoLwr_abs':{'driverAttr':'abs'},
                                                        'torsoLwr_facet':{'driverAttr':'facet'},
                                                        'torsoLwr_smooth':{'driverAttr':'smooth'}}},
                              
                              'hip_left':{'control':'l_upr_leg_bodyShaper',
                                          'wiringDict':{'hip_front_in_left':{'driverAttr':'-front_shape'},
                                                        'hip_front_out_left':{'driverAttr':'front_shape'},
                                                        'hip_front_define_left':{'driverAttr':'front_define'},
                                                        'hip_back_in_left':{'driverAttr':'-back_shape'},
                                                        'hip_back_out_left':{'driverAttr':'back_shape'},
                                                        'hip_back_define_left':{'driverAttr':'back_define'},                                                        
                                                        'hip_facet_left':{'driverAttr':'facet'},
                                                        'hip_smooth_left':{'driverAttr':'smooth'}}},
                              'hip_right':{'control':'r_upr_leg_bodyShaper',
                                           'wiringDict':{'hip_front_in_right':{'driverAttr':'-front_shape'},
                                                         'hip_front_out_right':{'driverAttr':'front_shape'},
                                                         'hip_front_define_right':{'driverAttr':'front_define'},
                                                         'hip_back_in_right':{'driverAttr':'-back_shape'},
                                                         'hip_back_out_right':{'driverAttr':'back_shape'},
                                                         'hip_back_define_right':{'driverAttr':'back_define'},  
                                                         'hip_facet_right':{'driverAttr':'facet'},
                                                         'hip_smooth_right':{'driverAttr':'smooth'}}}, 
                              
                              'legUpr_left':{'control':'l_kneeMeat_sub_bodyShaper',
                                             'wiringDict':{'legUpr_quad_in_left':{'driverAttr':'-quad_shape'},
                                                           'legUpr_quad_out_left':{'driverAttr':'quad_shape'},
                                                           'legUpr_hamstring_in_left':{'driverAttr':'-hamstring_shape'},
                                                           'legUpr_hamstring_out_left':{'driverAttr':'hamstring_shape'},
                                                           'legUpr_quad_left':{'driverAttr':'quad'},
                                                           'legUpr_hamstring_left':{'driverAttr':'hamstring'},
                                                           'legUpr_facet_left':{'driverAttr':'facet'},
                                                           'legUpr_smooth_left':{'driverAttr':'smooth'}}},
                              'legUpr_right':{'control':'r_kneeMeat_sub_bodyShaper',
                                              'wiringDict':{'legUpr_quad_in_right':{'driverAttr':'-quad_shape'},
                                                            'legUpr_quad_out_right':{'driverAttr':'quad_shape'},
                                                            'legUpr_hamstring_in_right':{'driverAttr':'-hamstring_shape'},
                                                            'legUpr_hamstring_out_right':{'driverAttr':'hamstring_shape'},
                                                            'legUpr_quad_right':{'driverAttr':'quad'},
                                                            'legUpr_hamstring_right':{'driverAttr':'hamstring'},
                                                            'legUpr_facet_right':{'driverAttr':'facet'},
                                                            'legUpr_smooth_right':{'driverAttr':'smooth'}}},
                              
                              'legLwr_left':{'control':'l_lwr_leg_bodyShaper',
                                             'wiringDict':{'legLwr_shin_in_left':{'driverAttr':'-shin_shape'},
                                                           'legLwr_shin_out_left':{'driverAttr':'shin_shape'},
                                                           'legLwr_calf_in_left':{'driverAttr':'-calf_shape'},
                                                           'legLwr_calf_out_left':{'driverAttr':'calf_shape'},
                                                           'legLwr_calf_left':{'driverAttr':'calf'},
                                                           'legLwr_shin_left':{'driverAttr':'shin'},
                                                           'legLwr_facet_left':{'driverAttr':'facet'},
                                                           'legLwr_smooth_left':{'driverAttr':'smooth'}}},
                              'legLwr_right':{'control':'r_lwr_leg_bodyShaper',
                                              'wiringDict':{'legLwr_shin_in_right':{'driverAttr':'-shin_shape'},
                                                            'legLwr_shin_out_right':{'driverAttr':'shin_shape'},
                                                            'legLwr_calf_in_right':{'driverAttr':'-calf_shape'},
                                                            'legLwr_calf_out_right':{'driverAttr':'calf_shape'},
                                                            'legLwr_calf_right':{'driverAttr':'calf'},
                                                            'legLwr_shin_right':{'driverAttr':'shin'},
                                                            'legLwr_facet_right':{'driverAttr':'facet'},
                                                            'legLwr_smooth_right':{'driverAttr':'smooth'}}},
                              
                              'ankle_left':{'control':'l_ankleMeat_bodyShaper',
                                             'wiringDict':{'ankle_in_left':{'driverAttr':'-shape'},
                                                           'ankle_out_left':{'driverAttr':'shape'},
                                                           'ankle_smooth_left':{'driverAttr':'smooth'},
                                                           'ankle_facet_left':{'driverAttr':'facet'},
                                                           'ankle_define_left':{'driverAttr':'define'}}},
                              'ankle_right':{'control':'r_ankleMeat_bodyShaper',
                                             'wiringDict':{'ankle_in_right':{'driverAttr':'-shape'},
                                                           'ankle_out_right':{'driverAttr':'shape'},
                                                           'ankle_smooth_right':{'driverAttr':'smooth'},
                                                           'ankle_facet_right':{'driverAttr':'facet'},
                                                           'ankle_define_right':{'driverAttr':'define'}}},                              

                              'hand_left':{'control':'l_hand_bodyShaper',
                                           'wiringDict':{'fingers_in_left':{'driverAttr':'-finger_shape'},
                                                         'fingers_out_left':{'driverAttr':'finger_shape'},
                                                         'fingers_wide_left':{'driverAttr':'finger_taper'},
                                                         'fingers_narrow_left':{'driverAttr':'-finger_taper'},
                                                         'fingers_knuckles_left':{'driverAttr':'finger_knuckles'},
                                                         'fingers_facet_left':{'driverAttr':'finger_facet'},
                                                         
                                                         'thumb_in_left':{'driverAttr':'-thmb_shape'},
                                                         'thumb_out_left':{'driverAttr':'thmb_shape'},
                                                         'thumb_wide_left':{'driverAttr':'thmb_taper'},
                                                         'thumb_narrow_left':{'driverAttr':'-thmb_taper'},
                                                         'thumb_knuckles_left':{'driverAttr':'thmb_knuckles'},
                                                         'thumb_facet_left':{'driverAttr':'thmb_facet'},}},   
                              'hand_right':{'control':'r_hand_bodyShaper',
                                           'wiringDict':{'fingers_in_right':{'driverAttr':'-finger_shape'},
                                                         'fingers_out_right':{'driverAttr':'finger_shape'},
                                                         'fingers_wide_right':{'driverAttr':'finger_taper'},
                                                         'fingers_narrow_right':{'driverAttr':'-finger_taper'},
                                                         'fingers_knuckles_right':{'driverAttr':'finger_knuckles'},
                                                         'fingers_facet_right':{'driverAttr':'finger_facet'},
                                                         
                                                         'thumb_in_right':{'driverAttr':'-thmb_shape'},
                                                         'thumb_out_right':{'driverAttr':'thmb_shape'},
                                                         'thumb_wide_right':{'driverAttr':'thmb_taper'},
                                                         'thumb_narrow_right':{'driverAttr':'-thmb_taper'},
                                                         'thumb_knuckles_right':{'driverAttr':'thmb_knuckles'},
                                                         'thumb_facet_right':{'driverAttr':'thmb_facet'},}},                                    
                              
                              
                              'shoulder_left':{'control':'l_shoulderMeat_bodyShaper',
                                               'wiringDict':{'shoulder_in_left':{'driverAttr':'-shape'},
                                                             'shoulder_out_left':{'driverAttr':'shape'},
                                                             'shoulder_define_left':{'driverAttr':'define'},
                                                             'shoulder_facet_left':{'driverAttr':'facet'},
                                                             'shoulder_smooth_left':{'driverAttr':'smooth'}}},
                              'shoulder_right':{'control':'r_shoulderMeat_bodyShaper',
                                                'wiringDict':{'shoulder_in_right':{'driverAttr':'-shape'},
                                                              'shoulder_out_right':{'driverAttr':'shape'},
                                                              'shoulder_define_right':{'driverAttr':'define'},
                                                              'shoulder_facet_right':{'driverAttr':'facet'},
                                                              'shoulder_smooth_right':{'driverAttr':'smooth'}}}, 
                              
                              'armUpr_left':{'control':'l_upr_arm_bodyShaper',
                                             'wiringDict':{'armUpr_bicep_in_left':{'driverAttr':'-bicep_shape'},
                                                           'armUpr_bicep_out_left':{'driverAttr':'bicep_shape'},
                                                           'armUpr_tricep_in_left':{'driverAttr':'-tricep_shape'},
                                                           'armUpr_tricep_out_left':{'driverAttr':'tricep_shape'},
                                                           'armUpr_bicep_left':{'driverAttr':'bicep'},
                                                           'armUpr_tricep_left':{'driverAttr':'tricep'},
                                                           'armUpr_facet_left':{'driverAttr':'facet'},
                                                           'armUpr_smooth_left':{'driverAttr':'smooth'}}},
                              'armUpr_right':{'control':'r_upr_arm_bodyShaper',
                                              'wiringDict':{'armUpr_bicep_in_right':{'driverAttr':'-bicep_shape'},
                                                            'armUpr_bicep_out_right':{'driverAttr':'bicep_shape'},
                                                            'armUpr_tricep_in_right':{'driverAttr':'-tricep_shape'},
                                                            'armUpr_tricep_out_right':{'driverAttr':'tricep_shape'},
                                                            'armUpr_bicep_right':{'driverAttr':'bicep'},
                                                            'armUpr_tricep_right':{'driverAttr':'tricep'},
                                                            'armUpr_facet_right':{'driverAttr':'facet'},
                                                            'armUpr_smooth_right':{'driverAttr':'smooth'}}},
                              
                              'armLwr_left':{'control':'l_lwr_arm_bodyShaper',
                                             'wiringDict':{'armLwr_in_left':{'driverAttr':'-shape'},
                                                           'armLwr_out_left':{'driverAttr':'shape'},
                                                           'armLwr_define_left':{'driverAttr':'define'},
                                                           'armLwr_facet_left':{'driverAttr':'facet'},
                                                           'armLwr_smooth_left':{'driverAttr':'smooth'}}},
                              'armLwr_right':{'control':'r_lwr_arm_bodyShaper',
                                             'wiringDict':{'armLwr_in_right':{'driverAttr':'-shape'},
                                                           'armLwr_out_right':{'driverAttr':'shape'},
                                                           'armLwr_define_right':{'driverAttr':'define'},
                                                           'armLwr_facet_right':{'driverAttr':'facet'},
                                                           'armLwr_smooth_right':{'driverAttr':'smooth'}}}, 
                              
                              'wrist_left':{'control':'l_wristMeat_bodyShaper',
                                             'wiringDict':{'wrist_in_left':{'driverAttr':'-shape'},
                                                           'wrist_out_left':{'driverAttr':'shape'},
                                                           'wrist_define_left':{'driverAttr':'define'},
                                                           'wrist_facet_left':{'driverAttr':'facet'},
                                                           }},
                              'wrist_right':{'control':'r_wristMeat_bodyShaper',
                                             'wiringDict':{'wrist_in_right':{'driverAttr':'-shape'},
                                                           'wrist_out_right':{'driverAttr':'shape'},
                                                           'wrist_define_right':{'driverAttr':'define'},
                                                           'wrist_facet_right':{'driverAttr':'facet'},
                                                           }},                                
                              }
"""

--torso
skinny_torsoUpr
skinny_torsoLwr
define_torsoUpr
define_torsoLwr
inflate_torsoUpr
inflate_torsoLwr

--neck
skinny_neck
define_neck
inlate_neck
--
l_define_armUpr
r_define_armUpr
l_skinny_armLwr
r_skinny_armLwr
l_skinny_armUpr
r_skinny_armUpr
l_inflate_legLwr
r_inflate_legLwr
l_inflate_legUpr
r_inflate_legUpr
l_define_legLwr
r_define_legLwr
l_define_legUpr
r_define_legUpr
l_skinny_legUpr
r_skinny_legUpr
l_skinny_legLwr
r_skinny_legLwr

--under
under_thick
under_thin
under_up
under_out

--brow
brow_out
lids_heavy
brow_back

--nose tip
bulb_thin
bulb_thick
bulb_short
bulb_tall
bulb_flat
bulb_out
bulb_inflate
bulb_facet
bulb_pinochio

--nose
nostrils_in
nostrils_wide
nostrils_out
nostrils_flat
nostrils_tall
nostrils_gone
--lips
big_upr_inr
big_lwr_inr
big_upr_outr
big_lwr_outr
thick_upr_inr
thick_lwr_inr
thick_upr_outr
thick_lwr_outr

--bridge
bridge_thin
bridge_thick
bridge_flat
bridge_out
bridge_facet

--chin
chin_in
chin_out
chin_cleft
chin_wide
chin_narrow

--lwrFace
cheekBones_out
cheekBone_back
jowels
cheeks_hollow
baby_outr
baby_innr
jaw_weak
jaw_strong

--uprNeck
neck_thick
neckJaw_disappear



"""
from cgm.lib import curves
def connect_customizationBlendshapes():
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['bodyShapers_blendShape'],MORPHYDATA._d_faceShapersBlendshapeWiring )
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['faceShapers_blendShape'],MORPHYDATA._d_faceShapersBlendshapeWiring )
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['eyebrow_bsNode'],MORPHYDATA._d_wiringEyebrowShapers )
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['l_round_ear_bsNode'],MORPHYDATA._d_wiring_earShapers_left )
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['l_reg_ear_bsNode'],MORPHYDATA._d_wiring_earShapers_left )
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['r_round_ear_bsNode'],MORPHYDATA._d_wiring_earShapers_right )
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['r_reg_ear_bsNode'],MORPHYDATA._d_wiring_earShapers_right )
    
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['teethUpr_bsNode'],MORPHYDATA._d_wiring_teethUprShapers )
    morphyF.face_connectAttrHolderToBSNodes('customization_attrHolder',True,['teethLwr_bsNode'],MORPHYDATA._d_wiring_teethLwrShapers )

def shaperShapeControls_verify(*args, **kws):
    """
    Function to verify the wiring on the facial joystick controls for Morpheus
    @kws
    Arg 0 | kw 'attributeHolder'(None)  -- AttributeHolder to wire
    """
    class fncWrap(cgmGeneral.cgmFuncCls):
        def __init__(self,*args, **kws):
            """
            """	
            super(fncWrap, self).__init__(*args, **kws)
            self._b_reportTimes = True
            self._str_funcName = 'shaperShapeControls_verify'	
            self._l_ARGS_KWS_DEFAULTS = [{'kw':'attributeHolder',"default":None,
                                          'help':"Name of the attribute Holder"},]	    
            self.l_funcSteps = [{'step':'Verify Attribute Holder','call':self._fncStep_attributeHolder_},
                                {'step':'Verify Attributes','call':self._fncStep_attributes_},
                                {'step':'Verify Control Wiring','call':self._fncStep_controlWiring_}
                                ]	    
            self.__dataBind__(*args, **kws)

        def _fncStep_attributeHolder_(self):
            """
            """
            _obj = False
            self._mi_obj = False
            self._d_controls = {}
            
            if self.d_kws['attributeHolder'] is not None:
                _obj = cgmValid.objString(arg=self.d_kws['attributeHolder'], noneValid=True, 
                                           calledFrom=self._str_funcName)
                if not _obj:
                   return self._FailBreak_("Bad obj")
               
                self._mi_obj = cgmMeta.cgmNode(_obj)
                
            if not _obj:
                self.log_info("Must create attributeHolder")
                self._mi_obj = cgmMeta.cgmObject()
                
            if not self._mi_obj:
                return self._FailBreak_("Should have had an object by now")
            else:#...check naming stuff
                self._mi_obj.addAttr('cgmName','customization')
                self._mi_obj.addAttr('cgmType','attrHolder')
                self._mi_obj.doName()
                
            self.log_info(self._mi_obj)
            self._str_attrHolder = self._mi_obj.mNode
            
        def _fncStep_attributes_(self):
            l_sections = _d_shaperBufferAttributes.keys()
            l_sections.sort()
            
            for section in l_sections:
                self._mi_obj.addAttr("XXX_{0}_attrs_XXX".format(section),
                                     attrType = 'int',
                                     keyable = False,
                                     hidden = False,lock=True) 
                
                _d_section = _d_shaperBufferAttributes[section]
                l_attrs = _d_section.get('attrs')
                l_attrs.sort()
                
                #Build our names
                l_sidesAttrs = _d_section.get('sideAttrs') or []
                if l_sidesAttrs == '*':
                    l_sidesAttrs = copy.copy(l_attrs)
                    
                for a in l_attrs:
                    l_name = [section,a]                    
                    l_names = []
                    _d = {'left':[],'right':[]}
                    if a in l_sidesAttrs:
                        for side in ['left','right']:
                            l_buffer = copy.copy(l_name)
                            l_buffer.append(side)
                            l_names.append(l_buffer)
                            #_d[side].append(l_buffer)
                    if not l_names:l_names = [l_name]
                    for n in l_names:
                        #self.log_info(n)
                        self._mi_obj.addAttr("_".join(n),attrType = 'float',hidden = False)
            
            #Set some base values...
            """for k in _d_baseFaceAttrValues.keys():
                try:
                    attributes.doSetAttr(self._mi_obj.mNode,k,_d_baseFaceAttrValues[k])
                except:
                    pass  """                  
                        
        def _fncStep_controlWiring_(self):
            for key in _d_shaperControlsToConnect.keys():
                try:
                    _d_control = _d_shaperControlsToConnect[key]
                    control = _d_control['control']
                    _d_wiring = _d_control['wiringDict']
                    _l_simpleArgs = _d_control.get('simpleArgs') or []
                    self.log_info(">>>>>>>>>> On {0}...".format(control))
                    if not mc.objExists(control):
                        raise ValueError,"Control not found: {0}".format(control)
                    
                    if _d_wiring:
                        try:
                            cgmNodeFactory.connect_controlWiring(control,self._mi_obj,
                                                                 _d_wiring,
                                                                 baseName = key,
                                                                 trackAttr = True,
                                                                 simpleArgs = _l_simpleArgs )
                        except Exception,error:
                            self.log_error(" Wire call fail | error: {0}".format(error)   ) 
                    elif _l_simpleArgs:
                            for arg in _l_simpleArgs:
                                self.log_info("On arg: {0}".format(arg))
                                try:
                                    cgmNodeFactory.argsToNodes(arg).doBuild()			
                                except Exception,error:
                                    self.log_error("{0} arg failure | error: {1}".format(arg,error))
                            
                    else:
                        self.log_error("No wiring data or simpleArgs for key: {0}".format(key))
                        
                    #self._d_controls[key] = cgmMeta.cgmObject(control)

                except Exception,error:
                    raise Exception,"Control '{0}' fail | error: {1}".format(key,error)                
                
    return fncWrap(*args,**kws).go()