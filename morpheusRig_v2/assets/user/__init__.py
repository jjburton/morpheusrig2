from cgm.lib.zoo.zooPy import path as zPath
import os
     
def find_files(base, pattern):
        import fnmatch
        import os
        
        #Return list of files matching pattern in base folder.
        # http://stackoverflow.com/questions/4296138/use-wildcard-with-os-path-isfile
        return [n for n in fnmatch.filter(os.listdir(base), pattern) if
                os.path.isfile(os.path.join(base, n))]


def returnFilesFromFolder(searchTerms = ['*.ma','*.mb']):
        import os
        thisFile = zPath.Path( __file__ )
        thisPath = thisFile.up()
        returnList = []
	for term in searchTerms:
        	returnList.extend(find_files(thisPath, term))
                     
        if returnList:
                return returnList
        else:
                return False
        
def returnMayaFilesFromFolder():
        return returnFilesFromFolder(['*.ma','*.mb'])
        
def returnPyFilesFromFolder():
        bufferList = returnFilesFromFolder(['*.py'])
        returnList = []
        
        for file in bufferList:
                if '__' not in file:
                        splitBuffer = file.split('.')
                        returnList.append(splitBuffer[0])               
        if returnList:
                return returnList
        else:
                return False

def returnThisPath():
	import os
        thisFile = zPath.Path( __file__ )
        thisPath = thisFile.up()
	return thisPath
        
__mayaFiles__ = returnMayaFilesFromFolder()
__pyFiles__ = returnPyFilesFromFolder()
__pathHere__ = returnThisPath()
